<?
class utils {
	/**
	 * php extension
	 * @var	string
	 */
	protected $debug;
	protected $result;
	protected $r;
	public $record;
	public $secrets;
	public $logins;
	public $d;
	public $host;
	
	
	/**
	 * Constructs a new instance of UTILS class.
	 */
	public function __construct ($ip='') {
		$this->host="http://dentistbydemand.com";
		$this->ip=$ip;
	}
	public function DBList() {
		$db=$this->query("SHOW DATABASES");
		$s = "<div class='www_box block'>";
		for ($i=0;$i<count($db);$i++) {
			$s .= "<a href='?db=" . $db[$i]['Database'] . "'><div class='www_box2 cell' onmouseover='this.className=\"www_box5 cell\"' onmouseout='this.className=\"www_box cell\"'>" . strtoupper($db[$i]['Database']) . "</div></a>";
		}
		$s .= "</div>";
		return $s;
	}
	public function tableList($db) {
		$tb=$this->query("SHOW TABLES FROM " . $db);
		$s = "<div class='www_box block'>";
		for ($i=0;$i<count($tb);$i++) {
			$s .= "<a onclick='delCookie(\"field\")' href='?tdb=$db&table=" . $tb[$i]["Tables_in_$db"] . "'><div class='www_box2 cell' onmouseover='this.className=\"www_box5 cell\"' onmouseout='this.className=\"www_box cell\"'>" . strtoupper($tb[$i]["Tables_in_$db"]) . "</div></a>";
		}
		$s .= "</div>";
		return $s;
	}
	public function latLngtoLocation($lat,$lng) {
			$sql="select city, state, zip, areacode, abs(round(lat, 4)-round(".$lat." ,4)) as lat, abs(round(lng, 4) - round(".$lng.", 4)) as lng from `gaysugardaddyforme`.`geo_city` where ((abs(round(lat, 4)-round(".$lat." ,4)) < 0.1)  and  (abs(round(lng, 4) - round(".$lng.", 4)) < 0.1) and zip is not null) order by lat asc limit 1";
			$result=$this->_sql_arr($sql)[0];
			return json_encode(array("city"=>$result['city'],"state"=>$result['state'],"areacode"=>$result['areacode'],"zip"=>$result['zip'],"lat"=>$lat,"lng"=>$lng));
	//	return $sql;
	}
	
	public function fieldList($db,$tb) {
		$fields=$this->query("show FIELDS from ". $db . "." . $tb);
		$s = "<div class='www_box block'><table style='width:100%!Important;'>";
		for ($i=0;$i<count($fields);$i++) {
			$fn=$fields[$i]['Field'];
			$s1="";
			$s1 .= "<span><select type='select' id='sel_$fn'>";
			$s1 .= "<option value='' id=''></option>";
			$s1 .= "<option value='is_equal_to'>=</option>";
			$s1 .= "<option value='lesser_than'>&lt;</option>";
			$s1 .= "<option value='greater_than'>&gt;</option>";
			$s1 .= "<option value='not_equal'>&lt;&gt;</option>";
			$s1 .= "<option value='like'>LIKE</option>";
			$s1 .= "<option value='in'>IN</option>";
			$s1 .= "</select>";
			$s .= "<tr id='row_$fn' onclick='this.style.background=\"gold\"' onmouseover='ov(\"row_$fn\")' onmouseout='oo(\"row_$fn\")' class='www_box2'><td><a href='javascript:selectField(\"$db\",\"$tb\",\"$fn\")'><div style='text-align:center!Important;' id='$fn'>" . strtoupper($fn) . "</div>";
			$s .= "</a></td><td>$s1</td><td><input type=text id='text_$fn' onblur='build_query(\"$fn\",document.all.sel_$fn,this)'></td></tr>";
		}
		$s .= "</table><div><a href='?shell_exec=yes'><input type='button' value='VIEW | EDIT Data' class='btn btn-lg btn-danger' style='width:100%'></a></div></div>";
		return $s;
	}
	
	public function buildQuery() {
		if ($_COOKIE[qry]) $sel = " WHERE " . $_COOKIE['qry'];
			else $sel = "";
		return $qry = "SELECT " . substr($_COOKIE[field],0,strlen($_COOKIE[field])-1) . " FROM `" . $_COOKIE[db] . "`.`" . $_COOKIE[table] . "`" . $sel . " LIMIT 100";
	}
	
	public function printForm($afo) {
			$afo=explode(",",$afo);
			for ($i=0;$i<count($afo)-1;$i++) {
				$f=$afo[$i];
				$html .= "<div class='row'><div class='col-md-6'>
						<div class='www_box5' style='text-align:left;;padding:20px'>
						<div class='' style='font-size:24px'>
							<h3>".strtoupper($f)."</h3>
						</div>
						<div class=''>
							<span><input id='{$f}' type='text' style='font-family:Economica;font-size:24px;height:40px;border:1px solid silver;width:200px;background:#fff;padding:5px;border-radius:5px;-moz-border-radius:5px;-webkit-border-radius:5px' onblur='validate_field(this)' onclick='clear_error(this)' onkeypress='clear_error(this)' value=''></span>
							<input id='{$f}_original' type='hidden' value=''>
					<div id='{$f}_err_txt'></div>
						</div>
						<div class=''>
							<span id='{$f}_updated' style='display:none'><span id='{$f}_final_message'>Updated Successfully.</span></span><img id='{$f}_updated_img' src='../assets/images/e5.png' style='width:25px;margin-top:5px;margin-left:70px;display:none'></span>
						</div>
					</div></div></div>";
	}
			return $html;
	}
	public function show_errors($obj=FALSE) {
		if ($obj===TRUE) {
			ini_set("error_reporting", 1);
			error_reporting(E_ALL); 
			ini_set("display_errors", 1); 
			ini_set("display_errors", "On"); 
		} else {
			ini_set("error_reporting", 0);
			ini_set("display_errors", 0); 
			ini_set("display_errors", "Off"); 
			
		}
	}

	public function header() {
		return '<!DOCTYPE html>
				<html lang="en">
				<head>		
					<title></title>		
					<link href="https://fonts.googleapis.com/css?family=Economica" rel="stylesheet" type="text/css">
					<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css">
					<link href="https://code.jquery.com/ui/1.11.4/themes/ui-darkness/jquery-ui.css" rel="stylesheet" type="text/css">
					<link href="https://sugardaddydays.com/assets/css/stream.css" rel="stylesheet" type="text/css">
					<script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
					<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
					<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
				</head>
				<body align=center>
					<div class="container">';
	}
	public function footer() {
		return '</div></body></html>';
	}
	
	public function style() {
		return '<style>
					html, body, div, span {
						font-family:Economica;
						font-size:18px!Important;
					}
					td{
						padding:10px;
					}
					.cell {
						padding:10px;
						margin: 5px;
						width:100%;
						font-size:24px;
					}
					.block {
						margin:10px;
						position:absolute;
						width:370px;
						left:0;
						right:0;
						margin:auto;
					}
				</style>';
	}

	public function script() {
		return '<script>function selectField(a,b,c){if(fl.indexOf(c)<0){fl.push(c);setCookie("db",a);setCookie("table",b);var x="";x = getCookie("field") + c + ",";setCookie("field",x);document.getElementById(c).style.background="gold";document.getElementById("row_"+c).style.background="gold";}}
	var qparts=0;
	var qry="";
	var af=[ ];
	var fl=[ ];
	setCookie(\'qry\',"")
	setCookie(\'field\',"")
	function setCookie(cname,cvalue)	{
			var d = new Date(); 
			d.setTime(d.getTime()+(1*24*60*60*1000)); 
			var expires = "expires="+d.toGMTString(); 
			document.cookie = cname + "=" + cvalue + "; " + expires; 
	} 

	function getCookie(cname)	{ 
			var name = cname + "="; 
			var ca = document.cookie.split(";"); 
			for(var i=0; i<ca.length; i++) { 
			  var c = ca[i].trim(); 
			  if (c.indexOf(name)==0) return c.substring(name.length,c.length); 
			} 
			return ""; 
	} 		
	function delCookie(cname) {
		var d = new Date();
		d.setTime(d.getTime());
		var expires = "expires="+d.toGMTString();
		document.cookie = cname + "=" + "" + "; " + expires;
	}		
	function oo(r) {
		eval("document.all."+r+".style.background=\'\'")
	}
	function ov(r) {
		eval("document.all."+r+".style.background=\'lavenderblush\'")
		
	}
	function build_query (fx,sl,tx) {
		if (tx.value==undefined) return false
		if (tx.value=="") return false
		if (sl.value==undefined) return false
		if (sl.value=="") return false
		if (af.indexOf(fx)>-1) return false
		var op
		var sel=sl.value
		if (sl.value=="is_equal_to") op = "=";
		if (sl.value=="lesser_than") op = "<";
		if (sl.value=="greater_than") op = ">";
		if (sl.value=="not_equal") op = "<>";
		if (sl.value=="like") {
			op = "LIKE";
			tx.value= "%" + tx.value + "%"
		}
		if (qparts>0) qry += " AND "
		qry += fx + " " + op + "\'" + tx.value + "\'"
		console.log(af)
		if (af.indexOf(fx)==-1) {
			af.push(fx)
			qparts++
			setCookie(\'qry\',qry)
		}
	}
	</script>';
	}


	public function guid() {
		for ($a=48;$a<=57;$a++) {
			$arr[]=chr($a);
		}
		for ($a=97;$a<=122;$a++) {
			$arr[]=chr($a);
		}
		for ($a=48;$a<=57;$a++) {
			$arr[]=chr($a);
		}
		for ($a=48;$a<=57;$a++) {
			$arr[]=chr($a);
		}
		$str="";
		for ($j=0;$j<4;$j++) {
			for ($k=0;$k<4;$k++) {
				$chr=$arr[rand(0,55)];
				if ($chr=='o') $chr='0';
				$str .= $chr;
			}
			$str .= "-";
		}
		return substr($str,0,strlen($str)-1);
	}
	
	public function create() {
		$op="";
		$op	.=$this->header();
		$op .= $this->style();
		$op .= $this->script();
		return $op;
	}
	public function connect($ipx="199.91.65.82",$dbx="lushmatch") {
			global $db;
			try {
				global $conn;
				$db = new mysqli($ipx, "root", "Shadow2015!", $dbx);
				$this->conn = $db;
				return $this->conn = $db;
			} catch (Exception $e) {
				return "Unable to connect";
				exit;
			}
	}

		public function close() {
		try {
			$this->conn->close();
			return "Closed";
		} catch (Exception $e) {
			return "Unable to Close";
			exit;
		}
	}

	protected function _ip() {
		$client  = @$_SERVER['HTTP_CLIENT_IP'];
		$forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
		$remote  = $_SERVER['REMOTE_ADDR'];
		if(filter_var($client, FILTER_VALIDATE_IP))
		{
			$ip = $client;
		}
		elseif(filter_var($forward, FILTER_VALIDATE_IP))
		{
			$ip = $forward;
		}
		else
		{
			$ip = $remote;
		}
		return $ip;
	}
	
	public function getIP() {
		$client  = @$_SERVER['HTTP_CLIENT_IP'];
		$forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
		$remote  = $_SERVER['REMOTE_ADDR'];
		$result  = "Unknown";
		if(filter_var($client, FILTER_VALIDATE_IP))
		{
			$ip = $client;
		}
		elseif(filter_var($forward, FILTER_VALIDATE_IP))
		{
			$ip = $forward;
		}
		else
		{
			$ip = $remote;
		}
		return $ip;
	}

	public function sql($sql) {
		return $this->_sql($sql);
	}

	public function delete($sql) {
		return $this->_insert($sql);
	}

	public function update($sql) {
		return $this->_insert($sql);
	}

	public function insert($sql) {
		return $this->_insert($sql);
	}

	public function query($sql) {
		return $this->_sql_arr($sql);
	}

	public function show($obj) {
		echo "<pre>";
			print_r($obj);
		echo "</pre>";
	}
	
	protected function _sql($sql) {
		global $result;
		try {	
				$db=$this->conn;
				if($this->result = $db->query($sql)){
					return $this->result;
				} else {
					return null;
				}
			} catch (Exception $e) {
			return null;
		}
	}

	protected function _insert($sql) {
		global $result;
		try {	
				$db=$this->conn;
				if($db->query($sql)){
					return $db->insert_id;
				} else {
					return null;
				}
			} catch (Exception $e) {
			return null;
		}
	}

	protected function _update($sql) {
		global $result;
		try {	
				$db=$this->conn;
				if($db->query($sql)){
					return $db->insert_id;
				} else {
					return null;
				}
			} catch (Exception $e) {
			return null;
		}
	}

	protected function _sql_arr($sql) {
		global $dbs;
		global $row;
		global $rowset;
		try {
				$db=$this->conn;
			try {
					if($r = $db->query($sql)){
						while ($row =  $r->fetch_assoc()) {
							$arr[] = $row;
						}
						$this->rowset = $arr;
						$r->free();
						return $this->rowset;
					} else {
							return null;
					}
				} catch (Exception $e) {
					return null;
				}
			return $res;
		} catch (Exception $e) {
			return "Unable to connect";
			exit;
		}
	}

	public function query2HTML($query, $c="") {
		echo '<style>';
		echo'.table_custom{font:Open Sans Condensed;color:#333; background:#f0f0f0; text-shadow:1px 1px 1px #fff} .table_header{color:#fff;background:#000;text-shadow:none} .table_row{color:#333; text-shadow:2px 2px 0px #fff} .table_row_alt{color:#000;background:lightblue}';
		echo '</style>';
		$x=0; $first_row = true;
		$header = "<table class='table_custom' cellpadding=10 cellspacing=0><tr id='th'>";	
		$f = $this->strAB('select','from', $query);
		$f = explode(',', $f);
		$f = $this->_sql_arr($query);
		$table  = $header;
		$rows = count($f);
			for($c=0; $c <= $rows; $c++){
				$class = ($x==0) ? 'table_row' : 'table_row_alt';
				$x = ($x == 1) ? 0 : 1;
				 if ($c == 0) $table .= "<tr class='$class' id='th'>";
					else $table .= "<tr class='$class' id='r$x'>";
					if ($f[$c]) {
					foreach($f[$c] as $_key => $_value)
						{
							if ($_key=='id') $id=$_value;
							if ($c == 0) $table .= "<td class='table_header'>" . strtoupper($_key) . "</td>";
								else $table .= "<td class='$class'><div contentEditable onblur=\"update(this,$_key)\" id=\"$_key|$id\">" . ($_value) . "</div></td>";
						}
					}
			}
		$table .= "</table>";
		echo $table;
	}

	public function queryEDIT($query, $c="") {
		$first=true;
		error_reporting(E_NONE);
		echo '<style>';
		echo'.table_custom{margin:auto;left:0;right:0;margin:auto;color:#333; background:#f0f0f0; text-shadow:1px 1px 1px #fff} .table_header{color:#fff;background:#000;text-shadow:none} .table_row{color:#333; text-shadow:2px 2px 0px #fff} .table_row_alt{color:#000;background:lightblue}';
		echo '</style>';
		$x=0; $first_row = true;
		$header = "<table style='border-radius:8px;margin-top:100px' class='table_custom'><tr id='th'>";	
		$arr=explode("from ",$query);
		$t=explode(" ",$arr[1])[0];
		$t="'" . $t . "'";
		$f = $this->strAB('select','from', $query);
		$f = explode(',', $f);
		$f = $this->_sql_arr($query);
		$table  = $header;
		$rows = count($f);
		for($c=0; $c <= $rows+1; $c++){
			$class = ($x==0) ? 'table_row' : 'table_row_alt';
			$x = ($x == 1) ? 0 : 1;
				$ctr=0;
					if ($first) {
						$first=false;
						$table .= "<tr style='height:50px' class='$class' id='th'>";
						foreach($f[$c] as $_key => $_value)	{
							$table .= "<td class='table_header'>" . strtoupper($_key) . "</td>";
							$h[]=$_key;
						}
						$c--;
						$table .= "</tr>";
						$ctr=0;
					} else {
						$table .= "<tr class='$class' id='r$x'>";
						foreach($f[$c] as $_key => $_value)	{
							if ($_key=='pid') $id=$_value;
							$fname="'".$h[$ctr]."'";
							$where="'".$h[0]."'";
							$table .= "<td class='$class'><div contentEditable id='$_key|$id' onblur=\"update_f($t,$fname,this.textContent,$where,$id)\">" . ($_value) . "</div></td>";
						$ctr++;
						}
					}
				}
		$table .= "</table>";
		echo $table;
	}
	
 
	public function cpu_load() {
		$output = `vmstat`;
		$s = strpos(trim($output),'wa st');
			return str_replace(" ","",trim(substr($output, $s+7, 5)));
	}


	static public function set($name, $value)
	{
		$GLOBALS[$name] = $value;
	}

	static public function get($name)
	{
		return $GLOBALS[$name];
	}

	protected function strAB($a, $b, $str)	{
		return substr($str, (strPos($str, $a) + strlen($a)), (strPos($str, $b) - (strPos($str, $a) + strlen($a))));
	}
	
	public function strGetAB($a, $b, $str)	{
		return substr($str, (strPos($str, $a) + strlen($a)), (strPos($str, $b) - (strPos($str, $a) + strlen($a))));
	}

	//Get all Characters between 2 strings or tags in a URL
	public function urlGetAB($a, $b, $url) {
		$str = file_get_contents($url);
		return substr($str, (strPos($str, $a) + strlen($a)), (strPos($str, $b) - (strPos($str, $a) + strlen($a))));
	}

	public function clean($objHTMLText){
		$event_desc = preg_replace("/(\\t|\\r|\\n)/","",trim($objHTMLText));  //recursively remove new lines \\n, tabs and \\r
	}

	 function date_to_words($strDate) {
		$ts1 = strtotime(date("Y")."-".date("m")."-".date("d"));
		$ts2 = strtotime($strDate);
		$dateDiff    = $ts1 - $ts2;
		$units="Day";
		$fd    = floor($dateDiff/(60*60*24));
		$plurl = ($fd==1)?"":"s";
		
		if ($fd == 0) $rt = "Today!";
			
		if ($fd > 0) {
			$rt = "$fd $units{$plurl} Ago";
		}
			
		if ($fd < 0) $rt = "In ".$fd*(-1)." ".$units{$plurl};

		if ($fd==7){
			$datap = "1";
			$prefx = "";
			$units = "Week";
			$plurl = "";
			$tense = "Ago";
			$rt="$prefx $datap $units{$plural} $tense";

		} elseif (($fd>7)&&($fd<31)) {
			$datap = round($fd/7,0);
			$prefx = "Approx";
			$units = "Week";
			$plurl = ($datap*1 < 2)?"":"s";
			$tense = "Ago";
			$rt="$prefx $datap $units{$plurl} $tense";

		} elseif (($fd>31)&&($fd<=365)) {
			$datap = round($fd/30.5,0);
			$prefx = "Approx";
			$units = "Month";
			$plurl = ($datap*1 < 2)?"":"s";
			$tense = "Ago";
			if ($datap*1 >= 12) {
				$e_months = $datap-12;
				$datap=1;
				$units='year';
				$plurl = ($datap*1 < 2)?"":"s";
				if ($e_months > 0) {
					$extra=' and ';
					$rt="$prefx $datap $units{$plurl} $extra $e_months{$e_plurl} $tense";		
				} else {
					$rt="$prefx $datap $units{$plurl} $tense";
				}
			} else {
				$rt="$prefx $datap $units{$plurl} $tense";
			}
		} elseif ($fd>=365) {
			$bal_days = $fd%365;
			$datap = round($fd/365,0);
			$prefx = "Approx";
			$units = "Year";
			$plurl = ($datap*1 < 2)?"":"s";
			$tense = "Ago";

			if ($bal_days > 15) {
				$extra = "and";
				$e_month="1";
				$e_unit="Month";
				$e_plurl=($e_month>1)?"s":"";
			}

			if ($bal_days > 29) {
				$extra = "and";
				$e_month=round($bal_days/30.5,0);
				$e_unit="Month";
				$e_plurl=($e_month*1 > 1) ? "s" : "";
				if ($e_month>11) {
					$e_month='';
					$e_unit='';
					$e_plurl='';
					$extra='';
					$datap++;
				}
			}
			$rt="$prefx $datap $units{$plurl} $extra $e_month $e_unit{$e_plurl} $tense";
		}
			return $rt;
	}

	public function is_mobile() {
		$useragent=$_SERVER['HTTP_USER_AGENT'];
		if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))) {
			return "mobile";
		} else {
			return FALSE;
		}
	}

   public function getStateCode($state="") {
		$state = str_replace("Outside United States","Non US",$state);
		$arr['California']="CA";
		$arr['Georgia']="GA";
		$arr['New Jersey']="NJ";
		$arr['New York']="NY";
		$arr['Arizona']="AZ";
		$arr['Texas']="TX";
		$arr['Oregon']="OR";
		$arr['Florida']="FL";
		$arr['Wyoming']="WY";
		$arr['Nevada']="NV";
		$arr['Pennsylvania']="PA";
		$arr['Illinois']="IA";
		$arr['Indiana']="IN";
		$arr['Nebraska']="NB";
		$arr['Wisconsin']="WI";
		$arr['Michigan']="MI";
		$arr['New Hampshire']="NH";
		$arr['North Carolina']="NC";
		$arr['South Carolina']="SC";
		$arr['Connecticut']="CN";
		$arr['Washington']="WA";
		$arr['North Dakota']="ND";
		$arr['South Dakota']="SD";
		$arr['Colorado']="CO";
		$arr['Oklahoma']="OK";
		$arr['Tennessee']="TN";
		$arr['Mississippi']="MS";
		$arr['Arkansas']="AK";
		$arr['New Mexico']="NM";
		$arr['Maryland']="MD";
		$arr['Delaware']="DA";
		$arr['Virginia']="VA";
		$arr['Massachusetts']="MA";
		$arr['Louisiana']="LA";
		$arr['Missouri']="MO"; 
		$arr['Montana']="MT"; 
		$arr['Utah']="UT";
		$arr['Kentucky']="KY";
		$arr['Alabama']="AL";
		$arr['Hawaii']="HI";
		$arr['West Virginia']="WV";
		$arr['Minnesota']="MN";
		$arr['Ohio']="OH";
		return(empty($arr[$state]))?$state:$arr[$state];
	}

	public function random_string($str_length=32) {
		$s=65;
		$r=97;
		$p=38;
		$length=128;
		if (1==1) {
			for ($i=48;$i<=57;$i++){
				$numbers[]=$i;
			}
		}

		if (1==1) {
			for ($i=$s;$i<$s+26;$i++){
				$upper[]=$i;
			}
		}
		if (1==1) {
			for ($i=$r;$i<$r+26;$i++){
				$lower[]=$i;
			}
		}
		if (1==1) {
			for ($i=176;$i<=179;$i++){
				$fractions[]=$i;
			}
		}
		if (1==1) {
			for ($i=188;$i<=190;$i++){
				$power[]=$i;
			}
			$power[]=137;
		}
		$symbols=array(58,59,60,61,62,63,64,153,169,174,156,167,176);
		$greek=array(140,166,167,164,133,134,135,181,182);
		$curr=array(128,131,142,154,158,159,161,162,163,165,169);

		$con=$numbers;
		$con=array_merge($con,$upper);
		$con=array_merge($con,$lower);
		for ($i=0;$i<=count($con)-1;$i++) {
			$arr[]=chr($con[$i]);
		}
		for ($i=0;$i<=$str_length;$i++){
			$str .= $arr[rand(0,count($arr)-1)];
		}
		return $str;
	}
	function adjustImage($filename1,$mode=0, $annotate="No Text Given!") {
		require "instagraph.php";
		require("/sites/home/lushmatch/public_html/assets/wi/WideImage.php");
		$base = "/sites/home/lushmatch/public_html/sx/";
		$filename = $base . $filename1;
		$filename_o = $base. "original/" . $filename1;
		$filename_u = $base. "undo/" . $filename1;
		$im=WideImage::load($filename);
		exec("chmod +777 $filename");
		if ($mode=="UNDO") {
			$im=WideImage::load($filename_u)->saveToFile($filename);
			return $filename;
		} else {
			$im->saveToFile($filename_u);
		}
		if (!getimagesize("/sites/home/lushmatch/public_html/sx/original/" . $filename1)) $im->saveToFile($filename_o);
		if (!getimagesize("/sites/home/lushmatch/public_html/sx/undo/" . $filename1)) $im->saveToFile($filename_u);
		if ($mode=="SINGLE") {
			$this->singleColor($filename1);
		} else {
			switch ( $mode ) {
				case "BRIGHT1":
					$img = new Imagick($filename);
					$img->brightnessContrastImage(30,10); 
					$img->writeImage($filename);
					break;
				case "BRIGHT2":
					$img = new Imagick($filename);
					$img->brightnessContrastImage(-30,-10); 
					$img->writeImage($filename);
					break;
				case "AUTOLEVEL":
					$img = new Imagick($filename);
					$img->autoLevelImage();
					$img->writeImage($filename);
					break;
				case "SHARPEN":
					$img = new Imagick($filename);
					$img->sharpenImage(2,1); //Increase contrast once
					$img->writeImage($filename);
					break;
				case "CONTRAST1":
					$img = new Imagick($filename);
					$img->contrastImage(2); //Increase contrast once
					$img->writeImage($filename);
					break;
				case "CONTRAST2":
					$img = new Imagick($filename);
					$img->contrastImage(2); //Increase contrast once
					$img->writeImage($filename);
					break;
				case "ROTATE1":
					$img = new Imagick($filename);
					$img->rotateImage('#00000000','90'); 
					$img->writeImage($filename);
					break;
				case "ROTATE2":
					$img = new Imagick($filename);
					$img->rotateImage('#00000000','-90'); 
					$img->writeImage($filename);
					break;
				case "NEGATE":
					$img = new Imagick($filename);
					$img->negateImage(FALSE); 
					$img->writeImage($filename);
					break;
				case "SEPIA":
					$img = new Imagick($filename);
					$img->sepiaToneImage(70);
					$img->writeImage($filename);
					break;
				case "EDGE":
					$img = new Imagick($filename);
					$img->edgeImage(5);
					$img->writeImage($filename);
					break;
				case "SOLARIZE":
					$img = new Imagick($filename);
					$img->solarizeImage( 30000 ); 
					$img->writeImage($filename);
					break;
				case "SHADE":
					$img = new Imagick($filename);
					$img->shadeImage(100,10,20); 
					$img->writeImage($filename);
					break;
				case "CHARCOAL":
					$img = new Imagick($filename);
					$img->charcoalImage(3,1); 
					$img->writeImage($filename);
					break;
				case "ENHANCE":
					$img = new Imagick($filename);
					$img->enhanceImage();
					$img->writeImage($filename);
					break;
				case "PAINT":
					$img = new Imagick($filename);
					$img->oilPaintImage( 4 ); 
					$img->writeImage($filename);
					break;					
			case "EQUALIZE":
					$img = new Imagick($filename);
					$img->equalizeImage();
					$img->writeImage($filename);
					break;
			case "SKETCH":
					exec("convert $filename \( -clone 0 -negate -blur 0x5 \) -compose colordodge -composite -modulate 100,0,100 -auto-level $filename");
					break;
			case "CARTOON":
					exec("convert $filename \( -clone 0 -blur 0x5 \) \( -clone 0 -fill black -colorize 100 \) \( -clone 0 -define convolve:scale='!' -define morphology:compose=Lighten -morphology Convolve  'Sobel:>' -negate -evaluate pow 5 -negate -level 30x100% \) -delete 0 -compose over -composite $filename");					
					return("convert $filename \( -clone 0 -blur 0x5 \) \( -clone 0 -fill black -colorize 100 \) \( -clone 0 -define convolve:scale='!' -define morphology:compose=Lighten -morphology Convolve  'Sobel:>' -negate -evaluate pow 5 -negate -level 30x100% \) -delete 0 -compose over -composite $filename");					
					break;
			case "RING":
					exec("sh polyring \( -clone 0 -blur 0x5 \) \( -clone 0 -fill black -colorize 100 \) \( -clone 0 -define convolve:scale='!' -define morphology:compose=Lighten -morphology Convolve  'Sobel:>' -negate -evaluate pow 5 -negate -level 30x100% \) -delete 0 -compose over -composite $filename");					
					break;
			case "TOONIFY":
					exec("sh toonify -b 4 -t 10 -e canny -q 8 -s 2 $filename $filename");					
					break;
			case "POLAROID":
					$img = new Imagick($filename);
					$img->polaroidImage(new ImagickDraw(), 25);
					$img->writeImage($filename);
					break;
			case "ANNOTATE":
					$off=getimagesize($filename)[1]-100;
					$img = new Imagick($filename);
					$draw = new ImagickDraw();
					$pixel = new ImagickPixel( 'gray' );
					/* Black text */
					$draw->setFillColor('white');
					/* Font properties */
					$draw->setFont('Bookman-DemiItalic');
					$draw->setFontSize( 28 );
					/* Create text */
					$img->annotateImage($draw, 10, $off, 0, $annotate);
					$img->writeImage($filename);
					break;
				case "KELVIN":
					$instagraph = Instagraph::factory($filename, $filename);
					$instagraph->kelvin();
					break;
				case "GOTHAM":
					$instagraph = Instagraph::factory($filename, $filename);
					$instagraph->gotham();
					break;
				case "NASHVILLE":
					$instagraph = Instagraph::factory($filename, $filename);
					$instagraph->nashville();
					break;
				case "TOASTER":
					$instagraph = Instagraph::factory($filename, $filename);
					$instagraph->toaster();
					break;
				case "LOMO":
					$instagraph = Instagraph::factory($filename, $filename);
					$instagraph->lomo();
					break;
				case "UNDO":
					$img=WideImage::load($filename_u)->saveToFile($filename);
					break;
				case "RESET":
					$img=WideImage::load($filename_o)->saveToFile($filename);
					break;
					return 'sb/' . explode('/',$filename)[(count(explode('/',$filename))-1)];
					imagedestroy($im);
			}
			return $filename;
		}
	}
	public function ftp($ftp_server,$ftp_user_name,$ftp_user_pass,$file,$remote_file) {
		 // set up basic connection
		 $conn_id = ftp_connect($ftp_server);

		 // login with username and password
		 $login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);

		 // upload a file
		 if (ftp_put($conn_id, $remote_file, $file, FTP_BINARY)) {
			return "success";
		 } else {
			return "error";
			}
		 // close the connection
		 ftp_close($conn_id);
	}
	
	public function curl($url, $method = 'GET', $data = false, $headers = false, $returnInfo = false) {    
		$ch = curl_init();
		
		if($method == 'POST') {
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_POST, true);
			if($data !== false) {
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
			}
		} else {
			if($data !== false) {
				if(is_array($data)) {
					$dataTokens = array();
					foreach($data as $key => $value) {
						array_push($dataTokens, urlencode($key).'='.urlencode($value));
					}
					$data = implode('&', $dataTokens);
				}
				curl_setopt($ch, CURLOPT_URL, $url.'?'.$data);
			} else {
				curl_setopt($ch, CURLOPT_URL, $url);
			}
		}
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		if($headers !== false) {
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		}

		$contents = curl_shell_exec($ch);
		
		if($returnInfo) {
			$info = curl_getinfo($ch);
		}

		curl_close($ch);

		if($returnInfo) {
			return array('contents' => $contents, 'info' => $info);
		} else {
			return $contents;
		}
	}
	public function cal($selected="",$size=5) {
 	$size=$size*5 + 30;
	$fs=$_GET[size]*2;
	$is=$_GET[size]*1+20;
	$fs=($fs<10)?10:$fs;
	$fs=($fs>24)?24:$fs;
	$fs = ($fs+2) . "px";

	$size_compact="text-align:center;max-height:" . $size . "px!Important;height:" . $size . "px!Important;max-width:" . $size . "px!Important;width:" . $size . "px!Important;font-size:$fs!Important;font-family:Open Sans!Important";
	$m=$_REQUEST['month'];
	$y=$_REQUEST['year'];
	if (strstr($selected,"-")){
		$sdx=explode(",",$selected);
		$sd=$sdx;//$c->show($sd);

	} else {
		$sd=explode(",",$selected);
	}
			
	for ($r=0;$r<count($sd);$r++) {
		if (strstr($sd[$r],'->')) {
			$sd1=explode("->",$sd[$r]);
				for ($q=$sd1[0];$q<=$sd1[1];$q++) {
					$selected_dates[]=$q;
				}
		} else {
			$selected_dates[]=$sd[$r];
		}
	}
	if (!$m) {
		$m=date('m');
		$mStr=date('m');;
	}
	if (!$y) $y=date('Y');
	$w=array('mon'=>1,'tue'=>2,'wed'=>3,'thu'=>4,'fri'=>5,'sat'=>6,'sun'=>7);
	$wk=array('Mon','Tue','Wed','Thu','Fri','Sat','Sun');
						echo '<div class="col-md-3"><table class="www_box2" style="border:0px solid #333;border-radius:4px">';
						if (!$_GET[mini]) {

						echo '	<tr>';
						echo '		<td colspan=7 align=center>';
						echo '			<a title="prev" onclick="prev()"><img style="cursor:hand;cursor:pointer;margin-bottom:2px;width:'.$is.'px" src="images/prev.png"></a>'; 
						echo '				<span style="font-family:Open Sans;margin-top:10px;font-size:'.$fs.'"><b>' . strtoupper(date('F Y', mktime(0,0,0,$m,1,$y))) . '</b></span>';
						echo '			<a title="next" onclick="next()"><img style="cursor:hand;cursor:pointer;margin-bottom:2px;width:'.$is.'px" src="images/next.png"></a>'; 
						echo '		</td>
								</tr>
							<tr>';
						}
						$sq=7;
						$mth=($m<10)?'0'.$m:$m;
						$m=$this->dates_month($m,$y,$fs,$size_compact);
						$wd=$m[0];
						$p_day=$m[2];
						$m=$m[1];
						$day_of_week=$w[strtolower($wd)];
						$skip_blocks=$day_of_week*1-1;
						$skip=1; $d=$i;
						$ctr=0;
						for ($j=1;$j<=7;$j++) {
							echo "<td valign='center' style='background:grey;color:#f0f0f0;$size_compact'><div style='background:grey;$size_compact;padding:2px;margin:0' class='www_box2'>" . $wk[$j-1] . "</div></td>";
						}
						echo "</tr><tr>";
							
						for ($i=2;$i<=count($m)+1+$skip_blocks;$i++) {
							$bc="";
							$d++;
							$day=(($i-2)<10)?"0".($i-2):($i-2);

							$w='';
							if (!$m[$i-$skip_blocks-1]) {
								$content = "";
							} else {
								$content = $m[$i*1-$skip_blocks*1-1];
								$content2 = $p_day[$i*1-$skip_blocks*1-1]*1;
								//To reserve & show weekends use line above (un-comment) and comment out line below
								$content2=$day*1;
								$action="onmouseover='on(this)' onmouseout='off(this)' onclick='toggle(this,$content2,$mth,$y)'";
							}
							if ($show_weekends) {
								if ((($i<8)&&($d>5)) || (($i>=7)&&($d>6))) {
									$weekend='style="background:white"';
									//$action='';
									$w='1';
								} else {
									$weekend='';
									$w=0;
								}
							
								if ($wk[$i-2]=="Sat") $weekend='style="background:Aliceblue"';
							} else {
								$w=0;
								$weekend='style="background:white"';
							}
							$selected='';
							$d=trim(explode('<',explode('>',$content)[1])[0]);
							if (in_array($d,$selected_dates)) $bc="yellow";
							
							for ($x=0;$x<count($selected_dates);$x++) {
								if (($d==explode("-",$selected_dates[$x])[2]) && (explode("-",$selected_dates[$x])[1]==$mth*1)) $bc="yellow";
								 ;
								//$d;
								//if (strstr($selected_dates[$x],$d)) $bc="url(images/bars.jpg)";
							}
							if ($content=="") $bc="lavender";
							$dx=($d*1<10)?'0'.$d:$d;
							$mdy=$mth.'-'.$dx.'-'.$y;
							$id=$mdy;
						//	if (stristr($content,"Sat")) $bc="";
						//	if (stristr($content,"Sun")) $bc="";
						//	if (stristr($content,"Sat")) $content=$i*1-$skip_blocks*1-1;
						//	if (stristr($content,"Sun")) $content=$i*1-$skip_blocks*1-1;
							if (empty($bc)) $bc='#fff';
							$content = "<div class='middle'>" . $content . "</div>";
							echo "<td style='padding:0;margin:0;$size_compact'><div id='$id' style='background:$bc;opacity:1;$size_compact' class='cal www_box2 $wwend $icon_class' $selected $action>".$content."</div></td>";
							$sq--;
							if (!(($i-1)%7)) {
								echo "</tr><tr>";
								$d=1;
								$sq=7;
							}
							$ctr++;
						}
						$mt=date('m');
						$yr=date('Y');
						
					if ($sq<7) {
							for ($i=1;$i<=$sq;$i++) {
								if ($mt==12) {
									$mt=1;
									$yr++;
								}
								if ($i>$sq-2) $weekend="weekend2";
									else $weekend="";
								$mktime=mktime(0,0,0,$mt+1,$i,$yr);
								if ((date("D",$mktime)=="Sat") || (date("D",$mktime)=="Sun")) $bgx="lavender";
									else $bgx="lavender";
								if (( date("D",$mktime) == "Sat")||( date("D",$mktime)  == "Sun")) $ww=date("D",$mktime);
									else $ww=date("j",$mktime);
								//$date="<div style='display:none' class='day_words_disabled'></div><div style='$size_compact;font-size:$fs' class='day_num_disabled'></div>";
								echo "<td style='padding:0;margin:0;$size_compact'><div id='$id' class='cal www_box1 $weekend' style='background:$bgx;$size_compact'>" . $date . "</div></td>";
							}
								echo "</tr>";
						}
							echo '</table></div>';
	}
	

	public function get_ebay($q) {
		$url="http://svcs.ebay.com/services/search/FindingService/v1?OPERATION-NAME=findItemsByKeywords&SERVICE-VERSION=1.0.0&SECURITY-APPNAME=GautamSh-8747-4434-9413-ecec57e3bc53&GLOBAL-ID=EBAY-US&keywords=$q&paginationInput.entriesPerPage=20";
		$resp = simplexml_load_file($url);
		foreach($resp->searchResult->item as $item) {
			$pic   = $item->galleryURL;
			$link  = $item->viewItemURL;
			$title = $item->title[0];
			$t=$title."";
			$id	   = $item->itemId;
				$id=$id*1;
				$r[]=array("label"=>"$t", "image"=>"$pic", "id" => $id);			
		}
		echo $r=json_encode(($r));
	} 

	public function getImageGoogle($k) {
		// domain .it works 26/04/2017
		$url = "https://www.google.it/search?q=##query##&tbm=isch";
		$web_page = $this->getPage( str_replace("##query##",urlencode($k), $url ));		
		preg_match_all("/ src=\"(http([^\"]*))\"/",$web_page,$a);
		return isset($a[1]) ? $a[1] : null;
	}
	public function getImage($key) {
		//
		// scraping content from picsearch
		$temp = file_get_contents("http://www.picsearch.com/index.cgi?q=".urlencode($key));
		preg_match_all("/<img class=\"thumbnail\" src=\"([^\"]*)\"/",$temp,$ar);
		if(is_array($ar[1])) return $ar[1];
		return false;
	}
	/*
		use the previous functions results to get a bigger picture 
		of the result.
	*/
	public function getImageBig($pic) {
		$ar = preg_split("/[\?\&]/",str_replace("&amp;","&",$pic));
		if(isset($ar[1])) {
			$temp = file_get_contents("http://www.picsearch.com/imageDetail.cgi?id=".$ar[1]."&amp;start=1&amp;q=");
			preg_match_all("/<a( rel=\"nofollow\")? href=\"([^\"]*)\">Full-size image<\/a>/i",$temp,$ar);
			if(isset($ar[2][0])) {
				return $ar[2][0];
			}
		}
	}
	
   function dates_month($month,$year,$fs,$size_compact) {
		$num = cal_days_in_month(CAL_GREGORIAN, $month, $year);
		$dates_month=array();
		for($i=1;$i<=$num;$i++) {
			$mktime=mktime(0,0,0,$month,$i,$year); 
			if (!$first) $first=date("D", $mktime);
			if (( date("D",$mktime) == "Sat")||( date("D",$mktime)  == "Sun")) {
				$ww=date("D",$mktime);
				$clr="color: silver;";
			} else {
				$ww=date("j", $mktime);
				$clr="color: black;";
			}
			if (( date("D",$mktime) == "Sat")||( date("D",$mktime)  == "Sun")) $nn="";
				else $nn=date("j", $mktime);
				
			$ww=date("j", $mktime);	
			$nn="";
			$date="<div style='$size_compact;font-size:$fs;$clr'>" .$ww . "</div><div style='$size_compact;font-size:$fs' class='day_num'></div>";
			$d[$i]=$ww;
			$dates_month[$i]=$date;
		}
		return array($first,$dates_month,$d);
	}

	public function getPage($url, $max_file_size=0) {

		if (!function_exists("curl_init")) die("getPage needs CURL module, please install CURL on your php.");
		$ch = curl_init();

		$https = preg_match("/^https/i",$url);

		if($this->use_file_get_contents=="yes") return file_get_contents($url);

		if($https && $this->use_file_get_contents=="https") {
			return file_get_contents($url);
		}

		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_FAILONERROR, 1);       // Fail on errors
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);    // allow redirects (abilitato per wikipedia)
		if($https) {
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			//curl_setopt($ch, CURLOPT_CERTINFO, true);
			//curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__)."/cacert.pem");
		}
		curl_setopt($ch, CURLOPT_ENCODING, 'gzip,deflate,sdch');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);     // return into a variable
		//curl_setopt($ch, CURLOPT_PORT, 80);           //Set the port number
		curl_setopt($ch, CURLOPT_TIMEOUT, 15);          // times out after 15s
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.1; he; rv:1.9.2.8) Gecko/20100722 Firefox/3.6.8");   // Webbot name
		curl_setopt($ch,CURLOPT_HTTPHEADER,array('Accept-Language: en-US;q=0.6,en;q=0.4'));
		if($max_file_size>0) {
			// if you want to reduce download size, set the byte size limit
			$this->max_file_size = $max_file_size;
			curl_setopt($ch, CURLOPT_HEADERFUNCTION, array($this, 'on_curl_header'));
			curl_setopt($ch, CURLOPT_WRITEFUNCTION, array($this, 'on_curl_write'));
		}
		$web_page = curl_shell_exec($ch);
		if(strlen($web_page) <= 1 && $max_file_size>0) {
			$web_page = $this->file_downloaded;
		}
		//if(curl_error($ch)) die(curl_error($ch));
		return $web_page;
	}

	public function google($q) {
		$this->query=$q;
		$this->userKey="AIzaSyCtYDYedySqc3VyEOsU2LyqC6l67MANF98&cx=002771127746944436221:nvhkzzltqom";
		$this->userKey="AIzaSyCAa916bQh0t1pz27oYQCPA7Z9z3Zdu_-k&cx=002771127746944436221:nvhkzzltqom";
		$this->curl = curl_init();
		curl_setopt( $this->curl , CURLOPT_URL , ltrim( "https://www.googleapis.com/customsearch/v1?key=" . $this->userKey . "&alt=atom&q=" . urlencode($this->query) ) );
		curl_setopt( $this->curl , CURLOPT_SSL_VERIFYPEER , false );
		curl_setopt( $this->curl , CURLOPT_RETURNTRANSFER , 1 );
		$this->response = curl_shell_exec($this->curl);
		curl_close($this->curl);
		return $this->response;
	}	
	
	public function mail($email, $name, $subject, $message, $email_type=""){
		$site="SDLocal App";
		$sender_name="SDLocal Support";
		$sender_email="lushdates001@gmail.com";
		$this->insert("INSERT INTO `terra`.`messageQ` (`email`, `name`, `subject`, `message`, `attachments`, `site`, `sender_name`, `sender_email`, `email_type`) VALUES ('$email', '$name', $subject, '$message', '$att', '$site', '$sender_name', '$sender_email', '$email_type')");
	}
}
