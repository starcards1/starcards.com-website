<?
use Twilio\Rest\Client;
use RedisClient\RedisClient;
use RedisClient\ClientFactory;


require "/sites/home/linqstar/public_html/redis/src/autoloader.php";
require "aes_enc.class.php";
require "aesctr.class.php";
error_reporting(1);

class utils {
	/**
	 * php extension
	 * @var	string
	 */
	protected $debug;
	protected $result;
	protected $r;
	public $record;
	public $secrets;
	public $logins;
	public $d;
	public $host;

	/**
	 * Constructs a new instance of UTILS class.
	 */
	  
	public function __construct ($ip='') {
		global $_CONSTANTS;
		$this->redis = new RedisClient([ 
			'server' => '199.91.65.83:7000', // or 'unix:///tmp/redis.sock'
		]);
		// $a=array("'");
		// $b=array("");
		// foreach (parse_ini_file("/sites/home/lushmatch/config.ini") as $key=>$value) { 
			// ${$key} = $value;
			// $_CONSTANTS[strtoupper($key)]=str_replace($a,$b,$value);
		// }
		// foreach($_CONSTANTS as $key => $val)
			// define(trim(strtoupper($key)), trim($val));

		$this->host="http://lushmatch.com";
		$this->hostname=HOSTNAME;
	}	
	public function show_errors($obj=FALSE) {
		if ($obj===TRUE) {
			ini_set("error_reporting", 1);
			error_reporting(E_ALL); 
			ini_set("display_errors", 1); 
			ini_set("display_errors", "On"); 
		} else {
			ini_set("error_reporting", 0);
			ini_set("display_errors", 0); 
			ini_set("display_errors", "Off"); 
			
		}
	}
	
	public function err($obj=0) {
		if ($obj===1) {
			ini_set("error_reporting", 1);
			error_reporting(E_ALL); 
			ini_set("display_errors", 1); 
			ini_set("display_errors", "On"); 
		} else {
			ini_set("error_reporting", 0);
			ini_set("display_errors", 0); 
			ini_set("display_errors", "Off"); 
		}
	}

	public function shell($cn, $fn){
		$stream = ssh2_exec($cn, $fn);
		stream_set_blocking($stream, true);
		return stream_get_contents($stream);		
	}
	
	public function title($title) {
		$words = explode(' ', $title);
		foreach ($words as $key => $word){
			$words[$key] = ucwords(strtolower($word));
		}

		return implode(' ', $words);
	}

	public function print_blocks($obj,$width="25px") {
		$obj=str_replace(' ','',$obj);
		$a=array('[',']','(',')','.','-');
		$b=array('','','','','','');
		str_replace($aa,$b,$obj);
		for ($i=0;$i<=strlen($obj)-1;$i++) {
			$str .= "<span><img onerror=\"this.src='http://txt.am/assets/img/space.png'\" src=\"https://lushmatch.com/assets/img/" . substr($obj,$i,1) . ".png\" style=\"width:$width\"></span>";
		}
		return $str;
	}		
	
	public function logs($x) {
		global $secrets;
		$d[]=$x;
		$this->logins=$d;
	}
	public function callerID ($mobile) {
		return file_get_contents("https://api.opencnam.com/v3/phone/$mobile?account_sid=eACce038bffa1ac4e3ca4ed5c78c891cc0e&auth_token=AU1044e512e8ba45ec9772cd35b8c94d93");
	}
	
	public function connect($ipx="199.91.65.83",$dbx="lushmatch") {
			global $db;
			try {
				global $conn;
				$db = new mysqli("199.91.65.83", "root", "Shadow2015!", $dbx);
//				$db = new mysqli("69.164.205.130", "root", "Coneja69!", "demo2");
				$this->conn = $db;
				return $this->conn = $db;
			} catch (Exception $e) {
				return "Unable to connect";
				exit;
			}
	}

		public function close() {
		try {
			$this->conn->close();
			return "Closed";
		} catch (Exception $e) {
			return "Unable to Close";
			exit;
		}
	}

	protected function _ip() {
		$client  = @$_SERVER['HTTP_CLIENT_IP'];
		$forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
		$remote  = $_SERVER['REMOTE_ADDR'];
		if(filter_var($client, FILTER_VALIDATE_IP))
		{
			$ip = $client;
		}
		elseif(filter_var($forward, FILTER_VALIDATE_IP))
		{
			$ip = $forward;
		}
		else
		{
			$ip = $remote;
		}
		return $ip;
	}

	public function getPort() {
		setCookie("port", WS_PORT, time()+3600*10, "/");
		return WS_PORT;
	}
	
	public function getIP() {
		$client  = @$_SERVER['HTTP_CLIENT_IP'];
		$forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
		$remote  = $_SERVER['REMOTE_ADDR'];
		$result  = "Unknown";
		if(filter_var($client, FILTER_VALIDATE_IP))
		{
			$ip = $client;
		}
		elseif(filter_var($forward, FILTER_VALIDATE_IP))
		{
			$ip = $forward;
		}
		else
		{
			$ip = $remote;
		}
		return $ip;
	}

	public function getLatLng() {
		$latLng=file_get_contents("http://199.91.65.85:8080/json/".$this->getIP());
		return array($latLng->lat, $latLng->lng);
	}

	public function getCitySateFromIP($ip='') {
		$result  = "Unknown";
		if ($ip=='') $ip=$this->_ip();
		$ip_data = file_get_contents("http://lushmatch.com/json/$ip");
		$result = array($ip_data->city,$ip_data->state);
		return $result;
	}

	public function getLatLngFromIP($ip='') {
		$result  = "Unknown";
		if ($ip=='') $ip=$this->_ip();
		$ip_data = file_get_contents("http://lushmatch.com/json/$ip");
		$result = array($ip_data->lat,$ip_data->lng);
		return $result;
	}

	public function icrop($filename) {
		list($current_width, $current_height) = getimagesize($filename);
		$left = 0;
		$top = 0;
		$crop_width = 200;
		$crop_height = 200;
		$canvas = imagecreatetruecolor($crop_width, $crop_height);
		$current_image = imagecreatefromjpeg($filename);
		imagecopy($canvas, $im, 0, 0, $left, $top, $current_width, $current_height);
		imagejpeg($canvas, $filename, 100);
	}

	public function singleColor($filename1,$c="yellow"){ 
		$filename="/webroot/lushmatch/public_html/v3.0/sb/" . $filename1;
		require("/webroot/lushmatch/public_html/assets/wi/WideImage.php");

		$im = imagecreatefromjpeg($filename); 
		$height = imagesy($im); 
		$width = imagesx($im); 
		for($x=0; $x<$width; $x++){ 
			for($y=0; $y<$height; $y++){ 
				$rgb = ImageColorAt($im, $x, $y); 
				$r = ($rgb >> 16) & 0xFF; 
				$g = ($rgb >> 8) & 0xFF; 
				$b = $rgb & 0xFF; 
				$c=($r+$g+$b)/3; 
				if ($c=="green") {$r=$c;$g=$c; $b=$c;}//leaves only green 
				if ($c=="blue") {$r=$c;$g=$c; $b=$c;}//only blue 
				if ($c=="red") {$r=$c;$g=$c; $b=$c;}//only red 
				if ($c=="yellow"){$r=$c;$g=$c; $b=$c;}//only yellow 
				imagesetpixel($im, $x, $y,imagecolorallocate($im, $r,$g,$b)); 
			} 
		} 
		return imagejpeg($im); 
		imagedestroy($im);
		$img=WideImage::load($filename);

		//sreturn $im->writeImage("/webroot/lushmatch/public_html/undo/test.jpg");
	}  	
	function adjustImage($filename1,$mode=0, $annotate="No Text Given!") {
		require("/sites/home/lushmatch/public_html/assets/wi/WideImage.php");
		$base = "/sites/home/lushmatch/public_html/uploads/";
		$filename = $base . $filename1;
		chmod($filename,0755);
		$filename_o = $base. "original/" . $filename1;
		$filename_u = $base. "undo/" . $filename1;
		$im=WideImage::load($filename);
		if ($mode=="UNDO") {
			$im=WideImage::load($filename_u)->saveToFile($filename);
			return $filename;
		} else {
			$im->saveToFile($filename_u);
		}
		if (!getimagesize("/sites/home/lushmatch/public_html/uploads/original/" . $filename1)) $im->saveToFile($filename_o);
		if (!getimagesize("/sites/home/lushmatch/public_html/uploads/undo/" . $filename1)) $im->saveToFile($filename_u);
		if ($mode=="SINGLE") {
			$this->singleColor($filename1);
		} else {
			switch ( $mode ) {
				case "BRIGHT1":
					$img = new Imagick($filename);
					$img->brightnessContrastImage(10,0); 
					$img->writeImage($filename);
					break;
				case "BRIGHT2":
					$img = new Imagick($filename);
					$img->brightnessContrastImage(-10,0); 
					$img->writeImage($filename);
					break;
				case "AUTOLEVEL":
					$img = new Imagick($filename);
					$img->autoLevelImage();
					$img->writeImage($filename);
					break;
				case "SHARPEN":
					$img = new Imagick($filename);
					$img->sharpenImage(2,1); //Increase contrast once
					$img->writeImage($filename);
					break;
				case "CONTRAST1":
					$img = new Imagick($filename);
					$img->contrastImage(1); //Increase contrast once
					$img->writeImage($filename);
					break;
				case "CONTRAST2":
					$img = new Imagick($filename);
					$img->contrastImage(0); //Increase contrast once
					$img->writeImage($filename);
					break;
				case "ROTATE1":
					$img = new Imagick($filename);
					$img->rotateImage('#00000000','90'); 
					$img->writeImage($filename);
					break;
				case "ROTATE2":
					$img = new Imagick($filename);
					$img->rotateImage('#00000000','-90'); 
					$img->writeImage($filename);
					break;
				case "NEGATE":
					$img = new Imagick($filename);
					$img->negateImage(FALSE); 
					$img->writeImage($filename);
					break;
				case "SEPIA":
					$img = new Imagick($filename);
					$img->sepiaToneImage(70);
					$img->writeImage($filename);
					break;
				case "EDGE":
					$img = new Imagick($filename);
					$img->edgeImage(10);
					$img->writeImage($filename);
					break;
				case "OIL":
					$img = new Imagick($filename);
					$img->oilPaintImage( 5 ); 
					$img->writeImage($filename);
					break;
				case "SOLARIZE":
					$img = new Imagick($filename);
					$img->solarizeImage( 30000 ); 
					$img->writeImage($filename);
					break;
				case "SHADE":
					$img = new Imagick($filename);
					$img->shadeImage(100,10,20); 
					$img->writeImage($filename);
					break;
				case "CHARCOAL":
					$img = new Imagick($filename);
					$img->charcoalImage(5,2); 
					$img->writeImage($filename);
					break;
				case "ENHANCE":
					$img = new Imagick($filename);
					$img->enhanceImage();
					$img->writeImage($filename);
					break;
				case "MODULATE":
					$brightness=150;
					$saturation=100;
					$hue=100;
					$img = new Imagick($filename);
					$img->modulateImage($brightness, $saturation, $hue);
					$img->writeImage($filename);
					break;
			case "EQUALIZE":
					$img = new Imagick($filename);
					$img->equalizeImage();
					$img->writeImage($filename);
					break;
			case "ANNOTATE":
					$off=getimagesize($filename)[1]-50;
					$img = new Imagick($filename);
					$draw = new ImagickDraw();
					$pixel = new ImagickPixel( 'gray' );
					/* Black text */
					$draw->setFillColor('white');
					/* Font properties */
					$draw->setFont('Bookman-DemiItalic');
					$draw->setFontSize( 32 );
					/* Create text */
					$img->annotateImage($draw, 10, $off, 0, $annotate);
					$img->writeImage($filename);
					break;
				case "PIXELATE1":
						$img = new Imagick($filename);
						$imageIterator = $img->getPixelIterator();
					 
						/** @noinspection PhpUnusedLocalVariableInspection */
						foreach ($imageIterator as $row => $pixels) { /* Loop through pixel rows */
							foreach ($pixels as $column => $pixel) { /* Loop through the pixels in the row (columns) */
								/** @var $pixel \ImagickPixel */
								if ($column % 1) {
									$pixel->setColor("rgba(50, 50, 50, 0)"); /* Paint every second pixel black*/
								}
							}
							$imageIterator->syncIterator(); /* Sync the iterator, this is important to do on each iteration */
						}
						$img->writeImage($filename);
						break;
				case "PIXELATE2":
						$img = new Imagick($filename);
						$imageIterator = $img->getPixelIterator();
					 
						/** @noinspection PhpUnusedLocalVariableInspection */
						foreach ($imageIterator as $row => $pixels) { /* Loop through pixel rows */
							foreach ($pixels as $column => $pixel) { /* Loop through the pixels in the row (columns) */
								/** @var $pixel \ImagickPixel */
								if ($column % 2) {
									$pixel->setColor("rgba(50, 50, 50, 0)"); /* Paint every second pixel black*/
								}
							}
							$imageIterator->syncIterator(); /* Sync the iterator, this is important to do on each iteration */
						}
						$img->writeImage($filename);
						break;
				case "PIXELATE3":
						$img = new Imagick($filename);
						$imageIterator = $img->getPixelIterator();
					 
						/** @noinspection PhpUnusedLocalVariableInspection */
						foreach ($imageIterator as $row => $pixels) { /* Loop through pixel rows */
							foreach ($pixels as $column => $pixel) { /* Loop through the pixels in the row (columns) */
								/** @var $pixel \ImagickPixel */
								if ($column % 3) {
									$pixel->setColor("rgba(50, 50, 50, 0)"); /* Paint every second pixel black*/
								}
							}
							$imageIterator->syncIterator(); /* Sync the iterator, this is important to do on each iteration */
						}
						$img->writeImage($filename);
						break;
				case "UNDO":
					$img=WideImage::load($filename_u)->saveToFile($filename);
					break;
				case "RESET":
					$img=WideImage::load($filename_o)->saveToFile($filename);
					break;
					return 'sb/' . explode('/',$filename)[(count(explode('/',$filename))-1)];
					imagedestroy($im);
			}
			return $filename;
		}
	}
	
	public function cal($selected="",$size=5) {
 	$size=$size*5 + 30;
	$fs=$_GET[size]*2;
	$is=$_GET[size]*1+20;
	$fs=($fs<10)?10:$fs;
	$fs=($fs>24)?24:$fs;
	$fs = ($fs+2) . "px";

	$size_compact="text-align:center;max-height:" . $size . "px!Important;height:" . $size . "px!Important;max-width:" . $size . "px!Important;width:" . $size . "px!Important;font-size:$fs!Important;font-family:Open Sans!Important";
	$m=$_REQUEST['month'];
	$y=$_REQUEST['year'];
	if (strstr($selected,"-")){
		$sdx=explode(",",$selected);
		$sd=$sdx;//$c->show($sd);

	} else {
		$sd=explode(",",$selected);
	}
			
	for ($r=0;$r<count($sd);$r++) {
		if (strstr($sd[$r],'->')) {
			$sd1=explode("->",$sd[$r]);
				for ($q=$sd1[0];$q<=$sd1[1];$q++) {
					$selected_dates[]=$q;
				}
		} else {
			$selected_dates[]=$sd[$r];
		}
	}
	if (!$m) {
		$m=date('m');
		$mStr=date('m');;
	}
	if (!$y) $y=date('Y');
	$w=array('mon'=>1,'tue'=>2,'wed'=>3,'thu'=>4,'fri'=>5,'sat'=>6,'sun'=>7);
	$wk=array('Mon','Tue','Wed','Thu','Fri','Sat','Sun');
						echo '<div class="col-md-3"><table class="www_box2" style="border:0px solid #333;border-radius:4px">';
						if (!$_GET[mini]) {

						echo '	<tr>';
						echo '		<td colspan=7 align=center>';
						echo '			<a title="prev" onclick="prev()"><img style="cursor:hand;cursor:pointer;margin-bottom:2px;width:'.$is.'px" src="images/prev.png"></a>'; 
						echo '				<span style="font-family:Open Sans;margin-top:10px;font-size:'.$fs.'"><b>' . strtoupper(date('F Y', mktime(0,0,0,$m,1,$y))) . '</b></span>';
						echo '			<a title="next" onclick="next()"><img style="cursor:hand;cursor:pointer;margin-bottom:2px;width:'.$is.'px" src="images/next.png"></a>'; 
						echo '		</td>
								</tr>
							<tr>';
						}
						$sq=7;
						$mth=($m<10)?'0'.$m:$m;
						$m=$this->dates_month($m,$y,$fs,$size_compact);
						$wd=$m[0];
						$p_day=$m[2];
						$m=$m[1];
						$day_of_week=$w[strtolower($wd)];
						$skip_blocks=$day_of_week*1-1;
						$skip=1; $d=$i;
						$ctr=0;
						for ($j=1;$j<=7;$j++) {
							echo "<td valign='center' style='background:grey;color:#f0f0f0;$size_compact'><div style='background:grey;$size_compact;padding:2px;margin:0' class='www_box2'>" . $wk[$j-1] . "</div></td>";
						}
						echo "</tr><tr>";
							
						for ($i=2;$i<=count($m)+1+$skip_blocks;$i++) {
							$bc="";
							$d++;
							$day=(($i-2)<10)?"0".($i-2):($i-2);

							$w='';
							if (!$m[$i-$skip_blocks-1]) {
								$content = "";
							} else {
								$content = $m[$i*1-$skip_blocks*1-1];
								$content2 = $p_day[$i*1-$skip_blocks*1-1]*1;
								//To reserve & show weekends use line above (un-comment) and comment out line below
								$content2=$day*1;
								$action="onmouseover='on(this)' onmouseout='off(this)' onclick='toggle(this,$content2,$mth,$y)'";
							}
							if ($show_weekends) {
								if ((($i<8)&&($d>5)) || (($i>=7)&&($d>6))) {
									$weekend='style="background:white"';
									//$action='';
									$w='1';
								} else {
									$weekend='';
									$w=0;
								}
							
								if ($wk[$i-2]=="Sat") $weekend='style="background:Aliceblue"';
							} else {
								$w=0;
								$weekend='style="background:white"';
							}
							$selected='';
							$d=trim(explode('<',explode('>',$content)[1])[0]);
							if (in_array($d,$selected_dates)) $bc="yellow";
							
							for ($x=0;$x<count($selected_dates);$x++) {
								if (($d==explode("-",$selected_dates[$x])[2]) && (explode("-",$selected_dates[$x])[1]==$mth*1)) $bc="yellow";
								 ;
								//$d;
								//if (strstr($selected_dates[$x],$d)) $bc="url(images/bars.jpg)";
							}
							if ($content=="") $bc="lavender";
							$dx=($d*1<10)?'0'.$d:$d;
							$mdy=$mth.'-'.$dx.'-'.$y;
							$id=$mdy;
						//	if (stristr($content,"Sat")) $bc="";
						//	if (stristr($content,"Sun")) $bc="";
						//	if (stristr($content,"Sat")) $content=$i*1-$skip_blocks*1-1;
						//	if (stristr($content,"Sun")) $content=$i*1-$skip_blocks*1-1;
							if (empty($bc)) $bc='#fff';
							$content = "<div class='middle'>" . $content . "</div>";
							echo "<td style='padding:0;margin:0;$size_compact'><div id='$id' style='background:$bc;opacity:1;$size_compact' class='cal www_box2 $wwend $icon_class' $selected $action>".$content."</div></td>";
							$sq--;
							if (!(($i-1)%7)) {
								echo "</tr><tr>";
								$d=1;
								$sq=7;
							}
							$ctr++;
						}
						$mt=date('m');
						$yr=date('Y');
						
					if ($sq<7) {
							for ($i=1;$i<=$sq;$i++) {
								if ($mt==12) {
									$mt=1;
									$yr++;
								}
								if ($i>$sq-2) $weekend="weekend2";
									else $weekend="";
								$mktime=mktime(0,0,0,$mt+1,$i,$yr);
								if ((date("D",$mktime)=="Sat") || (date("D",$mktime)=="Sun")) $bgx="lavender";
									else $bgx="lavender";
								if (( date("D",$mktime) == "Sat")||( date("D",$mktime)  == "Sun")) $ww=date("D",$mktime);
									else $ww=date("j",$mktime);
								//$date="<div style='display:none' class='day_words_disabled'></div><div style='$size_compact;font-size:$fs' class='day_num_disabled'></div>";
								echo "<td style='padding:0;margin:0;$size_compact'><div id='$id' class='cal www_box1 $weekend' style='background:$bgx;$size_compact'>" . $date . "</div></td>";
							}
								echo "</tr>";
						}
							echo '</table></div>';
	}
	

	public function get_ebay($q) {
		$url="http://svcs.ebay.com/services/search/FindingService/v1?OPERATION-NAME=findItemsByKeywords&SERVICE-VERSION=1.0.0&SECURITY-APPNAME=GautamSh-8747-4434-9413-ecec57e3bc53&GLOBAL-ID=EBAY-US&keywords=$q&paginationInput.entriesPerPage=20";
		$resp = simplexml_load_file($url);
		foreach($resp->searchResult->item as $item) {
			$pic   = $item->galleryURL;
			$link  = $item->viewItemURL;
			$title = $item->title[0];
			$t=$title."";
			$id	   = $item->itemId;
				$id=$id*1;
				$r[]=array("label"=>"$t", "image"=>"$pic", "id" => $id);			
		}
		echo $r=json_encode(($r));
	} 

	public function getImageGoogle($k) {
		// domain .it works 26/04/2017
		$url = "https://www.google.it/search?q=##query##&tbm=isch";
		$web_page = $this->getPage( str_replace("##query##",urlencode($k), $url ));		
		preg_match_all("/ src=\"(http([^\"]*))\"/",$web_page,$a);
		return isset($a[1]) ? $a[1] : null;
	}
	public function getImage($key) {
		//
		// scraping content from picsearch
		$temp = file_get_contents("http://www.picsearch.com/index.cgi?q=".urlencode($key));
		preg_match_all("/<img class=\"thumbnail\" src=\"([^\"]*)\"/",$temp,$ar);
		if(is_array($ar[1])) return $ar[1];
		return false;
	}
	/*
		use the previous functions results to get a bigger picture 
		of the result.
	*/
	public function getImageBig($pic) {
		$ar = preg_split("/[\?\&]/",str_replace("&amp;","&",$pic));
		if(isset($ar[1])) {
			$temp = file_get_contents("http://www.picsearch.com/imageDetail.cgi?id=".$ar[1]."&amp;start=1&amp;q=");
			preg_match_all("/<a( rel=\"nofollow\")? href=\"([^\"]*)\">Full-size image<\/a>/i",$temp,$ar);
			if(isset($ar[2][0])) {
				return $ar[2][0];
			}
		}
	}
	
   function dates_month($month,$year,$fs,$size_compact) {
		$num = cal_days_in_month(CAL_GREGORIAN, $month, $year);
		$dates_month=array();
		for($i=1;$i<=$num;$i++) {
			$mktime=mktime(0,0,0,$month,$i,$year);
			if (!$first) $first=date("D", $mktime);
			if (( date("D",$mktime) == "Sat")||( date("D",$mktime)  == "Sun")) {
				$ww=date("D",$mktime);
				$clr="color: silver;";
			} else {
				$ww=date("j", $mktime);
				$clr="color: black;";
			}
			if (( date("D",$mktime) == "Sat")||( date("D",$mktime)  == "Sun")) $nn="";
				else $nn=date("j", $mktime);
				
			$ww=date("j", $mktime);	
			$nn="";
			$date="<div style='$size_compact;font-size:$fs;$clr'>" .$ww . "</div><div style='$size_compact;font-size:$fs' class='day_num'></div>";
			$d[$i]=$ww;
			$dates_month[$i]=$date;
		}
		return array($first,$dates_month,$d);
	}

	public function getPage($url, $max_file_size=0) {

		if (!function_exists("curl_init")) die("getPage needs CURL module, please install CURL on your php.");
		$ch = curl_init();

		$https = preg_match("/^https/i",$url);

		if($this->use_file_get_contents=="yes") return file_get_contents($url);

		if($https && $this->use_file_get_contents=="https") {
			return file_get_contents($url);
		}

		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_FAILONERROR, 1);       // Fail on errors
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);    // allow redirects (abilitato per wikipedia)
		if($https) {
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			//curl_setopt($ch, CURLOPT_CERTINFO, true);
			//curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__)."/cacert.pem");
		}
		curl_setopt($ch, CURLOPT_ENCODING, 'gzip,deflate,sdch');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);     // return into a variable
		//curl_setopt($ch, CURLOPT_PORT, 80);           //Set the port number
		curl_setopt($ch, CURLOPT_TIMEOUT, 15);          // times out after 15s
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.1; he; rv:1.9.2.8) Gecko/20100722 Firefox/3.6.8");   // Webbot name
		curl_setopt($ch,CURLOPT_HTTPHEADER,array('Accept-Language: en-US;q=0.6,en;q=0.4'));
		if($max_file_size>0) {
			// if you want to reduce download size, set the byte size limit
			$this->max_file_size = $max_file_size;
			curl_setopt($ch, CURLOPT_HEADERFUNCTION, array($this, 'on_curl_header'));
			curl_setopt($ch, CURLOPT_WRITEFUNCTION, array($this, 'on_curl_write'));
		}
		$web_page = curl_exec($ch);
		if(strlen($web_page) <= 1 && $max_file_size>0) {
			$web_page = $this->file_downloaded;
		}
		//if(curl_error($ch)) die(curl_error($ch));
		return $web_page;
	}

	public function google($q) {
		$this->query=$q;
		$this->userKey="AIzaSyCtYDYedySqc3VyEOsU2LyqC6l67MANF98&cx=002771127746944436221:nvhkzzltqom";
		$this->userKey="AIzaSyCAa916bQh0t1pz27oYQCPA7Z9z3Zdu_-k&cx=002771127746944436221:nvhkzzltqom";
		$this->curl = curl_init();
		curl_setopt( $this->curl , CURLOPT_URL , ltrim( "https://www.googleapis.com/customsearch/v1?key=" . $this->userKey . "&alt=atom&q=" . urlencode($this->query) ) );
		curl_setopt( $this->curl , CURLOPT_SSL_VERIFYPEER , false );
		curl_setopt( $this->curl , CURLOPT_RETURNTRANSFER , 1 );
		$this->response = curl_exec($this->curl);
		curl_close($this->curl);
		return $this->response;
	}	

	function isValidMobile($mob) {
		$num = trim($mob);
		$arr_a = array("-","."," ","(",")");
		$arr_b = array("","","","","");
		$num = str_replace($arr_a, $arr_b, $num);

		if ((strlen($num) < 10) || (strlen($num) > 11) || (substr($num,0,1)=='0') || (substr($num,1,1)=='0') || ((strlen($num)==10)&&(substr($num,0,1)=='1'))||((strlen($num)==11)&&(substr($num,0,1)!='1'))) return false;
		$num = (strlen($num) == 11) ? $num : ('1' . $num);	
		
		if ((strlen($num) == 11) && (substr($num, 0, 1) == "1")) {
			return $num;
		} else {
			return false;
		}
	}
	function formatMobile($mob, $format=false) {
		$num = trim($mob);
		$arr_a = array("-","."," ","(",")");
		$arr_b = array("","","","","");
		$num = str_replace($arr_a, $arr_b, $num);

		if ((strlen($num) < 10) || (strlen($num) > 11) || (substr($num,0,1)=='0') || (substr($num,1,1)=='0') || (substr($num,1,1)=='1')) return false;
		$num = (strlen($num) == 11) ? $num : ('1' . $num);	
		
		if ((strlen($num) == 11) && (substr($num, 0, 1) == "1")) {
			if($format) {
				$num = "(" . substr($num,1,3) . ") " . substr($num,4,3) . "-" . substr($num,7,4); 
			}
			return $num;
		} else {
			return false;
		}
	}	
		public function enc($string,$key="") {
			$iv = mcrypt_create_iv(
				mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC),
				MCRYPT_DEV_URANDOM
			);

			$encrypted = base64_encode(
				$iv .
				mcrypt_encrypt(
					MCRYPT_RIJNDAEL_128,
					hash('sha256', $key, true),
					$string,
					MCRYPT_MODE_CBC,
					$iv
				)
			);
			return $encrypted;
		}
		
		public function dec($encrypted,$key) {
			$data = base64_decode($encrypted);
			$iv = substr($data, 0, mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC));

			$decrypted = rtrim(
				mcrypt_decrypt(
					MCRYPT_RIJNDAEL_128,
					hash('sha256', $key, true),
					substr($data, mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC)),
					MCRYPT_MODE_CBC,
					$iv
				),
				"\0"
			);
			return $decrypted;
		}
		
	public function distance($point1, $point2) { 
		$radius      = 3958;
		$deg_per_rad = 57.29578;
		$distance = ($radius * pi() * sqrt( 
					($point1['lat'] - $point2['lat']) 
					* ($point1['lat'] - $point2['lat']) 
					+ cos($point1['lat'] / $deg_per_rad)
					* cos($point2['lat'] / $deg_per_rad)
					* ($point1['lng'] - $point2['lng']) 
					* ($point1['lng'] - $point2['lng']) 
			) / 180); 
		return round($distance,1);
	} 
	public function getLoc($ip) {
		$gi = geoip_open("GeoLiteCity.dat", GEOIP_STANDARD);
		$record = geoip_record_by_addr($gi, $ip);
		$x_city =  $record->city;
		$x_state = $record->region;
		$x_zip = $record->postal_code;
		$x_lat = $record->latitude;
		$x_lng = $record->longitude;
		geoip_close($gi);
		return "$x_city, $x_state";
	}
	public function eSMS($to,$from="",$msg="") {
		require '../voice/vendor/autoload.php';
		//use Plivo\RestAPI;
		$auth_id = "MAOWUYZMM5MDM4MWRHMW";
		$auth_token = "NGIzN2VlY2Y1NzY5Y2Y5Mzg4ZjBlYzlkYTUwNjQx";
		$p = new RestAPI($auth_id, $auth_token);
		$msg = $_GET['message'];
		if (isset($from)) $src=$from;
			else $src="19256669699";
		// Set message parameters
			$params = array(
			'src' => $src, // Sender's phone number with country code
			'dst' => $to, // Receiver's phone number with country code
			'text' => $msg, // Your SMS text message
			// To send Unicode text
			//'text' => 'こんにちは、元気ですか？' # Your SMS Text Message - Japanese
			//'text' => 'Ce est texte généré aléatoirement' # Your SMS Text Message - French
			'url' => 'http://lushmatch.com/voice/report/', // The URL to which with the status of the message is sent
			'method' => 'POST' // The method used to call the url
		);
		// Send message
		$response = $p->send_message($params);

		// Print the response
		return $response['response'];
	}
	
	public function setSession($mobile,$data) {
		error_reporting('E_NONE');
		$this->_insert("insert into sms_sessions values('NULL','$mobile','$data')");
		return "insert into sms_sessions values('NULL','$mobile','$data')";
	}
	
	public function getLocationNew($ip="") {
		if ($ip=="") $ip=$this->getIP();
		return file_get_contents("http://199.91.65.85:8080/json/" . $ip);
	}
	public function getSession ($mobile) {
		return $this->query("select data from sms_sessions where mobile='$mobile'")[0]['data'];
	}
	

	function sms($to,$msg) {
		$msg=urlencode($msg);
		$to=$this->isValidMobile($to);
		if ($to) return file_get_contents("https://terrawire.com:9999/sms/$to/$msg");
			else return $to;
	}
	
	public function bg_all() {
		$bgs=array('#ffccff','#fad1ff','#f6d5ff','#f1daff','#ecdfff','#e8e3ff','#dfecff','#daf1ff','#d5f6ff','#d1faff','#ccffff','#ffffcc','#ffffdd','#ffffee','#ccffff','#ccffee','#ccffdd','#ccffcc','#cceecc','#cceedd','#cceeee','#cceeff','#ffeeff','#ffeeee','#ffeedd','#ccaaff','#ccbbff','#ccccff','#ccddff','#ffdddd');
		return json_encode($bgs);
	}
	
	public function bg() {
		$bg=array('#ffccff','#fad1ff','#f6d5ff','#f1daff','#ecdfff','#e8e3ff','#dfecff','#daf1ff','#d5f6ff','#d1faff','#ccffff','#ffffcc','#ffffdd','#ffffee','#ccffff','#ccffee','#ccffdd','#ccffcc','#cceecc','#cceedd','#cceeee','#cceeff','#ffeeff','#ffeeee','#ffeedd','#ccaaff','#ccbbff','#ccccff','#ccddff','#ffdddd');
		return $bg[rand(0,count($bg)-1)];
	}
	
	public function getCredits($user) {
		return $this->query("select credits from dt_anon_sms where credits where mobile='$from' or long_code='$from'")[0]['credits'];		
	}

	public function sql($sql) {
		return $this->_sql($sql);
	}

	public function insert($sql) {
		return $this->_insert($sql);
	}

	public function query($sql) {
		return $this->_sql_arr($sql);
	}

	public function show($obj) {
		echo "<pre>";
			print_r($obj);
		echo "</pre>";
	}
	
	public function user_info($id) {
		$arr=$this->_sql_arr("select * from dt_members where id='" . $id . "' or login='$id'");
		return $arr;
	}

	public function user_long_code($long_code) {
		$arr=$this->_sql_arr('select id, login, filename_1, email, mobile, pswd from dt_members where long_code='.$long_code);
		return $arr[0];
	}

	public function user_mobile($mobile) {
		$arr=$this->_sql_arr('select id, login, filename_1, email, mobile, pswd from dt_members where mobile='.$mobile);
		return $arr[0];
	}

	public function user_login($id) {
		$arr=$this->_sql_arr('select login, filename_1, email, mobile, pswd from dt_members where id='.$id);
		return $arr[0];
	}

	public function login_info($mid) {
		$arr=$this->query("select email, login, pswd from dt_members where id='".$mid."'");
		$str = "\r\n\r\nPASS: " . $arr[0][pswd] . "\r\nLOGIN: " . $arr[0][login] . "\r\nEMAIL: " . $arr[0][email];
		return $str;
	}

	public function user_photo($id) {
		$arr=$this->_sql_arr('select filename_1 from dt_photos where member_id=$id');
		return array( "<img src='photos/" . $arr[filename_1] , $arr[filename_1] );
	}


	public function user_thumb($id, $size=30) {
		$arr=$this->_sql_arr('select filename_1 from dt_members where id='.$id.' limit 1');
		$f=$arr[0][filename_1];
		$f=explode('.',$f);
		$f1=$f[0];
		$f2=$f[1];
		$x=$size . "px";
	return array("<img onError='this.src=no_photo' style='width:$x; height:$x' src='photos/" . $f1 . "." . $f2 . "'>",$f1.".".$f2);
	}
	
	public function user_thumb_search($f='axx.png', $size=30) {
		if (empty($f)) $f='axx.png';
		$f=explode('.',$f);
		$f1=$f[0];
		$f2=$f[1];
		$x=$size . "px";
		$x_src='"photos/a29.png"';
		return array("<img onError='this.src=$x_src' class='' style='border:2px solid skyblue;width:$x; max-height:$x; height:$x; border-radius: 150px; --moz-corner-radius: 150px; margin:0px;' src='photos/" . $f1 . "." . $f2 . "'>",$f1.".".$f2);
	}
	
	protected function _sql($sql) {
		global $result;
		try {	
				$db=$this->conn;
				if($this->result = $db->query($sql)){
					return $this->result;
				} else {
					return null;
				}
			} catch (Exception $e) {
			return null;
		}
	}

	protected function _insert($sql) {
		global $result;
		try {	
				$db=$this->conn;
				if($db->query($sql)){
					return $db->insert_id;
				} else {
					return null;
				}
			} catch (Exception $e) {
			return null;
		}
	}

	protected function _update($sql) {
		global $result;
		try {	
				$db=$this->conn;
				if($db->query($sql)){
					return $db->insert_id;
				} else {
					return null;
				}
			} catch (Exception $e) {
			return null;
		}
	}

	protected function _sql_arr($sql) {
		global $dbs;
		global $row;
		global $rowset;
		try {
				$db=$this->conn;
			try {
					if($r = $db->query($sql)){
						while ($row =  $r->fetch_assoc()) {
							$arr[] = $row;
						}
						$this->rowset = $arr;
						$r->free();
						return $this->rowset;
					} else {
							return null;
					}
				} catch (Exception $e) {
					return null;
				}
			return $res;
		} catch (Exception $e) {
			return "Unable to connect";
			exit;
		}
	}

	public function add_date($date, $days, $unit="Days"){
		$date = strtotime("+".$days." ".$unit, strtotime($date));
		return  date("Y-m-d H:i:s", $date);
	}

	public function query2HTML($query, $c,$headers="") {
		$h=0;
		echo '<style>';
		echo 'td {padding:15px;}';
		echo'.table_custom{font:Open Sans Condensed;color:#333; background:#f0f0f0; text-shadow:1px 1px 1px #fff; padding:15px} .table_header{color:#fff;background:#000;text-shadow:none} .table_row{color:#333; text-shadow:2px 2px 0px #fff} .table_row_alt{color:#000;background:lightblue}';
		echo '</style>';
		$x=0; $first_row = true;
		$header = "<table class='table_custom' cellpadding=10 cellspacing=0><tr id='th'>";	
		$f = $this->strAB('select','from', $query);
		$f = explode(',', $f);
		$f = $this->_sql_arr($query);
		$table  = $header;
		$rows = count($f)+1;
			for($c=0; $c <= $rows; $c++){
				if ($c>0)$rr=$c-1;
					else $rr=$c;
				$class = ($x==0) ? 'table_row' : 'table_row_alt';
				$x = ($x == 1) ? 0 : 1;
				 if ($x == 0){
						$table .= "<tr class='$class' id='th'>";
				 } else {
					 $table .= "<tr class='$class' id='r$x'>";
				 }
					foreach($f[$rr] as $_key => $_value)
						{
							if ($_key=='id') $id=$_value;
							if ($c == 0) {
								if ($headers) {
									$table .= "<td class='table_header'>" . strtoupper($headers[$h]) . "</td>";
								} else {
									$table .= "<td class='table_header'>" . strtoupper($_key) . "</td>";
								}
							} else {
								$table .= "<td class='$class'><div contentEditable id=\"$_key|$id\">" . ($_value) . "</div></td>";
							}
							$h++;
						}
					echo "</tr>";
			}
		$table .= "</table>";
		echo $table;
	}
	
	public function columnCount($db,$table) {
		$sql="SELECT COUNT(*) as `cols` FROM INFORMATION_SCHEMA.COLUMNS WHERE table_schema = '$db' AND table_name ='$table'";
		return $this->query($sql)[0]['cols'];
	}

	public function getFields($db,$table) {
		$sql="SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE table_schema = '$db' AND table_name ='$table'";
		return $this->query($sql);
	}
	
	public function queryEDIT($query,$wideView) {
		//Mandatory: $query must be a query, and not a table name;
		if (strlen(stristr($query,"limit"))>0) { 
			$query=str_replace("limit","LIMIT",$query);
			$q=explode("LIMIT",$query);
			//is it 'LIMIT x,y' or just 'LIMIT x'?
			if (strlen(stristr($q[1],","))>0) { 
				//It is 'LIMIT x,y'
				$sc=explode(",",$q[1]);
				$start=$sc[0];
				$count=$sc[1];
				$limit=" LIMIT " . $start . ", " . $count;
			} else {
				//It is 'LIMIT x'
				$count=$q[1];
				$limit="LIMIT " . $count;
			}
			$query=$q[0];
		} else {
			$limit = " limit " . ITEMS_PER_PAGE;
		}
		if ($this->is_mobile()=="mobile"){
			$x_m="mobile";
		}
		$first=true;
		$table  = '<style>';
		$table .= '.table_custom{padding:10px;margin:auto;left:0;right:0;margin:auto;color:#333; background:#fff; font-size:0.8em!Important; padding:10px; text-shadow:1px 1px 1px #fff} .table_header{color:#fff;background:#000;text-shadow:none} .table_row{background:aliceblue; color:#333; text-shadow:2px 2px 0px #fff} .table_row_alt{color:#000;background:#fff}';
		$table .= '</style>';
		$x=0; $first_row = true;
		$arr=explode("from ",$query);  
		$t=explode(" ",$arr[1])[0];
		$tab=$t;
		$t="'" . $t . "'";

		$columnCount=$this->columnCount($_COOKIE['db_name'],$tab)*1;
		if (($columnCount>10) || ($wideView=="1")) {
			return $this->queryEditWide($query . $limit);
		}
		$f = $this->strAB('select','from', $query);
		$f = explode(',', $f);
		$f = $this->_sql_arr($query . $limit);
		$rows = count($f);
		$header = "<div class='' style='margin-top:100px'>";	
		$table .= $header;
		for($c=0; $c <= $rows-1; $c++){
			$class = ($x==0) ? 'table_row' : 'table_row_alt';
			$x = ($x == 1) ? 0 : 1;
			$colCnt=0;
			$ctr=0;
					if ($first) {
						$first=false;
						$table .= "<div class='row $class' style='height:50px' id='th'>";
						foreach($f[$c] as $_key => $_value)	{
							$colCnt++;
						}
						$colCnt++;
						$width="";
						if ($x_m !== "mobile") {
							$width="width: " . ((1/$colCnt)*100) . '%';
						}
							foreach($f[$c] as $_key => $_value)	{
								$w=((1/$colCnt)*100) . '%';
								if ($x_m !== "mobile") {
									$table .= "<div style='$width' class='col-md-1 table_header'>" . strtoupper($_key) . "</div>";
								}
								$h[]=$_key;
							}
							$c--;
							if ($x_m !== "mobile") $table .= "<div style='$width' class='col-md-1 table_header'>DEL</div></div>";
							$ctr=0;
					} else {
						$table .= "<div class='row $class' id='r$x'>";
						foreach($f[$c] as $_key => $_value)	{
							if ($ctr==0) {
								$id=$_value;
								$index=$_key;
							}
							$fname="'".$h[$ctr]."'";
							$where="'".$h[0]."'";
							if ($x_m == "mobile") {
								$table .= "<div style='text-align:left;padding:20px;$width' onclick='setEdit(this,\"$_key|$id\")' onmouseover=\"mover(this)\" onmouseout=\"mout(this)\" class='col-md-1 table_cell'><div style='font-size:12px' contentEditable id='$_key|$id' onblur=\"update_fld($t,$fname,this,$where,$id)\"><span style='margin-left:0px'><b>" . strtoupper($_key). '</b></span><br>' . ($_value) . "</div></div>";
							} else {
								$table .= "<div style='text-align:left;padding:20px;$width' onclick='setEdit(this,\"$_key|$id\")' onmouseover=\"mover(this)\" onmouseout=\"mout(this)\" class='col-md-1 table_cell'><div style='font-size:12px' contentEditable id='$_key|$id' onblur=\"update_fld($t,$fname,this,$where,$id)\">" . ($_value) . "</div></div>";
							}
							$ctr++;
							
						}
						$table .= "<div style='$width'  class='col-md-1' style='text-align:center;opacity:0.3'><a href=\"javascript:delTableRow('$index',$id)\"><img src='images/trash.png' style='width:50px;vertical-align:center;position:;margin-top:10px'></a></div></div>";
					}
				}
		$table .= "</table>";
		$table .= "<script>
		var table
		var page=getCookie('page')
					setTimeout(function(){
						table='$tab'
						setCookie('table',table)
						if (qs('selectCell')) {
							cellSelector(qs('selectCell'))
							document.getElementsByClassName('rangeslider__handle')[0].innerHTML='<div style=\"width:150px;position:absolute;margin-bottom:50px;height:60px;bottom:0;\">Page: <span style=\"padding:5px;background:cyan;border-radius:6px\">' + (getCookie('last_page')*1+1) + '</span><br><br><br></div>'
						}
					},1000)
					function cellSelector(divID) {
						saveCSS=$$(divID).style.cssText
						console.log(saveCSS)
						$$(divID).style.background='white'
						$$(divID).style.border='5px solid red'
						$$(divID).contentEditable=true
						$$(divID).style.width='100%'
						$$(divID).style.margin='0'
						$$(divID).style.padding='10px'
						$$(divID).style.textShadow='none'
						sel(divID)
					}
					
					function mover(obj) {
						obj.style.background='gold'
					}
					function mout(obj) {
						obj.style.background=''
					}
					function $$(obj) {
						return (document.getElementById(obj))
					}
					function queryEDITCode() {
						var url='php_preview.php'
						jconfirm({content: 'url:php_preview.php', 'useBootstrap':false,'boxWidth':'850px','title':'Code For queryEDIT','theme':'material'})
					}

					var wl = window.location.href;
					var mob = (window.location.href.indexOf('file://')>=0);
		var wl = window.location.href;
		var mob = (window.location.href.indexOf('file://')>=0);";
$table .='
		function qs(name, url) {
			if (!url) {
			  url = window.location.href;
			}
			name = name.replace(/[\[\]]/g, "\\$&");
			var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
				results = regex.exec(url);
			if (!results) return null;
			if (!results[2]) return "";
			return decodeURIComponent(results[2].replace(/\+/g, " "));
		}';
$table .="
					function update_fld(a,b,c,d,e) {
						c.style.cssText=saveCSS
						c.style.paddingLeft='10px'
						var sql='" . HOST . "/x_form_update.php?sql=update `' + a + '` set ' + b + '=\"' + c.textContent + '\" where ' + d + '=' + e + '&db_server='+getCookie('db_server')+'&db_name='+getCookie('db_name')+'&db_user='+getCookie('db_user')+'&db_password='+getCookie('db_password')
				//		console.log(sql)
						$.ajax({
							url: sql,
							success: function(msg){
								c.style.cssText=saveCSS
								c.style.paddingLeft='10px'
							}
						})
					}	
					function addRow() {
						var table=getCookie('table')
						var url = 'addTableRow.php?db_name='+getCookie('db_name')+'&table='+table
						$.ajax({
							url:url,
							success:function(data){
								console.log(data)
								setCookie('sel',data)
								var url = 'SQLEdit.php?db_name='+getCookie('db_name')+'&table='+table+'&selectCell='+data+'&page='+getCookie('last_page')
								location.href=url
								
							}
						})
					}
					function delTableRow(index,id) {
						var table=getCookie('table')
						var url = 'delTableRow.php?db_name='+getCookie('db_name')+'&table='+table+'&index='+index+'&id='+id
						$.ajax({
							url:url,
							success:function(data){
								console.log(data)
								setCookie('sel',data)
								var url = 'SQLEdit.php?db_name='+getCookie('db_name')+'&table='+table+'&page='+getCookie('page')
								location.href=url
							}
						})
					}
					var saveCSS
					function setEdit(objC,divID) {
						// objC.style.background='white'
						// objC.style.border='none'
						// objC.style.border='none'
						// objC.style.margin='0'
						// objC.style.padding='0'
						saveCSS=$$(divID).style.cssText
						console.log(saveCSS)
						$$(divID).style.background='white'
						$$(divID).style.border='5px solid red'
						$$(divID).contentEditable=true
						$$(divID).style.width='100%'
						$$(divID).style.margin='0'
						$$(divID).style.padding='0'
						$$(divID).style.textShadow='none'
						sel(divID)
					}
					function sel(node) {
						node = document.getElementById(node);
						if (document.body.createTextRange) {
							const range = document.body.createTextRange();
							range.moveToElementText(node);
							range.select();
						} else if (window.getSelection) {
							const selection = window.getSelection();
							const range = document.createRange();
							range.selectNodeContents(node);
							selection.removeAllRanges();
							selection.addRange(range);
						} else {
							console.warn('Could not select text in node: Unsupported browser.');
						}
					}
					var pre
					function php_code(n,uri) {
						pre=$$('pre')
						pre.dataset.src=uri
					}

					function edit(table) {
						//alert()
						setCookie('table',table)
						var url = 'editQuery.php?db_name='+getCookie('db_name')+'&table='+table
						$.ajax({
							url:url,
							success:function(data){
								$$('wrapper').innerHTML=data
								$$('wrapper').style.marginTop='-150px!Important;'
								cellSelector(getCookie('sel'))
							}
						})
					}

					function setCookie(cname,cvalue)	{
						var d = new Date(); 
						d.setTime(d.getTime()+(1*24*60*60*1000));
						var expires = 'expires='+d.toGMTString(); 
						document.cookie = cname + '=' + cvalue + '; ' + expires; 
					}
					
					function getCookie(cname)	{ 
						var name = cname + '='; 
						var ca = document.cookie.split(';'); 
						for(var i=0; i<ca.length; i++) { 
						  var c = ca[i].trim(); 
						  if (c.indexOf(name)==0) return c.substring(name.length,c.length); 
						} 
						return ''; 
					} 		
				</script>";
			return $table;
	}
	
	public function queryBrowse($query) {
		$first=true;
		$table  = '<style>';
		$table .= '.table_custom{padding:10px;margin:auto;left:0;right:0;margin:auto;color:#333; background:#fff; font-size:0.8em!Important; padding:10px; text-shadow:1px 1px 1px #fff} .table_header{color:#fff;background:#000;text-shadow:none} .table_row{background:aliceblue; color:#333; text-shadow:2px 2px 0px #fff} .table_row_alt{color:#000;background:#fff}';
		$table .= '</style>';
		$x=0; $first_row = true;
		$header = "<div style='max-width:100%;margin-top:100px'>";	
		$arr=explode("from ",$query);  
		$t=explode(" ",$arr[1])[0];
		$t="'" . $t . "'";
		$f = $this->strAB('select','from', $query);
		$f = explode(',', $f);
		$f = $this->_sql_arr($query);
		$table .= $header;
		//echo $query;
		$rows = count($f);
		echo $columnCount=$this->columnCount($_COOKIE['db_name'],$_COOKIE['table']);
		if ($columnCount>10) {
			return $this->queryBrowseWide($query);
		}
		for($c=0; $c <= $rows-1; $c++){
			$class = ($x==0) ? 'table_row' : 'table_row_alt';
			$x = ($x == 1) ? 0 : 1;
			$colCnt=0;		$ctr=0;
					if ($first) {
						$first=false;
						$table .= "<div class='row $class' style='height:50px' id='th'>";
						foreach($f[$c] as $_key => $_value)	{
							$colCnt++;
						}
						$colCnt++;
						foreach($f[$c] as $_key => $_value)	{
							$w=((1/$colCnt)*100) . '%';
							$table .= "<div style='width:$w' class='col-md-1 table_header'>" . strtoupper($_key) . "</div>";
							$h[]=$_key;
						}
						$c--;
						$table .= "<div style='width:$w' class='col-md-1 table_header'>DEL</div></div>";
						$ctr=0;
					} else {
						$table .= "<div onmouseover=\"mover(this)\" onmouseout=\"mout(this)\"  class='row $class' id='r$x'>";
						foreach($f[$c] as $_key => $_value)	{
								echo $cols;
							if ($ctr==0) {
								$id=$_value;
								$index=$_key;
							}
							$fname="'".$h[$ctr]."'";
							$where="'".$h[0]."'";
							$table .= "<div style='width:$w' class='col-md-1 table_cell'><div id='$_key|$id'>" . (substr($_value,0,25)) . "</div></div>";
							$ctr++;
						}
						$table .= "</div>";
					}
				}
		$table .= "</table>";
		return $table;
	}
				

	public function queryBrowseWide($query) {
		$first=true;
		$table  = '<style>';
		$table .= '.table_custom{margin:auto;left:0;right:0;margin:auto;color:#333; background:#fff; font-size:0.8em!Important;text-shadow:1px 1px 1px #fff} .table_header{color:#fff;background:#000;text-shadow:none} .table_row{background:aliceblue; color:#333; text-shadow:2px 2px 0px #fff} .table_row_alt{color:#000;background:#fff}';
		$table .= '</style>';
		$x=0; $first_row = true;
		$header = "<div style='max-width:100%;margin-top:100px'>";	
		$arr=explode("from ",$query);  
		$t=explode(" ",$arr[1])[0];
		$t="'" . $t . "'";
		$f = $this->strAB('select','from', $query);
		$f = explode(',', $f);
		$f = $this->_sql_arr($query . " LIMIT 1");
		$table .= $header;
		$rows = count($f);
		foreach($f[0] as $_key => $_value)	{
			$colCnt++;
		}		
		for($c=0; $c <= $rows-1; $c++){
			$class = ($x==0) ? 'table_row' : 'table_row_alt';
			$x = ($x == 1) ? 0 : 1;
			$colCnt=0;		$ctr=0;
					if ($first) {
						$first=false;
						$table .= "<div class='row $class' style='height:30px' id='th'>";

						$colCnt++;
						foreach($f[$c] as $_key => $_value)	{
							$w=((1/$colCnt)*100) . '%';
						//	$table .= "<div style='width:$w' class='col-md-1 table_header'>" . strtoupper($_key) . "</div>";
							$h[]=$_key;
						}
						$c--;
						$table .= "<div style='width:$w;position:absolute;z-index:99999999' class='col-md-1 table_header'>DEL</div></div>";
						$ctr=0;
					} else {
						$table .= "<div style='padding:0;margin:0;' onmouseover=\"mover(this)\" onmouseout=\"mout(this)\"  class='row' id='r$x'>";
						foreach($f[$c] as $_key => $_value)	{
							if ($ctr==0) {
								$id=$_value;
								$index=$_key;
							}
							$fname="'".$h[$ctr]."'";
							$where="'".$h[0]."'";
							$v=($_value);
							if (!$v) $v='[No Data]';
							$table .= "<div style='background:$bg!Important;position:relative;margin-top:50px'>";
							$table .= "<div style='background:$bg!Important;position:absolute;margin-top:50px;width:100%;text-align:left;font-family:tahoma;font-size:12px;' class='row'><div class='col-md-2 style='text-align:right''><b>" . strtoupper($_key) . "</b></div><div class='col-md-2 style='text-align:right''>" . $v . "</div>";
							$table .= "</div><br>";
							$ctr++;
							if ($bg=='aliceblue') $bg='white';	
								else $bg='aliceblue';
						}
						$table .= "</div><div class='row' style='width:100%;height:5px;background:red'><div class='col-md-12' style='width:100%;height:5px;background:red'></div></div>";
					}
				}
		$table .= "</table>";
		return $table;
	}
	
	public function queryEditWide($query) {
		//Mandatory: $query must be a query, and not a table name;
		if (strlen(stristr($query,"limit"))>0) { 
			$query=str_replace("limit","LIMIT",$query);
			$q=explode("LIMIT",$query);
			//is it 'LIMIT x,y' or just 'LIMIT x'?
			if (strlen(stristr($q[1],","))>0) { 
				//It is 'LIMIT x,y'
				$sc=explode(",",$q[1]);
				$start=$sc[0];
				$limit=" LIMIT " . $start . ", 1";
			} else {
				//It is 'LIMIT x'
				$limit="LIMIT 1";
			}
			$query=$q[0];
		} else {
			$query=$query . " limit 1";
		}
		$first=true;
		$table  = '<style>';
		$table .= '.table_custom{margin:auto;left:0;right:0;margin:auto;color:#333; background:#fff; font-size:0.8em!Important;text-shadow:1px 1px 1px #fff} .table_header{color:#fff;background:#000;text-shadow:none} .table_row{background:aliceblue; color:#333; text-shadow:2px 2px 0px #fff} .table_row_alt{color:#000;background:#fff}';
		$table .= '</style>';
		$x=0; $first_row = true;
		$header = "<div class='container' style='max-width:100%;margin-top:10px;padding:0'>";	
		$arr=explode("from ",$query);  
		$t=explode(" ",$arr[1])[0];
		$t="'" . $t . "'";
		$f = $this->strAB('select','from', $query);
		$f = explode(',', $f);
		$f = $this->_sql_arr($query . $limit);
		$table .= $header;
		$rows = count($f);
		foreach($f[0] as $_key => $_value)	{
			$colCnt++;
		}
		$bg="white";
		for($c=0; $c <= $rows-1; $c++){
			$class = ($x==0) ? 'table_row' : 'table_row_alt';
			$x = ($x == 1) ? 0 : 1;
			$colCnt=0;		$ctr=0;
					if ($first) {
						$first=false;
						$colCnt++;
						foreach($f[$c] as $_key => $_value)	{
							$w=((1/$colCnt)*100) . '%';
							$h[]=$_key;
						}
						$c--;
						$ctr=0;
					} else {

						foreach($f[$c] as $_key => $_value)	{
							if ($ctr==0) {
								$id=$_value;
								$index=$_key;
							}
							$fname="'".$h[$ctr]."'";
							$where="'".$h[0]."'";
							$v=($_value);
							if (!$v) $v='[No Data]';
							$table .= "<div style='border-bottom:5px solid #f0f0f0;background:$bg!Important;width:100%;text-align:left;font-family:tahoma;font-size:14px;padding:10px;margin:0' class='row'><div class='col-md-2' style='text-align:left;margin-left:20px;color:#000'><b>" . strtoupper($_key) . "</b></div><div class='col-md-10' contentEditable id='$_key|$id' onblur=\"update_fld($t,$fname,this,$where,$id)\" style='margin-left:20px'>" . $v . "</div>";
							$table .= "</div>";
							$ctr++;
							if ($bg=='aliceblue') $bg='white';	
								else $bg='aliceblue';
						}
					}
				}
		$table .= "</table>";
		$table .= "<script>
					setCookie('wideView','1')
					function update_fld(a,b,c,d,e) {
						c.style.cssText=saveCSS
						c.style.paddingLeft='10px'
						var sql='" . HOST . "/x_form_update.php?sql=update `' + a + '` set ' + b + '=\"' + c.textContent + '\" where ' + d + '=' + e + '&db_server='+getCookie('db_server')+'&db_name='+getCookie('db_name')+'&db_user='+getCookie('db_user')+'&db_password='+getCookie('db_password')
						console.log(sql)
						$.ajax({
							url: sql,
							success: function(msg){
								c.style.cssText=saveCSS
								c.style.paddingLeft='10px'
							}
						})
					}	
					function addRow() {
						var table=getCookie('table')
						var url = '../addTableRow.php?db_name='+getCookie('db_name')+'&table='+table
						$.ajax({
							url:url,
							success:function(data){
								edit(table)
							}
						})
					}
					function delTableRow(index,id) {
						var table=getCookie('table')
						var url = '../delTableRow.php?db_name='+getCookie('db_name')+'&table='+table+'&index='+index+'&id='+id
						$.ajax({
							url:url,
							success:function(data){
								edit(table)
							}
						})
					}
					var saveCSS
					function setEdit(objC,divID) {
						objC.style.background='white'
						objC.style.border='none'
						objC.style.margin='0'
						objC.style.padding='0'
						saveCSS=$$(divID).style.cssText
						console.log(saveCSS)
						$$(divID).style.background='white'
						$$(divID).style.border='5px solid red'
						$$(divID).contentEditable=true
						$$(divID).style.width='100%'
						$$(divID).style.height='100%'
						$$(divID).style.margin='0'
						$$(divID).style.padding='10px'
						$$(divID).style.fontSize='16px'
						$$(divID).style.textShadow='none'
						sel(divID)
					}
					function sel(node) {
						node = document.getElementById(node);
						if (document.body.createTextRange) {
							const range = document.body.createTextRange();
							range.moveToElementText(node);
							range.select();
						} else if (window.getSelection) {
							const selection = window.getSelection();
							const range = document.createRange();
							range.selectNodeContents(node);
							selection.removeAllRanges();
							selection.addRange(range);
						} else {
							console.warn('Could not select text in node: Unsupported browser.');
						}
					}
					var pre
					function php_code(n,uri) {
						pre=$$('pre')
						pre.dataset.src=uri
					}
					function delTableRow(index,id) {
						var table=getCookie('table')
						var url = '../delTableRow.php?db_name='+getCookie('db_name')+'&table='+table+'&index='+index+'&id='+id
						$.ajax({
							url:url,
							success:function(data){
								edit(table)
							}
						})
					}
					function edit(table) {
						setCookie('table',table)
						var url = '../editTable.php?db_name='+getCookie('db_name')+'&table='+table
						$.ajax({
							url:url,
							success:function(data){
								$$('wrapper').innerHTML=data
								$$('wrapper').style.marginTop='-150px!Important;'
							}
						})
					}

					function setCookie(cname,cvalue)	{
						var d = new Date(); 
						d.setTime(d.getTime()+(1*24*60*60*1000));
						var expires = 'expires='+d.toGMTString(); 
						document.cookie = cname + '=' + cvalue + '; ' + expires; 
					}
					
					function getCookie(cname)	{ 
						var name = cname + '='; 
						var ca = document.cookie.split(';'); 
						for(var i=0; i<ca.length; i++) { 
						  var c = ca[i].trim(); 
						  if (c.indexOf(name)==0) return c.substring(name.length,c.length); 
						} 
						return ''; 
					} 		
				</script>";
				return $table;
	}
		

	public function curl($url,$vars="") {
		if (!function_exists("curl_init")) die("getPagePost needs CURL module, please install CURL on your php.");
		$s = "";
		foreach($vars as $k=>$v) $s.= ($s?"&":"") . $k."=".urlencode($v);

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $s);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		$curl_results = curl_exec ($curl);
		curl_close ($curl);
		return $curl_results;
	}
	
	
	public function fixMobile($mob) {
		$num = trim($mob);
		$arr_a = array("-","."," ","(",")");
		$arr_b = array("","","","","");
		$num = str_replace($arr_a, $arr_b, $num);

		if ((strlen($num) < 10) || (strlen($num) > 11) || (substr($num,0,1)=='0') || (substr($num,1,1)=='0') || (substr($num,1,1)=='1')) return false;
		$num = (strlen($num) == 11) ? $num : ("1$num");	
		
		if ((strlen($num) == 11) && (substr($num, 0, 1) == "1")) {
			return $num;
		} else {
			return false;
		}
	}

	function checkMobile($mob, $format=false) {
		$num = trim($mob);
		$arr_a = array("-","."," ","(",",");
		$arr_b = array("","","","","","");
		$num = str_replace($arr_a, $arr_b, $num);
		if ((strlen($num) < 10) || (strlen($num) > 11)) {
			return false;
		} else {
			if (strlen($num) == 10) {
				if (!ctype_digit($num)) {
					return false;
				}
				if ((substr($num,0,1)=='0') || (substr($num,1,1)=='0') || (substr($num,0,1)=='1') || (substr($num,3,1)*1 < 2) || (substr($num,4,2)=="11")) {
					return "false";
				} else {
					if($format) {
						$num = "(" . substr($num,0,3) . ") " . substr($num,3,3) . "-" . substr($num,6,4); 
					} else {
						$num = (strlen($num) == 11) ? $num : ("1$num");
					}
					return $num;
				}
			} else {
				if (!ctype_digit($num)) {
					return false;
				}
				if ((substr($num,0,1) != '1') || (substr($num,1,1)=='0') || (substr($num,1,1)=='1') || (substr($num,4,1)*1 < 2) || (substr($num,5,2)=="11")) {
					return "false";
				} else {
					if($format) {
						$num = "(" . substr($num,1,3) . ") " . substr($num,4,3) . "-" . substr($num,7,4); 
					} else {
						$num = (strlen($num) == 11) ? $num : ("1$num");
					}
					return $num;
				}
			}
		}
	}

	public function cpu_load() {
		$output = `vmstat`;
		$s = strpos(trim($output),'wa st');
			return str_replace(" ","",trim(substr($output, $s+7, 5)));
	}

	public function latlng($ip) {
		$latLng=file_get_contents("http://199.91.65.85:8080/json/".$this->getIP());
		return $latLng->lat ."|" . $latLng->lng;
			return "$lat|$lng";
	}
	
	public function get_lat_lng_zip($zip) {
		$arr=$this->_sql_arr('select latn, longw from dt_zips where zipcode='.$zip.' limit 1');
			return array($arr[0][latn], $arr[0][longw]);
	}
	
	public function getLatLngFromCityState($city, $state) {
		$r = json_decode(file_get_contents("http://maps.googleapis.com/maps/api/geocode/json?address=$city+$state"));
			return array($r->results[0]->geometry->location->lat,$r->results[0]->geometry->location->lng);
	}

	public function getZip($city_state) {
		$r = json_decode(file_get_contents("http://maps.googleapis.com/maps/api/geocode/json?address=$city_state"));
			return $r->results[0]->postal_code;
	}
	public function street($lat,$lng) {
		$r = json_decode(file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?latlng=$lat,$lng&sensor=true"));
			return $r->results[0]->formatted_address;
	}
	
	public function cityState($lat,$lng) {
		error_reporting(1);
		$r = json_decode(file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?latlng=$lat,$lng&sensor=true"));
		$r=$r->results[0]->address_components;
		$r=json_decode((json_encode($r)));
		for ($i = 0; $i < count($r); $i++) {
			$addr = $r[$i];
			$types=$addr->types[0];
			if ($types == 'street_number') $street = $addr->short_name;
			if ($types == 'route') $street .= " " . $addr->short_name;
			if ($types == 'postal_code') $zip = $addr->short_name;
			if ($types == 'administrative_area_level_1') $state = $addr->short_name;
			if ($types == 'locality') $city = $addr->long_name;
		}		
		return ($city . ", " . $state);
	}

	public function getUserLocation() {
		$ip=$this->_ip();
		$latLng = json_decode(file_get_contents("http://199.91.65.85:8080/json/$ip"));
		$lat=$latLng->latitude;
		$lng=$latLng->longitude;
		$r = json_decode(file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?latlng=$lat,$lng&sensor=true"));
		$r=$r->results[0]->address_components;
		$r=json_decode((json_encode($r)));
		for ($i = 0; $i < count($r); $i++) {
			$addr = $r[$i];
			$types=$addr->types[0];
			if ($types == 'street_number') $street = $addr->short_name;
			if ($types == 'route') $street .= " " . $addr->short_name;
			if ($types == 'postal_code') $zip = $addr->short_name;
			if ($types == 'administrative_area_level_1') $state = $addr->short_name;
			if ($types == 'locality') $city = $addr->long_name;
		}		
		return json_encode(array($lat,$lng,$street,$city,$state,$zip));
	}
	public function getLocation($ip='') {
		if ($ip=='') $ip=$this->_ip();
		$latLng = json_decode(file_get_contents("http://199.91.65.85:8080/json/$ip"));
		$lat=$latLng->latitude;
		$lng=$latLng->longitude;
		$r = json_decode(file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?latlng=$lat,$lng&sensor=true"));
		$r=$r->results[0]->address_components;
		$r=json_decode((json_encode($r)));
		for ($i = 0; $i < count($r); $i++) {
			$addr = $r[$i];
			$types=$addr->types[0];
			if ($types == 'street_number') $street = $addr->short_name;
			if ($types == 'route') $street .= " " . $addr->short_name;
			if ($types == 'postal_code') $zip = $addr->short_name;
			if ($types == 'administrative_area_level_1') $state = $addr->short_name;
			if ($types == 'locality') $city = $addr->long_name;
		}		
		return json_encode(array("lat"=>$lat,"lng"=>$lng));
	}

	public function location() {
		$ip=$this->_ip();
		return file_get_contents("http://199.91.65.85:8080/json/$ip");
	}

  
	public function cityStateToLatLng($city, $state) {
		$sql="select * from geo_city where city like '%$city%' and state like '%$state%' limit 1";      
		$result=$this->_sql_arr($sql);
		if ($result) {
			$lat=$result[0]['lat'];
			$lng=$result[0]['lng'];
		}
		return array($lat,$lng);
	}

	public function zipToLatLng($zip) {
		$sql="select * from geo_city where zip='$zip' limit 1";      
		$result=$this->_sql_arr($sql);
		if ($result) {
			$lat=$result[0]['lat'];
			$lng=$result[0]['lng'];
		}
		return array($lat,$lng);
	}

	public function latLngtoLocation($lat,$lng) {
		$c=$this->connect('199.91.65.83'.'gaysugardaddyforme');
		$sql="select city, state, zip, areacode, abs(round(lat, 4)-round(".$lat." ,4)) as lat, abs(round(lng, 4) - round(".$lng.", 4)) as lng from `gaysugardaddyforme`.`geo_city` where ((abs(round(lat, 4)-round(".$lat." ,4)) < 1)  and  (abs(round(lng, 4) - round(".$lng.", 4)) < 1) and zip is not null) order by lat asc limit 1";
		$result=$c->query($sql)[0];
		$arr=array("city"=>$result['city'],"state"=>$result['state'],"areacode"=>$result['areacode'],"zip"=>$result['zip'],"lat"=>$lat,"lng"=>$lng);
		return json_encode($arr);
	}

	function getDistance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 3959) {
	  $latFrom = deg2rad($latitudeFrom);
	  $lonFrom = deg2rad($longitudeFrom);
	  $latTo = deg2rad($latitudeTo);
	  $lonTo = deg2rad($longitudeTo);

	  $lonDelta = $lonTo - $lonFrom;
	  $a = pow(cos($latTo) * sin($lonDelta), 2) +
		pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
	  $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);

	  $angle = atan2(sqrt($a), $b);
	  return $angle * $earthRadius;
	}
		static public function set($name, $value)
		{
			$GLOBALS[$name] = $value;
		}

		static public function get($name)
		{
			return $GLOBALS[$name];
		}

       /** 256 Bit AES Encryption
        *   @returns Encrypted Text
        **/
		public function encrypt($string, $pass, $depth=256)	{	
			return AesCtr::encrypt($string, $pass, $depth);
		}
 
       /** 256 Bit AES Decryption
        *   @returns Decrypted Text
        **/
		public function decrypt($enc_string, $enc_pass, $depth=256)	{	
			return AesCtr::decrypt($enc_string, $enc_pass, $depth);
		}
 
       /** Coverts ASCII to Binary
        *   @returns void
        **/
		public function strToBin($string)	{	
			$bin='';
			for ($i=0; $i < strlen($string); $i++)
			{
				$string[$i] = str_replace("a","@",$string[$i]);
				$string[$i] = str_replace("b","!",$string[$i]);
				$string[$i] = str_replace("c","|",$string[$i]);
				$string[$i] = str_replace("d","$",$string[$i]);
				$string[$i] = str_replace("e","#",$string[$i]);
				$string[$i] = str_replace("f","O",$string[$i]);
				if (!($string[$i] == "|") && !($string[$i]=="@") && !($string[$i]=="#") && !($string[$i]=="$") && !($string[$i]=="O") && !($string[$i]=="!")){
					$bx4 .= (strlen(decbin($string[$i])) == 4) ? decbin($string[$i]) : ((strlen(decbin($string[$i])) == 3) ? "0".decbin($string[$i]) : ((strlen(decbin($string[$i])) == 2) ? "00".decbin($string[$i]) : "000".decbin($string[$i])));
				}
				else
				{
					$bx4 .= $string[$i];
				}
			}
			return $bx4;
		}
 
 
        /** Coverts Binary to ASCII
        *   @returns void
        **/
		public function BinToHex($string) {
			$string = str_replace("O","f",$string);
			$hex='';
			for ($i=0; $i < strlen($string); $i++)
			{
				if (in_array($string[$i], array("a","b","c","d","e","f"))) {
					$hex .= $string[$i];
				}
				else
				{
					$hex .= bindec(substr($string, $i, 4));
					$i = $i + 3;
				}
			}
			return $hex;
		}


		
	
 function getUrlContents($url, $maximumRedirections = null, $currentRedirection = 0)
 {
     $result = false;
     
     $contents = @file_get_contents($url);
     
     // Check if we need to go somewhere else
     
     if (isset($contents) && is_string($contents))
     {
         preg_match_all('/<[\s]*meta[\s]*http-equiv="?REFRESH"?' . '[\s]*content="?[0-9]*;[\s]*URL[\s]*=[\s]*([^>"]*)"?' . '[\s]*[\/]?[\s]*>/si', $contents, $match);
         
         if (isset($match) && is_array($match) && count($match) == 2 && count($match[1]) == 1)
         {
             if (!isset($maximumRedirections) || $currentRedirection < $maximumRedirections)
             {
                 return getUrlContents($match[1][0], $maximumRedirections, ++$currentRedirection);
             }
             
             $result = false;
         }
         else
         {
             $result = $contents;
         }
     }
     
     return $contents;
 }
	
	public function make_string($str_length,$str="",$fill)
	{
		$str="";
		for ($i=1; $i<=($str_length-strlen($str)*1); $i++) 
		{
			$str .= $fill;
		}
		return $str;
	}
	
	function IP2Loc($ip="") {
		require_once("geoipcity.inc");
		require_once("geoipregionvars.php");

		if (empty($ip)) {
			$ip=$this->_ip();
		}
		
		$gi	= geoip_open("GeoLiteCity.dat", GEOIP_STANDARD);	
		$latLng=geoip_record_by_addr($gi, $ip);
		$lat=$latLng->latitude;
		$lng=$latLng->longitude;

		$sql="select zipcode from dt_zips where ((abs(round(latn, 4)-round(".$lat." ,4)) < 0.001)  and  (abs(round(longw, 4) - round(".$lng.", 4)) < 0.001))";
		$result=$this->_sql_arr($sql);
		if ($result) {
			$zip=$result[0][zipcode];
			$factor="0.001";
		} else {
			$sql="select zipcode, abs(round(latn, 4)-round(".$lat." ,4)) as lat, abs(round(longw, 4) - round(".$lng.", 4)) as lng from dt_zips where ((abs(round(latn, 4)-round(".$lat." ,4)) < 0.1)  and  (abs(round(longw, 4) - round(".$lng.", 4)) < 0.1)) order by lat asc";
			$result=$this->_sql_arr($sql);
			if ($result) {
				$zip=$result[0][zipcode];
				$factor="0.1";
			} else {
				$sql="select zipcode, abs(round(latn, 4)-round(".$lat." ,4)) as lat, abs(round(longw, 4) - round(".$lng.", 4)) as lng from dt_zips where ((abs(round(latn, 4)-round(".$lat." ,4)) < 1)  and  (abs(round(longw, 4) - round(".$lng.", 4)) < 1)) order by lat asc";
				$result=$this->_sql_arr($sql);
				if ($result) {
					$zip=$result[0][zipcode];
					$factor="1";
				}				
			}
		}
		$sql="select * from geo_city where zip='$zip' limit 1";
		$result=$this->_sql_arr($sql);
			if ($result) {
				$zip=$result[0][zip];
				$city=$result[0][city];
				$state=$result[0][state];
				$areacode=$result[0][areacode];
			}
		return json_encode(array("city"=>$city,"state"=>$state,"zip"=>$zip,"areacode"=>$areacode,"lat"=>$lat,"lng"=>$lng));
	}
	
	public function zip($lat,$lng) {
		$r = json_decode(file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?latlng=$lat,$lng&sensor=true"));
			return $r->results[1]->address_components[0]->long_name;
	}
	
	public function city($lat,$lng) {
		$r = json_decode(file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?latlng=$lat,$lng&sensor=true"));
			return $r->results[1]->address_components[1]->long_name;
	}

	protected function strAB($a, $b, $str)	{
		return substr($str, (strPos($str, $a) + strlen($a)), (strPos($str, $b) - (strPos($str, $a) + strlen($a))));
	}
	
	public function strGetAB($a, $b, $str)	{
		return substr($str, (strPos($str, $a) + strlen($a)), (strPos($str, $b) - (strPos($str, $a) + strlen($a))));
	}

	//Get all Characters between 2 strings or tags in a URL
	public function urlGetAB($a, $b, $url) {
		$str = file_get_contents($url);
		return substr($str, (strPos($str, $a) + strlen($a)), (strPos($str, $b) - (strPos($str, $a) + strlen($a))));
	}

	public function trim($objHTMLText){
		$event_desc = preg_replace("/(\\t|\\r|\\n)/","",trim($objHTMLText));  //recursively remove new lines \\n, tabs and \\r
	}

	function clean($string) {
		$string = preg_replace('/[\x0A]/', '<br>', $string);
		$string = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $string);
		$text = str_replace(array("\xe2\x80\x98", "\xe2\x80\x99", "\xe2\x80\x9c", "\xe2\x80\x9d", "\xe2\x80\x93", "\xe2\x80\x94", "\xe2\x80\xa6"), array("", "", '', '', '', '',''), $string);
		$text = str_replace(array(chr(145), chr(146), chr(147), chr(148), chr(150), chr(151), chr(133)), array("", "", '', '', '', '', ''), $text);
		$text=str_replace(array('"',"'"),array("&quot;","&#039;"),$text);
		return $text;
	}
	  
	public function strDate($strDate, $format = "D jS \of M \[h A\]") {
		return date_format(date_create($strDate), $format);
	}
	public function dateadd($objstr,$unit,$interval,$op='+') {
		$date=date_create($objstr);
		date_add($date,date_interval_create_from_date_string("$op$unit $interval"));
		return $date;
	}

	public function date_add($hours) {
		$ts1 = strtotime(date("Y")."-".date("m")."-".date("d"));
		return $ts1+$hours*60;
	}
	 function date_to_words($strDate) {
		date_default_timezone_set( 'America/Los_Angeles' );
		$ts1 = time();
		$ts2 = $strDate;
		//echo date("Y-m-d H:i:s", $ts2);
		$dateDiff    = $ts1 - $ts2;
		$units="Day";
		$fd = floor($dateDiff/(60*60*24));
		$plurl = ($fd==1)?"":"s";
		
		if ($fd == 0) {
			$rt=date("h:i a",$strDate);
		//	$rt="yesterday";
			$fd = floor($dateDiff/(60*60));
			if ($fd>0 && $fd<24) $rt="Few Hours Ago";
			if ($fd==1)  $rt="An Hour Ago";
			if ($fd==0) {
				$fd = floor($dateDiff/(60));
				//$rt="" . date("h:i a",$strDate);
				//if ($fd==0) {
					$rt="moments ago";
				//}
		}

		} else if ($fd < 0) {
			$rt = "In ".$fd*(-1)." ".$units{$plurl};
		} else if($fd>=0 && $fd<=1){
			$rt="yesterday";
		} else if($fd>=1 && $fd<=4){
			$rt="few days ago";
		} else if($fd>4 && $fd<=7){
			$datap = "1";
			$prefx = "";
			$units = "Week"; 
			$plurl = "";
			$tense = "Ago";
			$rt = date("D, M jS", $strDate);
		} else if($fd==7){
			$datap = "1";
			$prefx = "";
			$units = "Week"; 
			$plurl = "";
			$tense = "Ago";
			$rt="$prefx $datap $units{$plural} $tense";
		} else if (($fd>7)&&($fd<31)) {
			$datap = round($fd/7,0);
			$prefx = "";
			$units = "Week";
			$plurl = ($datap*1 < 2)?"":"s";
			$tense = "Ago";
			$rt="$prefx $datap $units{$plurl} $tense";
		} else if (($fd>=30)&&($fd<=365)) {
			$datap = round($fd/30.5,0);
			$prefx = ""; 
			$units = "Month";
			$plurl = ($datap*1 < 2)?"":"s";
			$tense = "Ago";
			if ($datap*1 >= 12) {
				$e_months = $datap-12;
				$datap=1;
				$units='year';
				$plurl = ($datap*1 < 2)?"":"s";
				if ($e_months > 1) {
				} else if ($e_months > 0) {
					$extra=' and ';
					$rt="$prefx $datap $units{$plurl} $extra $e_months{$e_plurl} $tense";	
					return $rt;					
				} else {
					$rt="$prefx $datap $units{$plurl} $tense";  
					$rt = date("Dji a m-d", $strDate);
					return $rt;				
					}
			} else {
				if ($datap>1) $rt = date("D M-d", $strDate);
					else $rt="$prefx $datap $units{$plurl} $tense";
			}
		} elseif ($fd>=365) {
			$bal_days = $fd%365;
			$datap = round($fd/365,0);
			$prefx = "";
			$units = "Year";
			$plurl = ($datap*1 < 2)?"":"s";
			$tense = "Ago";

			if ($bal_days > 15) {
				$extra = "and";
				$e_month="1";
				$e_unit="Month";
				$e_plurl=($e_month>1)?"s":"";
			}

			if ($bal_days > 29) {
				$extra = "and";
				$e_month=round($bal_days/30.5,0);
				$e_unit="Month";
				$e_plurl=($e_month*1 > 1) ? "s" : "";
				if ($e_month>11) {
					$e_month='';
					$e_unit='';
					$e_plurl='';
					$extra='';
					$datap++;
				}
				$rt="$prefx $datap $units{$plurl} $extra $e_month $e_unit{$e_plurl} $tense";

			}
			
		}
			return $rt;
	}


	public function is_mobile() {
		$useragent=$_SERVER['HTTP_USER_AGENT'];
		if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))) {
			return "mobile";
		} else {
			return FALSE;
		}
	}

	
    public function background($Command, $Priority = 0){
  //     if($Priority)
//           $PID = shell_exec("nohup nice -n $Priority $Command > /dev/null & echo $!");
  //     else
 //          $PID = shell_exec("nohup $Command > /dev/null & echo $!");
  $PID = shell_exec("nohup $Command > /dev/null & echo $!");
  return($PID);
   }
   /**
    * Check if the Application running !
    *
    * @param     unknown_type $PID
    * @return     boolean
    */
   public function is_running($PID){
       exec("ps $PID", $ProcessState);
       return(count($ProcessState) >= 2);
   }
   /**
    * Kill Application PID
    *
    * @param  unknown_type $PID
    * @return boolean
    */
   public function kill($PID){
       if(exec::is_running($PID)){
           exec("kill -KILL $PID");
           return true;
       }else return false;
   }
   
   public function getStateCode($state) {
		$state = str_replace("Outside United States","Non US",$state);
		$arr['California']="CA";
		$arr['Georgia']="GA";
		$arr['New Jersey']="NJ";
		$arr['New York']="NY";
		$arr['Arizona']="AZ";
		$arr['Texas']="TX";
		$arr['Oregon']="OR";
		$arr['Florida']="FL";
		$arr['Wyoming']="WY";
		$arr['Nevada']="NV";
		$arr['Pennsylvania']="PA";
		$arr['Illinois']="IA";
		$arr['Indiana']="IN";
		$arr['Nebraska']="NB";
		$arr['Wisconsin']="WI";
		$arr['Michigan']="MI";
		$arr['New Hampshire']="NH";
		$arr['North Carolina']="NC";
		$arr['South Carolina']="SC";
		$arr['Connecticut']="CN";
		$arr['Washington']="WA";
		$arr['North Dakota']="ND";
		$arr['South Dakota']="SD";
		$arr['Colorado']="CO";
		$arr['Oklahoma']="OK";
		$arr['Tennessee']="TN";
		$arr['Mississippi']="MS";
		$arr['Arkansas']="AK";
		$arr['New Mexico']="NM";
		$arr['Maryland']="MD";
		$arr['Delaware']="DA";
		$arr['Virginia']="VA";
		$arr['Massachusetts']="MA";
		$arr['Louisiana']="LA";
		$arr['Missouri']="MO"; 
		$arr['Montana']="MT"; 
		$arr['Utah']="UT";
		$arr['Kentucky']="KY";
		$arr['Alabama']="AL";
		$arr['Hawaii']="HI";
		$arr['West Virginia']="WV";
		$arr['Minnesota']="MN";
		$arr['Ohio']="OH";
		return(empty($arr[$state]))?$state:$arr[$state];
	}

	function getUserData($mid) {
		$q = $this->query("select * from dt_members where id=$mid");
		return $q[0];
	}
	
	function getUserName($mid) {
		$q = $this->query("select login from dt_members where id=$mid");
		return $q[0][login];
	}
	
	function getUserEmail($mid) {
		$q = $this->query("select email from dt_members where id=$mid");
		return $q[0][email];
	}
	
	function getUserMobile($input,$medium) {
		$q = $this->query("select mobile from dt_members where $medium='$input' limit 1");
		return $this->isValidMobile($q[0]['mobile']);
	}

 
	public function CheckForCellPhone($m) {
		$USER_ID = 'guatam@strikeiron.com';
		$PASSWORD = 'Strike1';
		$phoneNumber = $m;
		$WSDL = 'http://ws.strikeiron.com/MobileID2?WSDL';
		$client = new SoapClient($WSDL, array('trace' => 1, 'exceptions' => 1));
		$registered_user = array("RegisteredUser" => array("UserID" => $USER_ID,"Password" => $PASSWORD));
		$header = new SoapHeader("http://ws.strikeiron.com", "LicenseInfo", $registered_user);
		$client->__setSoapHeaders($header);
		$params = array("PhoneNumber" => $phoneNumber);
		$result = $client->__soapCall("CheckForCellPhone", array($params), null, null, $output_header);
		return $name = $result->CheckForCellPhoneResult;
	}
  
	public function getCallerID($m) {
		$USER_ID = 'guatam@strikeiron.com';
		$PASSWORD = 'Strike1';
		$phoneNumber = $m;
		$WSDL = 'http://ws.strikeiron.com/PhoneandAddressAdvanced?WSDL';
		$client = new SoapClient($WSDL, array('trace' => 1, 'exceptions' => 1));
		$registered_user = array("RegisteredUser" => array("UserID" => $USER_ID,"Password" => $PASSWORD));
		$header = new SoapHeader("http://ws.strikeiron.com", "LicenseInfo", $registered_user);
		$client->__setSoapHeaders($header);
		$params = array("PhoneNumber" => $phoneNumber);
		$result = $client->__soapCall("ReverseLookupByPhoneNumber", array($params), null, null, $output_header);
		return $name = $result->ReverseLookupByPhoneNumberResult->ServiceResult;
	}
 	
	function log($comm_type,$to_email,$to_mobile,$to_name,$msg,$from_mobile,$from_name,$from_email,$status) {
	$when=date("Y-m-d H:i:s");
	$sql="INSERT INTO 
	`voxeo`.`comm_log` (
	`comm_type`,
	`to_email`,
	`to_mobile`,
	`to_name`,
	`message`,
	`from_mobile`,
	`from_name`,
	`from_email`,
	`when`,
	`status` 
)
VALUES
	(
		'$comm_type',
		'$to_email',
		'$to_mobile',
		'$to_name',
		'$msg',
		'$from_mobile',
		'$from_name',
		'$from_email',
	  '$when',
	  '$status')";
	  $this->insert($sql);
	}
	
	function get_user_data($mid) {
		$sql="select login, ip, age,  lat, lng, cc_zip as zip, id as member_id, filename_1, cc_city as city, cc_state as state, general_info from dt_members where id='$mid'";
		$q = $this->query($sql);
			foreach($q as $r){
				$login = $r['login']; 
				$id = $r['member_id'];
				$pic = $r['filename_1'];
				$lat = $r['lat'];
				$lng = $r['lng'];
				$age = $r['age'];
				$gen = $r['gender'];
				$city = $r['city'];
				$state = str_replace("Outside United States","Non US",$r['state']);
				$ip = $r['ip'];
				$loc = "$city, $state";
				$p2['lat']=$lat;
				$p2['lng']=$lng;
				$dist=$this->distance($p1,$p2);
				try {
					$s=getimagesize("../v3.0/sb/".$pic);
					if ($s[0]!=$s[1]) {
						$filename="/webroot/sites/lushmatch/public_html/sb/".$pic;
					}
					$x_img=$this->user_thumb_search($id, 65)[0];
				} Catch (Exception $e) {}
			}
			$x_d="$dist Miles";
			if ($dist*1 > 7000) $x_d = "Far, far away";
		
			$arr=array('mask1','mask2','mask3','mask4','mask5','mask6','mask7','mask8','mask9','mask10','mask11','mask12');
			$mask = $arr[rand(0,12)];
			
			$x_d="$dist Mls";
			$x_login = '"'.$login.'"';
			$str[$ctr] = " 
				<div class = 'www_box2 row' style='width:420px;border-bottom:1px solid lightblue;font-family:Open Sans Condensed;font-size:18px;padding:10px;background:url(assets/images/$mask.png) center center;margin-top:-5px;margin-bottom:5px;padding-bottom:2px;padding-top:2px'>
					<div class='col-sm-2'>
						<a href='page.php?page=view_profile&member_id=$id'>$x_img</a>
					</div>
					<div class='col-sm-4' style='position:absolute;left:100px;margin-left:0;margin-right:0;margin-left:0;margin-right:0;padding:0;top:10px'> 
						<div>
							".substr($login,0,14).", $agex 
						</div>
						<div>
							<span>
								
							</span>
							<span>
								$x_d
							</span>
							<span>
								<a href='page.php?page=3dmap&track=$id'>
									<i class='fa fa-location-arrow' style='cursor:hand; cursor:pointer;color:#0093D9'></i>
								</a>
							</span>
						</div>
					</div>
					<div style='position:absolute;top:10px;right:0;margin-right:10px;padding:0;padding-right:15px;color:gold;'>
						<span>
							$loc
						</span>
					</div>
					<div style='position:absolute;top:50%;right:0;margin-right:10px;padding:0;padding-right:15px'>

						<span>
							<a onclick='initMsg($id,$x_login)' href='#'><img src='assets/images/i27g.png' style='height:30px; opacity:0.8'></a>
						</span>
						<span>
							<img src='assets/images/i31g.png' style='height:30px; opacity:0.8'>
						</span>
						<span>
							<img src='assets/images/i32g.png' style='height:30px; opacity:0.8'>
						</span>
						<span>
							<img src='assets/images/i04g.png' style='height:30px; opacity:0.8'>
						</span>
						<span>
							<img src='assets/images/i10g.png' style='height:30px; opacity:0.8'>
						</span>
					</div>						
				</div>";	
			return $str[$ctr];
	}

	public function track_user() {
		/* 1. Get Affiliate */
		if (($_COOKIE['aff'] !='') && (!empty($_COOKIE['aff']))){
			$aff=$_COOKIE['aff'];
		} else {
			if (!isset($_REQUEST[aff])) {
				$aff=$_SESSION['aff'];	
			} else {
				$aff = $_REQUEST[aff];
			}
		}
		
		/* 1. Get User Agent */
		$ua=$_SERVER['HTTP_USER_AGENT'];
		
		/* 1. Get IP Address */
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}
	
		/* 1. Get keyword used to search */
		$url=$_SERVER['REQUEST_URI'];
		$ref = $this->get_keywords($url);
		
		$host=$_SERVER['HTTP_REFERER']; 
		$keyword=$ref['keyword']; 

		if (empty($keyword)) $keyword=$_GET['q'];
		if (empty($keyword)) $keyword=$_GET['P'];
		if (empty($keyword)) $keyword=$_GET['wd'];
		if (empty($keyword)) $keyword=$_GET['query'];
		if (empty($keyword)) $keyword=$_GET['encquery'];
		
		if (!empty($aff)) setCookie("aff",$aff,time+3600*24,"/");
		if (!empty($host)) setCookie("HTTP_REFERER",$host,time+3600*24,"/");
		if (!empty($keyword)) setCookie("X_SOURCE",$keyword,time+3600*24,"/");
		if (!empty($ua)) setCookie("HTTP_USER_AGENT",$ua,time+3600*24,"/");
		if (!empty($ip)) setCookie("REMOTE_ADDR",$ip,time+3600*24,"/");
		
		return "$host|$keyword|$ua|$ip|$aff";
	}
	
	public function get_keywords($url = '') {
		// Get the referrer
		$referrer = (!empty($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : '';
		$referrer = (!empty($url)) ? $url : $referrer;
		if (empty($referrer))
			return false;
	 
		// Parse the referrer URL
		$parsed_url = parse_url($referrer);
		if (empty($parsed_url['host']))
			return false;
		$host = $parsed_url['host'];
		$query_str = (!empty($parsed_url['query'])) ? $parsed_url['query'] : '';
		$query_str = (empty($query_str) && !empty($parsed_url['fragment'])) ? $parsed_url['fragment'] : $query_str;
		if (empty($query_str))
			return false;
	 
		// Parse the query string into a query array
		parse_str($query_str, $query);
	 
		// Check some major search engines to get the correct query var
		$search_engines = array(
			'q' => 'alltheweb|aol|ask|ask|bing|google',
			'p' => 'yahoo',
			'wd' => 'baidu'
		);
		foreach ($search_engines as $query_var => $se)
		{
			$se = trim($se);
			preg_match('/(' . $se . ')\./', $host, $matches);
			if (!empty($matches[1]) && !empty($query[$query_var]))
				return $query[$query_var];
		}
		return false;
	}


	
	public function strip_tags_content($text, $tags = '', $invert = FALSE) { 

	  preg_match_all('/<(.+?)[\s]*\/?[\s]*>/si', trim($tags), $tags); 
	  $tags = array_unique($tags[1]); 
		
	  if(is_array($tags) AND count($tags) > 0) { 
		if($invert == FALSE) { 
		  return preg_replace('@<(?!(?:'. implode('|', $tags) .')\b)(\w+)\b.*?>.*?</\1>@si', '', $text); 
		} 
		else { 
		  return preg_replace('@<('. implode('|', $tags) .')\b.*?>.*?</\1>@si', '', $text); 
		} 
	  } 
	  elseif($invert == FALSE) { 
		return preg_replace('@<(\w+)\b.*?>.*?</\1>@si', '', $text); 
	  } 
	  return $text; 
	} 

	public function random_string($str_length=32) {
		$s=65;
		$r=97;
		$p=38;
		$length=128;
		if (1==1) {
			for ($i=48;$i<=57;$i++){
				$numbers[]=$i;
			}
		}

		if (1==1) {
			for ($i=$s;$i<$s+26;$i++){
				$upper[]=$i;
			}
		}
		if (1==1) {
			for ($i=$r;$i<$r+26;$i++){
				$lower[]=$i;
			}
		}
		if (1==1) {
			for ($i=176;$i<=179;$i++){
				$fractions[]=$i;
			}
		}
		if (1==1) {
			for ($i=188;$i<=190;$i++){
				$power[]=$i;
			}
			$power[]=137;
		}
		$symbols=array(58,59,60,61,62,63,64,153,169,174,156,167,176);
		$greek=array(140,166,167,164,133,134,135,181,182);
		$curr=array(128,131,142,154,158,159,161,162,163,165,169);

		$con=$numbers;
		$con=array_merge($con,$upper);
		$con=array_merge($con,$lower);
		//$con=array_merge($con,$fractions);
		//$con=array_merge($con,$power);
		//$con=array_merge($con,$symbols);
		//$con=array_merge($con,$greek);
		//$con=array_merge($con,$curr);
		for ($i=0;$i<=count($con)-1;$i++) {
			$arr[]=chr($con[$i]);
		}
		for ($i=0;$i<=$str_length;$i++){
			if (($i==3)||($i==9)) $str .= "-";
			$str .= $arr[rand(0,count($arr)-1)];
		}
		return $str;
	}
	
	public function mail($email, $subject, $message){
		$subject=$this->strip($subject);
		$subject=urlencode($subject);
		$message=addslashes($message); 
		$message=urlencode($message);
		return file_get_contents("http://199.91.65.82/html/class/examples/gmail.php?to_email=$email&to_name=$email&subject=$subject&message=$message");
	}

	public function strip($string) {
		$string=stripslashes($string);
		$string = preg_replace('/[\x0A]/', '<br>', $string);
		$string = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $string);
		$text = str_replace(array("\xe2\x80\x98", "\xe2\x80\x99", "\xe2\x80\x9c", "\xe2\x80\x9d", "\xe2\x80\x93", "\xe2\x80\x94", "\xe2\x80\xa6"), array("", "", '', '', '', '',''), $string);
		$text = str_replace(array(chr(145), chr(146), chr(147), chr(148), chr(150), chr(151), chr(133)), array("", "", '', '', '', '', ''), $text);
		$string=str_replace(array('"',"'"),array("&quot;","&#039;"),$text);
		return $string;
	}
	public function ftp($ftp_server,$ftp_user_name,$ftp_user_pass,$file,$remote_file) {
		 // set up basic connection
		 $conn_id = ftp_connect($ftp_server);

		 // login with username and password
		 $login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);

		 // upload a file
		 if (ftp_put($conn_id, $remote_file, $file, FTP_BINARY)) {
			return "success";
		 } else {
			return "error";
			}
		 // close the connection
		 ftp_close($conn_id);
	}
}
