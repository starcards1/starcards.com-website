<?php
	require_once "../class/utils.class.php";
	$c=new utils;
	$c->connect("199.91.65.83","voxeo");
	parse_str(http_build_query($_GET));
	$arr1=array("/","+","%","==");
	$arr2=array("1001","2002","3003","");
	$conference_id=str_replace($arr2,$arr1,($conference_id));	
	$conference_id=$c->decrypt($conference_id,"");
	$room=$conference_id;
	$room="abc";
	$stream='stream' . rand(11111111,99999999);
?>
<!doctype html>
<head>
    <title>Conference</title>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta content="width=device-width, initial-scale=1, user-scalable=no" name="viewport">
    <script src="//webrtchacks.github.io/adapter/adapter-latest.js"></script>
    <script src="https://linqstar.com/webrtcexamples/lib/screenfull/screenfull.min.js"></script>
    <script src="https://linqstar.com/webrtcexamples/script/testbed-config.js"></script>
    <script src="https://linqstar.com/webrtcexamples/script/red5pro-utils.js"></script>
    <script src="https://linqstar.com/webrtcexamples/script/reachability.js"></script>
    <link rel="stylesheet" href="https://linqstar.com/webrtcexamples/css/reset.css">
    <link rel="stylesheet" href="https://linqstar.com/webrtcexamples/css/testbed.css">
    <link rel="stylesheet" href="https://linqstar.com/webrtcexamples/lib/red5pro/red5pro-media.css">
    <link rel='stylesheet' href='//fonts.googleapis.com/css?family=Lato%3A100%2C200%2C300%2C400%2C500%2C600%2C700%2C800%2C900%2C100i%2C200i%2C300i%2C400i%2C500i%2C600i%2C700i%2C800i%2C900i&#038;subset=latin&#038;ver=5.2.3' 
type='text/css' media='all' />
    
    <link rel="stylesheet" href="https://linqstar.com/webrtcexamples/css/conference.css">
    <script src="https://linqstar.com/webrtcexamples/script/subscription-status.js"></script>
  </head>
	  <body  style="background:url(https://linqstar.com/assets/images/account-bg.jpg)no-repeat;background-size:cover;text-align:center">
	    <img id="logo" style="width:200px;margin:auto;left:0;right:0;position:" src="https://linqstar.com/assets/images/logo2.png" alt="logo">
        <br><br>
		<style type="text/css">
		/* Top Bar CSS */
		#top-bar {
			padding: 16px 0 16px 0;
		}
		#top-bar-wrap {
			background-color: #3b3b3b;
		}
		#top-bar-wrap {
			border-color: #999999;
		}
		#top-bar-wrap, #top-bar-content strong {
			color: #dbdbdb;
		}
		#top-bar-content a, #top-bar-social-alt a {
			color: #dbdbdb;
		}
		#top-bar-content a:hover, #top-bar-social-alt a:hover {
			color: #db1f26;
		}
		.container {
			width: 1200px;
			max-width: 90%;
			margin: 0 auto;
		}
		@media only screen and (min-width: 1024px) {
		  #top-bar-content {
			float: none;
			text-align: right;
		  }
		}
		@media only screen and (max-width: 1023px) {
		  #top-bar-content {
			float: none;
			text-align: center;
		  }
		}
		#menu-top-menu {
		  letter-spacing: .6px;
		  box-sizing: border-box;
		  border: 0;
		  outline: 0;
		  vertical-align: baseline;
		  font-size: 100%;
		  margin: 0;
		  padding: 0;
		  list-style: none;
		  touch-action: pan-y;
		}
		
		.menu-item {
		  color: #dbdbdb;
		  letter-spacing: .6px;
		  box-sizing: border-box;
		  border: 0;
		  outline: 0;
		  vertical-align: baseline;
		  font-size: 100%;
		  margin: 0;
		  padding: 0;
		  list-style: none;
		  position: relative;
		  white-space: normal;
		  display: inline-block;
		  float: none;
		  margin-right: 15px;
		}
		
		/* Typography CSS */
		#top-bar-content, #top-bar-social-alt {
			font-size: 13px;
			letter-spacing: .6px;
		}
	.button-5 {
    border: none;
    -webkit-border-radius: 10px;
    -moz-border-radius: 10px;
    border-radius: 10px;
    background: -moz-linear-gradient(0deg, #e2906e 0%, #e83a99 100%);
    background: -webkit-linear-gradient(
0deg
, #e2906e 0%, #e83a99 100%);
    background: -ms-linear-gradient(0deg, #e2906e 0%, #e83a99 100%);
    box-shadow: 2.419px 9.703px 12.48px 0.52px rgb(232 58 153 / 50%);
    width: auto;
    padding: 10px 45px;
    color: #ffffff;
    margin-top: 20px;}	
		</style>
		<script src="https://linqstar.com/webrtcexamples/lib/smartmenus-1.1.0/libs/jquery/jquery.js"></script>
		<div id="app">
		  <div id="back-link-container" style="text-align: left; display:none">
			<p class="back-link"><a href="https://linqstar.com/webrtcexamples/index.html">Settings</a></p>
			<p class="version-field">Testbed Version: 8.0.0</p>
		  </div>
		  
		  <div class="conference-section">
			<div id="publisher-container" class="publisher-section publisher-container">
			  <div class="settings-area" style="display:none">
				<p>
				  <label for="room-field" class="settings-label">Room:</label>
				  <input id="room-field" name="room-field" class="settings-input">
				</p>
			  </div>
			  <div id="publisher-session" class="publisher-session hidden">
				<p id="status-field" style="background:url(https://linqstar.com/assets/icon_bg.png);background-size:cover;font-size:0.8em;color:white;opacity:1!Important" class="centered status-field">On hold.</p>
				<script src="https://linqstar.com/webrtcexamples/script/publisher-status.js"></script>
			  <input type="hidden" id="statistics-field" style="opacity:0" class="centered status-field statistics-field hidden"/>
				<!-- only updated from WebRTC clients. -->
				<span style="display:none">
				Bitrate: <span id="bitrate-field" class="bitrate-field">N/A</span>.&nbsp;
				Packets Sent: <span id="packets-field" class="packets-field">N/A</span>.
				<br/>
				<span>Resolution: <span id="resolution-field" class="resolution-field">0x0</span>.
			  </span>
			  </span>
			  </div>
			  <div class="centered video-holder" style="" >
				  <video id="red5pro-publisher" class="red5pro-publisher" playsinline autoplay  muted width="320" height="240"></video>
			  </div>
			  <div id="publisher-settings" class="publisher-settings hidden">
				<p class="remove-on-broadcast">
				  <label class="device-label" for="streamname-field">Stream Name:</label>
				  <input id="streamname-field" name="streamname-field" class="settings-input">
				</p>
				<hr class="paddedHR" />
				<p>
				  <label class="device-label" for="camera-select">Camera:</label>
				  <select class="control device-control settings-input" name="camera-select" id="camera-select"></select>
				</p>
				<p>
				  <label class="device-label" for="microphone-select">Microphone:</label>
				  <select class="control device-control settings-input" name="microphone-select" id="microphone-select"></select>
				</p>
				<p id="publisher-mute-notice" class="publisher-mute-notice remove-on-broadcast">
				  <span class="device-info">Video and Audio can be turned off after publish has started.</span>
				</p>
				<p id="publisher-mute-controls" class="publisher-mute-controls hidden">
				  <label for="video-check">Video: </label>
				  <input type="checkbox" id="video-check" name="video-check" checked>
				  <label for="audio-check">Audio: </label>
				  <input type="checkbox" id="audio-check" name="audio-check" checked>
				</p>
				<p id="publisher-name-field" class="publisher-name-field hidden"></p>
			  </div>
			</div>
			<div id="subscribers" class="subscribers"></div>
		  </div>
		  <div class="centered status-field hidden">
			<p>
			  <input type="text" id="input-field" />
			  <button id="send-button">send</button>
			</p>
			<hr>
			<p>
			  <textarea id="so-field" disabled style="width: 50%; max-height: 120px; max-width: 160px"></textarea>
			</p>
		  </div>
		</div>
		<div class="remove-on-broadcast">
		  <button id="join-button" class="button-5">Join!</button>
		</div>
		<script src="https://linqstar.com/webrtcexamples/lib/es6/es6-promise.min.js"></script>
		<script src="https://linqstar.com/webrtcexamples/lib/es6/es6-bind.js"></script>
		<script src="https://linqstar.com/webrtcexamples/lib/es6/es6-array.js"></script>
		<script src="https://linqstar.com/webrtcexamples/lib/es6/es6-object-assign.js"></script>
		<script src="https://linqstar.com/webrtcexamples/lib/es6/es6-fetch.js"></script>
		<script src="https://linqstar.com/webrtcexamples/lib/red5pro/red5pro-sdk.min.js"></script>
		<script>
			  (function(window) {
				var configuration = (function () {
				  var conf = sessionStorage.getItem('r5proTestBed');
				  try {
					return JSON.parse(conf);
				  }
				  catch (e) {
					console.error('Could not read testbed configuration from sessionstorage: ' + e.message);
				  }
				  return {}
				})();
			
				if (configuration.verboseLogging) {
				  window.publisherLog = function (message) {
					console.log('[Red5ProRTMPPublisher:SWF] - ' + message);
				  };
				  window.subscriberLog = function (message) {
					console.log('[Red5ProRTMPSubscriber:SWF] - ' + message);
				  };
				}
			
				if (configuration.authentication.enabled) {
				  var node = document.createElement('div');
				  node.classList.add('hint-block', 'auth-alert');
				  var note = document.createElement('span');
				  note.classList.add('strong');
				  note.innerHTML = '*Authentication is Enabled*';
				  var link = document.createElement('a');
				  link.innerText = 'Click here to disable.';
				  link.href= '../../index.html';
				  link.classList.add('auth-link');
				  node.appendChild(note);
				  node.appendChild(link);
				  var testBody = document.getElementById('back-link-container').nextElementSibling;
				  testBody.parentNode.insertBefore(node, testBody);
				}
			
			  })(this);
			</script>
			<script src="https://linqstar.com/webrtcexamples/test/conference/conference-subscriber.js"></script>
			<script src="https://linqstar.com/webrtcexamples/test/conference/device-selector-util.js"></script>
			<script>
// Defining/accessing testbed configuration.
(function (window, adapter) {

  if (typeof adapter !== 'undefined') {
    console.log('Browser: ' + JSON.stringify(adapter.browserDetails, null, 2));
  }

  // http://stackoverflow.com/questions/901115/how-can-i-get-query-string-values-in-javascript
  function getParameterByName(name, url) { // eslint-disable-line no-unused-vars
    if (!url) {
      url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
  }

  var build_version = '8.0.0';
  var protocol = window.location.protocol;
  var port = window.location.port;
  protocol = protocol.substring(0, protocol.lastIndexOf(':'));

  var isMoz = !!navigator.mozGetUserMedia;
  var isEdge = window.navigator.userAgent.indexOf('Edge') > -1;
  var isiPod = !!navigator.platform && /iPod/.test(navigator.platform);
  var config = sessionStorage.getItem('r5proTestBed');
  var json;
  var serverSettings = {
    "protocol": protocol,
    "httpport": port,
    "hlsport": 5080,
    "hlssport": 443,
    "wsport": 5080,
    "wssport": 443,
    "rtmpport": 1935,
    "rtmpsport": 1936
  };
   // var iceServers = [	{		urls: "stun:stun.linqstar.com:3478",
							// username: "linqstar",
						   // credential: "linqstar!"
						// },
						// {
								// urls: "turn:turn.linqstar.com:3478",
							// username: "linqstar",
						  // credential: "linqstar!"
						// },
						// {
								// urls: 'stun:stun2.l.google.com:19302'
						// }
					// ]


  var iceServers = [
						{
								urls: 'stun:stun2.l.google.com:19302'
						}
					]


  function assignStorage () {
    json = {
      "version": build_version,
      "host": 'mediaserver.linqstar.com',
      "port": 443,
      "stream1": "stream1",
      "stream2": "stream2",
      "app": "live",
      "proxy": "streammanager",
      "streamMode": "live",
      "cameraWidth": 160,
      "cameraHeight": 120,
      "embedWidth": "100%",
      "embedHeight": 120,
      "buffer": 0.5,
      "bandwidth": {
        "audio": 56,
        "video": 750
      },
      "signalingSocketOnly": true,
      "keyFramerate": 3000,
      "useAudio": true,
      "useVideo": true,
      "mediaConstraints": {
        "audio": isiPod ? false : true,
        "video": (isMoz || isEdge) ? true : {
          "width": {
            "min": 160,
            "max": 160
          },
          "height": {
            "min": 120,
            "max": 120
          },
          "frameRate": {
            "min": 8,
            "max": 15
          }
        }
      },
      "publisherFailoverOrder": "rtc,rtmp",
      "subscriberFailoverOrder": "rtc,rtmp,hls",
      "rtcConfiguration": {
        "iceServers": iceServers,
        "bundlePolicy": "max-bundle",
        "iceCandidatePoolSize": 2,
        "iceTransportPolicy": "all",
        "rtcpMuxPolicy": "require"
      },
      "googleIce": [
        {
          "urls": "stun:stun2.l.google.com:19302"
        }
      ],
      "mozIce": [
        {
          "urls": "stun:stun.services.mozilla.com:3478"
        }
      ],
      "iceTransport": "udp",
      "verboseLogging": true,
      "recordBroadcast": false,
      "streamManagerAPI": "4.0",
      "streamManagerAccessToken": "xyz123",
      "streamManagerRegion": undefined,
      "muteOnAutoplayRestriction": true,
      "authentication": {
        "enabled": false,
        "username": "user",
        "password": "pass"
      }
    };

    /**
    if (isMoz) {
      json.rtcConfiguration.iceServers = json.mozIce;
    }
    */
    sessionStorage.setItem('r5proTestBed', JSON.stringify(json));
  }



  if (config) {
    try {
      json = JSON.parse(config);
      if (json.version && json.version !== build_version) {
        console.log('We have replaced your stale session version: ' + json.version + ' with ' + build_version + '. Have fun streaming!');
        sessionStorage.removeItem('r5proTestBed');
        assignStorage();
      } else if (!json.version) {
        console.log('We recently added session swaps with the latest version: ' + build_version + '. Have fun streaming!');
        sessionStorage.removeItem('r5proTestBed');
        assignStorage();
      }
    }
    catch (e) {
      assignStorage();
    }
    finally {
      sessionStorage.setItem('r5proTestBed', JSON.stringify(json));
    }
  }
  else {
    assignStorage();
    sessionStorage.setItem('r5proTestBed', JSON.stringify(json));
  }
  sessionStorage.setItem('r5proServerSettings', JSON.stringify(serverSettings));
  return json;
})(this, window.adapter);

			(function(window, document, red5prosdk) {
			  'use strict';

			  var SharedObject = red5prosdk.Red5ProSharedObject;
			  var so = undefined; // @see onPublishSuccess
			  var isPublishing = false;

			  var serverSettings = (function() {
				var settings = sessionStorage.getItem('r5proServerSettings');
				try {
				  return JSON.parse(settings);
				}
				catch (e) {
				  console.error('Could not read server settings from sessionstorage: ' + e.message);
				}
				return {};
			  })();

			  var configuration = (function () {
				var conf = sessionStorage.getItem('r5proTestBed');
				try {
				  return JSON.parse(conf);
				}
				catch (e) {
				  console.error('Could not read testbed configuration from sessionstorage: ' + e.message);
				}
				return {}
			  })();
			  red5prosdk.setLogLevel(configuration.verboseLogging ? red5prosdk.LOG_LEVELS.TRACE : red5prosdk.LOG_LEVELS.WARN);

			  var updateStatusFromEvent = window.red5proHandlePublisherEvent; // defined in src/template/partial/status-field-publisher.hbs

			  var targetPublisher;
			  var roomName = window.query('room') || 'red5pro'; // eslint-disable-line no-unused-vars
			  var streamName = window.query('streamName') || ['publisher', Math.floor(Math.random() * 0x10000).toString(16)].join('-');

			  var roomField = document.getElementById('room-field');
			  roomField.value='<?=$room;?>'
			  // eslint-disable-next-line no-unused-vars
			  var publisherContainer = document.getElementById('publisher-container');
			  var publisherMuteControls = document.getElementById('publisher-mute-controls');
			  var publisherSession = document.getElementById('publisher-session');
			  var publisherNameField = document.getElementById('publisher-name-field');
			  var streamNameField = document.getElementById('streamname-field');
			  var publisherVideo = document.getElementById('red5pro-publisher');
			  var audioCheck = document.getElementById('audio-check');
			  var videoCheck = document.getElementById('video-check');
			  var joinButton = document.getElementById('join-button');
			  var statisticsField = document.getElementById('statistics-field');
			  var bitrateField = document.getElementById('bitrate-field');
			  var packetsField = document.getElementById('packets-field');
			  var resolutionField = document.getElementById('resolution-field');
			  var bitrateTrackingTicket;
			  var bitrate = 0;
			  var packetsSent = 0;
			  var frameWidth = 0;
			  var frameHeight = 0;

			  function updateStatistics (b, p, w, h) {
				statisticsField.classList.remove('hidden');
				bitrateField.innerText = b === 0 ? 'N/A' : Math.floor(b);
				packetsField.innerText = p;
				resolutionField.innerText = (w || 0) + 'x' + (h || 0);
			  }

			  function onBitrateUpdate (b, p) {
				bitrate = b;
				packetsSent = p;
				updateStatistics(bitrate, packetsSent, frameWidth, frameHeight);
				if (packetsSent > 100) {
				  establishSharedObject(targetPublisher, roomField.value, streamNameField.value);
				}
			  }

			  function onResolutionUpdate (w, h) {
				frameWidth = w;
				frameHeight = h;
				updateStatistics(bitrate, packetsSent, frameWidth, frameHeight);
			  }

			  roomField.value = '<?=$room;?>';
			  streamNameField.value = '<?=$stream;?>';
			  audioCheck.checked = configuration.useAudio;
			  videoCheck.checked = configuration.useVideo;

			  joinButton.addEventListener('click', function () {
				saveSettings();
				doPublish(streamName);
				setPublishingUI(streamName);
			  });

			  audioCheck.addEventListener('change', updateMutedAudioOnPublisher);
			  videoCheck.addEventListener('change', updateMutedVideoOnPublisher);

			  var soField = document.getElementById('so-field');

			  var protocol = serverSettings.protocol;
			  var isSecure = protocol == 'https';

			  function saveSettings () {
				streamName = streamNameField.value;
				roomName = roomField.value;
			  }

			  function updateMutedAudioOnPublisher () {
				if (targetPublisher && isPublishing) {
				  var c = targetPublisher.getPeerConnection();
				  var senders = c.getSenders();
				  var params = senders[0].getParameters();
				  if (audioCheck.checked) { 
					if (audioTrackClone) {
					  senders[0].replaceTrack(audioTrackClone);
					  audioTrackClone = undefined;
					} else {
					  try {
						targetPublisher.unmuteAudio();
						params.encodings[0].active = true;
						senders[0].setParameters(params);
					  } catch (e) {
						// no browser support, let's use mute API.
						targetPublisher.unmuteAudio();
					  }
					}
				  } else { 
					try {
					  targetPublisher.muteAudio();
					  params.encodings[0].active = false;
					  senders[0].setParameters(params);
					} catch (e) {
					  // no browser support, let's use mute API.
					  targetPublisher.muteAudio();
					}
				  }
				}
			  }

			  function updateMutedVideoOnPublisher () {
				if (targetPublisher && isPublishing) {
				  if (videoCheck.checked) {
					if (videoTrackClone) {
					  var c = targetPublisher.getPeerConnection();
					  var senders = c.getSenders();
					  senders[1].replaceTrack(videoTrackClone);
					  videoTrackClone = undefined;
					} else {
					  targetPublisher.unmuteVideo();
					}
				  } else { 
					targetPublisher.muteVideo(); 
				  }
				}
				!videoCheck.checked && showVideoPoster();
				videoCheck.checked && hideVideoPoster();
			  }

			  var audioTrackClone;
			  var videoTrackClone;
			  function updateInitialMediaOnPublisher () {
				var t = setTimeout(function () {
				  // If we have requested no audio and/or no video in our initial broadcast,
				  // wipe the track from the connection.
				  var audioTrack = targetPublisher.getMediaStream().getAudioTracks()[0];
				  var videoTrack = targetPublisher.getMediaStream().getVideoTracks()[0];
				  var connection = targetPublisher.getPeerConnection();
				  if (!videoCheck.checked) {
					videoTrackClone = videoTrack.clone();
					connection.getSenders()[1].replaceTrack(null);
				  }
				  if (!audioCheck.checked) {
					audioTrackClone = audioTrack.clone();
					connection.getSenders()[0].replaceTrack(null);
				  }
				  clearTimeout(t);
				}, 2000); 
				// a bit of a hack. had to put a timeout to ensure the video track bits at least started flowing :/
			  }

			  function showVideoPoster () {
				publisherVideo.classList.add('hidden');
			  }

			  function hideVideoPoster () {
				publisherVideo.classList.remove('hidden');
			  }

			  function getSocketLocationFromProtocol () {
				return !isSecure
				  ? {protocol: 'ws', port: serverSettings.wsport}
				  : {protocol: 'wss', port: serverSettings.wssport};
			  }

			  function onPublisherEvent (event) {
				console.log('[Red5ProPublisher] ' + event.type + '.');
				if (event.type === 'WebSocket.Message.Unhandled') {
				  console.log(event);
				} else if (event.type === red5prosdk.RTCPublisherEventTypes.MEDIA_STREAM_AVAILABLE) {
				  window.allowMediaStreamSwap(targetPublisher, targetPublisher.getOptions().mediaConstraints, document.getElementById('red5pro-publisher'));
				}
				updateStatusFromEvent(event);
			  }
			  function onPublishFail (message) {
				isPublishing = false;
				console.error('[Red5ProPublisher] Publish Error :: ' + message);
			  }
			  function onPublishSuccess (publisher) {
				isPublishing = true;
				window.red5propublisher = publisher;
				console.log('[Red5ProPublisher] Publish Complete.');
				// [NOTE] Moving SO setup until Package Sent amount is sufficient.
				//    establishSharedObject(publisher, roomField.value, streamNameField.value);
				if (publisher.getType().toUpperCase() !== 'RTC') {
				  // It's flash, let it go.
				  establishSharedObject(publisher, roomField.value, streamNameField.value);
				}
				try {
				  var pc = publisher.getPeerConnection();
				  var stream = publisher.getMediaStream();
				  bitrateTrackingTicket = window.trackBitrate(pc, onBitrateUpdate, null, null, true);
				  statisticsField.classList.remove('hidden');
				  stream.getVideoTracks().forEach(function (track) {
					var settings = track.getSettings();
					onResolutionUpdate(settings.width, settings.height);
				  });
				}
				catch (e) {
				  // no tracking for you!
				}
			  }
			  function onUnpublishFail (message) {
				isPublishing = false;
				console.error('[Red5ProPublisher] Unpublish Error :: ' + message);
			  }
			  function onUnpublishSuccess () {
				isPublishing = false;
				console.log('[Red5ProPublisher] Unpublish Complete.');
			  }

			  function getAuthenticationParams () {
				var auth = configuration.authentication;
				return auth && auth.enabled
				  ? {
					connectionParams: {
					  username: auth.username,
					  password: auth.password
					}
				  }
				  : {};
			  }

			  function getUserMediaConfiguration () {
				return {
				  mediaConstraints: {
					audio: configuration.useAudio ? configuration.mediaConstraints.audio : false,
					video: configuration.useVideo ? configuration.mediaConstraints.video : false
				  }
				};
			  }

			  function setPublishingUI (streamName) {
				publisherNameField.innerText = streamName;
				roomField.setAttribute('disabled', true);
				publisherSession.classList.remove('hidden');
				publisherNameField.classList.remove('hidden');
				publisherMuteControls.classList.remove('hidden');
				Array.prototype.forEach.call(document.getElementsByClassName('remove-on-broadcast'), function (el) {
				  el.classList.add('hidden');
				});
			  }

			  // eslint-disable-next-line no-unused-vars
			  function updatePublishingUIOnStreamCount (streamCount) {
				/*
				if (streamCount > 0) {
				  publisherContainer.classList.remove('margin-center');
				} else {
				  publisherContainer.classList.add('margin-center');
				}
				*/
			  }

			  var hasRegistered = false;
			  function appendMessage (message) {
				soField.value = [message, soField.value].join('\n');
			  }
			  // Invoked from METHOD_UPDATE event on Shared Object instance.
			  function messageTransmit (message) { // eslint-disable-line no-unused-vars
				soField.value = ['User "' + message.user + '": ' + message.message, soField.value].join('\n');
			  }
			  function establishSharedObject (publisher, roomName, streamName) {
				if (so) {
				  return;
				}
				// Create new shared object.
				so = new SharedObject(roomName, publisher)
				var soCallback = {
				  messageTransmit: messageTransmit
				};
				so.on(red5prosdk.SharedObjectEventTypes.CONNECT_SUCCESS, function (event) { // eslint-disable-line no-unused-vars
				  console.log('[Red5ProPublisher] SharedObject Connect.');
				  appendMessage('Connected.');
				});
				so.on(red5prosdk.SharedObjectEventTypes.CONNECT_FAILURE, function (event) { // eslint-disable-line no-unused-vars
				  console.log('[Red5ProPublisher] SharedObject Fail.');
				});
				so.on(red5prosdk.SharedObjectEventTypes.PROPERTY_UPDATE, function (event) {
				  console.log('[Red5ProPublisher] SharedObject Property Update.');
				  console.log(JSON.stringify(event.data, null, 2));
				  if (event.data.hasOwnProperty('streams')) {
					appendMessage('Stream list is: ' + event.data.streams + '.');
					var streams = event.data.streams.length > 0 ? event.data.streams.split(',') : [];
					if (!hasRegistered) {
					  hasRegistered = true;
					  so.setProperty('streams', streams.concat([streamName]).join(','));
					}
					streamsPropertyList = streams;
					processStreams(streamsPropertyList, streamName);
				  }
				  else if (!hasRegistered) {
					hasRegistered = true;
					streamsPropertyList = [streamName];
					so.setProperty('streams', streamName);
				  }
				});
				so.on(red5prosdk.SharedObjectEventTypes.METHOD_UPDATE, function (event) {
				  console.log('[Red5ProPublisher] SharedObject Method Update.');
				  console.log(JSON.stringify(event.data, null, 2));
				  soCallback[event.data.methodName].call(null, event.data.message);
				});
			  }

			  function determinePublisher () {

				var config = Object.assign({},
								  configuration,
								  {
									streamMode: configuration.recordBroadcast ? 'record' : 'live'
								  },
								  getAuthenticationParams(),
								  getUserMediaConfiguration());
				var rtcConfig = Object.assign({}, config, {
								  protocol: getSocketLocationFromProtocol().protocol,
								  port: getSocketLocationFromProtocol().port,
								  bandwidth: {
									video: 256
								  },
								  mediaConstraints: {
									audio: true,
									video: {
									  width: {
										exact: 160
									  },
									  height: {
										exact: 120
									  },
									  frameRate: {
										exact: 15
									  }
									}
								  },
								  streamName: streamName
							   });

				var publisher = new red5prosdk.RTCPublisher();
				return publisher.init(rtcConfig);

			  }

			  function doPublish (name) {
				targetPublisher.publish(name)
				  .then(function () {
					onPublishSuccess(targetPublisher);
					updateInitialMediaOnPublisher();
					//publisherVideo.play()
				  })
				  .catch(function (error) {
					var jsonError = typeof error === 'string' ? error : JSON.stringify(error, null, 2);
					console.error('[Red5ProPublisher] :: Error in publishing - ' + jsonError);
					console.error(error);
					onPublishFail(jsonError);
				   });
			  }

			  function unpublish () {
				if (so !== undefined)  {
				  var name = streamName;
				  var updateList = streamsPropertyList.filter(function (item) {
					return item !== name;
				  });
				  streamsPropertyList = updateList;
				  so.setProperty('streams', updateList.join(','));
				  so.close();
				}
				return new Promise(function (resolve, reject) {
				  var publisher = targetPublisher;
				  publisher.unpublish()
					.then(function () {
					  onUnpublishSuccess();
					  resolve();
					})
					.catch(function (error) {
					  var jsonError = typeof error === 'string' ? error : JSON.stringify(error, 2, null);
					  onUnpublishFail('Unmount Error ' + jsonError);
					  reject(error);
					});
				});
			  }

			  // Kick off.
			  determinePublisher()
				.then(function (publisherImpl) {
				  targetPublisher = publisherImpl;
				  targetPublisher.on('*', onPublisherEvent);
				  return targetPublisher.preview();
				})
				.catch(function (error) {
				  var jsonError = typeof error === 'string' ? error : JSON.stringify(error, null, 2);
				  console.error('[Red5ProPublisher] :: Error in publishing - ' + jsonError);
				  console.error(error);
				  onPublishFail(jsonError);
				 });

			  var shuttingDown = false;
			  function shutdown () {
				if (shuttingDown) return;
				shuttingDown = true;
				function clearRefs () {
				  if (targetPublisher) {
					targetPublisher.off('*', onPublisherEvent);
				  }
				  targetPublisher = undefined;
				}
				unpublish().then(clearRefs).catch(clearRefs);
				window.untrackBitrate(bitrateTrackingTicket);
			  }
			  window.addEventListener('beforeunload', shutdown);
			  window.addEventListener('pagehide', shutdown);

			  var streamsPropertyList = [];
			  var subscribersEl = document.getElementById('subscribers');
			  function processStreams (streamlist, exclusion) {
				var nonPublishers = streamlist.filter(function (name) {
				  return name !== exclusion;
				});
				var list = nonPublishers.filter(function (name, index, self) {
				  return (index == self.indexOf(name)) &&
					!document.getElementById(window.getConferenceSubscriberElementId(name));
				});
				var subscribers = list.map(function (name, index) {
				  return new window.ConferenceSubscriberItem(name, subscribersEl, index);
				});
				var i, length = subscribers.length - 1;
				var sub;
				for(i = 0; i < length; i++) {
				  sub = subscribers[i]; 
				  sub.next = subscribers[sub.index+1];
				}
				if (subscribers.length > 0) {
				  var baseSubscriberConfig = Object.assign({},
											  configuration,
											  {
												protocol: getSocketLocationFromProtocol().protocol,
												port: getSocketLocationFromProtocol().port
											  },
											  getAuthenticationParams(),
											  getUserMediaConfiguration());
				  subscribers[0].execute(baseSubscriberConfig);
				}

				updatePublishingUIOnStreamCount(nonPublishers.length);
			  }

			})(this, document, window.red5prosdk);
	</script>
	</body>
	<? include "x_track_streams.php"; ?>
	<script src="js/socket.io.js"></script>
	<script src="js/jsSocketNode.js"></script>
</html>