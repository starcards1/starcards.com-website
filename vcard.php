<?php

include_once 'File/IMC.php';
include_once '../class/utils.class.php';
$c=new utils;
if (!class_exists('File_IMC')) {
    die('SKIP File_IMC is not installed, or your include_path is borked.');
}


// create vCard parser
$parse = File_IMC::parse('vCard');

// parse a vCard file and store the data in $cardinfo
$cardinfo = $parse->fromFile(dirname(__FILE__) . '/all.vcf');

// view the card info array
$c->show(count($cardinfo['VCARD']));
$c->show($cardinfo);