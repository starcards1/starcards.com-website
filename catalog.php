<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>LinqStar Card Asset catalog</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="css/filetree.css" type="text/css" >
		<style>
		.catalog {
			width:100%;
			height:100%;
			background: white
		}
		.bt {
			color:white;
			background: #00d9a3;
			font-size: 12px;
			padding:5px 10px;
			margin-top: -7px;
			border-radius:4px;
			font-weight:bold;
		}
		.pc {
			color:#333;
			background:  #f0f0f0;
			font-size: 12px;
			padding:7px 10px;
			margin-top: -5px;
			border-radius:4px;
			
		}
		.pc2 {
			color:white;
			background:  #00d9a3;
			font-size: 12px;
			padding:7px 10px;
			margin-top: -5px;
			border-radius:4px;
			font-weight:bold;
		}
		</style>
		<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
	
</head>

<body id="page-top">
    <div id="wrapper">
        <div id="content-wrapper" class="d-flex flex-column">
   
<div id="container"> </div>
<div id="selected_file"></div>
<div id="cat" class="row" style="text-align:center">

</div>
</div>
</div>
</div>


<script type="text/javascript" >
$(document).ready( function() {

	$( '#container' ).html( '<ul class="filetree start"><li class="wait">' + 'Generating Tree...' + '<li></ul>' );
	getfilelist( $('#container') , 'vc3/1. Stickers' );
	
	var data=[ ]
	function getfilelist( cont, root ) {
	
		$( cont ).addClass( 'wait' );
			
		$.post( 'foldertree.php', { dir: root }, function( data ) {
			console.log(data)
			if (data.indexOf('<div')>=0) {
				$('#container').hide()
				var dd=document.createElement('div')
				dd.innerHTML=data
				$('#cat').append(dd)
			} else {
				$( cont ).find( '.start' ).html( '' );
				$( cont ).removeClass( 'wait' ).append( data );
				if( 'Sample' == root ) 
					$( cont ).find('UL:hidden').show();
				else 
					$( cont ).find('UL:hidden').slideDown({ duration: 500, easing: null });
			}
			
		});
	}
	
	$( '#container' ).on('click', 'LI A', function(e) {
		var entry = $(this).parent();
		e.preventDefault();
		if( entry.hasClass('folder') ) {
			if( entry.hasClass('collapsed') ) {
				entry.find('UL').remove();
				getfilelist( entry, escape( $(this).attr('rel') ));
				entry.removeClass('collapsed').addClass('expanded');
			}
			else {
				
				entry.find('UL').slideUp({ duration: 500, easing: null });
				entry.removeClass('expanded').addClass('collapsed');
			}
		} else {
			fs(e.target)
		}
	return false;
	});
	
});

function fs(obj) {
	console.log(obj.rel)
	$.alert({
		title:'Preview Image',
		content: 'url:iView.php?image='+obj.rel,
		useBootstrap: false,
		columnClass: '320px',
	})

}
function saveAsset(io) {
	window.localStorage.setItem('selected_asset',io)
	window.parent.close_catalog()
}

function fs(obj) {
	console.log(obj.rel)
	$.alert({
		title:'Preview Image',
		content: 'url:iView.php?image='+obj.rel,
		useBootstrap: false,
		columnClass: '320px',
	})

}


</script>
    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>

</body>

</html>