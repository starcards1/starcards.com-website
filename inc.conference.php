<!doctype html>
<!-- Copyright � 2015 Infrared5, Inc. All rights reserved.

The accompanying code comprising examples for use solely in conjunction with Red5 Pro (the "Example Code") 
is  licensed  to  you  by  Infrared5  Inc.  in  consideration  of  your  agreement  to  the  following  
license terms  and  conditions.  Access,  use,  modification,  or  redistribution  of  the  accompanying  
code  constitutes your acceptance of the following license terms and conditions.

Permission is hereby granted, free of charge, to you to use the Example Code and associated documentation 
files (collectively, the "Software") without restriction, including without limitation the rights to use, 
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
persons to whom the Software is furnished to do so, subject to the following conditions:

The Software shall be used solely in conjunction with Red5 Pro. Red5 Pro is licensed under a separate end 
user  license  agreement  (the  "EULA"),  which  must  be  executed  with  Infrared5,  Inc.   
An  example  of  the EULA can be found on our website at: https://account.red5pro.com/assets/LICENSE.txt.

The above copyright notice and this license shall be included in all copies or portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,  INCLUDING  BUT  
NOT  LIMITED  TO  THE  WARRANTIES  OF  MERCHANTABILITY, FITNESS  FOR  A  PARTICULAR  PURPOSE  AND  
NONINFRINGEMENT.   IN  NO  EVENT  SHALL INFRARED5, INC. BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN  AN  ACTION  OF  CONTRACT,  TORT  OR  OTHERWISE,  ARISING  FROM,  OUT  OF  OR  IN CONNECTION 
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. -->
<html lang="eng">
  <head>
    <title>Conference</title>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta content="width=device-width, initial-scale=1, user-scalable=no" name="viewport">
    <script src="//webrtchacks.github.io/adapter/adapter-latest.js"></script>
    <script src="webrtcexamples/lib/screenfull/screenfull.min.js"></script>
    <script src="webrtcexamples/script/testbed-config.js"></script>
    <script src="webrtcexamples/script/red5pro-utils.js"></script>
    <script src="webrtcexamples/script/reachability.js"></script>
    <link rel="stylesheet" href="webrtcexamples/css/reset.css">
    <link rel="stylesheet" href="webrtcexamples/css/testbed.css">
    <link rel="stylesheet" href="webrtcexamples/lib/red5pro/red5pro-media.css">
    <link rel='stylesheet' href='//fonts.googleapis.com/css?family=Lato%3A100%2C200%2C300%2C400%2C500%2C600%2C700%2C800%2C900%2C100i%2C200i%2C300i%2C400i%2C500i%2C600i%2C700i%2C800i%2C900i&#038;subset=latin&#038;ver=5.2.3' type='text/css' media='all' />
    
    <link rel="stylesheet" href="webrtcexamples/css/conference.css">
    <script src="webrtcexamples/script/subscription-status.js"></script>
  </head>
  <body>
    <style type="text/css">
    /* Top Bar CSS */
    #top-bar {
        padding: 16px 0 16px 0;
    }
    #top-bar-wrap {
        background-color: #3b3b3b;
    }
    #top-bar-wrap {
        border-color: #999999;
    }
    #top-bar-wrap, #top-bar-content strong {
        color: #dbdbdb;
    }
    #top-bar-content a, #top-bar-social-alt a {
        color: #dbdbdb;
    }
    #top-bar-content a:hover, #top-bar-social-alt a:hover {
        color: #db1f26;
    }
    .container {
        width: 1200px;
        max-width: 90%;
        margin: 0 auto;
    }
    @media only screen and (min-width: 1024px) {
      #top-bar-content {
        float: none;
        text-align: right;
      }
    }
    @media only screen and (max-width: 1023px) {
      #top-bar-content {
        float: none;
        text-align: center;
      }
    }
    #menu-top-menu {
      letter-spacing: .6px;
      box-sizing: border-box;
      border: 0;
      outline: 0;
      vertical-align: baseline;
      font-size: 100%;
      margin: 0;
      padding: 0;
      list-style: none;
      touch-action: pan-y;
    }
    
    .menu-item {
      color: #dbdbdb;
      letter-spacing: .6px;
      box-sizing: border-box;
      border: 0;
      outline: 0;
      vertical-align: baseline;
      font-size: 100%;
      margin: 0;
      padding: 0;
      list-style: none;
      position: relative;
      white-space: normal;
      display: inline-block;
      float: none;
      margin-right: 15px;
    }
    
    /* Typography CSS */
    #top-bar-content, #top-bar-social-alt {
        font-size: 13px;
        letter-spacing: .6px;
    }
    </style>
    <script src="webrtcexamples/lib/smartmenus-1.1.0/libs/jquery/jquery.js"></script>
    <div id="app">
      <div id="back-link-container" style="text-align: left; display:none">
        <p class="back-link"><a href="webrtcexamples/index.html">&lt; Settings</a></p>
        <p class="version-field">Testbed Version: 8.0.0</p>
      </div>
      <h1 class="centered test-title red-text hidden">Conference</h1>
      <div class="conference-section">
        <div id="publisher-container" class="publisher-section publisher-container">
          <div class="settings-area" style="display:none">
            <p>
              <label for="room-field" class="settings-label">Room:</label>
              <input id="room-field" name="room-field" class="settings-input">
            </p>
          </div>
          <div id="publisher-session" class="publisher-session hidden">

          </div>

          <div id="publisher-settings" class="publisher-settings hidden">
            <p class="remove-on-broadcast">
              <label class="device-label" for="streamname-field">Stream Name:</label>
              <input id="streamname-field" name="streamname-field" class="settings-input">
            </p>
            <hr class="paddedHR" />
            <p>
              <label class="device-label" for="camera-select">Camera:</label>
              <select class="control device-control settings-input" name="camera-select" id="camera-select"></select>
            </p>
            <p>
              <label class="device-label" for="microphone-select">Microphone:</label>
              <select class="control device-control settings-input" name="microphone-select" id="microphone-select"></select>
            </p>
            <p id="publisher-mute-notice" class="publisher-mute-notice remove-on-broadcast">
              <span class="device-info">Video and Audio can be turned off after publish has started.</span>
            </p>
            <p id="publisher-mute-controls" class="publisher-mute-controls hidden">
              <label for="video-check">Video: </label>
              <input type="checkbox" id="video-check" name="video-check" checked>
              <label for="audio-check">Audio: </label>
              <input type="checkbox" id="audio-check" name="audio-check" checked>
            </p>
            <p id="publisher-name-field" class="publisher-name-field hidden"></p>

          </div>
        </div>
        <div id="subscribers" class="subscribers"></div>
      </div>
      <div class="centered status-field hidden">
        <p>
          <input type="text" id="input-field" />
          <button id="send-button">send</button>
        </p>
        <hr>
        <p>
          <textarea id="so-field" disabled style="width: 50%; min-height: 240px; min-width: 320px"></textarea>
        </p>
      </div>
    </div>

 
  </body>
</html>
