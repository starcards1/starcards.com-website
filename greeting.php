<?php
	require_once "../class/utils.class.php";
	$c=new utils;
	$c->connect("199.91.65.83","voxeo");
	$r=$c->query("select * from greeting_cards where card_id=" . $_GET['card_id'])[0];
	$cover_id=$r['cover_id'];
	$asset=$r['asset'];
	$msg_canned=$r['msg_canned'];
	$msg_personal=$r['msg_personal'];
	$card_cover=$r['card_cover'];
	$to=$r['sender_mobile'];
	$recepient_name=$r['recepient_name'];
	$to_login=strtolower(explode('@',$r['recepient_name'])[0]);
	$from_login=strtolower(explode('@',$r['sender_name'])[0]) . rand(11,99);
	$from_login=str_replace(" ","",$from_login);
	$msg1="$recepient_name has opened your card. Click link below to get ready for video call";
	$msg2="https://linqstar.com/pcall.php?roomid=$to&login=$from_login&init=1";
	$msg3="https://linqstar.com/pcall.php?roomid=$to&login=$to_login";
	$c->sms($to,$msg1);
	$c->sms($to,$msg2);

?>
<!DOCTYPE html>
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="doc/css/custom.css" rel="stylesheet">
		<link rel="stylesheet" href="assets/css/bootstrap.min.css"/>
		<link rel="stylesheet" href="assets/magic.css"/>
		<link rel="stylesheet" href="assets/css/jquery-ui.min.css"/>
		<link rel="stylesheet" href="assets/css/main.css"/>
		<link rel="stylesheet" href="css/shadows.css"/>
		<link rel="stylesheet" href="dz/dropzone.css"/>
		<link href="style.css" rel="stylesheet" type="text/css" />
		<script src="js/jquery.js"></script>
		<script>
			window.localStorage.setItem('call_link','<?=$msg2;?>')
		</script>
		<link href='https://fonts.googleapis.com/css?family=Open+Sans:300|Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/css2?family=Allura&family=Gloria+Hallelujah&family=Homemade+Apple&display=swap" rel="stylesheet">	

	<style>
		body {
		  font-family: Arial, Helvetica, sans-serif;
		}

		.flip-card {
		  background-color: transparent;
		  width: 300px;
		  height: 400px;
		  perspective: 1000px;
		}

		.flip-card-inner {
		  position: relative;
		  width: 100%;
		  height: 100%;
		  text-align: center;
		  transition: transform 0.6s;
		  transform-style: preserve-3d;
		}

		.flip-card-inner {
			transform:translateX(-60px) translateX(-240px) rotateY(-180deg)
		}
		.flip-card-return {
			transform:translateX(0px) rotateY(0deg)
		}

		.flip-card-front, .flip-card-back {
		  position: absolute;
		  width: 100%;
		  height: 100%;
		  -webkit-backface-visibility: hidden;
		  backface-visibility: hidden;
		  z-index:999999999999999999;
		}

		.flip-card-front {
		  background-color: #bbb;
		  color: black;
		}

		.flip-card-back {
		  background-color: purple;
		  color: white;
		  transform: rotateY(180deg);
		}
		.dd {
			transition:2s
		}
		.left {
			transform:translateX(-300px)
		}
		.right {
			transform:translateX(0px)
		}
		.left2{
			transform:translateX(-150px)
		}
		.right2 {
			transform:translateX(0)
		}
		.wrap {
		  margin: 0 auto;
		  width: 600px;
		}

		.shadow {
		  position: relative;
		  margin: 3em auto;
		  padding-top: 2em;
		  width: 400px;
		  height: 100px;
		  border: 1px solid #777777;
		  border-radius: 5px;
		  background: #e5e5e5;
		  text-align: center;
		  box-shadow: 0 0 25px 0 rgba(50,50,50,.3) inset;
		}

		.shadow:after {
		  content: "";
		  position: relative;
		}

		.curved:after, .curved-2:after {
		  position: relative;
		  z-index: -2;
		}

		.curved:after {
			position: absolute;
			top: 50%;
			left: 12px;
			right: 12px;
			bottom: 0;
			box-shadow: 0 0px 10px 7px rgba(100,100,100,0.5);
			border-radius: 450px / 15px
		}

		.curved-2:after {
			position: absolute;
			top: 0;
			left: 12px;
			right: 12px;
			bottom: 0;
			box-shadow: 0 0px 10px 7px rgba(100,100,100,0.5);
			border-radius: 450px / 15px;
			z-index:-1!Important
		}
		</style>
	</head>
	<body style="">
		<audio autoplay controls style="display:none" id="ax">   
			<source src="audio/a1.mp3" />   
		</audio>
	<span style="display:none" id="upper">		
		<div id="c3" class="shadow curved-2" style="width:300px;height:400px;margin:auto;background:#f0f0f0;position:absolute;top:50px;z-index:-1;left:0;right:0"></div>
		<div id="c4" style="width:300px;height:400px;margin:auto;background:#f0f0f0;position:absolute;top:50px;z-index:0;left:0;right:0;padding:20px;color:blue"><div id="msg_canned" style="padding-top:40px;font-size:0.9em"><?=$msg_canned;?></div><div id="msg_personal" style="position:absolute;background:padding:10px;width:90%;height:200px;color:grey;font-size:14px;bottom:25px;font-family:Gloria Hallelujah"><?=$msg_personal;?></div></div>
		
		<div style="width:100%;max-width:300px;height:400px;margin:auto;position:relative;text-align:center;top:50px">
			<div id="ff" class="flip-card" onclick="anim()">
				<div id="inn">
					<div id="c1" class="flip-card-front">
					  <img src="assets/cards/<?=$card_cover;?>.png" style="width:300px;height:400px">
					</div>
					<div id="c2" class="flip-card-back">
						<div style="width:100%;height:100%">
							<? if ($asset) { ?>
								<video onended="location.href='<?=$msg3;?>'" playsinline id="vd" src="greetings/<?=$asset;?>" style="display:none;width:200px;height:200px;margin:auto;border-radius:200px;border:10px solid white;margin-top:50px;object-fit:cover;"></video>
								<button onclick="playNow()" style="position:absolute;bottom:50px;margin:auto;left:0;right:0" class="button-5">Play Video!</button>
							<? } ?>
						</div>
					</div>
				</div>
			</div>
		<div id="barDesktop" style="margin-top:25px">
			<button id="inside" class="button-5" style="font-size:0.8em;padding-left:0px;padding-right:0px;left:0px;right:0px;margin:auto;position:absolute;width:80px" onclick="anim();playM('a2')">Open</button>
			<button id="cover" class="button-5" style="font-size:0.8em;padding-left:0px;padding-right:0px;display:none;left:0;right:0;margin:auto;position:absolute;width:80px" onclick="anim_r()">Cover</button>
		</div>
		<div id="barMobile" style="margin-top:25px">
			<button id="left" class="button-5" style="font-size:0.8em;padding-left:5px;padding-right:5px;display:none;left:10px;margin:auto;position:absolute;width:85px;margin-left:-20px" onclick="left()">Left Panel</button>
			<button id="inside_m" class="button-5" style="font-size:0.8em;padding-left:5px;padding-right:5px;display:;left:0px;right:0px;margin:auto;position:absolute;width:70px" onclick="anim()">Open </button>
			<button id="cover_m" class="button-5" style="font-size:0.8em;padding-left:5px;padding-right:5px;display:none;left:0;right:0;margin:auto;position:absolute;width:70px" onclick="anim_r()">Cover</button>
			<button id="right" class="button-5" style="font-size:0.8em;padding-left:5px;padding-right:5px;display:none;right:10px;margin:auto;position:absolute;width:85px;margin-right:-20px" onclick="right()">Right Panel</button>
		</div>
	</div>
	<span style="display:none">
		<audio controls="none" id="a1">   
			<source src="audio/a1.mp3" />   
		</audio> 	
		<audio controls="none" id="a2">   
			<source src="audio/a2.mp3" />   
		</audio> 	
		<audio controls="none" id="a3">   
			<source src="audio/a3.mp3" />   
		</audio> 	
		<audio controls="none" id="a4">   
			<source src="audio/a4.mp3" />   
		</audio>
	</span>	
</span>	
		
	<script src="js/utils.js"type="text/javascript"></script>
	<script src="dz/dropzone.js"type="text/javascript"></script>
	<script src="https://apis.google.com/js/client:platform.js?onload=start" async defer></script>	
	
	<script>
		function isValidBrowser() {
			var validBrowser=false
			// Opera 8.0+
			// var isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
			// Firefox 1.0+
			// var isFirefox = typeof InstallTrigger !== 'undefined';
			// Safari 3.0+ "[object HTMLElementConstructor]" 
			validBrowser = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || (typeof safari !== 'undefined' && window['safari'].pushNotification));
			// Internet Explorer 6-11
			// var isIE = /*@cc_on!@*/false || !!document.documentMode;
			// Edge 20+
			validBrowser = !isIE && !!window.StyleMedia;
			// Chrome 1 - 79
			validBrowser = !!window.chrome && (!!window.chrome.webstore || !!window.chrome.runtime);
			if (validBrowser===false) {
				alert('Please select a vlid browser to use thos product. Valid browsers  currently are: IE, Chrome and Safari')
				return false
			}
		}
		setTimeout('isValidBrowser',1)

		var video_played=false
		function mobileCheck() {
			if ( navigator.userAgent.match(/Android/i)
				 || navigator.userAgent.match(/webOS/i)
				 || navigator.userAgent.match(/iPhone/i)
				 || navigator.userAgent.match(/iPad/i)
				 || navigator.userAgent.match(/iPod/i)
				 || navigator.userAgent.match(/BlackBerry/i)
				 || navigator.userAgent.match(/Windows Phone/i)) {
				return true 
			} else {
				return false
			}
		}
		function playM(af) {
			$$(af).play()
		}
		function playNow(xx) {
			if (xx==1) {
			
			} else {
				playM('a4')
			}
			document.getElementById('vd').style.display='block'
			document.getElementById('vd').play()
			video_played=true
		}

		var pane='right'
		function anim() {
			//tmr=setTimeout('playNow(1)',4000)
			if(mobileCheck()===true) {	
				tmr=setTimeout('_left()',3000)
			}
			document.getElementById('ff').className='dd flip-card flip-card-inner'
			if(mobileCheck()===true) {	
				document.getElementById('inside_m').style.display='none'
				document.getElementById('cover_m').style.display=''
				document.getElementById('inside').style.display='none'
				document.getElementById('cover').style.display='none'
			} else {
				document.getElementById('inside').style.display='none'
				document.getElementById('cover').style.display='block'
			}
			document.getElementById('left').style.display='block'
			document.getElementById('right').style.display='block'		
			document.getElementById('barDesktop').className='dd left2'		
			if (video_played===true) document.getElementById('vd').style.display='block'		
		}
		function anim_r() {
			document.getElementById('vd').style.display='none'		
			document.getElementById('ff').className='dd flip-card-return flip-card-inner'
			if(mobileCheck()===true) {	
				document.getElementById('inside_m').style.display=''
				document.getElementById('cover_m').style.display='none'
				document.getElementById('inside').style.display='none'
				document.getElementById('cover').style.display='none'
			} else {
				document.getElementById('inside').style.display='block'
				document.getElementById('cover').style.display='none'
			}
			document.getElementById('left').style.display='none'
			document.getElementById('right').style.display='none'
			document.getElementById('barDesktop').className='dd right2'				
		}
		function right() {
			pane='right'
			document.getElementById('inn').className='dd right flip-card-inner'
			if(mobileCheck()===true) {	
				document.getElementById('inside_m').style.display='none'
				document.getElementById('cover_m').style.display=''
				document.getElementById('inside').style.display='none'
				document.getElementById('cover').style.display='none'
			} else {
				document.getElementById('cover').style.display='block'
				document.getElementById('cover_m').style.display='none'
				document.getElementById('inside_m').style.display='none'
				document.getElementById('inside').style.display='none'
			}
		}
		function _left() {
			clearTimeout(tmr)
			tmr=setTimeout('left()',5000)
		}
		function left() {
			document.getElementById('inn').className='dd left flip-card-inner'
			document.getElementById('inside_m').style.display='none'
			document.getElementById('inside').style.display='none'
			document.getElementById('cover_m').style.display='none'
			document.getElementById('cover').style.display='none'
			$('#inside').hide()
			$('#cover').hide()
			$('#inside_m').hide()
			$('#cover_m').hide()
			pane='left'
		}
		if(mobileCheck()===true) {	
			document.getElementById('barDesktop').style.display='none'
			document.getElementById('barMobile').style.display=''
		} else {
			document.getElementById('barDesktop').style.display=''
			document.getElementById('barMobile').style.display='none'
		}
		function qs1() {
			var str1=[]
			var url=''
			var pre
		    str1=location.search.split('?')[1]
			for (var i=0; i<str1.split('&').length; i++) {
				if (i>0) pre='&'
					else pre='?'
				url+=pre+str1.split('&')[i].split('=')[0]+'="+'+str1.split('&')[i].split('=')[0]+'\+\"'
			}
			console.log(url)
		}
		Dropzone.autoDiscover=false
		var sites
		var imagesUploaded
		var r,k,c,t2,t3,t4,intr=50,currImg
		var rat=1
		var rx=0, t, ext
		var set=false
		var usr_img=[]
		var ext
		var collection_set=0
		var channel_set=0
		var cnn=0
		
		function getCookie(cname)	{
			var name = cname + "=";
			var ca = document.cookie.split(';');
			for(var i=0; i<ca.length; i++) {
			  var c = ca[i].trim();
			  if (c.indexOf(name)==0) return c.substring(name.length,c.length);
			}
			return "";
		}
		var myDropzone, tfiles=0
		var gui
		var src_element

		function add_assets(asset,asset_type,asset_media_type,card_id) {
			u="x_update_greeting.php?field=asset&value="+asset+"&key_field=card_id&key_value=" + card_id
			$.ajax({url:u,success:function(data){}})
			u="x_update_greeting.php?field=asset_media_type&value="+asset_media_type+"&key_field=card_id&key_value=" + card_id
			$.ajax({url:u,success:function(data){}})
			u="x_update_greeting.php?field=asset_type&value="+asset_type+"&key_field=card_id&key_value=" + card_id
			$.ajax({url:u,success:function(data){$.alert('Saved!')}})
		}
		
		function activate_upload(x) {
			var gauge=[ ];
			var target=[ ];
			var url="https://linqstar.com/php/x_greeting_uploads.php"
			if (!myDropzone) {
				myDropzone = new Dropzone("form#member"+x, { 
					url: url,
					timeout:0,
					maxFilesize: 1000000000,
					dictDefaultMessage: "<br><br><div id='xe"+x+"' style='background:;color:#fff;font-family:Open sans;color:#fff;font-weight:300;top:0;position:absolute;width:100%;left:0;margin-top:20px;top:8%;right:0;margin-left:auto;margin-right:auto;height:75%;z-index:99999999999999999;color:black;margin-top:-15px'>DRAG DROP FILES<br><b>OR</b><br>CLICK HERE<br><img style='width:70px' src='https://lushmatch.com/sdfm/images/cap04.png'></div>",
					createImageThumbnails:true,
					init: function() {
						this.on("sending", function(file, xhr, formData){
							gui=guid()
							var in_co, in_ch
							setCookie('attach_id',gui)
							ext=file.name.split('.')[1]
							try {
							if (ext=='mp4' || ext=='wmv' || ext=='qt' || ext=='mov' || ext=='avi' || ext=='mkv' || ext=='mpg') {
								$$('p'+x).innerHTML='<video src="greetings/'+gui+'.'+ext+'" style="width:100%;margin:10px"></video>'
								asset_media_type='video'
							} else if (ext=='mp3' || ext=='wma' || ext=='qt' || ext=='au' || ext=='wav') {
								$$('p'+x).innerHTML='<audio src="greetings/'+gui+'.'+ext+'" style="width:100%;margin:10px"></audio>'
								asset_media_type='audio'
							} else if (ext=='png' || ext=='jpg' || ext=='gif' || ext=='bmp'){
								asset_media_type='image'
							}
							if (x==10) {
								src_element='msg_image'
							} else if (x==12) {
								src_element='user_photo'
							} else if (x==11) {
								src_element='mood_music'
							} else if (x==9) {
								src_element='voice_over'
							} else if (x==8) {
								src_element='user_video'
							}
								formData.append("attach_id", gui);
								formData.append("user_type", getCookie('user_type'));
								formData.append("fan_mid", qs('fan_mid'));
								formData.append("provider_mid", getCookie('mid'));
								formData.append("card_id", card_id);
								formData.append("asset_media_type", asset_media_type);
								formData.append("asset_type", src_element);
								formData.append("asset", gui + '.' + ext);
							} catch (e) {}
						});
					}			
				});
			}
			myDropzone.on("addedfile", function(file) {
				//wait('Uploading File, Please Wait...')
			});
				
			myDropzone.on("complete", function(file) {
				if (x==8 || x==9 || x==11){
					if (x==8) {
						setTimeout(function(){
							$$('member'+x).innerHTML='<video src="greetings/'+getCookie('attach_id')+'.'+ext+'" style="max-height:250px!Important;opacity:1/border:10px solid white;" autoplay muted></video>'
						},3000)
						add_asset(getCookie('attach_id') +' .' + ext,src_element,asset_media_type)
					} else {
						setTimeout(function(){
							$$('member'+x).innerHTML='<audio src="greetings/'+getCookie('attach_id')+'.'+ext+'" style="max-height:120px!Important;opacity:1" autoplay muted></audio>'
						},3000)
					}
				}
			})
		}		
		var i=0
		function animateIt() {
			i=i+1
			$$('logo').style.transform='rotateY('+i+'deg)'
			setTimeout('animateIt()',1)
		}
		function showIt() {
			$$('logo_text').style.display='block'
		}

		function hideIt() {
			$$('upper').style.display=''
			$$('lower').style.display='none'
		}

		
		setTimeout('animateIt()',3000)
		setTimeout('showIt()',3000)


	</script>		
	<a href="javascript:hideIt()">
		<div id="lower"  style="top:0;bottom:0;left:0;right:0;margin:auto;width:100%;height:100%;background:purple;;position:absolute;z-index:99999999999999999999">
			<img id="logo" style="top:0;bottom:0;left:0;right:0;margin:auto;width:200px;position:absolute;z-index:999999999999" src="greetings/logo.png" />
			<img id="text" style="top:0;bottom:0;left:0;right:0;margin:auto;width:200px;position:absolute;z-index:999999999999;top:-45%" src="greetings/text.png">
			<img id="logo_text" style="top:0;bottom:0;left:0;right:0;margin:auto;width:200px;position:absolute;z-index:99999999;top:55%" src="greetings/logo_text.png" />
			<img class="www_box2" style="top:0;bottom:0;left:0;right:0;margin:auto;width:max-800px;width:100%;position:absolute;z-index:9999999" src="greetings/c.gif" />
		</div>
	</a>
	</body>
</html>
