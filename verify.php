<?php
	require_once "../class/utils.class.php";
	$c=new utils;
	$c->connect("199.91.65.83","voxeo");
	parse_str(http_build_query($_GET));
	$pro=$c->query("select * from celeb_profiles where mid=$provider_mid");
	foreach($pro[0] as $key => $value) {
		${$key}=$value;
	}
	$ta=$c->query("select * from celeb_tags where pid=$pid");
	$t=explode(",",$ta[0]['tags']);
?>
<script src="assets/js/jquery.js"type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js"type="text/javascript"></script>
<script src="assets/js/utils.js"type="text/javascript"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script> 
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Voxeo</title>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="assets/css/animate.css"/>
    <link rel="stylesheet" href="assets/css/jquery-ui.min.css"/>
    <link rel="stylesheet" href="assets/css/main.css"/>
	<link href="style.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon"/>
	<style>
		*{text-align:center;font-size:12px}
	</style>
</head>
<body  style="background:url(assets/images/account-bg.jpg)no-repeat;background-size:cover">
	    <img style="width:200px;margin:auto;left:0;right:0;position:absolute" src="assets/images/logo2.png" alt="logo">
        <br>
		<div id="notice" class="container text-center">
            <div class="account-wrapper" style="margin-top:200px;padding:25px">
                <div class="account-body" style="font-size:0.8em">
					<span id="section1">
						<div class="row">
							<div class="col-md-12">Before we proceed, we need to verify your mobile number. We will send you a 4 digit code via SMS that you will need to enter in the next section. Please enter mobile number below</div>
						</div><br>
						<div class="row">
							<div class="col-md-3"></div>
							<div class="col-md-6"><input type="text" id="mobile" style="font-size:14px;width:160px" placeholder="My Mobile Number" maxlength=11></div>
							<div class="col-md-3"></div>
						</div><br>
						<div class="row">
							<div class="col-md-12"><button class="button-5" onclick="send_verify()">Send Verification Code</button></div>
						</div>
					</span>
					<span id="section2" style="display:none">
						<div class="row">
							<div class="col-md-12">Please enter the 4 digit verification code that was sent to you via SMS</div>
						</div>
						<div class="row">
							<div class="col-md-2"></div>
							<div class="col-md-2"><input id="c1" onkeyup="next(this)" style="width:40px;padding:5px"></div>
							<div class="col-md-2"><input id="c2" onkeyup="next(this)" style="width:40px;padding:5px"></div>
							<div class="col-md-2"><input id="c3" onkeyup="next(this)" style="width:40px;padding:5px"></div>
							<div class="col-md-2"><input id="c4" onkeyup="next(this)" style="width:40px;padding:5px"></div>
							<div class="col-md-2"></div>
						</div>
						<div class="row">
							<div class="col-md-3"></div>
							<div class="col-md-6"><button class="button-5" onclick="send_verify()">Verify Code</button></div>
							<div class="col-md-3"></div>
						</div>
					</span>
				</div>
			</div>
		</div>
	<script src="js/utils.js"></script>
	<script src="js/wait.js"></script>
	<script src="js/aes.js"></script>
	<script>
		function send_verify() {
			var to=document.getElementById('mobile').value
			var cd=encrypt(Math.floor(Math.random()*10000)+'',"spacetime")
			to=isValidMobile(to)
			if (to===false) {
				$.alert('Invalid Mobile! Please enter a 10 digit mobile number without your country code.')
			} else {
				setCookie('code',cd)
				var url='https://linqstar.com/x_send_verify.php?to='+to+'&msg='+cd
				console.log(url)
				$.ajax({url:url})
				document.getElementById('section1').style.display='none'
				document.getElementById('section2').style.display='block'
			}
		}
		var code=''
		var ctr=0
		function next(o) {
			if (o.id=='c1') {
				code=o.value
				$$('c2').focus()
			} else if (o.id=='c2') {
				code+=o.value
				$$('c3').focus()
			} else if (o.id=='c3') {
				code+=o.value
				$$('c4').focus()
			} else if (o.id=='c4') {
				code+=o.value
				if (decrypt(getCookie('code'),'spacetime')==code) {
					$.confirm({
						title: 'Mobile Number Verified OK!',
						content: 'Congratulations, you entered the correct code. Click Next to proceed.',
						buttons: {
							Next: function () {
								setCookie('mobile',document.getElementById('mobile').value)
								location.href='join.php'
							}
						}
					});	
				} else {
					ctr++
					if (ctr<4) {
						$.confirm({
							title: 'Failure',
							content: 'Sorry, incorrect code. Press OK to retry',
							buttons: {
								Next: function () {
									$$('c1').value=''
									$$('c2').value=''
									$$('c3').value=''
									$$('c4').value=''
									$$('c4').focus()
								}
							}
						});	
					} else {
						$.confirm({
							title: 'Failure',
							content: 'Sorry, incorrect code. You have exceeded the amount of retries and will have to wait for 1 minute to receive a new code. Click Send to receive a new code',
							buttons: {
								Send: function () {
									wait()
									setTimeout(function(){
										history.go(0)
									},60000)
								}
							}
						});	
					}
				}
			}
		}
	function isValidMobile(mob) {
		if (!mob) return false
		var num = mob.trim();
		try {
			if (num.indexOf('+')>=0) num = num.replace(/\+/g,'');
		} catch (e) {}
		try {
			num = num.replace(/ /g,'');
			num = num.replace(/\./g,'');
			num = num.replace(/-/g,'');
			num = num.replace(/\(/g,'');
			num = num.replace(/\)/g,'');
			num = num.replace(/\[/g,'');
			num = num.replace(/\]/g,'');
			num = num.replace(/\~/g,'');
			num = num.replace(/\*/g,'');
			num = num.replace(/\{/g,'');
			num = num.replace(/\}/g,'');
			num = num.replace(/\+/g,'');
			if ((num+'').length<10) return false
			if (isNaN(num)) return false
			code='1'
			if ((num+'').length==10) {
				return (''+code+''+num+'')
			} else if ((num+'').length==11) {
				if (num.substr(0,1)==code) {
					return num
				} else {
					return false
				}
			} else if ((num+'').length>11) {
				return num
			}
		} catch(e) {}
	}
	
		
	</script>
</body>
</html>
<script src="assets/js/jquery.js"type="text/javascript"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
