<?
include "../class/utils.class.php";
$c=new utils;
$c->connect("199.91.65.83","voxeo");
$q1=$c->query("select celeb_category from celeb_providers");
$q2=$c->query("select pro_category from pro_providers order by pro_category");
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Mosto - Software and App Landing Pages HTML Template</title>

    <link rel="stylesheet" href="./assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="./assets/css/all.min.css">
    <link rel="stylesheet" href="./assets/css/animate.css">
    <link rel="stylesheet" href="./assets/css/nice-select.css">
    <link rel="stylesheet" href="./assets/css/owl.min.css">
    <link rel="stylesheet" href="./assets/css/jquery-ui.min.css">
    <link rel="stylesheet" href="./assets/css/magnific-popup.css">
    <link rel="stylesheet" href="./assets/css/flaticon.css">
    <link rel="stylesheet" href="./assets/css/main.css">
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:300|Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="assets/css/animate.css"/>
    <link rel="stylesheet" href="assets/css/jquery-ui.min.css"/>
    <link rel="stylesheet" href="assets/css/main.css"/>
	<link href="style.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon">
	<style>
		* {
		font-family:Open Sans!Important;
		font-weight:300;
		font-size:14px!Important
		}
		input {
			background:#fff!Important
		}
	</style>
  
</head>

<body>

    <div class="preloader">
        <div class="preloader-inner">
            <div class="preloader-icon">
                <span></span>
                <span></span>
            </div>
        </div>
    </div>

    <!--============= Sign In Section Starts Here =============-->
    <div class="account-section bg_img" data-background="./assets/images/account-bg.jpg">
        <div class="container">
            <div class="account-title text-center">
                <a href="#0" class="logo">
                    <img src="./assets/images/logo2.png" alt="logo">
                </a>
            </div>
            <div class="account-wrapper" style="background:#f0f0f0">
                <div class="account-body">
                    <div class="container">
                    <form class="account-form">
						<div id="step10">
							<div class="form-group">
								<label for="sign-up">WHO AM I</label>
								<div class="row">
									<div id="d1" onclick="toggleBorders('d1')" class="col-md-4" style="padding:25px;border:10px solid powderblue">
										<img src="celeb.png" style="width:70px">
									</div>
									<div id="d2" style="padding:25px" onclick="toggleBorders('d2')" class="col-md-4">
										<img src="professional.png" style="width:100px">
									</div>
									<div id="d3" onclick="toggleBorders('d3')" class="col-md-4" style="padding:25px;">
										<img src="fan.png" style="width:70px">
									</div>
								</div>
								<br>
								<div class="row">
									<div class="col-md-6" style="text-align:right;font-weight:bolder">
										<b>I AM A</b>
									</div>
									<div class="col-md-6" id="sel" style="text-align:left;font-weight:bolder">
										CELEBRITY
									</div>
								</div>
								<div class="row" id="celeb">
									<div class="col-md-6" style="text-align:right;font-weight:bolder">
										<b>CELEBRITY TYPE</b>
									</div>
									<div class="col-md-6" style="text-align:left;font-weight:bolder">
										<select onchange="setCelebCat(this.value)" style="height:50px;padding:10px;border-radius:6px" id="celeb_category">
											<option value="">Select One</option>
											<? for ($i=0;$i<count($q1); $i++) { ?>
												<option value="<?=$q1[$i]['celeb_category'];?>"><?=$q1[$i]['celeb_category'];?></option>
											<? } ?>
										</select>
									</div>
								</div>
								<div class="row" id="pro" style="display:none">
									<div class="col-md-6" style="text-align:right;font-weight:bolder">
										<b>PROFESSIONAL TYPE</b>
									</div>
									<div class="col-md-6" style="text-align:center;font-weight:bolder">
										<select onchange="setProCat(this.value)" style="height:50px;padding:10px;border-radius:6px" id="pro_category">
											<option value="">Select One</option>
											<? for ($i=0;$i<count($q2); $i++) { ?>
												<option value="<?=$q2[$i]['pro_category'];?>"><?=$q2[$i]['pro_category'];?></option>
											<? } ?>
										</select>
									</div>
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-md-12" style="text-align:center">
									<a href="javascript:step2()" class="button-4">NEXT: STEP 2</a>
								</div>
							</div>
						</div>
						<br>
						<div id="step20" style="display:none">
							<div class="form-group">
								<div class="row">
									<div class="col-md-12">
										<label for="sign-up">EMAIL</label>
										<input style="background:#fff" type="text" placeholder="Enter Your Email Address" id="email"   onblur="validate_field_register(this)">
										<div id="email_err_txt"></div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<label for="sign-up">PASSWORD</label>
										<input style="background:#fff" type="text" placeholder="Create a Password" id="pswd">
									</div>
									<div class="col-md-6">
										<label for="sign-up">GENDER</label>
										<select style="background:#fff;padding:10px;height:50px;border-radius:6px" type="select" id="gender">
											<option value="M">Male</option>
											<option value="F">Female</option>
											<option value="N">Neutral</option>
											<option value="T">Transgender</option>
										</select>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="sign-up">FULL NAME</label>
								<input style="background:#fff" type="text" placeholder="Enter Your Full Name " id="name">
							</div>
							<div class="form-group">
								<label for="sign-up">SELECT A LOGIN NAME</label>
								<input type="text" placeholder="Create a Login Name" id="login"   onblur="validate_field_register(this)">
								<div id="login_err_txt"></div>
							</div>
							<div class="form-group">
								<label for="location">LOCATION</label>
								<div id="xxx"></div>
							</div>
							<div class="form-group text-center">
								<a style="color:white" class="button-4" href="javascript:register()">SAVE PROFILE AND ENTER SITE</a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
    </div>
    <!--============= Sign In Section Ends Here =============-->

    <script src="./assets/js/jquery-3.3.1.min.js"></script>
    <script src="./assets/js/modernizr-3.6.0.min.js"></script>
    <script src="./assets/js/plugins.js"></script>
    <script src="./assets/js/bootstrap.min.js"></script>
    <script src="./assets/js/magnific-popup.min.js"></script>
    <script src="./assets/js/jquery-ui.min.js"></script>
    <script src="./assets/js/wow.min.js"></script>
    <script src="./assets/js/waypoints.js"></script>
    <script src="./assets/js/nice-select.js"></script>
    <script src="./assets/js/owl.min.js"></script>
    <script src="./assets/js/counterup.min.js"></script>
    <script src="./assets/js/paroller.js"></script>
    <script src="./assets/js/countdown.js"></script>
    <script src="./assets/js/main.js"></script>
    <script src="js/wait.js"></script>
    <script src="js/utils.js"></script>
    <script src="js/location.js"></script>
		<script type="text/javascript" src="https://lushmatch.com/sdfm/js/jquery.js"></script>
		<script src="https://lushmatch.com/sdfm/js/jquery-ui.js"></script>
		<script src="https://lushmatch.com/sdfm/js/jquery-ui.js"></script>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDAuvU_A1iMThoe1i6Joi26nSTDQkjKlDA&libraries=places"></script>

<script>
	function toggleBorders(id) {
		$$('d1').style.border='0px solid powderblue'
		$$('d2').style.border='0px solid powderblue'
		$$('d3').style.border='0px solid powderblue'
		$$(id).style.border='10px solid powderblue'
		$$('celeb').style.display='none'
		$$('pro').style.display='none'
		if (id=='d1'){
			$$('sel').innerHTML="CELEBRITY"
			setCookie('user_type','celebrity')
		}
		if (id=='d2') {
			$$('sel').innerHTML="PROF SERVICES PROVIDER"
			setCookie('user_type','professional')
		}
		if (id=='d3') {
			$$('sel').innerHTML="FAN"
			setCookie('user_type','fan')
		}
		if (id=='d1') $$('celeb').style.display=''
		if (id=='d2') $$('pro').style.display=''
	}
	
	function step2() {
		$$('step10').style.display='none'
		$$('step20').style.display=''
	}
	
	function setCelebCat(v) {
		setCookie('user_category',v)
	}

	function setProCat(v) {
		setCookie('user_category',v)
	}
	function validate_field_register(f)	{
		var e = '<span style="color:orange">' + f.value + '</span> Already Exists.<br><a href="/">Login</a> with this ' + f.id + ' instead?'
		if (f.value.length > 0) {
			var x = ''
			var r = f.id + '_err'
			var t = r + '_txt'
			var i = f.value
			r = document.getElementById(r)
			t = document.getElementById(t)
			var url = 'x_validate_field.php?d=' + f.id + '&i=' + i
			console.log(url)
			var request = $.ajax({
			url: url,
			type: "GET",
			dataType:'html',
			cache: false,
			success: function(msg) {
					console.log(msg)
					if (msg != 0) {
						console.log(f)
						f.style.background='lavenderblush'
						validate = false
						ns = false
						t.style.display = ''
						t.innerHTML = e
					}
					else {
						validate = true
						return true
					}
				}
			})
		}
	}	
	function register() {
		wait()
		var url='x_create.php?login='+$$('login').value+'&mobile='+getCookie('mobile').value+'&name='+$$('name').value+'&gender='+$$('gender').value+'&email='+$$('email').value+'&pswd='+$$('pswd').value+'&city='+getCookie('city')+'&state='+getCookie('state')+'&zip='+getCookie('zip')+'&lat='+getCookie('lat')+'&lng='+getCookie('lng')+'&user_type='+getCookie('user_type')+'&user_category='+getCookie('user_category')+'&country=1'
		console.log(url)
		$.ajax({
			url:url,
			success:function(data){
				hide()
				if (!isNaN(data)) {
					$.confirm({
						title: 'Complete!',
						content: 'Account was succesfully created. Click Ok to go to the home page where you can now login',
						buttons: {
							ok: function () {
								location.href='https://linqstar.com'
							}
						}
					});
				}
			}
		})
	}
	if (!getCookie('mobile')) location.href='verify.php'
</script>
</body>
</html>