<?php
	require_once "../class/utils.class.php";
	$c=new utils;
	$c->connect("199.91.65.83","voxeo");
	parse_str(http_build_query($_GET));
	$pro=$c->query("select * from celeb_profiles where mid=$provider_mid");
	foreach($pro[0] as $key => $value) {
		${$key}=$value;
	}

	
	$ta=$c->query("select * from celeb_tags where pid=$pid");
	$t=explode(",",$ta[0]['tags']);
?>
<script src="assets/js/jquery.js"type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js"type="text/javascript"></script>
<script src="assets/js/utils.js"type="text/javascript"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script> 

<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Voxeo - Checkout</title>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="assets/css/all.min.css"/>
    <link rel="stylesheet" href="assets/css/animate.css"/>
    <link rel="stylesheet" href="assets/css/nice-select.css"/>
    <link rel="stylesheet" href="assets/css/owl.min.css"/>
    <link rel="stylesheet" href="assets/css/jquery-ui.min.css"/>
    <link rel="stylesheet" href="assets/css/magnific-popup.css"/>
    <link rel="stylesheet" href="assets/css/flaticon.css"/>
    <link rel="stylesheet" href="assets/css/main.css"/>
	<link href="style.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon"/>
</head>
<style>
h1,h2,h3,h4,h5,h6{
	font-weight:300;
	text-transform:upperCase;
	margin:5px
}
.tags a {
	display: inline-block;
	height: 21px;
	margin: 0 10px 0 0;
	padding: 0 7px 0 14px;
	white-space: nowrap;
	position: relative;

	background: -moz-linear-gradient(top, #fed970 0%, #febc4a 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#fed970), color-stop(100%,#febc4a));
	background: -webkit-linear-gradient(top, #fed970 0%,#febc4a 100%);
	background: -o-linear-gradient(top, #fed970 0%,#febc4a 100%);
	background: linear-gradient(to bottom, #fed970 0%,#febc4a 100%);
	background-color: #FEC95B;

	color: #963;
	font: bold 11px/21px Arial, Tahoma, sans-serif; 
	text-decoration: none;
	text-shadow: 0 1px rgba(255,255,255,0.4);

	border-top: 1px solid #EDB14A;
	border-bottom: 1px solid #CE922E;
	border-right: 1px solid #DCA03B;
	border-radius: 1px 3px 3px 1px;
	box-shadow: inset 0 1px #FEE395, 0 1px 2px rgba(0,0,0,0.21)!Important;
}
a:before { 
  content: '';  
  position: absolute;  
  top: 5px;  
  left: -6px;  
  width: 10px;  
  height: 10px;  
  
  background: -moz-linear-gradient(45deg, #fed970 0%, #febc4a 100%);  
  background: -webkit-gradient(linear, left bottom, right top, color-stop(0%,#fed970), color-stop(100%,#febc4a));  
  background: -webkit-linear-gradient(-45deg, #fed970 0%,#febc4a 100%);  
  background: -o-linear-gradient(45deg, #fed970 0%,#febc4a 100%);  
  background: linear-gradient(135deg, #fed970 0%,#febc4a 100%);  
  background-color: #FEC95B;  
  
  border-left: 1px solid #EDB14A;  
  border-bottom: 1px solid #CE922E;  
  border-radius: 0 0 0 2px;  
  box-shadow: inset 1px 0 #FEDB7C, 0 2px 2px -2px rgba(0,0,0,0.33);  
}  
a:before { 
  -webkit-transform: scale(1, 1.5) rotate(45deg);  
  -moz-transform: scale(1, 1.5) rotate(45deg);  
  -ms-transform: scale(1, 1.5) rotate(45deg);  
  transform: scale(1, 1.5) rotate(45deg);  
} 
a:after { 
  content: '';  
  position: absolute;  
  top: 7px;  
  left: 1px;  
  width: 5px;  
  height: 5px;  
  background: #FFF;  
  border-radius: 4px;  
  border: 1px solid #DCA03B;  
  box-shadow: 0 1px 0 rgba(255,255,255,0.2), inset 0 1px 1px rgba(0,0,0,0.21);  
}  
div{overflow:none!Important}
</style>
    <link rel="stylesheet" href="css/shadows.css">

<body  style="background:url(assets/bg.png)no-repeat;background-size:cover">
	    <img style="width:200px;margin:auto;left:0;right:0;position:absolute" src="assets/images/logo2.png" alt="logo">
        <br>
		<div id="" class="container text-center">
            <div class="account-wrapper" style="overflow:hidden;margin:auto;margin-top:200px;padding:25px;max-width:800px;background:#edf2f6;max-height:auto;">
					<img src="<?=$photo;?>" style="height:200px;width:200px;border-radius:200px">
					<br><br>
					<div class="button-1">	
						<? for ($v=1; $v<count($t)-1; $v++) { ?>
							<a style="border-radius:4px;margin:3px;padding:2px;font-weight:lighter;font-size:10px;padding-left:5px;padding-right:5px" class="button-3" href="#"><?=$t[$v];?></a>  
						<? } ?>
					</div>						
					<div id="notice" class="container text-center">
						<div id="star" style="text-align:center">
							<span id='1'><img style="width:20px" id='r1' onclick="update_rating(1)" class="star" onmouseover='light_up(1)' onmouseout='light_dn(1)' src="https://linqstar.com/assets/star_off.png"></span>
							<span id='2'><img style="width:20px" id='r2' onclick="update_rating(2)" class="star" onmouseover='light_up(2)' onmouseout='light_dn(2)' src="https://linqstar.com/assets/star_off.png"></span>
							<span id='3'><img style="width:20px" id='r3' onclick="update_rating(3)" class="star" onmouseover='light_up(3)' onmouseout='light_dn(3)' src="https://linqstar.com/assets/star_off.png"></span>
							<span id='4'><img style="width:20px" id='r4' onclick="update_rating(4)" class="star" onmouseover='light_up(4)' onmouseout='light_dn(4)' src="https://linqstar.com/assets/star_off.png"></span>
							<span id='5'><img style="width:20px" id='r5' onclick="update_rating(5)" class="star" onmouseover='light_up(5)' onmouseout='light_dn(5)' src="https://linqstar.com/assets/star_off.png"></span>
							<span id='6'><img style="width:20px" id='r6' onclick="update_rating(6)" class="star" onmouseover='light_up(6)' onmouseout='light_dn(6)' src="https://linqstar.com/assets/star_off.png"></span>
							<span id='7'><img style="width:20px" id='r7' onclick="update_rating(7)" class="star" onmouseover='light_up(7)' onmouseout='light_dn(7)' src="https://linqstar.com/assets/star_off.png"></span>
							<span id='8'><img style="width:20px" id='r8' onclick="update_rating(8)" class="star" onmouseover='light_up(8)' onmouseout='light_dn(8)' src="https://linqstar.com/assets/star_off.png"></span>
							<span id='9'><img style="width:20px" id='r9' onclick="update_rating(9)" class="star" onmouseover='light_up(9)' onmouseout='light_dn(9)' src="https://linqstar.com/assets/star_off.png"></span>
							<span id='10'><img style="width:20px" id='r10' onclick="update_rating(10)" class="star" onmouseover='light_up(10)' onmouseout='light_dn(10)' src="https://linqstar.com/assets/star_off.png"></span>
							<span style="margin-left:10px;font-size:18px"id="rating_status2"></span>
						</div>
					</div>							
					
					<h5><?=$name;?></h5>
					<h6 style="font-size:0.8em;color:#000">A <?=$provider_type;?></h6>
					<a href="javascript:subscribe_public()"><button class="button-2" style="background:skyblue!Important">Chatroom</button></a> <a href="javascript:subscribe_public()"><button style="background:red" class="button-2">Subscribe</button></a>  <a href="javascript:subscribe_public()"><button style="background:skyblue" class="button-2">Follow</button></a>
					<div style="font-size:12px"> Subscribe to all available content on all channels</div>
					<a class="btn tooltipped" data-position="bottom" data-delay="50" data-tooltip="I am a tooltip">Hover me!</a>
					<hr>
					<div style="background:#fff;margin:auto;border-radius:10px;padding:15px"><br>
						<h3>REQUESTS</h3><br>
						<? $sn=explode(",",$service_names); ?>
						<? $sc=explode(",",$service_costs); ?>
						<div class="container">
							<div class="row">
								<? for  ($n=0;$n<count($sn);$n++) { ?>
									<div class="col-md-3">
										<? 
											if ($sn[$n]=="Recorded Message") $icon="rec1.png";
											if ($sn[$n]=="Live Call") $icon="rec2.png";
											if ($sn[$n]=="Social Functions") $icon="rec3.png";
											if ($sn[$n]=="Anon SMS Session") $icon="rec4.png";
										?>
										<? if ($n==1) { ?>
											<div class="www_box" style="text-align:center;background:#fff;width:100px;height:100px;border-radius:10px;margin:auto"><img style="width:90px;position:absolute;z-index:99;margin-top:-30px;margin-left:-55px;transform:rotate(-25deg)" src="https://linqstar.com/assets/most5.png"><img style="width:70px;margin-top:15px" src="https://linqstar.com/assets/<?=$icon;?>"></div>
										<? } else { ?>
											<div  class="www_box1" style="text-align:center;text-align:center;background:#fff;width:100px;height:100px;border-radius:10px;margin:auto"><img align=center style="width:70px;margin-top:15px" src="https://linqstar.com/assets/<?=$icon;?>"></div>										
										<? } ?>	
											<div style="text-align:center;font-size:12px;"><?=strtoupper($sn[$n]); ?></div>
											<button class="button-5" style="padding:7px;font-size:12px;width:120px">BUY FOR $<?=(!$sc[$n])?$sc[$n-1]:$sc[$n];?></button>
									</div>
								<? } ?>
							</div>
						</div><br/>
					</div>
			<BR><BR>
			<div style="background:#fff;margin:auto;border-radius:10px;padding:15px"><br>
				<h3>CONTENT CHANNELS</h3><br>
				<div style="font-size:12px">  List of exclusive channels that are delivery platforms for the very latest and periodically updated content, available only to the select few</div>
				<div class="row text-center" style="margin:auto">
			<?
				$sql="select * from celeb_channels where provider_mid=$provider_mid"; 
				$q=$c->query($sql);
				for ($r=0; $r<count($q); $r++) {
				$cn=$c->query("select * from celeb_channel_content where provider_mid=$provider_mid and channel_id=" . $q[$r]['channel_id'] . " order by last_updated DESC");
				$cn_cnt=count($cn);
				$last_updated=$cn[0]['last_updated'];
			?>
				<div class="col-md-3 www_box" style="margin:10px;text-align:center;text-align:center;background:url(assets/bg.png); background-size:cover; width:100px;height:240px;border-radius:10px;margin:30px">
					<button style="height:40px;font-family:Open Sans!Important;font-weight:300!Important;width:100%;position:absolute;top:0;left:0;opacity:0.6;background:#333;border:none;border-radius:10px 10px 0 0;color:white">
						SUBSCRIBE
					</button>
					<div style="font-family:Open Sans!Important;color:#fff;font-size:14px;margin-top:40px">
						<div style="color:silver">TITLE</div><div style="margin-top:-15px;font-size:0.9em"><b><?=strtoupper($q[$r]['title']);?></b></div>
					</div>
					<div style="font-family:Open Sans!Important;color:#fff;font-size:14px;margin-top:-2px">
						<div style="color:silver">TYPE</div><div style="margin-top:-15px;font-size:0.9em"><b><?=strtoupper($q[$r]['channel_type']);?></b></div>
					</div>
					<div style="font-family:Open Sans!Important;color:silver;font-size:14px;margin-top:0px">
						SUBSCRIBER COUNT
					</div>
					<div style="font-family:Open Sans!Important;color:white;font-size:21px;margin-top:-10px">
						<b><?=$cn_cnt;?></b>
					</div>
					<div style="font-family:Open Sans!Important;color:silver;font-size:14px;margin-top:0px">
						LAST UPDATED
					</div>
					<div style="font-family:Open Sans!Important;color:#fff;font-size:14px;margin-top:-10px">
						<div style="margin-top:-15px;font-size:0.9em"><b><?=$last_updated;?></b>TODAY</div>
					</div>
					<div style="font-family:Open Sans!Important;color:#fff;font-size:14px;margin-top:5px">
						<a href="javascript:sub_fbk()"><img src="assets/facebook.png" style="width:15px;margin-left:10px;margin-right:10px"></a>
						<a href="javascript:sub_twi()"><img src="assets/twitter.png" style="width:15px;margin-left:10px;margin-right:10px"></a>
						<a href="javascript:sub_pin()"><img src="assets/instagram.png" style="width:15px;margin-left:10px;margin-right:10px"></a>
					</div>
				</div>
			<?			
				}

			?>
			</div>
			</div>
			<BR><BR>
			<div style="background:#fff;margin:auto;border-radius:10px;padding:15px"><br>
				<h3>EVENT CALENDAR</h3><br>
				<div style="font-size:12px">  List of exclusive channels that are delivery platforms for the very latest and periodically updated content, available only to the select few</div>
			</div>
				<iframe src="https://linqstar.com/events/10980" style="border:none;overflow:none;width:100%;height:100%"></iframe>
				<BR><BR>
<?/*
				<div style="background:#fff;margin:auto;border-radius:10px;padding:15px"><br>
				<h3>SUBSCRIBERS</h3><br>
				<div style="font-size:12px">  List of exclusive channels that are delivery platforms for the very latest and periodically updated content, available only to the select few</div>
			</div>
			<BR><BR>
			<div style="background:#fff;margin:auto;border-radius:10px;padding:15px"><br>
				<h3>FOLLOWERS</h3><br>
				<div style="font-size:12px">  List of exclusive channels that are delivery platforms for the very latest and periodically updated content, available only to the select few</div>
			</div>
			<BR><BR>
			<div style="background:#fff;margin:auto;border-radius:10px;padding:15px"><br>
				<h3>REVIEWS</h3><br>
				<div style="font-size:12px">  List of exclusive channels that are delivery platforms for the very latest and periodically updated content, available only to the select few</div>
			</div>
*/?>
			<BR><BR>
			<div style="background:;margin:auto;border-radius:10px;padding:15px"><br>
				<h3>PUBLIC FEED</h3><br>				
				<iframe id="myf"  class="col-lg-12" style="border:0;overflow:hidden" src="https://linqstar.com/smedia/feed.php"></iframe>
			</div>
			</div>
		</div>
	<div id="auth_bar" style="display:none;width:100%;position:fixed;height:50px;background:#000;color:#fff;top:0;left:0;z-index:99999999999999"></div>
	<script src="assets/js/wait.js"></script>
	<script src="assets/js/common.js"></script>
	<script>

    var iframe = document.getElementById("myf");
    iframe.onload = function(){
    iframe.style.height = iframe.contentWindow.document.body.scrollHeight + 'px';
	}
		
	var r,k,c,t2,t3,t4,intr=50,currImg
	var rat=1
	var rx=0, t, ext
	var set=false
	function toggleThis(){
		if (document.getElementById('g1').className=='active') {
			document.getElementById('g1').className=''
			document.getElementById('g2').className='active'
		} else {
			document.getElementById('g2').className=''
			document.getElementById('g1').className='active'
		}
	}
	function light_up(obj) {
		if (set===true) return
		kill_all()
		c=obj
		for (var r=1;r<=obj;r++) {
			document.getElementById('r' + r).src='assets/star_on.png'
		}
	}
	function kill_all() {
		var maxId = setTimeout(function(){}, 0);
		for(var i=0; i < maxId; i+=1) { 
			clearTimeout(i);
		}
		for (var r=1;r<=10;r++) {
			document.getElementById('r' + r).src='assets/star_off.png'
			document.getElementById('r' + r).style.opacity='1'
		}
	}
	function light_dn(obj) {
		c=''
		if (set===true) return
		for (r=obj;r>(0);r--) {
			if (obj > rx) {
				t2=setTimeout('dim('+r+','+obj+')',500)
			}
		}
		//light_up(rat)
	}
	
	function dim(r1,obj) {
		intr=0
		if (c) return
		console.log('r'+r1)
		for (k=obj;k>0;k--) {
			intr+=100
			t3=setTimeout('off('+k+')',intr)
		}
	}
	function off(k) {
		console.log('r' + k);
		document.getElementById('r' + k).style.opacity='0.5'
		document.getElementById('r' + k).src='assets/star_off.png'	
		t4=setTimeout('trans('+k+')',250)
	}
	function trans(k) {
		console.log('r' + k);
		document.getElementById('r' + k).style.opacity='1'
	}
	var rate
	var lock=false
	function update_rating(rating) {
		if (lock===true) return false
		rate=rating
		set=(set==false)?true:false
		if (set===false) rating=0
		light_up(rating)
		document.getElementById('rating_status2').innerHTML='<span style=\'font-family:Open Sans;color:#111\'><span style=\'color:red\'>'+rating+'</span></span><br>';
		var url='https://linqstar.com/x_update_rating.php?rating='+rating+'&fan_mid='+getCookie('mid')+'&provider_mid='+qs('provider_mid')
		console.log(url)
		$.ajax({
			url: url,
			success: 
				function(data) {
					set_rating(data.split('|')[0])
				//	kill_all()
					lock=true
				}
			})
		}
		
		function get_rating() {
		$.ajax({
			url: 'https://linqstar.com/x_get_rating.php?mid='+getCookie('mid')+'&rated='+qs('provider_mid'),
			success: 
				function(data) {
					set_rating(data.split('|')[0])
				}
			})
		}
		
		function set_rating(rt) {
			if (lock===true) return false
			for (var i=1;i<=rt;i++) {
				$$('r'+i).src='assets/star_on.png'
			}		
		}
		
		function readit(msg) {
			var msg=''
			msg+=	'<div style="color:grey;font-size:12px"> Please proceed by paying below. Once payment is completed, your providser will be notified and will be connected to you via a live video call right now. Please note that'
			msg+=		'providers have the flexibility to re-schedule call within the next hour, if they are unavailable immediately. All you have to do now is gather your friends around a wait for the text message that will '
			msg+=		'invite you to join the call.<br><br>'
			msg+=		'Please enjoy the call, but at the same time please abide by the terms and conditions regarding etiquette and decency, or use of profanity. If you  make them feel uncomfortable, tehy are well within their right to terminate the session in a most non-refundable manner.'
			msg+=		'<br><br>'
			msg+=		'In short, use your common sense and be nice. It will go real well'
			msg+=		'and you may have a new friend today!'
			msg+=	'</div>'
			$.confirm({
				title: 'Eyes Only',
				content: msg,
				buttons: {
					cancel: function () {
						$.alert('Canceled!');
					},
					somethingElse: {
						text: 'Got It. Proceed to Payment',
						type:'supervan',
						btnClass: 'btn-blue',
						keys: ['enter', 'shift'],
						action: function(){
							$('#notice').hide()
						}
					}
				}
			});
		}
		var fan_mid
		setTimeout(function(){
		//	$$('fan_mid').value=getCookie('mid')
		},1)


		setTimeout(function(){
			$('#auth_bar').show()
			$('#auth_bar').html("<a class='button-1' style='padding:10px;border-radius:0;position:fixed;top:0px;left:0px;height:50px;z-index:99999999999999'>Welcome, " + getCookie('name') + "</a><a href='home.php?hash=" + getCookie('hash') + "' style='position:absolute;right:0'><img src='assets/tab_members_home.png' style='height:50px'></a>")
			$$('auth_bar').style.width=window_width()+'px'
		},2000)
	var url_to_share=''
	var media
	var description
	var text
	var url_pin='https://www.pinterest.com/pin-builder/?url='+url_to_share+'&media='+media+'&description='+description+'&method=button'
	var url_fbk='https://www.facebook.com/sharer/sharer.php?u='+url_to_share
	var url_twi='https://twitter.com/intent/tweet?text='+text+'&url='+url_to_share
	var Window;
	
	function sub_fbk() {
		windowOpen(url_fbk)
	}
	
	function sub_twi() {
		windowOpen(url_twi)
	}
	
	function sub_pin() {
		windowOpen(url_pin)
	}
	
	
	// Function that open the new Window
	function windowOpen(url) {
		Window = window.open(
		  url,"_blank", "width=400, height=450");
	}

		</script>
</body>
</html>
<script src="assets/js/jquery.js"type="text/javascript"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
