/*
	var encrypt=function(r,e){return Aes.Ctr.encrypt(r,e,256)},decrypt=function(r,e){return Aes.Ctr.decrypt(r,e,256)},Aes={cipher:function(r,e){for(var o=e.length/4-1,n=[[],[],[],[]],t=0;t<16;t++)n[t%4][Math.floor(t/4)]=r[t];n=Aes.addRoundKey(n,e,0,4);for(var a=1;a<o;a++)n=Aes.subBytes(n,4),n=Aes.shiftRows(n,4),n=Aes.mixColumns(n,4),n=Aes.addRoundKey(n,e,a,4);n=Aes.subBytes(n,4),n=Aes.shiftRows(n,4),n=Aes.addRoundKey(n,e,o,4);var f=new Array(16);for(t=0;t<16;t++)f[t]=n[t%4][Math.floor(t/4)];return f},keyExpansion:function(r){for(var e=r.length/4,o=e+6,n=new Array(4*(o+1)),t=new Array(4),a=0;a<e;a++){var f=[r[4*a],r[4*a+1],r[4*a+2],r[4*a+3]];n[a]=f}for(a=e;a<4*(o+1);a++){n[a]=new Array(4);for(var c=0;c<4;c++)t[c]=n[a-1][c];if(a%e==0){t=Aes.subWord(Aes.rotWord(t));for(c=0;c<4;c++)t[c]^=Aes.rCon[a/e][c]}else e>6&&a%e==4&&(t=Aes.subWord(t));for(c=0;c<4;c++)n[a][c]=n[a-e][c]^t[c]}return n},subBytes:function(r,e){for(var o=0;o<4;o++)for(var n=0;n<e;n++)r[o][n]=Aes.sBox[r[o][n]];return r},shiftRows:function(r,e){for(var o=new Array(4),n=1;n<4;n++){for(var t=0;t<4;t++)o[t]=r[n][(t+n)%e];for(t=0;t<4;t++)r[n][t]=o[t]}return r},mixColumns:function(r,e){for(var o=0;o<4;o++){for(var n=new Array(4),t=new Array(4),a=0;a<4;a++)n[a]=r[a][o],t[a]=128&r[a][o]?r[a][o]<<1^283:r[a][o]<<1;r[0][o]=t[0]^n[1]^t[1]^n[2]^n[3],r[1][o]=n[0]^t[1]^n[2]^t[2]^n[3],r[2][o]=n[0]^n[1]^t[2]^n[3]^t[3],r[3][o]=n[0]^t[0]^n[1]^n[2]^t[3]}return r},addRoundKey:function(r,e,o,n){for(var t=0;t<4;t++)for(var a=0;a<n;a++)r[t][a]^=e[4*o+a][t];return r},subWord:function(r){for(var e=0;e<4;e++)r[e]=Aes.sBox[r[e]];return r},rotWord:function(r){for(var e=r[0],o=0;o<3;o++)r[o]=r[o+1];return r[3]=e,r},sBox:[99,124,119,123,242,107,111,197,48,1,103,43,254,215,171,118,202,130,201,125,250,89,71,240,173,212,162,175,156,164,114,192,183,253,147,38,54,63,247,204,52,165,229,241,113,216,49,21,4,199,35,195,24,150,5,154,7,18,128,226,235,39,178,117,9,131,44,26,27,110,90,160,82,59,214,179,41,227,47,132,83,209,0,237,32,252,177,91,106,203,190,57,74,76,88,207,208,239,170,251,67,77,51,133,69,249,2,127,80,60,159,168,81,163,64,143,146,157,56,245,188,182,218,33,16,255,243,210,205,12,19,236,95,151,68,23,196,167,126,61,100,93,25,115,96,129,79,220,34,42,144,136,70,238,184,20,222,94,11,219,224,50,58,10,73,6,36,92,194,211,172,98,145,149,228,121,231,200,55,109,141,213,78,169,108,86,244,234,101,122,174,8,186,120,37,46,28,166,180,198,232,221,116,31,75,189,139,138,112,62,181,102,72,3,246,14,97,53,87,185,134,193,29,158,225,248,152,17,105,217,142,148,155,30,135,233,206,85,40,223,140,161,137,13,191,230,66,104,65,153,45,15,176,84,187,22],rCon:[[0,0,0,0],[1,0,0,0],[2,0,0,0],[4,0,0,0],[8,0,0,0],[16,0,0,0],[32,0,0,0],[64,0,0,0],[128,0,0,0],[27,0,0,0],[54,0,0,0]],Ctr:{}};Aes.Ctr.encrypt=function(r,e,o){if(128!=o&&192!=o&&256!=o)return"";r=Utf8.encode(r),e=Utf8.encode(e);for(var n=o/8,t=new Array(n),a=0;a<n;a++)t[a]=isNaN(e.charCodeAt(a))?0:e.charCodeAt(a);var f=Aes.cipher(t,Aes.keyExpansion(t));f=f.concat(f.slice(0,n-16));var c=new Array(16),d=(new Date).getTime(),i=d%1e3,u=Math.floor(d/1e3),A=Math.floor(65535*Math.random());for(a=0;a<2;a++)c[a]=i>>>8*a&255;for(a=0;a<2;a++)c[a+2]=A>>>8*a&255;for(a=0;a<4;a++)c[a+4]=u>>>8*a&255;var s="";for(a=0;a<8;a++)s+=String.fromCharCode(c[a]);for(var h=Aes.keyExpansion(f),C=Math.ceil(r.length/16),v=new Array(C),y=0;y<C;y++){for(var l=0;l<4;l++)c[15-l]=y>>>8*l&255;for(l=0;l<4;l++)c[15-l-4]=y/4294967296>>>8*l;var g=Aes.cipher(c,h),p=y<C-1?16:(r.length-1)%16+1,w=new Array(p);for(a=0;a<p;a++)w[a]=g[a]^r.charCodeAt(16*y+a),w[a]=String.fromCharCode(w[a]);v[y]=w.join("")}var m=s+v.join("");return m=Base64.encode(m)},Aes.Ctr.decrypt=function(r,e,o){if(128!=o&&192!=o&&256!=o)return"";r=Base64.decode(r),e=Utf8.encode(e);for(var n=o/8,t=new Array(n),a=0;a<n;a++)t[a]=isNaN(e.charCodeAt(a))?0:e.charCodeAt(a);var f=Aes.cipher(t,Aes.keyExpansion(t));f=f.concat(f.slice(0,n-16));var c=new Array(8);ctrTxt=r.slice(0,8);for(a=0;a<8;a++)c[a]=ctrTxt.charCodeAt(a);for(var d=Aes.keyExpansion(f),i=Math.ceil((r.length-8)/16),u=new Array(i),A=0;A<i;A++)u[A]=r.slice(8+16*A,8+16*A+16);r=u;var s=new Array(r.length);for(A=0;A<i;A++){for(var h=0;h<4;h++)c[15-h]=A>>>8*h&255;for(h=0;h<4;h++)c[15-h-4]=(A+1)/4294967296-1>>>8*h&255;var C=Aes.cipher(c,d),v=new Array(r[A].length);for(a=0;a<r[A].length;a++)v[a]=C[a]^r[A].charCodeAt(a),v[a]=String.fromCharCode(v[a]);s[A]=v.join("")}var y=s.join("");return y=Utf8.decode(y)};var Base64={code:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(r,e){e=void 0!==e&&e;var o,n,t,a,f,c,d,i,u=[],A="",s=Base64.code;if((c=(d=e?r.encodeUTF8():r).length%3)>0)for(;c++<3;)A+="=",d+="\0";for(c=0;c<d.length;c+=3)n=(o=d.charCodeAt(c)<<16|d.charCodeAt(c+1)<<8|d.charCodeAt(c+2))>>18&63,t=o>>12&63,a=o>>6&63,f=63&o,u[c/3]=s.charAt(n)+s.charAt(t)+s.charAt(a)+s.charAt(f);return i=(i=u.join("")).slice(0,i.length-A.length)+A},decode:function(r,e){e=void 0!==e&&e;var o,n,t,a,f,c,d,i,u=[],A=Base64.code;i=e?r.decodeUTF8():r;for(var s=0;s<i.length;s+=4)o=(c=A.indexOf(i.charAt(s))<<18|A.indexOf(i.charAt(s+1))<<12|(a=A.indexOf(i.charAt(s+2)))<<6|(f=A.indexOf(i.charAt(s+3))))>>>16&255,n=c>>>8&255,t=255&c,u[s/4]=String.fromCharCode(o,n,t),64==f&&(u[s/4]=String.fromCharCode(o,n)),64==a&&(u[s/4]=String.fromCharCode(o));return d=u.join(""),e?d.decodeUTF8():d}},Utf8={encode:function(r){var e=r.replace(/[\u0080-\u07ff]/g,function(r){var e=r.charCodeAt(0);return String.fromCharCode(192|e>>6,128|63&e)});return e=e.replace(/[\u0800-\uffff]/g,function(r){var e=r.charCodeAt(0);return String.fromCharCode(224|e>>12,128|e>>6&63,128|63&e)})},decode:function(r){var e=r.replace(/[\u00e0-\u00ef][\u0080-\u00bf][\u0080-\u00bf]/g,function(r){var e=(15&r.charCodeAt(0))<<12|(63&r.charCodeAt(1))<<6|63&r.charCodeAt(2);return String.fromCharCode(e)});return e=e.replace(/[\u00c0-\u00df][\u0080-\u00bf]/g,function(r){var e=(31&r.charCodeAt(0))<<6|63&r.charCodeAt(1);return String.fromCharCode(e)})}};

	function encrypt(str) {
		var key = [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 ];	
		var textBytes = aesjs.utils.utf8.toBytes(str);
		var aesCtr = new aesjs.ModeOfOperation.ctr(key, new aesjs.Counter(5));
		var encryptedBytes = aesCtr.encrypt(textBytes);
		var encryptedHex = aesjs.utils.hex.fromBytes(encryptedBytes);
		return encryptedHex;
	}

	function decrypt(str) {
		var key = [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 ];
		var encryptedBytes = aesjs.utils.hex.toBytes(str);
		var aesCtr = new aesjs.ModeOfOperation.ctr(key, new aesjs.Counter(5));
		var decryptedBytes = aesCtr.decrypt(encryptedBytes);
		var decryptedText = aesjs.utils.utf8.fromBytes(decryptedBytes);
		return decryptedText;
	}    
*/
function $$(id) {
    return document.getElementById(id)
}

function qs(name, url) {
    if (!url) {
        url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

var wl = window.location.href;
var mob = (window.location.href.indexOf('file://') >= 0);

function setCookie(cname, cvalue) {
    if (mob === true) {
        window.localStorage.setItem(cname, cvalue);
    } else {
        var d = new Date();
        d.setTime(d.getTime() + (1 * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toGMTString();
        document.cookie = cname + "=" + cvalue + "; " + expires;
    }
}

function getCookie(cname) {
    if (mob === true) {
        var cvalue = window.localStorage.getItem(cname);
        return cvalue
    } else {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i].trim();
            if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
        }
        return "";
    }
}

function clear_all() {
    if (mob === true) {
        window.localStorage.clear();
    } else {
        document.cookie.split(";").forEach(function(c) {
            document.cookie = c.replace(/^ +/, "").replace(/=.*/, "=;expires=" + new Date().toUTCString() + ";path=/");
        });
    }
}

function isValidMobile(mob, fmt) {
    if (!mob) return false
    var num = mob.trim();
    num = num.replace(/ /g, '');
    num = num.replace(/\./g, '');
    num = num.replace(/-/g, '');
    num = num.replace(/\(/g, '');
    num = num.replace(/\)/g, '');
    num = num.replace(/\[/g, '');
    num = num.replace(/\]/g, '');
    num = num.replace(/\~/g, '');
    num = num.replace(/\*/g, '');
    num = num.replace(/\{/g, '');
    num = num.replace(/\}/g, '');
    num = num.replace(/\+/g, '');
    if ((num + '').length < 10) return false
    if (isNaN(num)) return false
    code = '1'
    if ((num + '').length == 10) {
        return ('' + code + '' + num + '')
    } else if ((num + '').length == 11) {
        if (num.substr(0, 1) == code) {
            if (fmt === true) {
                return num.substr(0, 1) + ' (' + num.substr(1, 3) + ') ' + num.substr(4, 3) + '-' + num.substr(7, 4)
            } else {
                return num
            }
        } else {
            return false
        }
    } else if ((num + '').length > 11) {
        return false
    }
}
var winH, winW

function screen_dimensions() {
    var h = window.screen.availHeight
    var w = window.screen.availWidth
    return new Array(w, h)
}

function window_height() {
    if (document.body) {
        winH = document.body.offsetHeight;
    }

    if (document.compatMode == 'CSS1Compat' &&
        document.documentElement &&
        document.documentElement.offsetHeight) {
        winH = document.documentElement.offsetHeight;
        return winH
    }

    if (window.innerHeight && window.innerHeight) {
        winH = window.innerHeight;
        return winH;
    }
}

function window_width() {
    if (document.body) {
        winW = document.body.offsetWidth;
    }

    if (document.compatMode == 'CSS1Compat' &&
        document.documentElement &&
        document.documentElement.offsetWidth) {
        winW = document.documentElement.offsetWidth;
        return winW
    }

    if (window.innerWidth && window.innerWidth) {
        winW = window.innerWidth;
        return winW;
    }
}

function process(url) {
    get(url).then(JSON.parse).then(function(response) {
        return response
    })
}

function isValidBrowser() {
    var validBrowser = false
    // Opera 8.0+
    // var isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
    // Firefox 1.0+
    // var isFirefox = typeof InstallTrigger !== 'undefined';
    // Safari 3.0+ "[object HTMLElementConstructor]" 
    validBrowser = /constructor/i.test(window.HTMLElement) || (function(p) {
        return p.toString() === "[object SafariRemoteNotification]";
    })(!window['safari'] || (typeof safari !== 'undefined' && window['safari'].pushNotification));
    // Internet Explorer 6-11
    // var isIE = /*@cc_on!@*/false || !!document.documentMode;
    // Edge 20+
    validBrowser = !isIE && !!window.StyleMedia;
    // Chrome 1 - 79
    validBrowser = !!window.chrome && (!!window.chrome.webstore || !!window.chrome.runtime);
    if (validBrowser === false) {
        alert('Please select a valid browser to use thos product. Valid browsers  currently are: IE, Chrome and Safari')
        return false
    }
}
setTimeout('isValidBrowser', 1)

var video_played = false

function mobileCheck() {
    if (navigator.userAgent.match(/Android/i) ||
        navigator.userAgent.match(/webOS/i) ||
        navigator.userAgent.match(/iPhone/i) ||
        navigator.userAgent.match(/iPad/i) ||
        navigator.userAgent.match(/iPod/i) ||
        navigator.userAgent.match(/BlackBerry/i) ||
        navigator.userAgent.match(/Windows Phone/i)) {
        return true
    } else {
        return false
    }
}

function isIphone() {
    if (navigator.userAgent.match(/iPhone/i)) {
        return true
    } else {
        return false
    }
}

function urlencode(str) {
    str = (str + '')
    return encodeURIComponent(str)
        .replace(/!/g, '%21')
        .replace(/'/g, '%27')
        .replace(/\(/g, '%28')
        .replace(/\)/g, '%29')
        .replace(/\*/g, '%2A')
        .replace(/~/g, '%7E')
        .replace(/%20/g, '+')
}