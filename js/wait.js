	var wait_ldr, wait_img, wait_txt
	var op = 1

	function wait_light(txt) {
	    wait3(txt, 0.8)
	}

	function wait(txt) {
	    wait2(txt, 0.8)
	}

	function wait(txt) {
	    wait3(txt)
	}

	function wait3(txt, op) {
	    $('.wait_cnt').remove()
	    if (!op) op = 1
	    if (!txt) txt = 'Loading...'
	    wait_cnt = document.createElement('div')
	    wait_ldr = document.createElement('div')
	    wait_txt = document.createElement('div')
	    wait_ldr.id = 'wait_ldr'
	    wait_txt.id = 'wait_txt'
	    wait_cnt.className = 'wait_cnt'
	    wait_cnt.style.cssText = 'width:300px!Important;max-width:70%; margin:auto; z-index:99999999999999999;position:fixed;top:0;left:0;right:0;bottom:0;'
	    wait_ldr.style.cssText = 'width:300px!Important;max-width:90%;background:#fff;display:none;z-index:99999999999999999;position:fixed;top:0;left:0;right:0;bottom:0;height:65px;opacity:1;margin:auto;background:#fff;border-radius:12px;opacity:1'
	    wait_txt.style.cssText = 'width:300px!Important;max-width:90%;display:none;opacity:1;left:75px;opacity:1;margin:auto;height:50px;text-align:left;margin-top:5px'
	    wait_txt.innerHTML = '<div style="font-size:1" id="loader_txt"><img style="width:50px;margin-top:15px;position:absolute" src="assets/loading.gif"><span style="position:absolute;left:60px;top:25px;font-family:Open Sans;font-size:15px"><b>' + txt + '</b></span></div>'
	    wait_cnt.appendChild(wait_ldr)
	    wait_ldr.appendChild(wait_txt)
	    document.documentElement.appendChild(wait_cnt)
	    wait_ldr.style.display = ''
	    wait_txt.style.display = ''
	    setTimeout(function() {
	        $('.wait_cnt').remove()
	    }, 60000)
	}

	function wait5(txt, op) {
	    $('.wait_cnt').remove()
	    if (!op) op = 1
	    if (!txt) txt = 'Loading...'
	    wait_cnt = document.createElement('div')
	    wait_ldr = document.createElement('div')
	    wait_txt = document.createElement('div')
	    wait_ldr.id = 'wait_ldr'
	    wait_txt.id = 'wait_txt'
	    wait_cnt.className = 'wait_cnt'
	    wait_cnt.style.cssText = 'width:300px;max-width:70%;margin:auto; z-index:99999999999999999;position:fixed;top:0;left:0;right:0;bottom:0;'
	    wait_ldr.style.cssText = 'width:300px;max-width:90%;background:#fff;display:none;z-index:99999999999999999;position:fixed;top:0;left:0;right:0;bottom:0;height:100%;opacity:1;margin:auto;background:#f0f0f0;border-radius:0;font-family:Open Sans;opacity:0.95;width:100%;max-width:100%;'
	    wait_txt.style.cssText = 'width:300px;max-width:90%;display:none;margin:auto;height:50px;text-align:center;margin-top:-15px'
	    wait_txt.innerHTML = '<div style="width:300px;max-width:100%font-size:12px;height:30%;font-weight:100!Important;margin:auto;position:fixed;top:0;bottom:0;left:0;right:0;text-align:center;font-family:Open Sans Condensed" id="loader_txt"><img style="width:100px;height:100px;margin:auto;margin-left:0px;;box-shadow:inset 5px,10px,20px #000;border-radius:100%" src="https://sugarmatch.com/mobile/images/ye.gif"><br><br>LOADING ... PLEASE WAIT</div>'
	    wait_cnt.appendChild(wait_ldr)
	    wait_ldr.appendChild(wait_txt)
	    document.documentElement.appendChild(wait_cnt)
	    wait_ldr.style.display = ''
	    wait_txt.style.display = ''
	    setTimeout(function() {
	        $('.wait_cnt').remove()
	    }, 60000)
	    setTimeout(function() {
	        hide()
	    }, 5000)
	}

	function wait2(txt, op) {
	    if (!op) op = 0.9
	    wait_ldr = document.createElement('div')
	    wait_txt = document.createElement('div')
	    wait_img = document.createElement('img')
	    wait_ldr.id = 'wait_ldr'
	    wait_txt.id = 'wait_txt'
	    wait_img.id = 'wait_img'
	    wait_ldr.style.cssText = 'display:none;z-index:99999999999999999;position:fixed;top:0;left:0;right:0;bottom:0;opacity:' + op + ';margin:auto;background:none;font-family:Open Sans Condensed;width:90%;max-width:90%;'
	    wait_img.style.cssText = 'display:none;z-index:999999999999999999;position:fixed;top:0;left:0;right:0;bottom:0;opacity:1;margin:auto;'
	    wait_img.src = 'js/loading.gif'
	    wait_txt.style.cssText = 'display:none;opacity:1;margin:auto;width:90%;height:50px;text-align:center;width:90%;max-width:90%;'
	    document.documentElement.appendChild(wait_ldr)
	    document.documentElement.appendChild(wait_img)
	    document.documentElement.appendChild(wait_txt)
	    if (!txt) var txt = ''
	    else {
	        wait_txt.innerHTML = ''
	    }
	    wait_ldr.style.display = ''
	    wait_img.style.display = ''
	    wait_txt.style.display = ''
	    setTimeout(function() {
	        hide()
	    }, 5000)
	}

	function waitx(txt) {
	    var di = document.createElement('div')
	    var si = document.createElement('span')
	    var mi = document.createElement('img')
	    di.style.cssText = 'position:fixed;z-index:9999999999;left:0;right:0;margin:auto;width:80%;top:5px; text-align: center'
	    si.style.cssText = 'font-size:12px; font-family:Open Sans; color: #333'
	    mi.style.cssText = 'width:50px'
	    mi.src = 'images/data.gif'
	    si.innerHTML = txt
	    di.appendChild(mi)
	    di.appendChild(si)
	    di.id = 'waitx'
	    di.className = 'wait_cnt'
	    document.documentElement.appendChild(di)
	    setTimeout(function() {
	        hide()
	    }, 5000)
	}

	function waitz(txt, el) {
	    var di = document.createElement('div')
	    var si = document.createElement('span')
	    var mi = document.createElement('img')
	    di.style.cssText = 'position:absolute;left:0;right:0;margin:auto;width:275px;top:50%; height:70px; border-radius:6px; text-align: center;background:#fff'
	    si.style.cssText = 'font-size:14px; font-family:Open Sans; color: #333;margin:auto;margin-top:25px;position:absolute;right:20px'
	    mi.style.cssText = 'width:70px;margin:auto;margin-top:5px;position:absolute;left:20px'
	    mi.src = 'https://mobile.sugarmatch.com/images/data.gif'
	    si.innerHTML = txt
	    di.appendChild(mi)
	    di.appendChild(si)
	    di.id = 'waitz'
	    el.appendChild(di)
	}

	function hide() {
	    $('.wait_cnt').hide()
	    $('.wait_cnt').remove()
	}

	function showerror(msg) {
	    $.alert({
	        title: '',
	        theme: 'supervan',
	        content: '<div style="color:white!Important;width:100%!Important;font-size:16px">' + msg + '</div>',
	        onClose: function() {
	            showerror(msg)
	        }
	    })
	}

	function $$(objID) {
	    return document.getElementById(objID)
	}