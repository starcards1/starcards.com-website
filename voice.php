<?php 
require_once 'vendor/autoload.php';
use Twilio\Rest\Client;
use Twilio\TwiML\VoiceResponse;
require_once "../class/utils.class.php";
$c=new utils;
$c->connect("199.91.65.83","voxeo");
$from=$_GET['from'];
$to=$_GET['to'];
$to_mobile=$_GET['to_mobile'];
$to_email=$_GET['to_email'];
$card_id=$_GET['card_id'];
//$message="Hello $to! And congratulations!! We are pleased to inform you that you have received a LinqStar Greeting from $from! To access this greeting, please check your email and text messages. By the way, are you aware that you can convey your thanks to $from played out via an automated phone call made to $from on your behalf? If so, in order to record this thank you note, please start speaking after the beep. When finished, just hang up!";
$message="Hello $to! And congratulations!! We are pleased to inform you that you have received a LinqStar Greeting from $from! To access this greeting, please check your email and text messages.";
$created=time();
$sql="INSERT INTO `voxeo`.`greeting_vox_logs` ( 
		`card_id`,
		`to_mobile`,
		`to`,
		`from`,
		`created`,
		`status`)
	VALUES
	   ('$card_id',
		'$to_mobile',
		'$to',
		'$from',
		'$created',
		'9')";
$vox_id=$c->insert($sql);
$c->close();
header('Content-Type: application/xml');
$response = new VoiceResponse();
$response->say($message);
//$response->play("https://linqstar.com/assets/beep.mp3");
//$gather = $response->gather(['action' => "/completed.php?card_id=$card_id&to=$to&from=$from&to_mobile=$to_mobile&to_email=$to_email",'method' => 'GET', 'input'=>'speech','timeout'=>3,'enhanced'=>'true','speech_model'=>'phone']);
echo $response;
