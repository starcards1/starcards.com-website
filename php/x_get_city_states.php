<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
include "utils.class.php";
$c=new utils;
$c->connect(); 
$state=$_GET['state'];
$sql="SELECT
	a.city,a.state,
	count( a.city ) AS 'cnt',
	b.lat,
	b.lng 
FROM
	`new_sdfm`.`dt_members` `a`, `new_sdfm`.`geo_city` `b` 
WHERE
	a.state = '$state' 
	AND a.state = b.state 
	AND a.city = b.city 
GROUP BY
	city 
ORDER BY
	`city` ASC";
$ct=$c->query($sql); 
for ($i=0;$i<count($ct); $i++) {
	$arr=$c->cityStateToLatLng(trim(strtolower($ct[$i]['city'])),strtolower($state));
	$data[]=array("city"=>$ct[$i]['city'],"state"=>$ct[$i]['state'],"lat"=>$arr[0],"lng"=>$arr[1],"cnt"=>$ct[$i]['cnt']);
}  
echo json_encode($data);
$c->close();
