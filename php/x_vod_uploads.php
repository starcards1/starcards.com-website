<?php
$ds = DIRECTORY_SEPARATOR;  //1
$storeFolder = 'uploads';   //2
$storeThumbFolder = 'thumbs';   //2
$mid=$_POST['provider_mid'];
include "../assets/wi/WideImage.php";
include "../../class/utils.class.php";
$u=new utils;
$u->connect();
if (!empty($_FILES)) {
 	$tempFile = $_FILES['file']['tmp_name'];    	//3             
	$ex=explode(".",$_FILES['file']['name']);
	$ext=$ex[count($ex)-1];
	$title=str_replace("." . $ext,"",$_FILES['file']['name']);
	$targetFileName=$_POST['attach_id'] . ".$ext";
	if ($ext=="wav") $type="audio";
	$targetThumbName = "t-" . $targetFileName;
	$mime=mime_content_type($tempFile);
    $targetPath = "/sites/home/linqstar/public_html/" . $storeFolder . $ds;  //4
    $targetThumbPath = "/sites/home/linqstar/public_html/" . $storeThumbFolder . $ds;  //4
	$targetFile =  $targetPath . $targetFileName;  //
	if(strstr($mime, "image/")){
		$targetThumb =  $targetThumbPath . $targetThumbName;  // 
		$msg=$targetFile;
		$fname=$_FILES['file']['name'];
		$size = getimagesize($tempFile);
		$w=$size[0];
		$h=$size[1];
		if ($w>$h) {
			$w=$h;
		} else {
			$h=$w;
		}
		WideImage::load($tempFile)->crop('center','middle',$w,$h)->saveToFile($targetFile);
		WideImage::load($targetFile)->resize(400,400)->saveToFile($targetThumb);
		$type="photo";
	} else {
		if(strstr($mime, "video/")){
			$type="video";
			$op=shell_exec("ffprobe -v quiet -print_format json -show_format -show_streams $tempFile");
			$width=trim(explode(",", explode('"width": ',$op)[1])[0]);
			$height=trim(explode(",", explode('"height": ',$op)[1])[0]);
			echo shell_exec("ffmpeg -i $tempFile -i wm.png -filter_complex '[1:v] scale=$width:-1 [ol], [0:v] [ol] overlay=W-w:H-h' -codec:a copy $targetFile");
		} else if (strstr($mime, "audio/")){
			$type="audio";
			move_uploaded_file($tempFile,$targetFile);
		}
	}
}
	$order_id=$_POST['order_id'];
	$fan_mid=$_POST['fan_mid'];
	$in_co=$_POST['in_collections'];
	$channel_id=$_POST['channel_id'];
	$exists=$u->query("select media_id,media_in_channels from celeb_media where mid=$mid and title=$title limit 1");
	if ($exists) {
		$cid=$exists[0]['media_in_channels'] . "," . $channel_id;
		$sql="	INSERT INTO `voxeo`.`celeb_media` (`media_filename`, `mid`, `media_title`, `media_type`,`media_in_collections`,`media_in_channels`,`media_tags`)
				VALUES
				(
					'$targetFileName',
					'$mid',
					'$title',
					'$type',
					'$in_co',
					'$cid',
					'$type,$ext'
				)
		";
	} else {
		$sql="	INSERT INTO `voxeo`.`celeb_media` (`media_filename`, `mid`, `media_title`, `media_type`,`media_in_collections`,`media_in_channels`,`media_tags`)
				VALUES
				(
					'$targetFileName',
					'$mid',
					'$title',
					'$type',
					'$in_co',
					'$channel_id',
					'$type,$ext'
				)
		";
	}
	//Create basic Asset
	$media_id=$u->insert($sql);

	//Update Channel Table
	$ca=$u->query("select channel_asset_ids from `voxeo`.`celeb_channels` where channel_id=$channel_id");
	if (!$ca) {
		$channel_asset_ids=$media_id;
	} else {
		$channel_asset_ids=$ca[0]['channel_asset_ids'] . "," . $media_id;
	}
	//Update Channels
	$u->insert("update `voxeo`.`celeb_channels` set `channel_asset_ids`='$channel_asset_ids' where channel_id=$channel_id");
	$status="created";
	$created_date=date("Y-m-d H:i:s");
	$token=md5($order_id);
	$weblink="https://linqstar.com/view/$token";
	//Update Orders Table
	if ($order_id) {
		$sql="
		UPDATE 
			`voxeo`.`jobs` 
		SET 
			`media_id` = $media_id,
			`targetFileName` = '$targetFileName',
			`weblink` = '$weblink',
			`token` = '$token',
			`asset_type` = '$type',
			`asset_sub_type` = '$asset_sub_type',
			`completed_date` = '$created_date' 
		WHERE
			`order_id` = $order_id";
		$u->insert($sql);
		//Update Orders Table
		$sql="update `voxeo`.`orders` set (`order_status`='UPLOADED') where `id`='$order_id'";
		$u->insert($sql);
	}
	$sql1="INSERT INTO `voxeo`.`celeb_channel_content` ( `provider_mid`, `channel_id`, `content_id`, `content_asset_id`, `content_type_id` )
		VALUES
		(
			$mid,
			$channel_id,
			$media_id,
			0,
			999
		)";		
		$u->insert($sql1);
		
		//Close and bug-out
		$u->close();
?>  