<?php
use \PhpPot\Service\StripePayment;
require_once "config.php";
require_once "../../class/utils.class.php";
$c=new utils;
if (!empty($_POST["token"])) {
    require_once 'StripePayment.php';
    $stripePayment = new StripePayment();
    try {
    $stripeResponse = $stripePayment->chargeAmountFromCard($_POST);
	} catch($e) {
		echo $e;
	}
    $c->show($_POST);
    require_once "DBController.php";
    $dbController = new DBController();
    
    $amount = $stripeResponse["amount"] /100;
    
    $param_type = 'ssdssss';
    $param_value_array = array(
        $_POST['email'],
        $_POST['item_number'],
        $amount,
        $stripeResponse["currency"],
        $stripeResponse["balance_transaction"],
        $stripeResponse["status"],
        json_encode($stripeResponse)
    );
    $query = "INSERT INTO tbl_payment (email, item_number, amount, currency_code, txn_id, payment_status, payment_response) values (?, ?, ?, ?, ?, ?, ?)";
    $id = $dbController->insert($query, $param_type, $param_value_array);
    
    if ($stripeResponse['amount_refunded'] == 0 && empty($stripeResponse['failure_code']) && $stripeResponse['paid'] == 1 && $stripeResponse['captured'] == 1 && $stripeResponse['status'] == 'succeeded') {
        $successMessage = "Stripe payment is completed successfully. The TXN ID is " . $stripeResponse["balance_transaction"];
    }
}
?>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Voxeo - Checkout</title>
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/css/all.min.css">
    <link rel="stylesheet" href="../assets/css/animate.css">
    <link rel="stylesheet" href="../assets/css/nice-select.css">
    <link rel="stylesheet" href="../assets/css/owl.min.css">
    <link rel="stylesheet" href="../assets/css/jquery-ui.min.css">
    <link rel="stylesheet" href="../assets/css/magnific-popup.css">
    <link rel="stylesheet" href="../assets/css/flaticon.css">
    <link rel="stylesheet" href="../assets/css/main.css">
	<link href="style.css" rel="stylesheet" type="text/css"/ >
    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon">
</head>
<body>
    <div class="account-section bg_img" style="background:url(../assets/images/account-bg.jpg)">
        <div class="container">
             <img style="width:200px;margin:auto;left:0;right:0;position:absolute" src="../assets/images/logo2.png" alt="logo">
             <div class="account-wrapper" style="margin-top:200px">
                <div class="account-body">
				<?php if(!empty($successMessage)) { ?>
					<div id="success-message"><?php echo $successMessage; ?></div>
				<?php  } ?>
					<div id="error-message"></div>
					<form id="frmStripePayment" action=""
						method="post">
						<div class="field-row">
							<label>Card Holder Name</label> <span
								id="card-holder-name-info" class="info"></span><br>
							<input type="text" id="name" name="name"
								class="demoInputBox">
						</div>
						<div class="field-row">
							<label>Card Number</label> <span
								id="card-number-info" class="info"></span><br> <input
								type="text" id="card-number" name="card-number"
								class="demoInputBox">
						</div>
						<div class="row">
							<div class="col-sm-4" style="max-width:33%">
								<label>Month</label>
							</div>
							<div class="col-sm-4" style="max-width:33%">
								<label>Year</label>
							</div>
							<div class="col-sm-4" style="max-width:33%">
								<label>CVV</label>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-4" style="max-width:33%">
								<select name="month" id="month"
									class="demoSelectBox">
									<option value="08">08</option>
									<option value="09">9</option>
									<option value="10">10</option>
									<option value="11">11</option>
									<option value="12">12</option>
								</select>
							</div>
							<div class="col-sm-4" style="max-width:33%">
								<select name="year" id="year"
									class="demoSelectBox">
									<option value="18">2018</option>
									<option value="19">2019</option>
									<option value="20">2020</option>
									<option value="21">2021</option>
									<option value="22">2022</option>
									<option value="23">2023</option>
									<option value="24">2024</option>
									<option value="25">2025</option>
									<option value="26">2026</option>
									<option value="27">2027</option>
									<option value="28">2028</option>
									<option value="29">2029</option>
									<option value="30">2030</option>
								</select>
							</div>
							<div class="col-sm-4" style="max-width:33%">
								<input type="text" name="cvc" id="cvc" class="demoInputBox cvv-input">
							</div>
						</div>
						<div class="field-row">
							<label>Email</label> <span id="email-info"
								class="info"></span><br> <input type="text"
								id="email" name="email" class="demoInputBox">
						</div>						
						<div class="row">
							<div class="col-md-12">
								<span id="cvv-info" class="info"></span><br>
								<input type="submit" name="pay_now" style="height:55px;width:200px" value="Submit"
									id="submit-btn" class="button-4"
									onClick="stripePay(event);">
							</div>
						</div>
						<div id="loader">
							<img alt="loader" src="LoaderIcon.gif">
						</div>
						<input type='hidden' name='amount' value='<?=$_GET['cost']*1/100; ?>'> <input
							type='hidden' name='currency_code' value='USD'> <input
							type='hidden' name='item_name' value='<?=$_GET['name'];?>'>
						<input type='hidden' name='item_number'
							value='PHPPOTEG#1'>
					</form>
				</div>
			</div>
		</div>
	</div>
     <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script src="vendor/jquery/jquery-3.2.1.min.js"
        type="text/javascript"></script>
    <script>
function cardValidation () {
    var valid = true;
    var name = $('#name').val();
    var email = $('#email').val();
    var cardNumber = $('#card-number').val();
    var month = $('#month').val();
    var year = '20'+$('#year').val()+'';
    var cvc = $('#cvc').val();
	alert(month)
	alert(year)
    $("#error-message").html("").hide();

    if (name.trim() == "") {
        valid = false;
    }
    if (email.trim() == "") {
    	   valid = false;
    }
    if (cardNumber.trim() == "") {
    	   valid = false;
    }

    if (month.trim() == "") {
    	    valid = false;
    }
    if (year.trim() == "") {
        valid = false;
    }
    if (cvc.trim() == "") {
        valid = false;
    }

    if(valid == false) {
        $("#error-message").html("All Fields are required").show();
    }

    return valid;
}
//set your publishable key
Stripe.setPublishableKey("<?php echo STRIPE_PUBLISHABLE_KEY; ?>");

//callback to handle the response from stripe
function stripeResponseHandler(status, response) {
    if (response.error) {
        //enable the submit button
        $("#submit-btn").show();
        $( "#loader" ).css("display", "none");
        //display the errors on the form
        $("#error-message").html(response.error.message).show();
    } else {
        //get token id
        var token = response['id'];
        //insert the token into the form
        $("#frmStripePayment").append("<input type='hidden' name='token' value='" + token + "' />");
        //submit form to the server
        $("#frmStripePayment").submit();
    }
}
function stripePay(e) {
    e.preventDefault();
    var valid = cardValidation();

    if(valid == true) {
        $("#submit-btn").hide();
        $( "#loader" ).css("display", "inline-block");
        Stripe.createToken({
            number: $('#card-number').val(),
            cvc: $('#cvc').val(),
            exp_month: $('#month').val(),
            exp_year: '20'+$('#year').val()+''
        }, stripeResponseHandler);

        //submit from callback
        return false;
    }
}
</script>
</body>
</html>