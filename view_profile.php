<?php
	require_once "../class/utils.class.php";
	$c=new utils;
	$c->connect("199.91.65.83","voxeo");
	parse_str(http_build_query($_GET));
	$pro=$c->query("select * from celeb_profiles where mid=$provider_mid");
	foreach($pro[0] as $key => $value) {
		${$key}=$value;
	}

	
	$ta=$c->query("select * from celeb_tags where pid=$pid");
	$t=explode(",",$ta[0]['tags']);
?>
<script src="assets/js/jquery.js"type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js"type="text/javascript"></script>
<script src="assets/js/utils.js"type="text/javascript"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script> 

<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Voxeo - Checkout</title>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="assets/css/all.min.css"/>
    <link rel="stylesheet" href="assets/css/animate.css"/>
    <link rel="stylesheet" href="assets/css/nice-select.css"/>
    <link rel="stylesheet" href="assets/css/owl.min.css"/>
    <link rel="stylesheet" href="assets/css/jquery-ui.min.css"/>
    <link rel="stylesheet" href="assets/css/magnific-popup.css"/>
    <link rel="stylesheet" href="assets/css/flaticon.css"/>
    <link rel="stylesheet" href="assets/css/main.css"/>
	<link href="style.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon"/>
</head>
<style>
h1,h2,h3,h4,h5,h6{
	font-weight:300;
	text-transform:upperCase;
	margin:5px
}
.tags a {
	display: inline-block;
	height: 21px;
	margin: 0 10px 0 0;
	padding: 0 7px 0 14px;
	white-space: nowrap;
	position: relative;

	background: -moz-linear-gradient(top, #fed970 0%, #febc4a 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#fed970), color-stop(100%,#febc4a));
	background: -webkit-linear-gradient(top, #fed970 0%,#febc4a 100%);
	background: -o-linear-gradient(top, #fed970 0%,#febc4a 100%);
	background: linear-gradient(to bottom, #fed970 0%,#febc4a 100%);
	background-color: #FEC95B;

	color: #963;
	font: bold 11px/21px Arial, Tahoma, sans-serif; 
	text-decoration: none;
	text-shadow: 0 1px rgba(255,255,255,0.4);

	border-top: 1px solid #EDB14A;
	border-bottom: 1px solid #CE922E;
	border-right: 1px solid #DCA03B;
	border-radius: 1px 3px 3px 1px;
	box-shadow: inset 0 1px #FEE395, 0 1px 2px rgba(0,0,0,0.21)!Important;
}
a:before { 
  content: '';  
  position: absolute;  
  top: 5px;  
  left: -6px;  
  width: 10px;  
  height: 10px;  
  
  background: -moz-linear-gradient(45deg, #fed970 0%, #febc4a 100%);  
  background: -webkit-gradient(linear, left bottom, right top, color-stop(0%,#fed970), color-stop(100%,#febc4a));  
  background: -webkit-linear-gradient(-45deg, #fed970 0%,#febc4a 100%);  
  background: -o-linear-gradient(45deg, #fed970 0%,#febc4a 100%);  
  background: linear-gradient(135deg, #fed970 0%,#febc4a 100%);  
  background-color: #FEC95B;  
  
  border-left: 1px solid #EDB14A;  
  border-bottom: 1px solid #CE922E;  
  border-radius: 0 0 0 2px;  
  box-shadow: inset 1px 0 #FEDB7C, 0 2px 2px -2px rgba(0,0,0,0.33);  
}  
a:before { 
  -webkit-transform: scale(1, 1.5) rotate(45deg);  
  -moz-transform: scale(1, 1.5) rotate(45deg);  
  -ms-transform: scale(1, 1.5) rotate(45deg);  
  transform: scale(1, 1.5) rotate(45deg);  
} 
a:after { 
  content: '';  
  position: absolute;  
  top: 7px;  
  left: 1px;  
  width: 5px;  
  height: 5px;  
  background: #FFF;  
  border-radius: 4px;  
  border: 1px solid #DCA03B;  
  box-shadow: 0 1px 0 rgba(255,255,255,0.2), inset 0 1px 1px rgba(0,0,0,0.21);  
}  
</style>

<body  style="background:url(assets/images/account-bg.jpg)no-repeat;background-size:cover">
	    <img style="width:200px;margin:auto;left:0;right:0;position:absolute" src="assets/images/logo2.png" alt="logo">
        <br>
		<div id="notice" class="container text-center">
            <div class="account-wrapper" style="margin-top:200px;padding:25px">
                <div class="account-body">
					<img src="<?=$photo;?>" style="height:200px;width:200px;border-radius:200px">
					<br><br>
					<h5><?=$name;?></h5>
					<h6 style="font-size:0.8em;color:#000">A <?=$provider_type;?></h6>
					<? $sn=explode(",",$service_names); ?>
					<? $sc=explode(",",$service_costs); ?>
					<div class="container">
						<div class="row">
							<? for  ($n=0;$n<count($sn);$n++) { ?>
								<div class="col-md-6">
									<h4 style="color:#000">$ <?=(!$sc[$n])?$sc[$n-1]:$sc[$n];?></h4>
									<h6 class="button-5" style="color:maroon;font-size:14px;color:white;max-width:50%;min-width:100%;word-wrap:break-word;padding-left:2px;padding-right:2px;margin-top:5px"><?=$sn[$n];?></h6>
								</div>
							<? } ?>
						</div>
					</div>
					<br/>
		<div id="notice" class="container text-center">
						<div id="star" style="text-align:center">
							<span id='1'><img style="width:20px" id='r1' onclick="update_rating(1)" class="star" onmouseover='light_up(1)' onmouseout='light_dn(1)' src="https://linqstar.com/assets/star_off.png"></span>
							<span id='2'><img style="width:20px" id='r2' onclick="update_rating(2)" class="star" onmouseover='light_up(2)' onmouseout='light_dn(2)' src="https://linqstar.com/assets/star_off.png"></span>
							<span id='3'><img style="width:20px" id='r3' onclick="update_rating(3)" class="star" onmouseover='light_up(3)' onmouseout='light_dn(3)' src="https://linqstar.com/assets/star_off.png"></span>
							<span id='4'><img style="width:20px" id='r4' onclick="update_rating(4)" class="star" onmouseover='light_up(4)' onmouseout='light_dn(4)' src="https://linqstar.com/assets/star_off.png"></span>
							<span id='5'><img style="width:20px" id='r5' onclick="update_rating(5)" class="star" onmouseover='light_up(5)' onmouseout='light_dn(5)' src="https://linqstar.com/assets/star_off.png"></span>
							<span id='6'><img style="width:20px" id='r6' onclick="update_rating(6)" class="star" onmouseover='light_up(6)' onmouseout='light_dn(6)' src="https://linqstar.com/assets/star_off.png"></span>
							<span id='7'><img style="width:20px" id='r7' onclick="update_rating(7)" class="star" onmouseover='light_up(7)' onmouseout='light_dn(7)' src="https://linqstar.com/assets/star_off.png"></span>
							<span id='8'><img style="width:20px" id='r8' onclick="update_rating(8)" class="star" onmouseover='light_up(8)' onmouseout='light_dn(8)' src="https://linqstar.com/assets/star_off.png"></span>
							<span id='9'><img style="width:20px" id='r9' onclick="update_rating(9)" class="star" onmouseover='light_up(9)' onmouseout='light_dn(9)' src="https://linqstar.com/assets/star_off.png"></span>
							<span id='10'><img style="width:20px" id='r10' onclick="update_rating(10)" class="star" onmouseover='light_up(10)' onmouseout='light_dn(10)' src="https://linqstar.com/assets/star_off.png"></span>
							 
						</div>
						<div id="cv" style="width:320px;height:100px;position:absolute;z-index:9999999999999999999;background:none;margin-top:-50px;display:none"></div>
						<br>
						<div id="rating_status2" style="font-size:0.8em"></div>
						<span id="rating_status1"></span>
						<span></span></div>
					</div>	
<br>					
					<div class="button-1">	
						<? for ($v=1; $v<count($t)-1; $v++) { ?>
							<a style="border-radius:4px;margin:3px;padding:2px;font-weight:lighter;font-size:10px;padding-left:5px;padding-right:5px" class="button-3" href="#"><?=$t[$v];?></a>  
						<? } ?>
					</div>
					<br/><br/>					
				</div>
				<h3>PUBLIC CHANNEL</h3>
				<a href="javascript:subscribe_public()" class="button-4">Subscribe</a>
				<h3>PRIVATE CHANNELs</h3>

			</div>
		</div>
	<div id="auth_bar" style="display:none;width:100%;position:fixed;height:50px;background:#000;color:#fff;top:0;left:0;z-index:99999999999999"></div>
	<script src="assets/js/wait.js"></script>
	<script src="assets/js/common.js"></script>
	<script>
	var r,k,c,t2,t3,t4,intr=50,currImg
	var rat=1
	var rx=0, t, ext, rtx
	rtx=1
	var set=false
	function toggleThis(){
		if (document.getElementById('g1').className=='active') {
			document.getElementById('g1').className=''
			document.getElementById('g2').className='active'
		} else {
			document.getElementById('g2').className=''
			document.getElementById('g1').className='active'
		}
	}
	function light_up(obj) {
		if (set===true) return
		kill_all()
		c=obj
		for (var r=1;r<=obj;r++) {
			document.getElementById('r' + r).src='assets/star_on.png'
		}
	}
	function kill_all() {
		var maxId = setTimeout(function(){}, 0);
		for(var i=0; i < maxId; i+=1) { 
			clearTimeout(i);
		}
		for (var r=1;r<=10;r++) {
			document.getElementById('r' + r).src='assets/star_off.png'
			document.getElementById('r' + r).style.opacity='1'
		}
	}
	function light_dn(obj) {
		c=''
		if (set===true) return
		for (r=obj;r>(0);r--) {
			if (obj > rx) {
				t2=setTimeout('dim('+r+','+obj+')',500)
			}
		}
		light_up(rat)
		get_rating()
	}
	
	function dim(r1,obj) {
		intr=0
		if (c) return
		console.log('r'+r1)
		for (k=obj;k>0;k--) {
			intr+=100
			t3=setTimeout('off('+k+')',intr)
		}
		get_rating()
	}
	function off(k) {
		console.log('r' + k);
		document.getElementById('r' + k).style.opacity='0.5'
		document.getElementById('r' + k).src='assets/star_off.png'	
		t4=setTimeout('trans('+k+')',250)
	}
	function trans(k) {
		console.log('r' + k);
		document.getElementById('r' + k).style.opacity='1'
	}
	var rate
	var lock=false
	function update_rating(rating) {
		$$('cv').style.display=''
		if (lock===true) return false
		set_rating(rating)
		var url='https://linqstar.com/x_update_rating.php?rating='+rating+'&fan_mid='+getCookie('mid')+'&provider_mid='+qs('provider_mid')
		console.log(url)
		$.ajax({
			url: url,
			success: 
				function(data) {
					rtx=data.split('|')[0]
					ctx=data.split('|')[1]
					document.getElementById('rating_status2').innerHTML='<div style="margin-top:-25px"><span style=\'font-family:Open Sans;color:#111;font-size:0.8em\'><b>Rating</b>: <span style=\'color:red\'>' + Math.round(rtx,2) + '</span> <b>Votes</b>: <span style=\'color:red\'>'+ctx+'</span></span></div>';
					//kill_all()
					setTimeout(function(){
						set_rating(rtx)
						setTimeout(function(){
							lock=true
						},1000)
					},1000)
				}
			})
		}
		
	function get_rating() {
		$.ajax({
			url: 'https://linqstar.com/x_get_rating.php?provider_mid='+qs('provider_mid'),
			success: 
				function(data) {
					rtx=data.split('|')[0]
					ctx=data.split('|')[1]
					set_rating(rtx)
					document.getElementById('rating_status2').innerHTML='<div style="margin-top:-25px"><span style=\'font-family:Open Sans;color:#111;font-size:0.8em\'><b>Rating</b>: <span style=\'color:red\'>' + Math.round(rtx,2) + '</span> <b>Votes</b>: <span style=\'color:red\'>'+ctx+'</span></span></div>';
				}
			})
		}
		
		function set_rating(rt) {
			for (var i=1;i<=rt;i++) {
				$$('r'+i).src='assets/star_set.png'
			}		
				if (lock===true) kill_all()
		}
		get_rating()

		function readit(msg) {
			var msg=''
			msg+=	'<div style="color:grey;font-size:12px"> Please proceed by paying below. Once payment is completed, your providser will be notified and will be connected to you via a live video call right now. Please note that'
			msg+=		'providers have the flexibility to re-schedule call within the next hour, if they are unavailable immediately. All you have to do now is gather your friends around a wait for the text message that will '
			msg+=		'invite you to join the call.<br><br>'
			msg+=		'Please enjoy the call, but at the same time please abide by the terms and conditions regarding etiquette and decency, or use of profanity. If you  make them feel uncomfortable, tehy are well within their right to terminate the session in a most non-refundable manner.'
			msg+=		'<br><br>'
			msg+=		'In short, use your common sense and be nice. It will go real well'
			msg+=		'and you may have a new friend today!'
			msg+=	'</div>'
			$.confirm({
				title: 'Eyes Only',
				content: msg,
				buttons: {
					cancel: function () {
						$.alert('Canceled!');
					},
					somethingElse: {
						text: 'Got It. Proceed to Payment',
						type:'supervan',
						btnClass: 'btn-blue',
						keys: ['enter', 'shift'],
						action: function(){
							$('#notice').hide()
						}
					}
				}
			});
		}
		var fan_mid
		setTimeout(function(){
		//	$$('fan_mid').value=getCookie('mid')
		},1)


		setTimeout(function(){
			$('#auth_bar').show()
			$('#auth_bar').html("<a class='button-1' style='padding:10px;border-radius:0;position:fixed;top:0px;left:0px;height:50px;z-index:99999999999999'>Welcome, " + getCookie('name') + "</a><a href='home.php?hash=" + getCookie('hash') + "' style='position:absolute;right:0'><img src='assets/tab_members_home.png' style='height:50px'></a>")
			$$('auth_bar').style.width=window_width()+'px'
		},2000)

		</script>
</body>
</html>
<script src="assets/js/jquery.js"type="text/javascript"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
