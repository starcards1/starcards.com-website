<link rel="stylesheet" href="css/simple-line-icons.css">
<link rel="stylesheet" href="css/magnific-popup.css">
<link rel="stylesheet" href="css/style.css">
<script src="js/jquery-3.1.0.min.js"></script>
<script src="js/jquery.tweet.min.js"></script>
<!-- xmAlert -->
<script src="js/jquery.xmalert.min.js"></script>
<!-- Magnific Popup -->
<script src="js/jquery.magnific-popup.min.js"></script>

<script>
	function alert_user(type) {
        $('body').xmalert({ 
            x: 'right',
            y: 'bottom',
            xOffset: 30,
            yOffset: 30,
            alertSpacing: 20,
            lifetime: 6000,
            fadeDelay: 0.3,
            template: 'item',
            title: 'Backend Driven Custom Alert',
			paragraph: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.',
            timestamp: '2 hours ago',
            imgSrc: 'images/alerts/'+type+'.png',
            iconClass: 'icon-heart'
        });
    }
	alert_admin()
	function alert_admin() {
        $('body').xmalert({ 
            x: 'right',
            y: 'bottom',
            xOffset: 30,
            yOffset: 30,
            alertSpacing: 50,
            lifetime: 6000,
            fadeDelay: 0.3,
            template: 'survey',
            title: 'What would you improve?',
            paragraph: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.',
            timestamp: '2 hours ago',
            imgSrc: 'images/dashboard/alert-logo.png',
            buttonSrc: [ '#','#' ],
            buttonText: 'Take the <span class="primary">Survey!</span>',
        });
    }

	function alert_tip() {
        $('body').xmalert({ 
            x: 'right',
            y: 'bottom',
            xOffset: 30,
            yOffset: 30,
            alertSpacing: 20,
            lifetime: 6000,
            fadeDelay: 0.3,
            template: 'review',
            title: 'New Fast Checkout',
            paragraph: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.',
            timestamp: '14 hours ago',
            buttonSrc: [ '#','#' ],
        });
    }
	
	function alert_info() {
        $('body').xmalert({ 
            x: 'right',
            y: 'top',
            xOffset: 30,
            yOffset: 30,
            alertSpacing: 40,
            lifetime: 6000,
            fadeDelay: 0.3,
            template: 'messageInfo',
            title: 'Info Alert Box',
            paragraph: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.',
        });
    }

	function alert_success() {
	if (!msg) msg='Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.'
        $('body').xmalert({ 
            x: 'right',
            y: 'top',
            xOffset: 30,
            yOffset: 30,
            alertSpacing: 40,
            lifetime: 6000,
            fadeDelay: 0.3,
            template: 'messageSuccess',
            title: 'Success Alert Box',
            paragraph: msg
        });
    }

	function alert_error(msg) {
	if (!msg) msg='Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.'
        $('body').xmalert({ 
            x: 'right',
            y: 'top',
            xOffset: 30,
            yOffset: 30,
            alertSpacing: 40,
            lifetime: 6000,
            fadeDelay: 0.3,
            template: 'messageError',
            title: 'Error Alert Box',
            paragraph: msg,
        });
	}
    /*------------------
		PROMO POPUP
	------------------*/
	function alert_promo(trigger_element) {
		$('#'+trigger_element.id).magnificPopup({
			type: 'inline',
			removalDelay: 300,
			mainClass: 'mfp-fade',
			closeMarkup: '<div class="close-btn mfp-close"><svg class="svg-plus"><use xlink:href="#svg-plus"></use></svg></div>'
		});
	}
</script>