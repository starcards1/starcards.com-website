<?
$count=10;
error_reporting(0);
include "../class/utils.class.php";
$c=new utils;
$c->connect("199.91.65.83","voxeo");
?>
<script>
	var wl  = window.location.href;
	var mob = (window.location.href.indexOf('file://')>=0);

	function setCookie(cname,cvalue)	{
		window.localStorage.setItem(cname, cvalue);
		if (mob===true) {
			
		} else {
			var d = new Date(); 
			d.setTime(d.getTime()+(1*24*60*60*1000)); 
			var expires = "expires="+d.toGMTString(); 
			document.cookie = cname + "=" + cvalue + "; " + expires; 
		}
	} 

	function getCookie(cname)	{ 
		if (mob===true) {
			var cvalue = window.localStorage.getItem(cname);
			return cvalue
		} else {
			var name = cname + "="; 
			var ca = document.cookie.split(';'); 
			for(var i=0; i<ca.length; i++) { 
			  var c = ca[i].trim(); 
			  if (c.indexOf(name)==0) return c.substring(name.length,c.length); 
			} 
			return ""; 
		}
	} 
</script>
<?
$mid=$c->decrypt(urldecode($_COOKIE['session']),$key);
if ($mid) {
	$q=$c->query("select * from celeb_members where mid=$mid");
	$email=$q[0]['email']; 
	$mobile=$q[0]['mobile']; 
	$login=$q[0]['login']; 
	$os=$q[0]['os']; 
?>
<script>
	var session,fos,fmobile
	try {
		setTimeout(function(){
			session='<?=json_encode($q[0]);?>';
			var mid='<?=$mid;?>';
			var email='<?=$email;?>';
			var login='<?=$login;?>';
			fmobile='<?=$mobile;?>';
			fos='<?=$os;?>';
		},1)
	} catch(e) {}
</script>
<? } else { ?>
<script>
	var session
	try {
		setTimeout(function(){
			session=''
			setCookie('session','')
		},1)
	} catch(e) {}
</script>
<? } ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>LinQStar.com - Celebrities at your  fingertips </title>
    <link rel="stylesheet" href="./assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="./assets/css/all.min.css">
    <link rel="stylesheet" href="./assets/css/animate.css">
    <link rel="stylesheet" href="./assets/css/nice-select.css">
    <link rel="stylesheet" href="./assets/css/owl.min.css">
    <link rel="stylesheet" href="./assets/css/jquery-ui.min.css">
    <link rel="stylesheet" href="./assets/css/magnific-popup.css">
    <link rel="stylesheet" href="./assets/css/flaticon.css">
    <link rel="stylesheet" href="./assets/css/main.css">
    <link href="css/jquery.tagit.css" rel="stylesheet" type="text/css">
    <link href="css/magic.css" rel="stylesheet" type="text/css">
    <!-- If you want the jQuery UI "flick" theme, you can use this instead, but it's not scoped to just Tag-it like tagit.ui-zendesk is: -->

    <!-- jQuery and jQuery UI are required dependencies. -->
    <!-- Although we use jQuery 1.4 here, it's tested with the latest too (1.8.3 as of writing this.) -->

    <link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon">
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:300|Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
	<style>
	body {
		font-family: 'Source Sans Pro', sans-serif;
		font-size: 14px;
		color: #666;
}

h1 {
		text-align: center;
		margin-bottom: 0;
		margin-top: 60px;
}

#lean_overlay {
		position: fixed;
		z-index: 100;
		top: 0px;
		left: 0px;
		height: 100%;
		width: 100%;
		background: #000;
		display: none;
}

.popupContainer {
		position: absolute;
		width: 330px;
		height: auto;
		left: 45%;
		top: 60px;
		background: #FFF;
}

#modal_trigger {
		margin: 40px auto;
		width: 200px;
		display: block;
		border: 1px solid #DDD;
		border-radius: 4px;
}

.btn {
		padding: 10px 20px;
		background: #F4F4F2;
}

.btn_red {
		background: #ED6347;
		color: #FFF;
}

.btn:hover {
		background: #E4E4E2;
}

.btn_red:hover {
		background: #C12B05;
}

a.btn {
		color: #666;
		text-align: center;
		text-decoration: none;
}

a.btn_red {
		color: #FFF;
}

.one_half {
		width: 50%;
		display: block;
		float: left;
}

.one_half.last {
		width: 45%;
		margin-left: 5%;
}
/* Popup Styles*/

.popupHeader {
		font-size: 16px;
		text-transform: uppercase;
}

.popupHeader {
		background: #F4F4F2;
		position: relative;
		padding: 10px 20px;
		border-bottom: 1px solid #DDD;
		font-weight: bold;
}

.popupHeader .modal_close {
		position: absolute;
		right: 0;
		top: 0;
		padding: 10px 15px;
		background: #E4E4E2;
		cursor: pointer;
		color: #aaa;
		font-size: 16px;
}

.popupBody {
		padding: 20px;
}
/* Social Login Form */

.social_login {}

.social_login .social_box {
		display: block;
		clear: both;
		padding: 10px;
		margin-bottom: 10px;
		background: #F4F4F2;
		overflow: hidden;
}

.social_login .icon {
		display: block;
		width: 10px;
		padding: 5px 10px;
		margin-right: 10px;
		float: left;
		color: #FFF;
		font-size: 16px;
		text-align: center;
}

.social_login .fb .icon {
		background: #3B5998;
}

.social_login .google .icon {
		background: #DD4B39;
}

.social_login .icon_title {
		display: block;
		padding: 5px 0;
		float: left;
		font-weight: bold;
		font-size: 16px;
		color: #777;
}

.social_login .social_box:hover {
		background: #E4E4E2;
}

.centeredText {
		text-align: center;
		margin: 20px 0;
		clear: both;
		overflow: hidden;
		text-transform: uppercase;
}

.action_btns {
		clear: both;
		overflow: hidden;
}

.action_btns a {
		display: block;
}
/* User Login Form */

.user_login {
		display: none;
}

.user_login label {
		display: block;
		margin-bottom: 5px;
}

.user_login input[type="text"],
.user_login input[type="email"],
.user_login input[type="password"] {
		display: block;
		width: 90%;
		padding: 10px;
		border: 1px solid #DDD;
		color: #666;
}

.user_login input[type="checkbox"] {
		float: left;
		margin-right: 5px;
}

.user_login input[type="checkbox"]+label {
		float: left;
}

.user_login .checkbox {
		margin-bottom: 10px;
		clear: both;
		overflow: hidden;
}

.forgot_password {
		display: block;
		margin: 20px 0 10px;
		clear: both;
		overflow: hidden;
		text-decoration: none;
		color: #ED6347;
}
/* User Register Form */

.user_register {
		display: none;
}

.user_register label {
		display: block;
		margin-bottom: 5px;
}

.user_register input[type="text"],
.user_register input[type="email"],
.user_register input[type="password"] {
		display: block;
		width: 90%;
		padding: 10px;
		border: 1px solid #DDD;
		color: #666;
}

.user_register input[type="checkbox"] {
		float: left;
		margin-right: 5px;
}

.user_register input[type="checkbox"]+label {
		float: left;
}

.user_register .checkbox {
		margin-bottom: 10px;
		clear: both;
		overflow: hidden;
}
		* {
			font-family:Open Sans!Important;
		}
		h5 {
			font-family:Open Sans Condensed!Important;
			text-transform:upperCase;
			color:#000!Important
		}
		h1 {
			font-family:Open Sans Condensed!Important;
			text-transform:upperCase
		}
		h2 {
			font-family:Open Sans Condensed!Important;
			text-transform:upperCase
		}
		h3 {
			font-family:Open Sans Condensed!Important;
			text-transform:upperCase
		}
		h4 {
			font-family:Open Sans Condensed!Important;
			text-transform:upperCase
		}
		h6 {
			font-family:Open Sans Condensed!Important;
			text-transform:upperCase
		}
		p {
			font-family:Open Sans Condensed!Important;
			text-transform:upperCase
		}
		.bgx {
			background: -webkit-linear-gradient(0deg, #e1358f 0%, #7e6ce7 100%);
			box-shadow: 4.232px 12.292px 10.56px 0.44px rgb(121 107 232 / 50%);
			padding:10px!Important;
			color:white!Important;
			border-radius:50px!Important;
			font-size:20px!Important;
			text-align:center!Important;
		}
		.bgx1 {
			background: -webkit-linear-gradient(0deg, #e1358f 0%, #7e6ce7 100%);
			box-shadow: 4.232px 12.292px 10.56px 0.44px rgb(121 107 232 / 50%);
			padding:6px!Important;
			color:white!Important;
			border-radius:5px!Important;
			font-size:20px!Important;
			text-align:center!Important;
		}
		.gc {
			padding:5px;border-radius:6px;background:gold;font-size:16px;font-family:Open Sans Condensed;
		}
		.gcs {
			padding:5px;border-radius:6px;background:lightcyan;font-size:16px;font-family:Open Sans Condensed;color:black;font-weight:bold
		}
		.gcr {
			padding:5px;border-radius:6px;background:red;font-size:16px;font-family:Open Sans Condensed;
		}
		.hide {
			display:none
		}
	</style>
</head>
	
    <!--============= Amazing Feature Section Starts Here =============-->
    <section class="amazing-feature-section pos-rel" id="feature">
        <div class="shape-container oh">
            <div class="bg_img feature-background" data-background="pur_bg.png"></div>
            <div class="feature-top-shape d-none d-lg-block">
                <img src="./assets/css/img/feature-shape.png" alt="css">
            </div>
        </div>
        <div class="topper-feature oh padding-top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <a href="" class="feature-video-area popup">
							<div style="top:140px;width:100%;max-width:500px;border:0px solid white!Important;height:400px;display:;position:fixed;right:0;left:0;z-index:999;opacity:1;margin:auto;color:#f0f0f0;font-size:21px;font-family:Open Sans Condensed;">
							</div>
							<div class="thumb">
                                <img src="./assets/images/feature/fature-video.png" alt="feature">
								<div style="width:100%;max-width:500px;height:400px" id="videos-container"></div>
							</div>
                            <div class="button-area">
								<img src="incoming.png" style="width:80%;max-width:300px;position:absolute;left:0;right:0;margin:auto;margin-top:30px">
								<img src="calling.gif" style="width:50px;position:absolute;left:0;right:0;margin:auto;margin-top:-10px;margin-right:65px;z-index:99999999999999">
                            </div>
                        </a> 
                    </div>
                </div>
            </div>
        </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="./assets/js/modernizr-3.6.0.min.js"></script>
    <script src="./assets/js/plugins.js"></script>
    <script src="./assets/js/bootstrap.min.js"></script>
    <script src="./assets/js/magnific-popup.min.js"></script>
    <script src="./assets/js/jquery-ui.min.js"></script>
    <script src="./assets/js/wow.min.js"></script>
    <script src="./assets/js/waypoints.js"></script>
    <script src="./assets/js/nice-select.js"></script>
    <script src="./assets/js/owl.min.js"></script>
    <script src="./assets/js/counterup.min.js"></script>
    <script src="./assets/js/paroller.js"></script>
    <script src="./assets/js/main.js"></script>
    <script src="assets/js/common.js"></script>
    <script src="js/aes.js"></script>
    <script src="js/tag-it.js" type="text/javascript" charset="utf-8"></script>	
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDAuvU_A1iMThoe1i6Joi26nSTDQkjKlDA&libraries=places"></script>
	
	<div id="auth_bar" style="display:none;width:100%;position:fixed;height:50px;background:#000;color:#fff;top:0;left:0;z-index:99999999999999">
	</div>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
</body>
<script type="text/javascript">	
	var arrF=[ ]
	function $$(id) {
		return document.getElementById(id)
	}
	function qs(name, url) {
		if (!url) {
		  url = window.location.href;
		}
		name = name.replace(/[\[\]]/g, "\\$&");
		var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
			results = regex.exec(url);
		if (!results) return null;
		if (!results[2]) return '';
		return decodeURIComponent(results[2].replace(/\+/g, " "));
	}

	var wl  = window.location.href;
	var mob = (window.location.href.indexOf('file://')>=0);

	function setCookie(cname,cvalue)	{
		if (mob===true) {
			window.localStorage.setItem(cname, cvalue);
		} else {
			var d = new Date(); 
			d.setTime(d.getTime()+(1*24*60*60*1000)); 
			var expires = "expires="+d.toGMTString(); 
			document.cookie = cname + "=" + cvalue + "; " + expires; 
		}
	} 

	function getCookie(cname)	{ 
		if (mob===true) {
			var cvalue = window.localStorage.getItem(cname);
			return cvalue
		} else {
			var name = cname + "="; 
			var ca = document.cookie.split(';'); 
			for(var i=0; i<ca.length; i++) { 
			  var c = ca[i].trim(); 
			  if (c.indexOf(name)==0) return c.substring(name.length,c.length); 
			} 
			return ""; 
		}
	} 
	function clear_all() {
		if (mob===true) {
			 window.localStorage.clear();
		} else {
			document.cookie.split(";").forEach(function(c) { document.cookie = c.replace(/^ +/, "").replace(/=.*/, "=;expires=" + new Date().toUTCString() + ";path=/"); });
		}	
	}		
		
		var var_who,var_service,var_occupation,var_cname
		var filters=['who','service','occupation','cname','location','cost'];
		var currFilter=0
		var filter, oldFilter
		var xt=[ ]
		var nx=false
		filter=filters[0]
	 
	function isValidMobile(mob) {
		if (!mob) return false
		var num = mob.trim();
		num = num.replace(/ /g,'');
		num = num.replace(/\./g,'');
		num = num.replace(/-/g,'');
		num = num.replace(/\(/g,'');
		num = num.replace(/\)/g,'');
		num = num.replace(/\[/g,'');
		num = num.replace(/\]/g,'');
		num = num.replace(/\~/g,'');
		num = num.replace(/\*/g,'');
		num = num.replace(/\{/g,'');
		num = num.replace(/\}/g,'');
		num = num.replace(/\+/g,'');
		if ((num+'').length<10) return false
		if (isNaN(num)) return false
		code='1'
		if ((num+'').length==10) {
			return (''+code+''+num+'')
		} else if ((num+'').length==11) {
			if (num.substr(0,1)==code) {
				return num
			} else {
				return false
			}
		} else if ((num+'').length>11) {
			return num
		}
	}	
	</script>


		<img src="https://terrawire.com/talk/lcdd.png" style="width:100%;max-width:500px;height:100%;position:fixed;right:0px;top:0px;bottom:0;z-index:99;opacity:1;margin:auto;color:#f0f0f0;font-size:18px;font-family:Open Sans Condensed;left:0"></div>
		<img id="mic_active"  src="https://terrawire.com/mic-animate.gif" style="display:none;width:100px;position:fixed;right:0px;top:0px;bottom:0;left:0;z-index:999;opacity:0.5;margin:auto;" />
		<img id="mic_inactive"  src="https://terrawire.com/mic.png" style="display:none;width:100px;position:fixed;right:0px;top:0px;bottom:0;left:0;z-index:999;opacity:0.5;margin:auto;" />
	   <select id="convert-from" onchange="changelanguage(this.value)" style="display:none;padding:5px;border:none!Important;overflow:hidden;position:fixed;width:150px;left:5px;top:175px;box-shadow:inset 3px 5px 5px 5 #000;z-index:999;opacity:1;margin:auto;color:#000;text-shadow:1px 0px 2px #fff;font-size:16px;font-family:Open Sans Condensed;background:whitesmoke;height:40px;width:150px"><option selected value="SET">SET LANGUAGE</option><option onclick="changelanguage(this.value)"  value="en">English</option><option value="af">Afrikaans</option><option value="sq">Albanian</option><option value="am">Amharic</option><option value="ar">Arabic</option><option value="hy">Armenian</option><option value="az">Azerbaijani</option><option value="eu">Basque</option><option value="be">Belarusian</option><option value="bn">Bengali</option><option value="bs">Bosnian</option><option value="bg">Bulgarian</option><option value="ca">Catalan</option><option value="ceb">Cebuano</option><option value="ny">Chichewa</option><option value="zh-CN">Chinese 1</option><option value="zh-TW">Chinese 2</option><option value="co">Corsican</option><option value="hr">Croatian</option><option value="cs">Czech</option><option value="da">Danish</option><option value="nl">Dutch</option><option value="eo">Esperanto</option><option value="et">Estonian</option><option value="tl">Filipino</option><option value="fi">Finnish</option><option value="fr">French</option><option value="fy">Frisian</option><option value="gl">Galician</option><option value="ka">Georgian</option><option value="de">German</option><option value="el">Greek</option><option value="gu">Gujarati</option><option value="ht">Haitian Creole</option><option value="ha">Hausa</option><option value="haw">Hawaiian</option><option value="iw">Hebrew</option><option value="hi">Hindi</option><option value="hmn">Hmong</option><option value="hu">Hungarian</option><option value="is">Icelandic</option><option value="ig">Igbo</option><option value="id">Indonesian</option><option value="ga">Irish</option><option value="it">Italian</option><option value="ja">Japanese</option><option value="jw">Javanese</option><option value="kn">Kannada</option><option value="kk">Kazakh</option><option value="km">Khmer</option><option value="rw">Kinyarwanda</option><option value="ko">Korean</option><option value="ku">Kurdish</option><option value="ky">Kyrgyz</option><option value="lo">Lao</option><option value="la">Latin</option><option value="lv">Latvian</option><option value="lt">Lithuanian</option><option value="lb">Luxembourgish</option><option value="mk">Macedonian</option><option value="mg">Malagasy</option><option value="ms">Malay</option><option value="ml">Malayalam</option><option value="mt">Maltese</option><option value="mi">Maori</option><option value="mr">Marathi</option><option value="mn">Mongolian</option><option value="my">Myanmar</option><option value="ne">Nepali</option><option value="no">Norwegian</option><option value="or">Odia (Oriya)</option><option value="ps">Pashto</option><option value="fa">Persian</option><option value="pl">Polish</option><option value="pt">Portuguese</option><option value="pa">Punjabi</option><option value="ro">Romanian</option><option value="ru">Russian</option><option value="sm">Samoan</option><option value="gd">Scots Gaelic</option><option value="sr">Serbian</option><option value="st">Sesotho</option><option value="sn">Shona</option><option value="sd">Sindhi</option><option value="si">Sinhala</option><option value="sk">Slovak</option><option value="sl">Slovenian</option><option value="so">Somali</option><option value="es">Spanish</option><option value="su">Sundanese</option><option value="sw">Swahili</option><option value="sv">Swedish</option><option value="tg">Tajik</option><option value="ta">Tamil</option><option value="tt">Tatar</option><option value="te">Telugu</option><option value="th">Thai</option><option value="tr">Turkish</option><option value="tk">Turkmen</option><option value="uk">Ukrainian</option><option value="ur">Urdu</option><option value="ug">Uyghur</option><option value="uz">Uzbek</option><option value="vi">Vietnamese</option><option value="cy">Welsh</option><option value="xh">Xhosa</option><option value="yi">Yiddish</option><option value="yo">Yoruba</option><option value="zu">Zulu</option></select>	
 	   <select id="convert-from-language" onchange="changeFromLanguage(this.value)" style="display:none;padding:5px;border:none!Important;overflow:hidden;position:fixed;width:150px;right:0;left:0;top:175px;box-shadow:inset 3px 5px 5px 5 #000;z-index:999;opacity:1;margin:auto;color:#000;text-shadow:1px 0px 2px #fff;font-size:16px;font-family:Open Sans Condensed;background:whitesmoke;height:40px;width:150px"><option selected value="SET">SET LANG FROM</option><option onselect="changelanguage(this.value)" value="en">English</option><option value="af">Afrikaans</option><option value="sq">Albanian</option><option value="am">Amharic</option><option value="ar">Arabic</option><option value="hy">Armenian</option><option value="az">Azerbaijani</option><option value="eu">Basque</option><option value="be">Belarusian</option><option value="bn">Bengali</option><option value="bs">Bosnian</option><option value="bg">Bulgarian</option><option value="ca">Catalan</option><option value="ceb">Cebuano</option><option value="ny">Chichewa</option><option value="zh-CN">Chinese 1</option><option value="zh-TW">Chinese 2</option><option value="co">Corsican</option><option value="hr">Croatian</option><option value="cs">Czech</option><option value="da">Danish</option><option value="nl">Dutch</option><option value="eo">Esperanto</option><option value="et">Estonian</option><option value="tl">Filipino</option><option value="fi">Finnish</option><option value="fr">French</option><option value="fy">Frisian</option><option value="gl">Galician</option><option value="ka">Georgian</option><option value="de">German</option><option value="el">Greek</option><option value="gu">Gujarati</option><option value="ht">Haitian Creole</option><option value="ha">Hausa</option><option value="haw">Hawaiian</option><option value="iw">Hebrew</option><option value="hi">Hindi</option><option value="hmn">Hmong</option><option value="hu">Hungarian</option><option value="is">Icelandic</option><option value="ig">Igbo</option><option value="id">Indonesian</option><option value="ga">Irish</option><option value="it">Italian</option><option value="ja">Japanese</option><option value="jw">Javanese</option><option value="kn">Kannada</option><option value="kk">Kazakh</option><option value="km">Khmer</option><option value="rw">Kinyarwanda</option><option value="ko">Korean</option><option value="ku">Kurdish</option><option value="ky">Kyrgyz</option><option value="lo">Lao</option><option value="la">Latin</option><option value="lv">Latvian</option><option value="lt">Lithuanian</option><option value="lb">Luxembourgish</option><option value="mk">Macedonian</option><option value="mg">Malagasy</option><option value="ms">Malay</option><option value="ml">Malayalam</option><option value="mt">Maltese</option><option value="mi">Maori</option><option value="mr">Marathi</option><option value="mn">Mongolian</option><option value="my">Myanmar</option><option value="ne">Nepali</option><option value="no">Norwegian</option><option value="or">Odia (Oriya)</option><option value="ps">Pashto</option><option value="fa">Persian</option><option value="pl">Polish</option><option value="pt">Portuguese</option><option value="pa">Punjabi</option><option value="ro">Romanian</option><option value="ru">Russian</option><option value="sm">Samoan</option><option value="gd">Scots Gaelic</option><option value="sr">Serbian</option><option value="st">Sesotho</option><option value="sn">Shona</option><option value="sd">Sindhi</option><option value="si">Sinhala</option><option value="sk">Slovak</option><option value="sl">Slovenian</option><option value="so">Somali</option><option value="es">Spanish</option><option value="su">Sundanese</option><option value="sw">Swahili</option><option value="sv">Swedish</option><option value="tg">Tajik</option><option value="ta">Tamil</option><option value="tt">Tatar</option><option value="te">Telugu</option><option value="th">Thai</option><option value="tr">Turkish</option><option value="tk">Turkmen</option><option value="uk">Ukrainian</option><option value="ur">Urdu</option><option value="ug">Uyghur</option><option value="uz">Uzbek</option><option value="vi">Vietnamese</option><option value="cy">Welsh</option><option value="xh">Xhosa</option><option value="yi">Yiddish</option><option value="yo">Yoruba</option><option value="zu">Zulu</option></select>	
	   <select id="convert-to-language" onchange="changeToLanguage(this.value)" style="display:none;padding:5px;border:none!Important;overflow:hidden;position:fixed;width:150px;right:0px;left:0; top:175px;box-shadow:inset 3px 5px 5px 5 #000;z-index:999;opacity:1;margin:auto;color:#000;text-shadow:1px 0px 2px #fff;font-size:16px;font-family:Open Sans Condensed;background:whitesmoke;height:40px;width:150px"><option selected value="SET">SET LANG TO</option><option onselect="changelanguage(this.value)" value="en">English</option><option value="af">Afrikaans</option><option value="sq">Albanian</option><option value="am">Amharic</option><option value="ar">Arabic</option><option value="hy">Armenian</option><option value="az">Azerbaijani</option><option value="eu">Basque</option><option value="be">Belarusian</option><option value="bn">Bengali</option><option value="bs">Bosnian</option><option value="bg">Bulgarian</option><option value="ca">Catalan</option><option value="ceb">Cebuano</option><option value="ny">Chichewa</option><option value="zh-CN">Chinese 1</option><option value="zh-TW">Chinese 2</option><option value="co">Corsican</option><option value="hr">Croatian</option><option value="cs">Czech</option><option value="da">Danish</option><option value="nl">Dutch</option><option value="eo">Esperanto</option><option value="et">Estonian</option><option value="tl">Filipino</option><option value="fi">Finnish</option><option value="fr">French</option><option value="fy">Frisian</option><option value="gl">Galician</option><option value="ka">Georgian</option><option value="de">German</option><option value="el">Greek</option><option value="gu">Gujarati</option><option value="ht">Haitian Creole</option><option value="ha">Hausa</option><option value="haw">Hawaiian</option><option value="iw">Hebrew</option><option value="hi">Hindi</option><option value="hmn">Hmong</option><option value="hu">Hungarian</option><option value="is">Icelandic</option><option value="ig">Igbo</option><option value="id">Indonesian</option><option value="ga">Irish</option><option value="it">Italian</option><option value="ja">Japanese</option><option value="jw">Javanese</option><option value="kn">Kannada</option><option value="kk">Kazakh</option><option value="km">Khmer</option><option value="rw">Kinyarwanda</option><option value="ko">Korean</option><option value="ku">Kurdish</option><option value="ky">Kyrgyz</option><option value="lo">Lao</option><option value="la">Latin</option><option value="lv">Latvian</option><option value="lt">Lithuanian</option><option value="lb">Luxembourgish</option><option value="mk">Macedonian</option><option value="mg">Malagasy</option><option value="ms">Malay</option><option value="ml">Malayalam</option><option value="mt">Maltese</option><option value="mi">Maori</option><option value="mr">Marathi</option><option value="mn">Mongolian</option><option value="my">Myanmar</option><option value="ne">Nepali</option><option value="no">Norwegian</option><option value="or">Odia (Oriya)</option><option value="ps">Pashto</option><option value="fa">Persian</option><option value="pl">Polish</option><option value="pt">Portuguese</option><option value="pa">Punjabi</option><option value="ro">Romanian</option><option value="ru">Russian</option><option value="sm">Samoan</option><option value="gd">Scots Gaelic</option><option value="sr">Serbian</option><option value="st">Sesotho</option><option value="sn">Shona</option><option value="sd">Sindhi</option><option value="si">Sinhala</option><option value="sk">Slovak</option><option value="sl">Slovenian</option><option value="so">Somali</option><option value="es">Spanish</option><option value="su">Sundanese</option><option value="sw">Swahili</option><option value="sv">Swedish</option><option value="tg">Tajik</option><option value="ta">Tamil</option><option value="tt">Tatar</option><option value="te">Telugu</option><option value="th">Thai</option><option value="tr">Turkish</option><option value="tk">Turkmen</option><option value="uk">Ukrainian</option><option value="ur">Urdu</option><option value="ug">Uyghur</option><option value="uz">Uzbek</option><option value="vi">Vietnamese</option><option value="cy">Welsh</option><option value="xh">Xhosa</option><option value="yi">Yiddish</option><option value="yo">Yoruba</option><option value="zu">Zulu</option></select>	
	  <textarea id="out" style="background:url(lcdy.png)center center;background-size:cover;width:100%;max-width:500px;border:0px solid rgba(255,255,255,0.4)!Important;height:105px;display:;position:fixed;right:0;left:0;top:0px;z-index:999;opacity:1;margin:auto;color:#fff!Important;font-size:21px;font-family:Open Sans Condensed;top:0"></textarea>
       <div id="out1" style="background:none;background-size:cover;width:100%;max-width:500px;border:none!Important;display:;position:fixed;right:0;left:0;top:0px;z-index:99999999;opacity:1;margin:auto;padding-left:10px;padding-top:10px"></div>
       <div id="out2" style="display:;background:none;background-size:cover;width:100%;max-width:500px;border:none!Important;display:;position:fixed;right:0;left:0;top:35px;z-index:99999999;opacity:1;margin:auto;padding-left:10px;display:;"></div>
       <div id="out3" style="background:none;background-size:cover;width:100%;max-width:500px;border:none!Important;display:;position:fixed;right:0;left:0;top:60px;height:40px;z-index:99999999;opacity:1;margin:auto;padding-left:10px"></div>
		<table style="background-size:cover;height:40px;position:fixed;z-index:999;width:100%;max-width:500px;left:0;right:0;margin:auto;top:105px">
			<tr style="display:none">
				<td style="width:20%;text-align:center">
				   <img onclick="javascript:sp_tt()" id="sp1"  src="https://terrawire.com/sw_off.png" style="height:25px;display:block;opacity:1;margin:auto">
				   <img onclick="javascript:sp_tt()" id="sp2"  src="https://terrawire.com/sw_on.png" style="height:25px;display:none;opacity:1;margin:auto">
				</td>
				<td style="width:20%;text-align:center">
				   <img onclick="javascript:sp_ss()" id="sp3"  src="https://terrawire.com/sw_off.png" style="height:25px;display:block;opacity:1;margin:auto">
				   <img onclick="javascript:sp_ss()" id="sp4"  src="https://terrawire.com/sw_on.png" style="height:25px;display:none;opacity:1;margin:auto">
				</td>
				<td colspan=2 style="width:60%;text-align:center;font-family: Open Sans Condensed">
					
					SELECT <a href="javascript:select_mode_standalone()"><img  src="https://terrawire.com/ons.png" id="ions" style="height:25px;display:"></a><a href="javascript:select_mode_2way()"><img id="ioffs"  src="https://terrawire.com/offs.png" style="height:25px;display:none"></a> MODE
				</td>
			</tr>
			<tr>
				<td colspan=4>

				</td>
			</tr>
		</table>
		<table style="position:absolute;background:url('lcdx.png');background-size:cover;height:40px;margin-top:0px;opacity:1;width:100%;background-size:cover;top:105px;z-index:999999;left:0;right:0;margin:auto">
			<tr>
				<td onclick="javascript:sp_tt()" style="width:30%;text-align:left">
					<span class="" id="trans" style="display:;padding-right:10px;background:white;position:absolute;border-radius:0px;margin-top:-14px;padding:2px;padding-left:0;font-family:Open Sans Condensed; font-size:13px;padding-right:4px;margin-left:5px;"><span id="trans_bg" style="background:red;color:white;padding:2px;font-family:Open Sans Condensed; font-size:13px;padding-left:4px;padding-right:4px;border-radius:0px;opacity:1">TRANSLATE</span><span id="lab_trans"> OFF </span></span>
				</td>
				<td onclick="javascript:sp_ss()" style="width:30%;text-align:left">
					<span class="" id="speech" style="display:;padding-right:10px;background:white;position:absolute;border-radius:0px;margin-top:-14px;padding:2px;padding-left:0;font-family:Open Sans Condensed; font-size:13px;padding-right:4px;margin-left:5px;"><span  id="speech_bg" style="background:red;color:white;padding:2px;font-family:Open Sans Condensed; font-size:13px;padding-left:4px;padding-right:4px;border-radius:0px;opacity:1">SPEECH</span><span id="lab_speech"> OFF </span></span>
				</td>
				<td style="width:30%;text-align:left">
					<a href="javascript:toggle_select_mode()"><span class="" id="span_standalone" style="display:;padding-right:10px;background:white;position:absolute;border-radius:0px;margin-top:-14px;padding:2px;padding-left:0;font-family:Open Sans Condensed; font-size:13px;padding-right:4px;margin-left:5px;"><span  id="bg_standalone" style="background:red;color:white;padding:2px;font-family:Open Sans Condensed; font-size:13px;padding-left:4px;padding-right:4px;border-radius:0px;opacity:1">MODE</span> <span id="mode_txt"> <b>2 WAY</b> </span></span></a>
				</td>
<!--							<td style="width:20%;text-align:center;text-align:left">
					<a href="javascript:select_mode_2way()"><span class="" id="span_2way" style="display:"><span  id="bg_2way" style="position:absolute;border-radius:0px;margin-top:-14px;;margin-left:28px;background:red;color:white;padding:2px;font-family:Open Sans Condensed; font-size:13px;padding-left:4px;padding-right:4px;border-radius:0px"><b>2 WAY</b></span></span></a>
				</td>
				-->
			</tr>
		</table>		
		<img id="h1"  src="https://terrawire.com/h1.png" style="display:none;right:65px;top:160px;width:200px;position:absolute;z-index:999999;margin-left:-25px;opacity:1!Important;">
		<img id="h2"  src="https://terrawire.com/h2.png" style="display:none;left:60px;top:15px;width:210px;position:absolute;z-index:999999;margin-left:-25px;opacity:1!Important;">
		<img id="h3"  src="https://terrawire.com/h3.png" style="display:none;left:60px;bottom:135px;width:100px;position:absolute;z-index:999999;margin-left:-25px;opacity:1!Important;">
		<img id="h4"  src="https://terrawire.com/h4.png" style="display:none;right:10px;bottom:55px;width:105px;position:absolute;z-index:999999;margin-left:-25px;opacity:1!Important;">
		<table style="width:100%;display:;position:fixed;right:0;left:0;bottom:0px;z-index:999999;opacity:1;margin:auto;max-width:500px">
			<tr>
				<td style="width:100%;text-align:right">
					<a href="javascript:show_bar()"><img id="bb"  src="https://terrawire.com/menu.png" style="height:100px"></a>
				</td>
			</tr>
		</table>
		<table id="t1" style="width:100%;display:none;position:fixed;right:0;left:0;bottom:0px;z-index:999;opacity:1;margin:auto;">
		<tr>
			<td colspan=5 style="width:100%;max-width:100%;padding-left:2px;padding-bottom:4px;opacity:0.7">
				<span class="www_box" id="label_my_language"><span style="margin-bottom:2px;background:red;color:white;padding:2px;font-family:Open Sans Condensed; font-size:12px;padding-left:4px;padding-right:4px;border-radius:0px">MY LANG</span><span style="margin-bottom:2px;background:white;color:#000;padding:2px;font-family:Open Sans Condensed; font-size:12px;padding-left:4px;padding-right:4px;border-radius:0px"><span id="text_my_language"></span></span></span>
				<span class="www_box2" id="label_from_language" style="display:none"><span style="margin-bottom:2px;background:red;color:white;padding:2px;font-family:Open Sans Condensed; font-size:12px;padding-left:4px;padding-right:4px;border-radius:0px">FROM LANG</span><span style="margin-bottom:2px;background:white;color:#000;padding:2px;font-family:Open Sans Condensed; font-size:12px;padding-left:4px;padding-right:4px;border-radius:0px"><span id="text_from_language"></span></span></span>
				<span class="www_box4" id="label_to_language" style="display:none"><span style="margin-bottom:2px;background:red;color:white;padding:2px;font-family:Open Sans Condensed; font-size:12px;padding-left:4px;padding-right:4px;border-radius:0px">TO LANG</span><span style="margin-bottom:2px;background:white;color:#000;padding:2px;font-family:Open Sans Condensed; font-size:12px;padding-left:4px;padding-right:4px;border-radius:0px"><span id="text_to_language"></span></span></span>
				<span class="www_box5" id="label_mode"><span style="margin-bottom:2px;background:red;color:white;padding:2px;font-family:Open Sans Condensed; font-size:12px;padding-left:4px;padding-right:4px;border-radius:0px">MODE</span><span style="margin-bottom:2px;background:white;color:#000;padding:2px;font-family:Open Sans Condensed; font-size:12px;padding-left:4px;padding-right:4px;border-radius:0px"><span id="text_mode"></span></span></span>
			</td>
		</tr>
		<tr>
			<td style="width:10%;max-width:10%">				 
				<a href="javascript:set_my_language()"><img id="my_language_set"  src="https://terrawire.com/my_language_set.png" style="width:100%;height:50px;display:"></a>
				<a href="javascript:set_my_language()"><img id="my_language"  src="https://terrawire.com/my_language.png" style="width:100%;height:50px;display:none"></a>
				<a href="javascript:set_from_language()"><img id="from_language"  src="https://terrawire.com/from_language.png" style="width:100%;height:50px;display:none"></a>
				<a href="javascript:set_from_language()"><img id="from_language_set"  src="https://terrawire.com/from_language_set.png" style="width:100%;height:50px;display:none"></a>
			</td>
			<td style="width:10%;max-width:10%">
				<a href="javascript:showFileSelector()"><img id="top3"  src="https://terrawire.com/inject.png" style="width:100%;height:50px;display:"></a>
				<a href="javascript:set_to_language()"><img id="to_language"  src="https://terrawire.com/to_language.png" style="width:100%;height:50px;display:none"></a>
				<a href="javascript:set_to_language()"><img id="to_language_set"  src="https://terrawire.com/to_language_set.png" style="width:100%;height:50px;display:none"></a>
			</td>
			<td style="width:20%;max-width:20%">
				<a href="javascript:inviteRemoteUser()"><img id="invite"  src="https://terrawire.com/invite_btn.png" style="width:100%;height:50px"></a>
			</td>
			<td style="width:20%;max-width:20%">
				<a href="javascript:showFileTransfer()"><img src="https://terrawire.com/upload.png" style="width:100%;height:50px"></a>
			</td>
			<td style="width:20%;max-width:20%">
				<a href="javascript:hide_me()"><img id="top5"  src="https://terrawire.com/more.png" style="width:100%;height:50px"></a>
			</td>
		</tr>
		<tr>
			<td style="width:20%;max-width:20%">
				<a href="javascript:mute()"><img id="muteOp"  src="https://terrawire.com/bk5.png" style="width:100%"></a>
				<a href="javascript:unMute()"><img id="unMuteOp"  src="https://terrawire.com/bk5_off.png" style="width:100%;display:none"></a>
			</td>
			<td style="width:20%;max-width:20%"> 
				<a href="javascript:toggle_video()"><img id="bk3"  src="https://terrawire.com/bk3.png" style="width:100%"></a>
			</td>
			<td style="width:20%;max-width:20%">
				<a href="javascript:disable_mic()"><img id="mic_on"  src="https://terrawire.com/bk1.png" style="width:100%"></a>
				<a href="javascript:enable_mic()"><img id="mic_off"  src="https://terrawire.com/bk1_off.png" style="width:100%;display:none"></a>
			</td>
			<td style="width:20%;max-width:20%">
				<span id="span-start-recording">
					<a href="javascript:startUserTranslation()">
						<img id="btn-start-recording"  src="https://terrawire.com/btn_single_user_mode.png" style="width:100%">
					</a>
					<a href="javascript:userStopsRecording()">
						<img id="btn-stop-recording"  src="https://terrawire.com/btn_single_user_mode.gif" style="width:100%;display:none">
					</a>
				</span>
			</td>
			<td style="width:20%;max-width:20%">
				<a href="javascript:txt2SpeechOn()"><img id="tsOn7"  src="https://terrawire.com/btndial.png" style="width:100%"></a>
				<a href="javascript:txt2SpeechOff()"><img id="tsOff7"  src="https://terrawire.com/btndial_on.png" style="width:100%;display:none"></a>
			</td>
		</tr>
	</table>
	<video style="opacity:0;position:absolute;z-index:-9" id="v1"></video>
	<div id="dial" style="width:200px;height:125px;position:fixed;right:0;left:0;bottom:0px;top:0;margin:auto;z-index:999999999999999999;">
		<div style="position:absolute;padding:10px;border-radius:6px;width:200px;border:0px solid black!Important;height:200px;opacity:0.5;margin:auto;color:#000;font-size:21px;font-family:Open Sans Condensed;background:#fff"></div>
		<img src="call.gif" style="position:absolute;margin:auto;left:0;right:0;width:150px" />
		<div style="position:absolute;margin:auto;left:0;right:0;width:100%;text-align:center;border:0px solid black!Important;height:75px;opacity:1;margin:auto;margin-top:100px;color:#000;font-size:16px;font-family:Open Sans Condensed">Connecting, please wait...</div>
		<button onclick="hide_dial()" class="btn btn-danger" style=";margin:auto;color:white;background:red;margin-top:140px;position:absolute;left:0;right:0;">Hangup</button>
	</div>
	<form action="x_gallery_uploads.php"  style="display:none;width:100%;border:none;background:none" id="member"></form></body>
	
<div class="fullscreen-bg" id="videos-container"></div>
<div id="demo" style="width:100%;margin:auto;left:0;right:0;text-align:center;position:fixed;height:80px;bottom:-45px;z-index:9999999999999999999;color:white;padding:10px;font-size:44px;font-family:Bungee Outline;display:none"></div>
<script src="js/rtc.js"></script>
<script src="https://terrawire.com/rtc/node_modules/webrtc-adapter/out/adapter.js"></script>
<script src="https://terrawire.com/rtc/dev/getHTMLMediaElement.js"></script>


<script>
window.getExternalIceServers = true;
var video,localVideo,remoteVideo,localStream,remoteStream,localMediaElement,remoteMediaElement,st,rst
var tokens=10
var countDownDate
var x
// Set the date we're counting down to
	function show_dial() {
		$('#dial').show()
	}
	
	function hide_dial() {
		$('#dial').hide()
	}
	
	function $$(obj) {
		return document.getElementById(obj)
	}
	function qs(name, url) {
		if (!url) {
		  url = window.location.href;
		}
		name = name.replace(/[\[\]]/g, "\\$&");
		var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
			results = regex.exec(url);
		if (!results) return null;
		if (!results[2]) return '';
		return decodeURIComponent(results[2].replace(/\+/g, " "));
	}

	var wl  = window.location.href;
	var mob = (window.location.href.indexOf('file://')>=0);

	function setCookie(cname,cvalue)	{
		if (mob===true) {
			window.localStorage.setItem(cname, cvalue);
		} else {
			var d = new Date(); 
			d.setTime(d.getTime()+(1*24*60*60*1000)); 
			var expires = "expires="+d.toGMTString(); 
			document.cookie = cname + "=" + cvalue + "; " + expires; 
		}
	} 

	function getCookie(cname)	{ 
		if (mob===true) {
			var cvalue = window.localStorage.getItem(cname);
			return cvalue
		} else {
			var name = cname + "="; 
			var ca = document.cookie.split(';'); 
			for(var i=0; i<ca.length; i++) { 
			  var c = ca[i].trim(); 
			  if (c.indexOf(name)==0) return c.substring(name.length,c.length); 
			} 
			return ""; 
		}
	} 
	function clear_all() {
		if (mob===true) {
			 window.localStorage.clear();
		} else {
			document.cookie.split(";").forEach(function(c) { document.cookie = c.replace(/^ +/, "").replace(/=.*/, "=;expires=" + new Date().toUTCString() + ";path=/"); });
		}	
	}		

	function showFileTransfer() {
		location.href='file?login='+qs('login')+'&mobile='+qs('mobile')
	}

	function deduct_balance() {
		$.ajax({
			url:'https://lushmatch.com/sdfm/api/x_update_balance.php?mid='+localStorage.getItem('mid')+'&type=video_balance',
			success:function(data){}
		})
	}
 
var codec = 'H264';
var roomid = qs('login');
var action = qs('action');
var mobile = qs('mob');

var vlocal,vremote

var connection = new superRTC();
connection.socketURL = 'https://terrawire.com:9001/';
connection.socketMessageEvent = 'celebrity';
connection.session = {
    audio: true,
    video: true
};
connection.sdpConstraints.mandatory = {
    OfferToReceiveAudio: true,
    OfferToReceiveVideo: true
};
connection.iceServers = [{
    'urls': [
        'stun:stun.l.google.com:19302',
        'stun:stun1.l.google.com:19302',
        'stun:stun2.l.google.com:19302',
        'stun:stun.l.google.com:19302?transport=udp',
    ]
}];
connection.codecs.video = codec;


connection.videosContainer = document.getElementById('videos-container');
connection.onstream = function(event) {
    var width = parseInt(connection.videosContainer.clientWidth / 2) - 20;
    video=event.mediaElement
	localStream=event.stream
	st=event.stream
	if(event.type === 'local') {
		video.id='local'
		var mediaElement = getHTMLMediaElement(video, {
			title: 'LOCAL',
			buttons: ['full-screen'],
			width: width,
			height: screen.height,
			showOnMouseEnter: false 
		});
		vlocal=mediaElement
		vlocal.style.cssText='position:absolute;top:0px;left:0;width:100%;height:100%;'
	} else {
		hide_dial() 
		video.id='remote'
		var mediaElement = getHTMLMediaElement(video, {
			title: 'REMOTE',
			buttons: ['full-screen'],
			width: screen.width,
			height: screen.height,
			showOnMouseEnter: false
		});
		vremote=mediaElement
		var v_w
		if ((/Android|webOS|iPhone|iPad|BlackBerry|Windows Phone|Opera Mini|IEMobile|Mobile/i.test(navigator.userAgent)) || (screen.width<640)) {
			vremote.width=screen.width+'px'
			v_w=375
		} else {
			vremote.width='500px'
			v_w=500
		}
		vremote.style.cssText='position:absolute;top:0px;right:0;height:100%!Important;width:'+v_w+'px!Important;object-fit:cover'
		vlocal.style.cssText='position:fixed;top:150px;right:10px;width:100px;height:120px;z-index:999999999999999999999;border:5px solid white'
		vlocal.height='120px'
		vlocal.width='100px'
		$$('local').setAttribute('width','100')
		$$('local').setAttribute('height','120')

		vremote.media.muted=false
		vremote.media.volume=1
		remoteStream=event.stream
		rst=event.stream
	}
	
    connection.videosContainer.appendChild(mediaElement);

    setTimeout(function() {
        mediaElement.media.play();
    }, 3000);

    mediaElement.id = event.streamid;
    mediaElement.setAttribute('data-userid', event.userid);
	

};
connection.onstreamended = function(event) {
    if (vremote) {
        vremote.parentNode.removeChild(vremote);
    }
	vlocal.style.cssText='position:absolute;top:0px;left:0;width:100%;height:100%'
};


	connection.mediaConstraints.audio = {
		echoCancellation: true,
		noiseSuppression: true,
		autoGainControl: true,
		  googEchoCancellation: true,
		  googEchoCancellation2: true,
		  googNoiseSuppression: true,
		  googNoiseSuppression2: true,
		  googAutoGainControl: true,
		  googAutoGainControl2: true,
		  googHighpassFilter: true,
		  googTypingNoiseDetection: true,
		  googAudioMirroring: false,		
	};

	setTimeout(function(){
		connect()
	},1)
	function  connect() {
		try {
			connection.openOrJoin(qs('login'))
		} catch(e) {
			console.log(e)
			setTimeout('connect()',1000)
		}
	}


	Dropzone.autoDiscover=false 
	var sites
	var imagesUploaded
	var r,k,c,t2,t3,t4,intr=50,currImg
	var rat=1
	var rx=0, t, ext
	var set=false
	var usr_img=[]
	function getCookie(cname)	{
		var name = cname + "=";
		var ca = document.cookie.split(';');
		for(var i=0; i<ca.length; i++) {
		  var c = ca[i].trim();
		  if (c.indexOf(name)==0) return c.substring(name.length,c.length);
		}
		return "";
	}
	var url="https://terrawire.com/x_gallery_uploads.php"
	var myDropzone, tfiles=0
	if (!myDropzone) {
		myDropzone = new Dropzone("form#member", { 
			url: url,
			maxFilesize: 1000000000,
			dictDefaultMessage: "<div id='xe' style='background:;color:#fff;font-family:Open sans;color:#fff;font-weight:300;top:0;position:fixed;width:100%;left:0;top:7%;right:0;margin:auto;height:75%;z-index:99999999999999999;color:black'><br><br><br>TO SELECT PHOTOS OR VIDEOS<br>CLICK BELOW<br><br><img style='width:70px' src='https://lushmatch.com/sdfm/images/cap04.png'></div>",
            init: function() {
                this.on("sending", function(file, xhr, formData){
					formData.append("mid", getCookie('mid'));
					formData.append("fn", file.name);
                });
            }			
		});
		
	}
	myDropzone.on("addedfile", function(file) {
		console.log(file)
		if (file.size>100000000) {
			alert('too big')
			abort=true
			return false
		} else {
			abort=false
		}
		if (abort===true) return false
		ctr++
	});
	var file,src	
	myDropzone.on("complete", function(file) {
		ctr--
		fn=file.name
		ext_url='https://terrawire.com/aux/' + fn
		if (ctr==0) {
			var video = $$('v1')
			if ((file.size/(1024*1024))>=2) {
				video.src=ext_url
			} else {
				video.src = URL.createObjectURL(file);
			}
			video.loop = true;
			console.log('size :' + file.size)
			video.play();
			video.id='v1'
			inject_video()
		}
	});
	
	function showFileSelector() {
		$('#member').click()
	}
	
	var peer
	var ctr=0
	var userID
</script>
	<form action="x_gallery_uploads.php"  style="display:none;width:100%;border:none;background:none" id="member"></form></body>
	<div id="xdownload" style="display:none;position:fixed;z-index:999999999999999999999999999;left:0;right:0;margin:auto;width:150px;height:50px;bottom:150px"><button class="btn btn-danger">Download</button></div>
	<script src='https://terrawire.com/hark.js'></script>
	<script src="https://terrawire.com/js/jsSocketNode.js"></script>
	<script src="https://terrawire.com/RecordRTC.js"></script>
	<script>
		window.getExternalIceServers = true;
		var codec = 'H264';
		var roomid = login;
		var don=0
		var tik=1
		var con,at,st,vt,ev,localStream
		var remote_connected=false
		var rst		
		setCookie('login',login)
		setCookie('mobile',mobile)
		var audio

		function led1 (str, size, color) {
			$('#out1').html(ledify(str, size, color))
		}

		function led2 (str, size, color) {
			$('#out2').html(ledify(str, size, color))
		}

		function led3 (str, size, color) {
			$('#out3').html(ledify(str, size, color))
		} 
		
		function show_bar() {
			if ($$('t1').style.display=='none') {
				$$('t1').style.display=''
				$$('bb').style.display='none'
			} else { 
				$$('t1').style.display='none'
				$$('bb').style.display=''
			}
		}
		
		function hide_me() {
			$$('t1').style.display='none'
				$$('bb').style.display=''
		}

		function isValidMobile(mob) {
			if (!mob) return false
			var num = mob.trim();
			num = num.replace(/ /g,'');
			num = num.replace(/\./g,'');
			num = num.replace(/-/g,'');
			num = num.replace(/\(/g,'');
			num = num.replace(/\)/g,'');
			num = num.replace(/\[/g,'');
			num = num.replace(/\]/g,'');
			num = num.replace(/\~/g,'');
			num = num.replace(/\*/g,'');
			num = num.replace(/\{/g,'');
			num = num.replace(/\}/g,'');
			num = num.replace(/\+/g,'');
			if ((num+'').length<10) return false
			if (isNaN(num)) return false
			code='1'
			if ((num+'').length==10) {
				return (''+code+''+num+'')
			} else if ((num+'').length==11) {
				if (num.substr(0,1)==code) {
					return num
				} else {
					return false
				}
			} else if ((num+'').length>11) {
				return num
			}
		}
	
		function muteSpeech(){
			if (typeof(audioE)=='object') {
				audioE.muted=true
				$('#muteOp').hide()
				$('#unMuteOp').show()
			}
		}
		
		function unMuteSpeech(){
			if (typeof(audioE)=='object') {
				audioE.muted=false
				$('#muteOp').show()
				$('#unMuteOp').hide()
			}
		}

		function disable_mic() {
			if (localStream) localStream.getAudioTracks()[0].enabled = false
			if (remoteStream) remoteStream.getAudioTracks()[0].enabled = false
			$('#mic_on').hide()
			$('#mic_off').show()
		}	

		function enable_mic() {
			if (localStream) localStream.getAudioTracks()[0].enabled = true
			if (remoteStream) remoteStream.getAudioTracks()[0].enabled = true
			$('#mic_on').show()
			$('#mic_off').hide()
		}

		function unMuteSpeaker() {
			document.querySelector('video').volume='0.7'
			document.querySelector('video').muted=false
			$$('bk2').src='bk5.png'
			localStream.media.volume=0.7
			localStream.media.muted=false
		}
		
		function muteSpeaker() {
			// This just kills the output of your MIC on your own speaker; 
			// the remote user can still hear you. Done to stop feedback;
			document.querySelector('video').volume=0
			document.querySelector('video').muted=true
			mediaStream.media.volume=0
			localStream.media.muted=true
			$('#tsOn').hide()
		}
		
		function muteAudioOutput() {
			// Same as muting your MIC
			// Remote user cannot hear you any more
			$('#muteOp').hide()
			$('#unMuteOp').show()
			st.getAudioTracks()[0].enabled=false
		}
		
		function unMuteAudioOutput() {
			$('#muteOp').show()
			$('#unMuteOp').hide()
			st.getAudioTracks()[0].enabled=true
		}
		
		function showFileSelector() {
		$('#member').click()
	}
	
	
	var peer
	var ctr=0
	var userID
	var peer
	var userID
	function inject_video() {
		userID=connection.userid
		var stream=localStream
		if (connection.peers) peer=connection.peers.selectFirst()
		localStream.removeTrack(localStream.getTracks()[1]);
		setTimeout(function(){
			connection.addStream(($$('v1').captureStream()))
		},3000)
		setTimeout(function(){
			var newStream=localStream.addTrack($$('v1').captureStream().getTracks()[0])
			var mediaElement=$$('videos-container').children[0]
			connection.videosContainer.appendChild(mediaElement);

		},3000)
	}

	function toggle_video() {
		if (st.getTracks()[1].kind=="video") {
			if (st.getTracks()[1].enabled==false) {
				st.getTracks()[1].enabled=true
				$$('bk3').src='bk3.png'
			} else {
				st.getTracks()[1].enabled=false
				$$('bk3').src='bk3_off.png'
			}
		} else {
			if (st.getTracks()[0].enabled==false) {
				st.getTracks()[0].enabled=true
				$$('bk3').src='bk3_off.png'
			} else {
				st.getTracks()[0].enabled=false
				$$('bk3').src='bk3.png'
			}
		}
	}

	function changelanguage(ob) {
		$('#h3').hide()
		setCookie('lang_set_attempt','1')
		setCookie('user_language',ob)
		setCookie('user_language_text',$('#convert-from  option:selected').text().toUpperCase())
		$$('text_my_language').innerHTML=$('#convert-from  option:selected').text().toUpperCase()
		notify_language_set(getCookie('mobile'), getCookie('user_language'))
		$('#convert-from').hide()
		$('#label_my_language').show()		
		setTimeout(function(){ 
			led1('new language: ','16',1)
			led3('','16',1)
			led2($('#convert-from  option:selected').text(),'16',2)
		},1000)
	}

	function changeFromLanguage(ob) {
		$('#h3').hide()
		$('#from_language_set').show()
		$('#from_language').hide()
		setCookie('lang_set_attempt','1')
		setCookie('from_language',ob)
		setCookie('text_from_language',$('#convert-from-language  option:selected').text().toUpperCase())
		$('#convert-from-language').hide()
		$('#text_from_language').html($('#convert-from-language  option:selected').text().toUpperCase())
		setTimeout(function(){ 
			led1('new language: ','16',2)
			led3('','16',1)
			led2($('#convert-from-language  option:selected').text(),'16',2)
		},1000)
	}
	var translating=false
	function changeToLanguage(ob) {
		$('#h3').hide()
		$('#to_language_set').show()
		$('#to_language').hide()
		setCookie('lang_set_attempt','1')
		setCookie('to_language',ob)
		setCookie('text_to_language',$('#convert-to-language  option:selected').text().toUpperCase())
		$('#convert-to-language').hide()
		$('#text_to_language').html($('#convert-to-language  option:selected').text().toUpperCase())
		setTimeout(function(){ 
			led1('new language: ','16',2)
			led3('','16',1)
			led2($('#convert-to-language  option:selected').text(),'16',2)
		},1000)
	}
	
	</script>

	<script src="https://lushmatch.com/sdfm/js/common.js"></script>
	<script src='https://terrawire.com/speech_translation.js'></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
	
	</html>
<? $c->close(); ?>