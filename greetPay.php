<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <title>Accept a card payment</title>
    <meta name="description" content="A demo of a card payment on Stripe" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
	<style>
		textarea {
			width:100%;
			height:70px;
			border-radius:4px;
			border: 1px solid skyblue
		}
		b {
				font-size:15px;
		}
		.row{
			--bs-gutter-x:0.1rem!Important;
			--bs-gutter-y:0.1rem!Important;
			margin-left:0!Important;
			margin-right:0!Important;
	</style>
  
	<link rel="stylesheet" href="global.css" />
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
	
	<script>
	function qs(name, url) {
		if (!url) {
		  url = window.location.href;
		}
		name = name.replace(/[\[\]]/g, "\\$&");
		var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
		results = regex.exec(url);
		if (!results) return null;
		if (!results[2]) return '';
		return decodeURIComponent(results[2].replace(/\+/g, " "));
	}

		var card_id=qs('card_id')
		var mid=qs('mid')
		var provider_type=qs('provider_type').split('_')[1]
		var provider_name=qs('name')
		var provider_mid=qs('provider_mid')
		var service_name=qs('item_name')
		var service_cost=qs('amt')
		var to_email=qs('to_email')
		var to_mobile=qs('to_mobile')
		var from_mobile=qs('from_mobile')
	</script>
	
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
    <script src="https://linqstar.com/js/jquery.js"></script>
    <script src="https://linqstar.com/js/utils.js"></script>
    <script src="https://polyfill.io/v3/polyfill.min.js?version=3.52.1&features=fetch"></script>
    <script src="https://js.stripe.com/v3/"></script>
</head>

  <body style="background:#fcfcfc">
	<div class="container">
    <!-- Display a payment form -->

	<div class="row no-gutters"  style="--bs-gutter-x:0rem;padding:20px;padding-top:10px">
	<div style="width:100%;text-align:center">
		<h2>PAYING NOW FOR</h2>
		<div>Video Greeting from <?=$_GET['name'];?></div>
	</div>

	<div class="row" style="text-align:center">
		<div class="col-md-6">
			<img src="https://linqstar.com/assets/male.png" style="width:75px;height:75px;border-radius:75px">
		</div>
		<div style="max-width:100%" class="col-md-6" id="p_rating">
			<img src="assets/1r.png" style="margin:10px">
			<img src="assets/1r.png" style="margin:10px">
			<img src="assets/1r.png" style="margin:10px">
			<img src="assets/1r.png" style="margin:10px">
			<img src="assets/1g.png" style="margin:10px">
		</div>		
	</div>
	
	<div class="row">

		<div style="max-width:35%" class="col-md-3">
				<b></b>
		</div>
	</div>
	<div class="row" style="margin-top:10px">
		<div style="max-width:40%" class="col-md-3">
				<b>COST</b>
		</div>
		<div style="max-width:60%;text-align:right" class="col-md-3">
			<b>OCCUPATION</b>
		</div>
	</div>
	<div class="row">

		<div style="max-width:40%" class="col-md-2" id="p_cost">
			$500
		</div>
		<div style="max-width:60%;text-align:right" class="col-md-4" id="p_type">
			
		</div>
	</div>
	<div class="row" style="margin-top:10px">
		<div class="col-md-6">
			<b>NOTES FOR CELEBRITY</b>
		</div>
		<div id=="divNotes" class="col-md-6" style="display:">
		<div>
			Occasion, who for, what it should say etc; Remember, the more you tell them, the more natural and real the greeting will sound.
		</div>
	</div>
	<div class="row" style="margin-top:10px">
		<div>
			<textarea id="note_about" style="height:175px"></textarea>
		</div>
	</div>

	<div class="row" style="margin-top:10px">
		<div class="col-md-6">
			  <form action="https://terrawire.com:4242/create-checkout-session/<?=$_GET['provider_mid'];?>/3000/<?=$_GET['card_id'];?>" method="GET">
					<button style="width:100%" class="btn btn-info" type="submit">Checkout</button>
			  </form>

		</div>
	</div>

	</div>
	<script>
		$('#p_name').html(provider_name)
		$('#p_type').html(provider_type)
		$('#p_service').html('Video Recording')
		$('#p_cost').html('$' + service_cost + '.00')
		$('#p_rating').html('<img src="https://linqstar.com/assets/1r.png" style="margin:10px"><img src="https://linqstar.com/assets/1r.png" style="margin:10px"><img src="https://linqstar.com/assets/1r.png" style="margin:10px"><img src="https://linqstar.com/assets/1r.png" style="margin:10px"><img src="https://linqstar.com/assets/1g.png" style="margin:10px">')
	</script>
  </body>
</html>
