<?php
	include "x_auth.php";
	require_once "../class/utils.class.php";
	$c=new utils;
	$c->connect("199.91.65.83","voxeo");
	parse_str(http_build_query($_GET));
	$pro=$c->query("select * from celeb_profiles where mid=$provider_mid");
	foreach($pro[0] as $key => $value) {
		${$key}=$value;
	}
	$ta=$c->query("select * from celeb_tags where pid=$pid");
	$t=explode(",",$ta[0]['tags']);
?>
<script src="assets/js/jquery.js"type="text/javascript"></script>
<script src="assets/js/jquery-ui.min.js" type="text/javascript" charset="utf-8"></script>	
<script src="js/tag-it.js" type="text/javascript" charset="utf-8"></script>	
<script src="assets/js/bootstrap.min.js"type="text/javascript"></script>
<script src="assets/js/utils.js"type="text/javascript"></script>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>LinqStar Confrence Setup</title>
	<link href="doc/css/bootstrap.min.css" rel="stylesheet">
	<link href="doc/css/style.css" rel="stylesheet">
	<link href="doc/css/menu.css" rel="stylesheet">
	<link href="doc/css/vendors.css" rel="stylesheet">
	<link href="doc/css/icon_fonts/css/all_icons_min.css" rel="stylesheet">
    
	<!-- YOUR CUSTOM CSS -->
	<link href="doc/css/custom.css" rel="stylesheet">
	
    <link rel="stylesheet" href="assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="assets/css/animate.css"/>
    <link rel="stylesheet" href="assets/css/jquery-ui.min.css"/>
    <link rel="stylesheet" href="assets/css/main.css"/>
	<link href="style.css" rel="stylesheet" type="text/css" />
    <link href="css/jquery.tagit.css" rel="stylesheet" type="text/css">
	<style>
		div {font-size:12px;}
	</style>
    <link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon"/>
</head>
<body  style="background:url(assets/images/account-bg.jpg)no-repeat;background-size:cover">
	    <img style="width:200px;margin:auto;left:0;right:0;position:absolute;top:10px" src="assets/images/logo2.png" alt="logo"/>
		<div class="text-center" style="margin-top:200px">
            <div class="account-wrapper" style="margin:auto;padding:10px;max-width:600px">
                <div id="apex" class="account-body">
					<div class="row">
						<div class="col-md-12 text-center">
							<h3 style="color:maroon">NEW CONFERENCE CALL</h3>
						</div>
					</div>
					<span id="part1">
						<br>
						<div class="row text-left">
							<div class="col-md-12">
								<div><b>WHATS IT ABOUT?</b></div><input placeholder="Its Optional. Skip it if you like" class="form-control" type="text" id="title" style="font-size:1em">
							</div>
						</div>
						<div class="row text-left">
							<div class="col-md-6">
								<div><b>WHEN IS IT?</b></div><input class="form-control td-input" type="text" id="booking_date" style="font-size:1em">
							</div>
							<div class="col-md-6">
								<div><b>WHAT TIME?</b></div><input class="form-control td-input" type="text" id="start_time" style="font-size:1em">
							</div>
						</div>
						<br>
						<div class="row text-center">
							<div class="col-md-12">
								<div style="line-height:1.6">
									Just tell us when. And yes, there is no end time. Talk all night if that's what floats your boat.
									BTW, no more call in numbers, secret PINs yada yada. Click NEXT when you're done
								</div>
								<div>
									<button class="button-5" onclick="next()">NEXT</button>
								</div>
							</div>
						</div>
					</span>					
					<br>
					<span id="part2" style="display:none">
						<div class="row" style="padding:10px;padding-bottom:0">
							<div class="col-md-4 text-left" style="width:25%">
								<div><b>ATTENDEEE MOBILE</b></div>
							</div>
							<div class="col-md-6 text-left" style="width:65%">
								<div><b>ATTENDEEE NAME</b></div>
							</div>
							<div class="col-md-2 text-left" style="width:10%">
								<div><a class="button-1" href="auth()"><b>IMPORT</b></a></div>
							</div>
							</div>
						<div class="row text-left" style="padding:10px;padding-top:0">
							<div class="col-md-4" style="width:25%">
								<input class="form-control" type="text" id="mobile_1"  style="font-size:16px;padding-top:5px" onblur="getCallerID(this.id,this.value)">
							</div>
							<div class="col-md-6" style="width:65%">
								<div class="form-control" id="name_1" style="font-size:16px;padding-top:5px"></div>
							</div>
							<div class="col-md-2" style="width:10%;max-width:35px!Important">
							</div>
						</div>
					</span>
				</div>
					<span id="part3" style="display:none">
						<br/><br/>
						<div class="row text-center">
							<div class="col-md-7 text-center">
								<a style="border-radius:10px;margin-top:20px;color:white" href="javascript:add_to_list()" class="button-4">ADD ANOTHER PARTICIPANT</a>
							</div>
							<div class="col-md-5 text-center">
								<a style="padding:14px;margin-bottom:25px" href="javascript:add_conference()" class="button-5"><b>IM DONE. INVITE ALL!</b></a>
							</div>
						</div>				
						<br/><br/>
					</span>
				</div>
			<br>
		</div>
		</div>
	<script src="assets/js/wait.js"></script>
	<script src="assets/js/common.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="./assets/js/modernizr-3.6.0.min.js"></script>
    <script src="./assets/js/plugins.js"></script>
    <script src="./assets/js/bootstrap.min.js"></script>
    <script src="./assets/js/jquery-ui.min.js"></script>
    <script src="./assets/js/main.js"></script>
    <script src="assets/js/common.js"></script>
    <script src="js/aes.js"></script>
    <script src="js/tag-it.js" type="text/javascript" charset="utf-8"></script>	
    <script src="js/location.js" type="text/javascript" charset="utf-8"></script>	
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDAuvU_A1iMThoe1i6Joi26nSTDQkjKlDA&libraries=places"></script>
	<script src="js/utils.js"type="text/javascript"></script>
	<script src="https://apis.google.com/js/client:platform.js?onload=start" async defer></script>	
	<script type="text/javascript">
	function auth() {
		var scopes = 'https://www.googleapis.com/auth/contacts.readonly';
		gapi.auth.authorize({client_id: '289543317222-16rpriap2hs2u7d33rvsd28c7jblo7hq.apps.googleusercontent.com', scope: scopes, immediate: false}, fetch);
	}
	function fetch(token) {
		wait3()
		var url= "https://linqstar.com/x_read_contacts.php?access_token=" + token.access_token + "&mid="+getCookie('mid')
		console.log(url)
	    $.ajax({
		    url:url,
		    success:function(data) {
				hide()
				alert(data + ' contacts imported securely to your account')
			}
		})
	}
	
	var is_default=0
	function swx() {
		private_invite=(private_invite==0)?1:0
	}
	var is_ppv=0
	function pwx() {
		is_ppv=(is_ppv==0)?1:0
	}
	
	function next() {
		$('#part1').hide()
		$('#part2').show()
		$('#part3').show()
	}
	
	var cnt=2
	function add_to_list() {
		var dg=document.getElementById('apex')
		var dt=document.createElement('div')
		var d1=document.createElement('div')
		var d2=document.createElement('div')
		var d3=document.createElement('div')
		dt.className="row text-left"
		dt.style.cssText="padding:10px"
		
		d1.className="col-md-4"
		d1.style.cssText="width:25%"
		d1.innerHTML='<input class="form-control" type="text" id="mobile_'+cnt+'"  style="font-size:16px;padding-top:5px" onblur="getCallerID(this.id,this.value)">'
		dt.appendChild(d1)
		
		d2.className="col-md-6"
		d2.style.cssText="width:65%"
		d2.innerHTML='<div class="form-control" id="name_'+cnt+'" style="font-size:16px;padding-top:5px"></div>'
		dt.appendChild(d2)
		
		d3.className="col-md-2"
		d3.style.cssText="width:10%;max-width:35px!Important"
		if (cnt==2) d3.innerHTML='<a href="javascript:auth()" class="button-2" style="border-radius:8px;line-height:1.5;margin:0;padding:10px">IMPORT</a>'
		dt.appendChild(d3)
		
		dg.appendChild(dt)
		cnt++
	}
	
	function toggle_default(img) {
		if (img.src.indexOf('chk_on')>=0) {
			img.src='chk_off.png'
			is_default=0
		} else {
			img.src='chk_on.png'
			is_default=1
		}
	}
	var x=[ ]
	var private_invite=0
	function chk_defs(){
		if (!getCookie('channel_service_id')) {
			var url='x_get_channel_service.php?channel_id='+qs('channel_id')
			console.log(url)
			$.ajax({
				url:url,
				success:function(data){
					x=JSON.parse(data)
					if (x) {
						$('#channel_service_desc').html(x.channel_service_desc)
						setCookie('channel_service_id',x.channel_service_id)
						setCookie('channel_service_name',x.channel_service_name)
						setCookie('channel_service_design',x.designPage)
						setCookie('channel_service_view',x.viewPage)
						setCookie('channel_service_control',x.controlPage)
					}
				}
			})
		}
	}
	var invts=[ ], mobls=[ ]
	setTimeout(function(){
		chk_defs()
	},10)
	var arn
	function getCallerID(id,mobile){
			mobile=isValidMobile(mobile)
			if (mobile===false) {$.alert('Invalid Mobile');return}
			$$(id).value=mobile
			arn=id.split('_')[1]
			var url='https://linqstar.com/cid.php?mobile='+mobile
			console.log(url)
			$.ajax({
				url:url,
				success:function(data){
					var name
					if (data) {
						name=data
					} else {
						name='User ' + arn
					}
					$$('name_'+arn).innerHTML=name
					if (invts.indexOf(name.trim())<0) invts.push(name)
					if (mobls.indexOf(mobile.trim())<0) mobls.push(mobile)
				}
			})
		}
	
	var content,provider_mid
	
	function add_conference() {
		var strName=''
		var strMobile=''
		for (var j=0; j<mobls.length; j++) {
			strMobile += mobls[j] + '|'
		}
		strMobile=strMobile.substr(0,strMobile.length-1)
		for (var k=0; k<invts.length; k++) {
			strName += invts[k] + '|'
		}
		strName=strName.substr(0,strName.length-1)

		var url='x_add_conference.php?title='+$$('title').value+'&booking_date='+$$('booking_date').value+'&start_time='+$$('start_time').value+'&viewPage='+getCookie('channel_service_view')+'&controlPage='+getCookie('channel_service_control')+'&initiated_by=celeb&channel_service_id='+getCookie('channel_service_id')+'&start_date='+$$('booking_date').value+'&start_time='+$$('start_time').value+'&channel_id='+qs('channel_id')+'&mobiles='+strMobile+'&names='+strName+'&provider_mid='+getCookie('mid')+'&channel_service_id='+getCookie('channel_service_id')+'&channel_service_name='+getCookie('channel_service_name')
		console.log(url)
		$.ajax({
				url:url,
			success:
				function(msg){
					if (isNaN(msg)) {
							content='Are you ready to start your conference? You are all set! If you scheduled the call for now, then Click below to go to call.<br><br>If the call was scheduled for some time in the future,  you may exit and go do your thing. We will contact you when its time.'
							$.alert({
								title: 'Success',
								content: content,
								buttons: {
									b1: {
										text: 'Start My Call!',
										btnClass: 'btn-white',
										action: function(){
											location.href=msg
										}
									},
									b2: {
										text: 'Home',
										btnClass: 'btn-white',
										action: function(){
											location.href='home.php'
										}
									}
								}
							});
						}
					}
				})
			}
	var sites
	var imagesUploaded
	var r,k,c,t2,t3,t4,intr=50,currImg
	var rat=1
	var rx=0, t, ext
	var set=false
	var usr_img=[]
	var collection_set=0
	var channel_set=0
	var channel_id
	function getCookie(cname)	{
		var name = cname + "=";
		var ca = document.cookie.split(';');
		for(var i=0; i<ca.length; i++) {
		  var c = ca[i].trim();
		  if (c.indexOf(name)==0) return c.substring(name.length,c.length);
		}
		return "";
	}

	function setCookie(cname,cvalue,exdays)	{
		var d = new Date();
		d.setTime(d.getTime()+(exdays*24*60*60*1000));
		var expires = "expires="+d.toGMTString();
		document.cookie = cname + "=" + cvalue + "; " + expires;
	 }	
	
	function print_blocks(ppx) {
		if (ppx=='undefined') return false
		ppx=ppx.toString()
		var f
		var px=''
		var x=''
		if (ppx*1<10) x='00'+ppx
		if ((ppx*1>9) && (ppx*1<100)) x='0'+ppx
		if (ppx*1==100) x=ppx
			
		if (!wd) var wd='20px'
		for (f=0;f<=x.length-1;f++) {
			var dg=x.substring(f,f+1)
			if (dg !== '.')
				px=px+'<span><img src="https://gaysugardaddyfinder.com/assets/img/' + dg + '.png" style="width:'+wd+'"></span>'
		}
		return '<span style="padding-top:5px">'+px+'</span>'
	}
	
	var done=false
		function win() {
			if (done===true) return false
			$.alert({
				title: 'Success',
				content: 'Image(s) uploaded, saved and cataloged. Whats next?',
				btnClose:true,
				buttons: {
					b1: {
						text: 'Home',
						btnClass: 'btn-white',
						action: function(){
							location.href='home.html'
						}
					},
					b2: {
						text: 'My Uploads',
						btnClass: 'btn-white',
						action: function(){
							location.href='show_uploads.html'
						}
					},
					b3: {
						text: 'My Account',
						btnClass: 'btn-red',
						action: function(){
							location.href='settings.html'
						}
					}
				}
			});
			done=true
		}
		function fail(error) {
			alert("An error has occurred: "+error)
		}	
		var currPage='upload_photo.html'
		function fs(el) {
		if (!el) el = document.documentElement
		, rfs = // for newer Webkit and Firefox
			   el.requestFullScreen
			|| el.webkitRequestFullScreen
			|| el.mozRequestFullScreen
			|| el.msRequestFullScreen
		;
		if(typeof rfs!="undefined" && rfs){
		  rfs.call(el);
		} else if(typeof window.ActiveXObject!="undefined"){
		  // for Internet Explorer
		  var wscript = new ActiveXObject("WScript.Shell");
		  if (wscript!=null) {
			 wscript.SendKeys("{F11}");
		  }
		}
		}
	//setTimeout('fs()',1000)
	function guid() {
	  function s4() {
		return Math.floor((1 + Math.random()) * 0x10000)
		  .toString(16)
		  .substring(1);
	  }
	  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
		s4() + '-' + s4() + s4() + s4();
	}
	var sa=[]
	var ra=[]
	function saveSite(s) {
		sites=$('.select-1').val().toString()
		console.log(sites)
	}
	
			function generateRandomString(j){
				if (!j) j=16
				var op=''
				var arr=new Array('a','b','c','d','e','f','i','j','k','m','n','p','q','r','s','t','u','v','w','x','y','z',0,1,2,3,4,5,6,7,8,9,0,0,1,2,3,4,5,6,7,8,9,0)
				for (k=0;k<j;k++) {
					var inx=randomIntFromInterval(0,arr.length-1)
					op += arr[inx]
				}
				return op;
			}
			function guid() {
			  function s4() {
				return Math.floor((1 + Math.random()) * 0x10000)
				  .toString(16)
				  .substring(1);
			  }
			  return s4() + '-' + s4();
			}
			function wait(txt) {
				if (!txt) var txt='Please wait...'
				var tx=document.createElement('div')
				var md=document.createElement('div')
				var im=document.createElement("img")
				tx.id='tx'
				md.id='md'
				im.id='im'
				im.src="https://gaysugardaddyfinder.com/assets/images/load.gif"
				im.style.cssText='width:40px;position:fixed;left:0;right:0;top:0;bottom:0;margin:auto;z-index:999999999999999999999'
				md.style.cssText='width:100%;height:100%;position:fixed;left:0;right:0;top:0;bottom:0;margin:auto;z-index:999999999999999999999;background:#000;opacity:0.3'
				tx.style.cssText='color:#fff;width:300px;height:50px;text-align:center;position:fixed;left:0;right:0;top:0;bottom:0;margin:auto;z-index:99999999999999999999999;padding-top:100px'
				tx.textContent=txt
				document.documentElement.appendChild(md)
				document.documentElement.appendChild(im)
				document.documentElement.appendChild(tx)
			}
			function wait_alt(txt) {
				if (!txt) var txt='Please wait...'
				var tx=document.createElement('div')
				var md=document.createElement('div')
				var im=document.createElement("img")
				tx.id='tx'
				md.id='md'
				im.id='im'
				im.src="https://gaysugardaddyfinder.com/assets/images/load.gif"
				im.style.cssText='width:40px;position:fixed;left:0;right:0;top:0;bottom:0;margin:auto;z-index:99'
				md.style.cssText='width:100%;height:100%;position:fixed;left:0;right:0;top:0;bottom:0;margin:auto;z-index:99;background:#000;opacity:0.3'
				tx.style.cssText='color:#fff;width:300px;height:50px;text-align:center;position:fixed;left:0;right:0;top:0;bottom:0;margin:auto;z-index:99;padding-top:100px'
				tx.textContent=txt
				document.documentElement.appendChild(md)
				document.documentElement.appendChild(tx)
			}
	
			function hide() {
				$$('im').style.display='none'
				$$('tx').style.display='none'
				$$('md').style.display='none'
			}		
		function $$(obj) {
			return document.getElementById(obj)
		}		
	
	function delCookie(cname) {
		var d = new Date();
		d.setTime(d.getTime());
		var expires = "expires="+d.toGMTString();
		document.cookie = cname + "=" + "" + "; " + expires;
	 } 
	function setCookie(cname,cvalue,exdays)	{
		var d = new Date();
		d.setTime(d.getTime()+(exdays*24*60*60*1000));
		var expires = "expires="+d.toGMTString();
		document.cookie = cname + "=" + cvalue + "; " + expires;
	 }
	function c_us() {
		window.top.contactUs()
	}
	function $$(objID) {
		return document.getElementById(objID)
	}
	function getCookie(cname)	{ 
		if (window.location.href.indexOf('file://')>=0) {
			var cvalue = window.localStorage.getItem(cname);
			return cvalue
		} else {
			var name = cname + "="; 
			var ca = document.cookie.split(';'); 
			for(var i=0; i<ca.length; i++) { 
			  var c = ca[i].trim(); 
			  if (c.indexOf(name)==0) return c.substring(name.length,c.length); 
			} 
			return ""; 
		}
	} 
	var tgs=[ ]
	setTimeout(function(){
		$("#channeltags").tagit({
				singleField: true,
				allowSpaces: true,
				tagLimit:10,
			afterTagAdded: function(event, ui) {
				tgs.push(ui.tag[0].innerText)
			}
		})
	},1000)

</script>
<script src="doc/js/common_scripts.min.js"></script>
<script src="doc/js/functions.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script> 
</body>
</html>