<?php
	require_once "../class/utils.class.php";
	$c=new utils;
	$c->connect("199.91.65.83","voxeo");
	parse_str(http_build_query($_GET));
	$pro=$c->query("select * from celeb_profiles where mid=$provider_mid");
	foreach($pro[0] as $key => $value) {
		${$key}=$value;
	}
	$ta=$c->query("select * from celeb_tags where pid=$pid");
	$t=explode(",",$ta[0]['tags']);
?>
<script src="assets/js/jquery.js"type="text/javascript"></script>
<script src="assets/js/jquery-ui.min.js" type="text/javascript" charset="utf-8"></script>	
<script src="js/tag-it.js" type="text/javascript" charset="utf-8"></script>	
<script src="assets/js/bootstrap.min.js"type="text/javascript"></script>
<script src="assets/js/utils.js"type="text/javascript"></script>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>LinqStar</title>
	<link href="doc/css/bootstrap.min.css" rel="stylesheet">
	<link href="doc/css/style.css" rel="stylesheet">
	<link href="doc/css/menu.css" rel="stylesheet">
	<link href="doc/css/vendors.css" rel="stylesheet">
	<link href="doc/css/icon_fonts/css/all_icons_min.css" rel="stylesheet">
    
	<!-- YOUR CUSTOM CSS -->
	<link href="doc/css/custom.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="assets/css/animate.css"/>
    <link rel="stylesheet" href="assets/css/jquery-ui.min.css"/>
    <link rel="stylesheet" href="assets/css/main.css"/>
    <link rel="stylesheet" href="css/shadows.css"/>
	<link href="style.css" rel="stylesheet" type="text/css" />
    <link href="css/jquery.tagit.css" rel="stylesheet" type="text/css">
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:300|Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
	<style>
		* {font-family:Open Sans;
		font-size:14px}
		div input span {font-size:14px;}
		.card_cover{
			background:aliceblue;
			border:0px solid white;
			height:175px;
			margin:10px;
			padding:0
		}
	</style>
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Allura&family=Gloria+Hallelujah&family=Homemade+Apple&display=swap" rel="stylesheet">	
    <link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon"/>
</head>
<body  style="background:url(assets/bg.png)no-repeat;background-size:cover">
	<img style="width:200px;margin:auto;left:0;right:0;position:absolute;top:10px" src="assets/images/logo2.png" alt="logo"/>
	<div class="text-center" style="margin-top:200px">
		<div class="account-wrapper" style="margin:auto;padding:10px;max-width:1000px;background:none">
			<div id="apex" class="account-body">
				<div class="row">
					<div class="col-md-12 text-center">
						<h2 style="color:maroon">CREATE NEW CARD</h2>
					</div>
				</div>
				<span id="part1">
					<div class="row text-center" style="background:;width:100%;margin:0;padding:0">
						<div class="col-md-12 text-center"><br>
						<h4 style="color:white">Select Card Cover</h3>
							<div class="row text-center">
								<?	for ($i=1; $i<=20; $i++) { ?>
									<div onclick="step2('c<?=$i;?>')" class="col-md-2 card_cover www_box">
										<img src="assets/cards/c<?=$i;?>.png" style="width:100%;height:100%">
									</div>
								<? } ?>	
							</div>
						</div>
					</div>
				</span>	
				<br>				
				<span id="part2" style="display:none">
					<div class="row text-left">
						<div class="col-md-12 text-center">
						Please enter at least one
							<div class="col-md-6 text-left" style="margin:auto;border:10px solid white;border-radius:8px;padding:20px;background:aliceblue">
								<div>
									<label>Recepient Name</label>
									<input class="form-control" type="text" id="to_name" />
								</div>
								<div>
									<label>Recepient Mobile</label>
									<input class="form-control" type="text" id="to_mobile" />
								</div>
								<div>
									<label>Recepient Email</label>
									<input class="form-control" type="text" id="to_email" />
								</div>
								<div>
									<label>From Display Name</label>
									<input class="form-control" type="text" id="from_name" />
								</div>

							</div>
						</div>
						<div class="col-md-12 text-center">
							<div class="col-md-6 text-center" style="margin:auto">
								<button style="width:100%;max-width:400px" onclick="step1()" class="button-5">Go back</button>
								<button style="width:100%;max-width:400px" onclick="step3()" class="button-5">Next - Enter Message</button>
							</div>
						</div>
					</div>
				</span>					
				<span id="part3" style="display:none">
					<div class="row text-left">
						<div class="col-md-12 text-center">
							<div class="col-md-6 text-left" style="margin:auto;border:10px solid white;border-radius:8px;padding:20px;background:aliceblue">
								<label>Salutation</label>
								<input style="min-width:100%;background:white;font-size:14px;min-height:40px" class="form-control" id="msg1" />
								<br>
								<label>Please enter a personalized message</label>
								<textarea style="min-width:100%;background:white;font-size:14px;min-height:250px" class="form-control" id="msg"></textarea>
								<br>
								<label>Sign Off Message</label>
								<input style="min-width:100%;background:white;font-size:14px;min-height:40px" class="form-control" id="msg2" />
								<br>
							</div>
						</div>
						<div class="col-md-12 text-center">
							<div class="col-md-6 text-center" style="margin:auto">
								<button style="width:100%;max-width:400px" onclick="step4()" class="button-5">Save and Preview</button>
								<button style="width:100%;max-width:400px" onclick="step2()" class="button-5">Go back</button>
							</div>
						</div>
					</div>
				</span>					
				<span id="part4" style="display:none">
					<h4 style="color:white">Preview Card Cover</h4>
					<div class="row text-center">
						<div class="col-md-6 www_box" style="width:45%;height:600px; background:#f0f0f0; margin:auto;padding:0">
							<img id="card_cover" style="width:100%;height:100%;margin:0;padding:0" />
						</div>
					</div>
					<div class="col-md-12 text-center">
						<div class="col-md-6 text-center" style="margin:auto">
							<button style="width:100%;max-width:400px" onclick="step3()" class="button-5">Go back</button>
							<button style="width:100%;max-width:400px" onclick="step5()" class="button-5">Preview Inside</button><br>
							<button style="width:100%;max-width:400px" onclick="step6()" class="button-5">Add Video Shout-out</button><br>
							<button style="width:100%;max-width:400px" onclick="step7()" class="button-5">Configure Delivery and Send</button>
						</div>
					</div>						
				</span>					
				<span id="part5" style="display:none">
					<h4 style="color:white">Preview Card Inside</h4>
					<div class="row text-left">
							<div class="col-md-6 www_box" style="width:45%;height:600px; background:silver; margin:0;padding:0">
								<div id="pane1" style="display:none;margin:auto;width:70%;height:70%;margin-top:20%;margin-bottom:15%;background:url(assets/bg.png);color:white;text-align:center;vertical-align:middle;line-height:1.4em;" class="www_box"><div style="display:none;position:absolute;left:0;right:0;top:0;bottom:0;border-radius:300px;background:#660066;width:225px;height:225px;margin:auto;font-family:Gloria Hallelujah;font-size:24px;box-shadow:0 0 15px #000;opacity:0.5"><br><br>Thanks<br>for using<br><span style="line-height:1.4em;font-size:36px;font-family:Gloria Hallelujah!Important">LinqStar!</span></div></div>
							</div>
							<div class="col-md-6 www_box" style="width:45%;height:600px; background:#f0f0f0; margin:0;padding:0">
								<div id="pane2" style="margin:auto;width:70%;height:70%;margin-top:20%;margin-bottom:15%;background:aliceblue;color:blue;text-align:left;vertical-align:middle;line-height:1.3em;font-size:21px;font-family:Gloria Hallelujah!Important;padding:20px" class="www_box"></div>
							</div>
						</div>
						<div class="col-md-12 text-center">
							<div class="col-md-6 text-center" style="margin:auto">
								<button style="width:100%;max-width:400px" onclick="step6()" class="button-5">Add Video Shout-out</button><br>
								<button style="width:100%;max-width:400px" onclick="step7()" class="button-5">Configure Delivery and Send</button>
								<button style="width:100%;max-width:400px" onclick="step4()" class="button-5">Go back</button>
							</div>
						</div>						
					</div>
				</span>					
				<span id="part6" style="display:none">
					<div class="row text-center">
						<div class="col-md-12 text-center">
							<div class="col-md-6" style="margin:auto">
								<h4 style="color:white">Add Video Shout-out</h4>
								<button style="width:100%;max-width:400px" class="button-5">Record Video Message Now</button><br>
								<button style="width:100%;max-width:400px" class="button-5">Pay for Celebrity Video Message</button><br>
								<button onclick="step7()" style="width:100%;max-width:400px" class="button-5">Configure Delivery and Send</button>
							</div>
						</div>
					</div>
				</span>	
				<span id="part7" style="display:none">
					<div class="row text-center">
						<div class="col-md-12 text-center">
							<div class="col-md-6" style="margin:auto">
								<h4 style="color:white">Configure Delivery</h4>
								<button onclick="step1()" style="width:100%;max-width:400px" class="button-5">Start Over</button><br>
								<button style="width:100%;max-width:400px;font-family:Open Sans!Important" class="button-5">Send it Now</button><br>
								<div id="scl8r1" class="row text-left" style="display:none">
									<div class="col-md-1">

									</div>
									<div class="col-md-5">
										<div style="color:white"><b>SEND DATE</b></div><input class="form-control td-input" type="text" id="booking_date" style="font-size:1em">
									</div>
									<div class="col-md-5">
										<div style="color:white"><b>SEND TIME</b></div><input class="form-control td-input" type="text" id="start_time" style="font-size:1em">
									</div>
									<div class="col-md-1">

									</div>
								</div>
								<div id="scl8r2" class="row text-left" style="display:none">
										<div class="col-md-1">

										</div>
										<div class="col-md-10">
											<button onclick="save_later()" style="width:100%; background:url(assets/bg.png);background-size:cover;color:white" class="button-5">Save and Schedule</button><br>
										</div>
										<div class="col-md-1">

										</div>
									</div>
								<button onclick="later()" style="width:100%;max-width:400px" class="button-5">Schedule for Later</button><br>
								<button style="width:100%;max-width:400px" class="button-5">Send On My Cue</button>
							</div>
						</div>
					</div>
				</span>					
			</div>
		</div>
	</div>
	<script src="assets/js/wait.js"></script>
	<script src="assets/js/common.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="./assets/js/modernizr-3.6.0.min.js"></script>
    <script src="./assets/js/plugins.js"></script>
    <script src="./assets/js/bootstrap.min.js"></script>
    <script src="./assets/js/jquery-ui.min.js"></script>
    <script src="./assets/js/main.js"></script>
    <script src="assets/js/common.js"></script>
    <script src="js/location.js" type="text/javascript" charset="utf-8"></script>	
	<script src="js/utils.js"type="text/javascript"></script>
	<script src="https://apis.google.com/js/client:platform.js?onload=start" async defer></script>	
	<script type="text/javascript">
		function step1(){
			$('#part2').hide()
			$('#part3').hide()
			$('#part4').hide()
			$('#part1').show()
			$('#part5').hide()
			$('#part6').hide()
			$('#part7').hide()
		}
		function step2(id){
			$('#part1').hide()
			$('#part3').hide()
			$('#part4').hide()
			$('#part2').show()
			$('#part5').hide()
			$('#part6').hide()
			$('#part7').hide() 
			setCookie('card_cover',id)
		}
		function step3(){
			$('#part1').hide()
			$('#part3').show()
			$('#part4').hide()
			$('#part2').hide()
			$('#part5').hide()
			$('#part6').hide()
			$('#part7').hide()
		}
		function step4(){
			$('#part1').hide()
			$('#part3').hide()
			$('#part4').show()
			$('#part2').hide()
			$('#part5').hide()
			$('#part6').hide()
			$('#part7').hide()
			setCookie('msg',$$('msg').value)
			$$('card_cover').src='assets/cards/'+getCookie('card_cover')+'.png'
			$$('pane2').innerHTML=$$('msg1').value + '<br><br>' + $$('msg').value + '<br><br>' + $$('msg2').value
		}
		function step5(){
			$('#part1').hide()
			$('#part3').hide()
			$('#part5').show()
			$('#part2').hide()
			$('#part4').hide()
			$('#part6').hide()
			$('#part7').hide()
		}
		function step6(){
			$('#part1').hide()
			$('#part3').hide()
			$('#part6').show()
			$('#part2').hide()
			$('#part4').hide()
			$('#part5').hide()
			$('#part7').hide()
		}
		function step7(){
			$('#part1').hide()
			$('#part3').hide()
			$('#part7').show()
			$('#part2').hide()
			$('#part4').hide()
			$('#part5').hide()
			$('#part6').hide()
		}
		function later() {
			if ($$('scl8r1').style.display=='none') {
				$$('scl8r1').style.display='' 
				$$('scl8r2').style.display='' 
			} else {
				$$('scl8r1').style.display='none' 
				$$('scl8r2').style.display='none' 
			}
		}
	</script>
	<script src="doc/js/common_scripts.min.js"></script>
	<script src="doc/js/functions.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script> 
</body>
</html>