<?php
include "../class/utils.class.php";
$c=new utils;
$c->connect("199.91.65.83","voxeo");
parse_str(http_build_query($_GET));
$public=($private_invite=='0')?1:0;

function date_a($date, $q) {
	return date("Y-m-d", floor(strtotime($date) + $q*30*24*3600));
}

for ($u=0; $u<=$repeat*1; $u++) {
	$start_date=date_a($start_date,$u);
	$x_start=strtotime($start_date . " " . $start_time);
	$x_end=strtotime($start_date . " " . $end_time);
	$end_date=$start_date;
	$date_from=date("d-m-Y h:i a",$x_start);
	$date_to=date("d-m-Y h:i a",$x_end);

	$sql="
INSERT INTO `voxeo`.`greeting_cards` (
	`title`,
	`sender_name`,
	`sender_email`,
	`recepient_name`,
	`recepient_email`,
	`recepient_mobile`,
	`cover_id` 
)
VALUES
	(
		'$title',
		'$name_from',
		'$email_from',
		'$name_to',
		'$email_to',
		'$mobile_to',
		'$cover_id')";
	$c->insert($sql);


	$sql="INSERT INTO `voxeo`.`celeb_broadcasts` (
		`provider_mid`,
		`title`,
		`description`,
		`tags`,
		`cover`,
		`start_date`,
		`start_time`,
		`end_time`,
		`public`,
		`invite_only`,
		`ppv`,
		`subscription`,
		`channel_service_name`,
		`cost`,
		`channel_id`,
		`channel_service_id`,
		`status`,
		`x_start`
	)
	VALUES
		(
			'$provider_mid',
			'$broadcast_name',
			'$desc',
			'$tags',
			'$cover',
			'$start_date',
			'$start_time',
			'$end_time',
			'$public',
			'$private_invite',
			'$ppv',
			'$subscription',
			'$channel_service_name',
			'$bcost',
			'$channel_id',
			'$channel_service_id',
			'SCHEDULED',
			'$x_start'
			)";
	$broadcast_id=$c->insert($sql);
	$link1="https://linqstar.com/broadcast/" . $c->encrypt($broadcast_id,"");
	$link2="https://linqstar.com/viewcast/" . $c->encrypt($broadcast_id,"");
	$c->insert("update `voxeo`.`celeb_broadcasts` set `link`='$link2' where `broadcast_id`=$broadcast_id");
	
	if ($u==0) echo $link1;

	/*
	//add to either:
	1. 	celeb_media
	2. 	celeb_broadcasts
	3.	celeb_vod
	4.	celeb_anon
	5.	celeb_rental
	6.	celeb_conf
	*/
	if (!$broadcast_asset_id) $broadcast_asset_id=0;
	$sql="INSERT INTO `voxeo`.`celeb_channel_content` ( 
							`provider_mid`,
							`channel_id`,
							`content_id`,
							`content_asset_id`,
							`content_type_id`)
					VALUES
						(
							$provider_mid,
							$channel_id,
							$broadcast_id,
							$broadcast_asset_id,
							$channel_service_id
						)";
	$c->insert($sql);
	$arr=array('==');
	$stream=str_replace($arr,"",$broadcast_id);
	$app="live";
	$sql="INSERT INTO `voxeo`.`celeb_live_events` ( `event_id`, `event_type`, `owner_mid`, `channel_id`, `event_type_id`, `stream_name`, `app`, `launch_url`, `launch_time`, `room`, `x_start` )
	VALUES
		(
			'$broadcast_id',
			'$channel_service_name',
			'$provider_mid',
			'$channel_id',
			'$channel_service_id',
			'$stream',
			'$app',
			'$link1',
			'$launch_time',
			'',
			'$x_start')";
	$c->insert($sql);
												  
	//Write to Subscription_ packages table 'celeb_subscription'
	//'active_subscriptions' will actually account and track all subscriptions bought and what for
	if ($ppv=='1') {
		$sql="INSERT INTO `voxeo`.`celeb_subscriptions` (`mid`, `subscription_title`, `subscription_channel`, `subscription_content`, `subscription_content_type_id`, `subscription_amount`, `period`, `period_qty`) VALUES ($provider_mid, '$broadcast_name', '$channel_id', '$broadcast_id', '$channel_service_id', '$bcost', '$period', $qty_period)";
	} else {
		$sql="INSERT INTO `voxeo`.`celeb_subscriptions` (`mid`, `subscription_title`, `subscription_channel`, `subscription_content`, `subscription_content_type_id`, `subscription_amount`, `period`, `period_qty`) VALUES ($provider_mid, '$broadcast_name', '$channel_id', '$broadcast_id', '$channel_service_id', '$bcost', '$period', $qty_period)";
		$c->insert($sql);
	}
	//Add to celeb_live_event_members

		$mobile=$_COOKIE['mobile'];
		$status='pending';
		

		
	if ($x_start>time()) {
		$msg1=urlencode("To start broadcast please click link you will receive in the next message. Do NOT share this with your fans.");
		$sms1="https://linqstar.com/x_send_sms.php?msg=$msg1&to=" . $mobile;
		$sql1="INSERT INTO `voxeo`.`dt_sms_queue` (`job_id`, `to_mobile`, `message`, `created_date_time`, `delivery_date_time`, `url_execute`,`status`) VALUES (
			'$broadcast_id',
			'". $mobile ."',
			'$msg1', 
			'". date("Y-m-d H:i") ."',
			'$x_start', 
			'$sms1',
			'$status'
		)";
		$c->insert($sql1);
		
		$msg2=urlencode("https://linqstar.com/broadcast/" . $c->encrypt($broadcast_id,""));
		$sms2="https://linqstar.com/x_send_sms.php?msg=$msg2&to=" . $mobile;
		$sql2="INSERT INTO `voxeo`.`dt_sms_queue` (`job_id`, `to_mobile`, `message`, `created_date_time`, `delivery_date_time`, `url_execute`, `status`) VALUES (
			'$broadcast_id',
			'". $mobile ."',
			'$msg2', 
			'". date("Y-m-d H:i") ."',
			'$x_start', 
			'$sms2',
			'$status'
		)";
		$c->insert($sql2);

		$msg3=urlencode("Send the next link you get to your fans so that they may view your broadcast.");
		$sms3="https://linqstar.com/x_send_sms.php?msg=$msg3&to=" . $mobile;
		$sql3="INSERT INTO `voxeo`.`dt_sms_queue` (`job_id`, `to_mobile`, `message`, `created_date_time`, `delivery_date_time`, `url_execute`,`status`) VALUES (
			'$broadcast_id',
			'". $mobile ."',
			'$msg3', 
			'". date("Y-m-d H:i") ."',
			'$x_start', 
			'$sms3',
			'$status'
		)";
		$c->insert($sql3);

		$msg4=urlencode("https://linqstar.com/viewcast/" . $c->encrypt($broadcast_id,""));
		$sms4="https://linqstar.com/x_send_sms.php?msg=$msg4&to=" . $mobile;
		$sql4="INSERT INTO `voxeo`.`dt_sms_queue` (`job_id`, `to_mobile`, `message`, `created_date_time`, `delivery_date_time`, `url_execute`, `status`) VALUES (
			'$broadcast_id',
			'". $mobile ."',
			'$msg4', 
			'". date("Y-m-d H:i") ."',
			'$x_start', 
			'$sms4',
			'$status'
		)";
		$c->insert($sql4);	

	} else {

		$msg1=urlencode("To start broadcast please click link you will receive in the next message. Do NOT share this with your fans.");
		$sms1="https://linqstar.com/x_send_sms.php?msg=$msg1&to=" . $mobile;
		$sql1="INSERT INTO `voxeo`.`dt_sms_queue` (`job_id`, `to_mobile`, `message`, `created_date_time`, `delivery_date_time`, `url_execute`,`status`) VALUES (
			'$broadcast_id',
			'". $mobile ."',
			'$msg1', 
			'". date("Y-m-d H:i") ."',
			'$x_start', 
			'$sms1',
			'$status'
		)";
		$c->insert($sql1);
		file_get_contents($sms1);
		
		$msg2=urlencode("https://linqstar.com/broadcast/" . $c->encrypt($broadcast_id,""));
		$sms2="https://linqstar.com/x_send_sms.php?msg=$msg2&to=" . $mobile;
		$sql2="INSERT INTO `voxeo`.`dt_sms_queue` (`job_id`, `to_mobile`, `message`, `created_date_time`, `delivery_date_time`, `url_execute`, `status`) VALUES (
			'$broadcast_id',
			'". $mobile ."',
			'$msg2', 
			'". date("Y-m-d H:i") ."',
			'$x_start', 
			'$sms2',
			'$status'
		)";
		$c->insert($sql2);
		file_get_contents($sms2);

		$msg3=urlencode("Send the next link you get to your fans so that they may view your broadcast.");
		$sms3="https://linqstar.com/x_send_sms.php?msg=$msg3&to=" . $mobile;
		$sql3="INSERT INTO `voxeo`.`dt_sms_queue` (`job_id`, `to_mobile`, `message`, `created_date_time`, `delivery_date_time`, `url_execute`,`status`) VALUES (
			'$broadcast_id',
			'". $mobile ."',
			'$msg3', 
			'". date("Y-m-d H:i") ."',
			'$x_start', 
			'$sms3',
			'$status'
		)";
		$c->insert($sql3);
		file_get_contents($sms3);

		$msg4=urlencode("https://linqstar.com/viewcast/" . $c->encrypt($broadcast_id,""));
		$sms4="https://linqstar.com/x_send_sms.php?msg=$msg4&to=" . $mobile;
		$sql4="INSERT INTO `voxeo`.`dt_sms_queue` (`job_id`, `to_mobile`, `message`, `created_date_time`, `delivery_date_time`, `url_execute`, `status`) VALUES (
			'$broadcast_id',
			'". $mobile ."',
			'$msg4', 
			'". date("Y-m-d H:i") ."',
			'$x_start', 
			'$sms4',
			'$status'
		)";
		$c->insert($sql4);	
		file_get_contents($sms4);
	}
}
$c->close();
?>