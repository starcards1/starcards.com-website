<? date_default_timezone_set("America/New_York");?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">  
    <link rel="stylesheet" href="./assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="./assets/css/jquery-ui.min.css">
    <link href="css/magic.css" rel="stylesheet" type="text/css">
    <link href="css/gloss.css" rel="stylesheet" type="text/css"> 
    <link href="css/shadows.css" rel="stylesheet" type="text/css">
    <link href="auction.css" rel="stylesheet" type="text/css">
    <link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Gugi&family=Open+Sans+Condensed:wght@300&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Big+Shoulders+Text&display=swap" rel="stylesheet">
	<style>
	</style>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript" charset="utf-8"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/4.0.1/socket.io.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="js/utils.js"></script>
	<script>
		var socket 
		var members  
		var jmids=[ ]  
		var objData=[] 
		var charge  
		var name
		var login=getCookie('login'),mobile=getCookie('mobile'),name=getCookie('name') 
		var room=qs('room')
		if (!room) room='room<?=rand(1,2);?>' 
		room='room1'
		function register() {
			socket = io.connect('https://terrawire.com:7774/',{query: {'room': room}});   
			socket.on("connect", function() { 
				var arr=[ ]
				// if (!login || login==null || login=='null') login='guest<?=rand(1,99);?>'
				// if (!mobile || mobile==null || mobile=='null') mobile='<?=rand(1111111111,9999999999);?>'
				async function fetchUsers(endpoint) {
					const res = await fetch(endpoint);
					let data = await res.json();
					console.log(data);
					arr=[{'room':room,'login':data.login,'mobile':data.mobile,'name':data.name,'photo':data.filename_1}] 
					console.log(arr)
					socket.emit('register', JSON.stringify(arr)); 	
					$$('reg_link').style.display='none'
					$$('bid_link').style.display=''
					$$('reg_bid_text').innerHTML='CLICK to BID!'
				}
				var url='https://linqstar.com/x_get_user_data.php?mid=<?=rand(1,10000);?>'
				fetchUsers(url);				
			});
			socket.on("not_started", function(err) {
				alert('Auction Registration has not started yet. Unable to locate room - please chack again, later.')
			});
			
			socket.on('welcome', function(data) {
				$('#title').html(data.toUpperCase())
				$('#title').addClass('magictime vanishIn') 
			})

			socket.on('joined', function(data) {
				toaster(data)
			})
 
			socket.on('bidders', function(data) {
				var bidders=[ ]
				bidders=JSON.parse(data)
				$$('bidder_count').innerHTML=bidders.length
				for (var j=0; j < bidders.length; j++) {
					var bidder=bidders[j]
					var dv=document.createElement('span')
					var im=document.createElement('img')
					var tx=document.createElement('span')
					im.src=bidder.photo
					im.style.cssText='width:40px;height:40px;border-radius:40px'
					tx.innerHTML=bidder.name
					tx.style.fontSize='5px'
					dv.appendChild(im)
					dv.appendChild(tx)
					dv.style.padding='5px'
					$$('usrs').appendChild(dv)
					console.log($$('usrs').innerHTML)
				}
			})

			socket.on('user_bids', function(data) {
				
			})
			socket.on('history', function(data) {
				
			})
			socket.on('headline', function(data) {
				console.log(data)
				$('#headline').html(data.toUpperCase())
				$('#headline').addClass('magictime vanishIn')
			})
			socket.on('highlight_success', function(data) {
				console.log(data)
				highlight_success(data)
			})
			socket.on('highlight_failure', function(data) {
				highlight_failure(data)
			})
			socket.on('title', function(data) {
				console.log(data)
				$('#title').html(data.toUpperCase())
				$('#title').addClass('magictime vanishIn')
			}) 
			socket.on('print', function(data) {
				console.log(data)
				$('#print').html(data)	
				$('#print').addClass('magictime vanishIn')
			})
			socket.on('message', function(data) {
				$$('title').innerHTML=data
				$('#title').addClass('magictime vanishIn')
			})
			socket.on('current_bid', function(data) {
				show_highest_bid(data)
			})
			socket.on('highest_bid', function(data) {
				show_highest_bid(data)
			})
			socket.on('asking_bid', function(data) {
				show_asking_bid(data)
			})
			socket.on('highest_bidder', function(data) {
				$$('highest_bidder_data').innerHTML=data  
			}) 
			socket.on('execute', function(data) {
				eval(data)
			})
			socket.on('won', function(data) {
				stop_red_alert()
				$$('cvr').style.display='block'
				$$('cvr').innerHTML='<div style="font-size:48px;color:#00FF80"><br><br>CONGRATULATIONS</div><br><br><br><br><div style="font-size:21px;color:#f0f0f0">YOU HAVE WON THE AUCTION FOR $' + data + '!'
			})
			socket.on('ended_reserve', function(data) {
				stop_red_alert()
				$$('cvr').style.display='block'
				$$('cvr').innerHTML='<div style="font-size:48px;color:#00FF80"><br><br>SORRY, AUCTION ENDED</div><br><br><br><br><div style="font-size:21px;color:#f0f0f0">YOU HAVE WON THE AUCTION FOR $' + data + '!'
			})
			socket.on('ended_zero', function(data) {
				stop_red_alert()
				$$('cvr').innerHTML='<div style="font-size:48px;color:#00FF80"><br><br>, AUCTION ENDED</div><br><br><br><br><div style="font-size:21px;color:#f0f0f0">THE AUCTION ENDED BECAUSE</div><br><br><div style="font-size:21px;color:red">RESERVE PRICE WASNT MET</div>'
				$$('cvr').style.display='block'
			})
			socket.on('meta_data', function(dat) {
				console.log(dat)
				var data=JSON.parse(dat)
				var scheduled_start_time=data.scheduled_start_time
				var starting_bid=data.starting_bid 
				var current_bid=0
				var asking_bid=starting_bid
				var item_owner=data.item_owner
				var item_name=data.item_name
				var item_desc=data.item_desc
				var item_image=data.item_image
				$$('scheduled_start').innerHTML=scheduled_start_time.split(' ')[1]
				$$('starting_bid').innerHTML=starting_bid
				$$('item_name').innerHTML=item_name
				$$('owner_name').innerHTML=item_owner
				show_asking_bid(asking_bid)
				show_highest_bid(current_bid)
				$('.x_detail').css('opacity', '0.8');
				$('.x_detail').addClass('magictime vanishIn')
			})
		}
		
		var cv=[ ]
		cv[1]=1
		cv[2]=1
		cv[3]=1
		cv[4]=1

		function highlight_success(str) {
			$$('headline').style.background='#00D9A3'
			$$('headline').style.color='#fff'
			$$('headline').style.textShadow='0px 0px 5px #fff'
			$$('headline').innerHTML=str.toUpperCase()
			$('#headline').addClass('magictime vanishIn')
			setTimeout(function(){
				$$('headline').style.background='none'
				$$('headline').style.color='#000'
				$$('headline').style.boxShadow='none'
			},3000)
		}
		function highlight_failure(str) {
			$$('headline').style.background='red'
			$$('headline').style.color='#fff'
			$$('headline').style.textShadow='0px 0px 5px #fff'
			$$('headline').innerHTML=str.toUpperCase()
			$('#headline').addClass('magictime vanishIn')
			setTimeout(function(){
				$$('headline').style.background='none'
				$$('headline').style.color='#000'
				$$('headline').style.boxShadow='none'
			},3000)
		}
		function bid_timer_start(m,s) {
			$$('block_cover1').className='gshade w80'
			start(m,s)
		}
		
		function toggle_block_cover(no) {
			if (cv[no]==1) {
				$$('block_cover'+no).style.height = '30px'
				cv[no]=0
			} else {
				$$('block_cover'+no).style.height = '140px'
				cv[no]=1
			}
		}
		var tmr_red
		var interval=250
		var  alt1='bshade w80'
		var  alt2='rshade1 w80'
		var  alt3='rshade2 w80'
		
		function red_alert() {
			r_a1()
		}

		function red_alert_fast() {
			alt1=alt2
			alt2=alt3
			$('#block_cover1').show()
			$$('block_cover1').style.background='none!Important'
			r_a1()
		}

		function stop_red_alert() {
			$$('block_cover1').className=alt2
			$$('block_cover1').className='w80'
		//	$$('big_btn').src='assets/btn_blue.png'
			clearTimeout(tmr_red)
		}

		function r_a1() {
			if ($$('block_cover1').className.indexOf(alt1)>=0) {
				setTimeout(function(){
					$$('block_cover1').className=alt2
				//	$$('big_btn').src='assets/btn_red.png'
				},1)
			} else {
				setTimeout(function(){
					$$('block_cover1').className=alt1 
			//		$$('big_btn').src='assets/btn_blue.png'
				},1)
			}
			tmr_red=setTimeout(function(){
				r_a1()
			},interval)
		}
	
		function bid() {
			if (socket) {
				var arr=[ ]
				var bid=getCookie('my_bid')
				arr=[{'room':room,'login':login,'mobile':mobile,'name':name,'bid':bid}]
				socket.emit('bid', JSON.stringify(arr));  
			} else {
				alert('Wait for the manager please')
			}
		}
		
		if (!qs('secs')) var secs=10
			else secs=qs('secs')
		
		if (!qs('mins')) var mins=10
			else mins=qs('mins')
		
		var js=mins*60+secs*1
		
		var tmr
		function timer() {
			if (js>0) {
				js--
			}
			if (js==10) red_alert()
			
			mins=Math.floor(js/60)
			secs=js%60
			
			if (mins<10) {
				mins='0' + ('' + mins)
			}  else {
				mins = '' + mins 
			}

			if (secs<10) {
				secs='0' + ('' + secs)
			}  else {
				secs = '' + secs
			}

			$$('mt').src='assets/' + mins.substr(0,1) + '.png'
			$$('mu').src='assets/' + mins.substr(1,1) + '.png'
			$$('st').src='assets/' + secs.substr(0,1) + '.png'
			$$('su').src='assets/' + secs.substr(1,1) + '.png'
			
			tmr=setTimeout(function(){
				if (js==0) {
					stop()
					stop_red_alert()
				} else {
					timer()
				}
			},990)
		}
		
		function stop() {
			clearTimeout(tmr)
			$$('mt').src='assets/0d.png'
			$$('mu').src='assets/0d.png'
			$$('st').src='assets/0d.png'
			$$('su').src='assets/0d.png'
			secs=0
			mins=0
			js=0
		}
		
		function pause() {
			clearTimeout(tmr)
		}
		
		function resume() {
			tmr=setTimeout(function(){
				timer()
			},990)
		}

		function start(m,s) {
			stop()
			mins=m
			secs=s+1
			js=mins*60+secs*1
			tmr=setTimeout(function(){
				timer()
			},990)
		}
		var my_bid=0
		var dollar='<img src="assets/dollar.png" style="left:30px;height:50px;margin-top:5px;margin-right:10px">'
		function max(x) {
			my_bid+=x
			setCookie('my_bid',my_bid)
			var op=''
			var str=my_bid + ''
			for (var j=0; j<str.length; j++) {
				op+='<img src="assets/g'+str.substr(j,1)+'.png" style="left:30px;height:50px;position:;margin-top:5px"> '
			}
			$$('my_lcd_bid').innerHTML=dollar+op
		}
	
		function min(y) {
			my_bid-=y
			if (my_bid<0) my_bid=0
			setCookie('my_bid',my_bid)
			var op=''
			var str=my_bid + ''
			for (var j=0; j<str.length; j++) {
				op+='<img src="assets/g'+str.substr(j,1)+'.png" style="left:30px;height:50px;position:;margin-top:5px"> '
			}
			$$('my_lcd_bid').innerHTML=dollar+op
		}
		
		function show_highest_bid(highest_bid) {
			setCookie('highest_bid',highest_bid)
			var hp=''
			var str=highest_bid + ''
			for (var j=0; j<str.length; j++) {
				hp+='<img src="assets/'+str.substr(j,1)+'.png" style="left:30px;height:30px;position:;margin-top:5px"> '
			}
			$$('highest_bid_data').innerHTML=hp
		}

		function show_asking_bid(asking_bid) {
			setCookie('asking_bid',asking_bid)
			var ap=''
			var str=asking_bid + ''
			for (var j=0; j<str.length; j++) {
				ap+='<img src="assets/'+str.substr(j,1)+'.png" style="left:30px;height:30px;position:;margin-top:5px"> '
			}
			$$('asking_bid_data').innerHTML=ap
		}
		var ww=(window_width()/12)
		function build_cams() {
			var str=''
			for (var i=0; i<12; i++) {
				str+='<div id="close_bottom'+i+'" class="col-md-1 www_box5" style="width:'+ww+'px;height:120px">'
				str+='<a href="javascript:hideStream(\'close_bottom'+i+'\')"><img src="assets/close.png" style="z-index:9999999;cursor:hand; cursor:pointer; position:absolute;width:15px;right:10px;top:10px"></a>'
				str+='<img src="assets/fm.png" style="position:absolute;width:100%;left:0">'	
				str+='</div>'
			}
			$('#cam_row_bottom').html(str)
			str=''
			for (var i=0; i<12; i++) {
				str+='<div id="close_top'+i+'" class="col-md-1 www_box5" style="width:'+ww+'px;height:120px">'
				str+='<a href="javascript:hideStream(\'close_top'+i+'\')"><img src="assets/close.png" style="z-index:9999999;cursor:hand; cursor:pointer; position:absolute;width:15px;right:10px;top:10px"></a>'
				str+='<img src="assets/fm.png" style="position:absolute;width:100%;left:0">'	
				str+='</div>'
			}
			$('#cam_row_top').html(str)
		}
		setTimeout(function(){
			build_cams()
		},1)
		
		function showHideVideoStreams() {
			if ($$('cam_row_bottom').style.display=='') {
				$$('cam_row_bottom').style.display='none'
				$$('cam_row_top').style.display='none'
				$$('shv').src='assets/shv.png'
			} else {
				$$('cam_row_bottom').style.display=''
				$$('cam_row_top').style.display=''
				$$('shv').src='assets/shv_hide.png'
			}
		}
		
		function hideStream(id) {
			$$(id).style.display='none'
		}

	
	</script>
	<body style="background:url(assets/bb3.png); background-size:cover">
		<div class="container outer www_box2">
			<div class="row www_box2" style="height:110px">
				<a href="javascript:showHideVideoStreams()"><img id="shv" src="assets/shv_hide.png" style="height:75px;position:absolute;margin:10px"></a>
				<div id="headline" class="col-md-12 text-center">
					WELCOME TO LIVE AUCTIONS
				</div>
			</div>
			<div class="row" style="height:60px">
				<div id="title" class="col-md-12 text-center">
					Just Like Real Life
				</div>
			</div>
			<div class="row www_box2">
				<div id="print" class="col-md-12 text-center" style="padding-top:25px">
				</div>
			</div>
			<div class="row text-center www_box5"  style="height:120px">
				<div id="timer_block" class="col-md-3 w80">
					<div id="block_cover1" style="margin-left:-35px;position:absolute" class="w80"></div>
					<div style="font-family:Big Shoulders Text!Important;color:white!Important;position:absolute;left:0;right:0;margin:auto;margin-bottom:0px;font-size:24px; text-align:center"><span style="margin-bottom:10px;font-family:Big Shoulders Text!Important"> BID TIMER</span></div>
					<div id="timer_block_data" style="margin-top:35px;background:black;border-radius:6px;padding:10px;text-align:center;font-size:24px">
						<img id="mt" src="assets/0d.png" style="width:24px">
						<img id="mu" src="assets/0d.png" style="width:24px">
						<img src="assets/dotsd.png" style="width:8px">
						<img id="st" src="assets/0d.png" style="width:24px">
						<img id="su" src="assets/0d.png" style="width:24px">
					</div>
				</div>
				<div id="top_bidder" class="col-md-3 w80">
					<div id="block_cover2" style="margin-left:-35px;position:absolute" class="w80"></div>
					<div style="font-family:Big Shoulders Text!Important;color:white!Important;position:absolute;left:0;right:0;margin:auto;margin-bottom:0px;font-size:24px; text-align:center"><span style="margin-bottom:10px;font-family:Big Shoulders Text!Important;padding-bottom:15px"> TOP BIDDDER</span></div>
					<div id="highest_bidder_data"  style="margin-top:35px;background:black;border-radius:6px;padding:10px;text-align:center;font-size:24px">
						Top Bidder 
					</div>
				</div>
				<div id="amount_asking" class="col-md-3 w80">
					<div id="block_cover2" style="margin-left:-35px;position:absolute" class="w80"></div>
					<div style="font-family:Big Shoulders Text!Important;color:white!Important;position:absolute;left:0;right:0;margin:auto;margin-bottom:0px;font-size:24px; text-align:center"><span style="margin-bottom:10px;font-family:Big Shoulders Text!Important;padding-bottom:15px"> NOW ASKING</span></div>
					<div id="asking_bid_data"  style="margin-top:35px;background:black;border-radius:6px;padding:10px;text-align:center;font-size:24px">
						 <img src="assets/0d.png" style="width:24px;margin-right:2px">
					 	 <img src="assets/0d.png" style="width:24px">
						 <img src="assets/0d.png" style="width:24px;margin-left:2px">
					</div>
				</div>
				<div id="highest_bid" class="col-md-3 w80">
					<div id="block_cover4" style="margin-left:-35px;position:absolute" class="w80"></div>
					<div style="font-family:Big Shoulders Text!Important;color:white!Important;position:absolute;left:0;right:0;margin:auto;margin-bottom:0px;font-size:24px; text-align:center"><span style="margin-bottom:10px;font-family:Big Shoulders Text!Important"> TOP BID</span></div>
					<div id="highest_bid_data"  style="margin-top:35px;background:black;border-radius:6px;padding:10px;text-align:center;font-size:24px">
						 <img src="assets/0d.png" style="width:24px;margin-right:2px">
					 	 <img src="assets/0d.png" style="width:24px">
						 <img src="assets/0d.png" style="width:24px;margin-left:2px">
					 </div>
				</div>
			</div>
			<div class="row text-left "  style="height:230px">
				<div class="col-md-2 www_box5" style="padding-top:25px; padding-bottom:25px">
					<div style="font-size:20px!Important">
						ITEM NAME
					</div>
					<div id="item_name" class="x_detail www_box5">
					
					</div>
					<div style="text-align:left;font-size:20px;margin-top:10px">
						OWNER NAME
					</div>
					<div id="owner_name" class="x_detail www_box5">
					
					</div>
					<span style="display:none">
						<div>
							ROOM NAME
						</div>
						<div id="auction_room" class="x_detail www_box5">

						</div>
					</span>
				</div>
				
			<div  class="col-md-8 www_box5" style="bottom:0!Important;width:100%;padding:0px;left:0;text-align:center;background:none!Important">
				<input type="hidden" id="minus">
				<table style="width:100%;position:;left:0;right:0;margin:auto;height:160px">
					<tr>  
						<td>
							<a href="javascript:min(10)"><img id="min3" src="assets/min3.png" style="width:20px;margin:auto;margin-bottom:10px;border-radius:5px"></a>
						</td>
						<td>
							<a href="javascript:min(50)"><img id="min2" src="assets/min3.png" style="width:30px;margin:auto;margin-bottom:10px;border-radius:5px"></a>
						</td>
						<td>
							<a href="javascript:min(100)"><img id="min1" src="assets/min3.png" style="width:40px;margin:auto;margin-bottom:10px;border-radius:5px"></a>
						</td>
						<td>
							<span id="both_btns">
								<span id="reg_link"><a href="javascript:register()"><img id="big_btn_blue" src="assets/btn_blue.png" style="width:120px;margin:auto;margin-top:10px"></a></span>
								<span id="bid_link" style="display:none;"><a href="javascript:bid()"><img id="big_btn_green" src="assets/btn_green.png" style="width:120px;margin:auto;margin-top:10px"></a></span>
							<span>
						</td>
						<td>
							<a href="javascript:max(100)"><img id="max1" src="assets/max3.png" style="width:40px;height:40px;border-radius:5px;margin:auto;margin-bottom:10px"></a>
						</td>
						<td>
							<a href="javascript:max(50)"><img id="max2" src="assets/max3.png" style="width:30px;height:30px;border-radius:5px;margin:auto;margin-bottom:10px"></a>
						</td>
						<td>
							<a href="javascript:max(10)"><img id="max3" src="assets/max3.png" style="width:20px;height:20px;border-radius:5px;margin:auto;margin-bottom:10px"></a>
						</td>
					</tr>
					<tr style="text-align:center;height:20px">
						<td>
							$10
						</td>
						<td>
							$50 
						</td>
						<td>
							$100
						</td>
						<td>
							<div id="reg_bid_text" style="font-size:32px">CLICK TO REGISTER!</div>
						</td>
						<td>
							$100
						</td>
						<td>
							$50
						</td>
						<td>
							$10
						</td>
					</tr>
					<tr>
						<td colspan="100">
							<div id="usrs" style="width:100%;height:50px;background:none"></div>
						</td>
					</tr>
				</table>
			</div>				

				<div class="col-md-2 www_box5" style="padding-top:25px; padding-bottom:25px">
					<div style="text-align:left;font-size:20px!Important">
						STARTING BID 
					</div>
					<div id="starting_bid" class="x_detail www_box5">

					</div>
					<div style="text-align:left;font-size:20px;margin-top:10px">
						START TIME
					</div>
					<div id="scheduled_start" class="x_detail www_box5">

					</div>
					<span style="display:none">
						<div style="text-align:right;font-size:18px">
							BIDDER COUNT
						</div>
						<div id="bidder_count" class="x_detail www_box5">

						</div>
					</span>
				</div>
			</div>

		<div class="row www_box5" style="height:80px;padding-bottom:0px">
			<div class="col-md-4 " style="width:100%;height:100%;margin:0">
			
			</div>
			<div class="col-md-4"  style="background:url(assets/bs8.png);opacity:1;background-size:cover;border-radius:0px;margin:0;text-align:center;padding-top:5px;margin:0;padding:0;margin:10px">
				<div id="my_lcd_bid" style="position:relative;text-align:centerl;margin-left:-20px">
					<img src="assets/dollar.png" style="height:40px;margin:10px;">
					<img src="assets/g0.png" style="height:40px;margin:10px">
				</div>
			</div>
			<div class="col-md-4" style="width:100%;height:100%;margin:0">
			
			</div>
		</div>
		
		<input type="hidden" id="asking_bid">
		<input type="hidden" id="my_bid">
		</div>
	</div>
	<div class="container" style="position:fixed; width:100%; top:0;text-align:center;max-width:100%!Important">
		<div class="row" id="cam_row_top">

		</div>
	</div>
	<div class="container" style="position:fixed; width:100%; bottom:0;text-align:center;max-width:100%!Important">
		<div class="row" id="cam_row_bottom">
		</div>
	</div>
	<div id="cvr" style="display:none;position:fixed; width:100%; bottom:0;top:0;width:100%;height:100%;z-index:99999999999999999999999; background:#000;opacity:0.9;text-align:center;max-width:100%!Important">
	</div>
	</body>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
