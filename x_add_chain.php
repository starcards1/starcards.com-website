<?php

/*
	Creating a conference event will write to the following tables:
	
	1.	event_events
		Records the conference event for updating and displaying event in a calendar
	2.	celeb_channel_content
		Contains summary info about channel content and content type. Content is a collection of various types of assets.
	3.	celeb_conferences
		Conference is a type of asset, and hence the details for that channel that has conferences is stored here
	3.	celeb_conference_participants
		Conference is a type of asset, and hence the details for that channel that has conferences is stored here
	4.	celeb_live_events
		The act of distributing content is simply the process of releasing or publishing its individual assets,, and this, in its entirety, constitutes an event
	5.	celeb_live_event_members
		Recorded actions and  time-lines of all the members that participated in the event
	6.	celeb_sms_logs
		Records the details and purpose of any sms that is sent
	7.	celeb_action_logs
		Record of all Business activity level actions that are executed by any member
	8.	celeb_activity_tracker
		Click level detail logs of member actions.
		
	Before this:
	
	1.	Views and edits an existing channel to add this event to. Table: celeb_channels
	2.	Creates a new channel to store the event. Table: celeb_channels
	
	TO DO: 
	1.	security
	2.	payments and subscriptions
	
*/
include "../class/utils.class.php";
$c=new utils;
$c->connect("199.91.65.83","voxeo");
parse_str(http_build_query($_GET));
// $c->show($_GET);
$start_time=date("h:i a",strtotime($start_time));
$end_time=date_format(date_add(date_create($start_time), new DateInterval('PT1H')), "h:i a");

$date_from=date("d-m-Y h:i a");
$date_to=date_format(date_add(date_create($date_from), new DateInterval('PT1H')), "h:i a");
	
$sql="INSERT INTO `voxeo`.`celeb_conferences` ( `title`, `booking_date`, `start_date`, `start_time`, `end_time`, `audio_video_type`, `channel_id`, `status`, `link` )
VALUES
	(
		'$title', 
		'$booking_date',
		'$start_date',
		'$start_time',
		'$end_time',
		'video',
		1,
		'1',
		'$link')";
$conference_id=$c->insert($sql);
$stream_name=$c->encrypt($conference_id,"");
$arr1=array("/","+","%","=");
$arr2=array("1001","2002","3003","");
$stream_name=str_replace($arr1,$arr2,($stream_name));
$room=$stream_name;
$app="live";
$x_start=strtotime("$start_date $start_time");
$start=date("Y-m-d H:i",strtotime($start_date . " " . $start_time));
echo $link="https://linqstar.com/conf/" . $stream_name;
$c->insert("update `voxeo`.`celeb_conferences` set `link`='$link' where `conference_id`=$conference_id");

if (!$conference_asset_id) $conference_asset_id=0;
$status=1;

$mobs=explode("|",$mobiles);
$nams=explode("|",$names);

for ($i=0; $i<count($mobs); $i++) {
	$sql="INSERT INTO `voxeo`.`celeb_conference_participants` ( `conference_id`, `provider_mid`, `participant_mobile`, `participant_name`, `status` )
	VALUES
		(
			$conference_id,
			$provider_mid,
			'". $mobs[$i] ."',
			'". $nams[$i] ."',
			$status)";
	$c->insert($sql);
	$sql="INSERT INTO `voxeo`.`celeb_live_event_members` (`live_event_id`, `mobile`) VALUES ($conference_id, '". $mobs[$i] ."')";
	$c->insert($sql);
//	$c->show($sql);
}
for ($i=0; $i<count($mobs); $i++) {
	$msg1="Hey " . $nams[$i] . "! " . $nams[0] . " has requested your  attendance in a conference call today at $start_time. To join, simple click the link you are going to receive next";
	$msg1=urlencode($msg1);
	$msg2=urlencode($link);
	$sms1="https://linqstar.com/x_send_sms.php?msg=$msg1&to=" . $mobs[$i];
	$sms2="https://linqstar.com/x_send_sms.php?msg=$msg2&to=" . $mobs[$i];
	$status='pending';
	if ((strtotime($start)-time())<=100) {
		file_get_contents($sms1);
		file_get_contents($sms2);
		$status='sent';
	}
	$sql="INSERT INTO `voxeo`.`dt_sms_queue` (`job_id`, `to_mobile`, `message`, `created_date_time`, `delivery_date_time`, `url_execute`,`status`, `x_start`) VALUES (
						'$conference_id',
						'". $mobs[$i] ."',
						'$msg1', 
						'". date("Y-m-d H:i") ."',
						'$start', 
						'$sms1',
						'$status',
						'$x_start'
						)";
	$c->insert($sql);
		
	$sql="INSERT INTO `voxeo`.`dt_sms_queue` (`job_id`, `to_mobile`, `message`, `created_date_time`, `delivery_date_time`, `url_execute`, `status`, `x_start`) VALUES (
						'$conference_id',
						'". $mobs[$i] ."',
						'$msg2', 
						'". date("Y-m-d H:i") ."',
						'$start', 
						'$sms2',
						'$status',
						'$x_start'
						)";
	$c->insert($sql);
}
$c->close();