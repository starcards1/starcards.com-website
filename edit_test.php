<?php
	require_once "../class/utils.class.php";
	$c=new utils;
	$c->connect("199.91.65.83","voxeo");
	parse_str(http_build_query($_GET));
	$c->insert("UPDATE `voxeo`.`greeting_cards` SET `card_cover` = '$card_cover', `occasion_id` = '$occasion_id' WHERE `card_id` = $card_id");
	$r=$c->query("select * from greeting_cards where card_id=" . $_GET['card_id'])[0];
	$card_cover=$r['card_cover'];
	$asset=$r['asset'];
	$msg=$r['msg'];
?>
<!DOCTYPE html>
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="doc/css/custom.css" rel="stylesheet">
		<link rel="stylesheet" href="assets/css/bootstrap.min.css"/>
		<link rel="stylesheet" href="assets/magic.css"/>
		<link rel="stylesheet" href="assets/css/jquery-ui.min.css"/>
		<link rel="stylesheet" href="assets/css/main.css"/>
		<link rel="stylesheet" href="css/shadows.css"/>
		<link rel="stylesheet" href="dz/dropzone.css"/>
		<link href="style.css" rel="stylesheet" type="text/css" />
		<script src="js/jquery.js"></script>
		<link href='https://fonts.googleapis.com/css?family=Open+Sans:300|Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/css2?family=Allura&family=Gloria+Hallelujah&family=Homemade+Apple&display=swap" rel="stylesheet">	

	<style>
		body {
		  font-family: Arial, Helvetica, sans-serif;
		}

		.flip-card {
		  background-color: transparent;
		  width: 300px;
		  height: 400px;
		  perspective: 1000px;
		}

		.flip-card-inner {
		  position: relative;
		  width: 100%;
		  height: 100%;
		  text-align: center;
		  transition: transform 0.0s;
		  transform-style: preserve-3d;
		}

		.flip-card-inner {
			transform:translateX(-60px) translateX(-240px) rotateY(-180deg)
		}
		.flip-card-return {
			transform:translateX(0px) rotateY(0deg)
		}

		.flip-card-front, .flip-card-back {
		  position: absolute;
		  width: 100%;
		  height: 100%;
		  -webkit-backface-visibility: hidden;
		  backface-visibility: hidden;
		  z-index:999999999999999999;
		}

		.flip-card-front {
		  background-color: #bbb;
		  color: black;
		}

		.flip-card-back {
		  background: url(assets/bs.png) no-repeat;
		  background-size:cover;
		  color: white;
		  transform: rotateY(180deg);
		}
		.dd {
			transition:2s
		}
		.left {
			transform:translateX(-300px)
		}
		.right {
			transform:translateX(0px)
		}
		.left2{
			transform:translateX(-150px)
		}
		.right2 {
			transform:translateX(0)
		}
		.wrap {
		  margin: 0 auto;
		  width: 600px;
		}

		.shadow {
		  position: relative;
		  margin: 3em auto;
		  padding-top: 2em;
		  width: 400px;
		  height: 100px;
		  border: 1px solid #777777;
		  border-radius: 5px;
		  background: #e5e5e5;
		  text-align: center;
		  box-shadow: 0 0 25px 0 rgba(50,50,50,.3) inset;
		}

		.shadow:after {
		  content: "";
		  position: relative;
		}

		.curved:after, .curved-2:after {
		  position: relative;
		  z-index: -2;
		}

		.curved:after {
			position: absolute;
			top: 50%;
			left: 12px;
			right: 12px;
			bottom: 0;
			box-shadow: 0 0px 10px 7px rgba(100,100,100,0.5);
			border-radius: 450px / 15px
		}

		.curved-2:after {
			position: absolute;
			top: 0;
			left: 12px;
			right: 12px;
			bottom: 0;
			box-shadow: 0 0px 10px 7px rgba(100,100,100,0.5);
			border-radius: 450px / 15px;
			z-index:-1!Important
		}
		*{font-size:0.8}
		audio{
			display:none!Important;
			position:absolute
		}
		</style>
	</head>
	<body style="background:none">
			
		
		<div id="c3" class="shadow curved-2" style="width:300px;height:400px;margin:auto;background:#f0f0f0;position:absolute;top:50px;z-index:-1;left:0;right:0"></div>
		<div id="c4" contentEditable style="width:300px;height:400px;margin:auto;background:#f0f0f0;position:absolute;top:50px;z-index:0;left:0;right:0;font-family:Open Sans;padding:10px;color:blue;padding-top:0px;font-size:12px">
			<?=$msg;?>
		</div>
		<div id="top_level" style="width:100%;max-width:300px;height:400px;margin:auto;position:relative;text-align:canter;top:50px">
			<button class="button-5" style="width:300px; height:40px; border:none; border-radius:0; box-shadow:none;position: absolute; top:-20px; right:35%; font-size: 11px; color: white; padding:5px; font-size:11px; padding-left:5px; padding-right:5px; background: black; left:0" onclick="fem()">EDIT MESSAGE</button>
			<button class="button-5" style="width:60px;height:40px; border:none; border-radius:0; box-shadow:none; position: absolute; top:-20px; right:85%; font-size: 11px; color: white; padding:5px; font-size:11px; padding-left:5px; padding-right:5px; left:0" onclick="dec_row()">PREV</button>
			<button class="button-5" style="width:60px; height:40px; border:none; border-radius:0; box-shadow:none; position: absolute; top:-20px; border-radius:0; right:5%; font-size: 11px; color: white; padding:5px; font-size:11px; padding-left:5px; padding-right:5px;right:0" onclick="inc_row()">NEXT</button>
			<button id="finalize" class="button-5" style="display:none;font-size:14px;padding-left:0px;padding-right:0px;right:0px;margin:auto;position:absolute;width:150px;background:#a3d039;bottom:0;left:0;border-radius:0; width:300px;box-shadow:none" onclick="finalizeS()">Finalize and Send</button>
			<div id="ff" class="flip-card" onclick="anim()">
				<div id="inn"> 
					<a href="javascript:window.parent.editcover()">
						<div id="c1" class="flip-card-front">
							<img id="card_cover" src="assets/cards/c1.png" style="width:300px;height:400px"></a>
						</div>
					</a>
					<a style="position:absolute; left:0" href="javascript:window.parent.editcover()"><button id="edc" class="button-5" style="width:300px; height:40px; border:none; border-radius:0; box-shadow:none; z-index:99999999999999999999; position:absolute ; top:-20px; right:35%; font-size: 11px; color: white; padding:5px; font-size:11px; padding-left:5px; padding-right:5px; background: url(assets/bs1.png); background-size:cover; left:0">CLICK HERE TO SELECT YOUR CARD COVER</button></a>
					<div id="c2" class="flip-card-back">
						<div style="width:100%;height:100%">
							<video loop playsinline muted autoplay id="vd" src="assets/160.mp4" style="display:none;width:200px;height:200px;margin:auto;border-radius:200px;border:none;opacity:0.7;margin-top:50px;object-fit:cover;"></video>
							<form action="php/x_greeting_uploads.php"  style="width:100%;border:none;background:none;margin-top:-40px;background:none" class="dropzone" id="member8"></form>
							<div style="position:absolute;width:100%;text-align:center;bottom:25px">
								<a onclick="get_payment()"><img src="assets/upcel.png" style="width:80px;position:absolute;left:25px;bottom:0px"/></a> <a href="javascript:activate_upload(8)"><img src="assets/upyou.png" style="width:80px;position:absolute;right:25px;bottom:0px"/></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		<div id="barDesktop" style="margin-top:25px">
			<button id="inside" class="button-5" style="font-size:14px;padding-left:0px;padding-right:0px;left:0px;right:0px;margin:auto;position:absolute;width:80px" onclick="anim()">Next</button>
			<button id="cover" class="button-5" style="font-size:14px;padding-left:0px;padding-right:0px;display:none;left:0;right:0;margin:auto;position:absolute;width:80px" onclick="anim_r()">Cover</button>
		</div>
		<div id="barMobile" style="margin-top:25px">
			<button id="left" class="button-5" style="font-size:0.8em;padding-left:5px;padding-right:5px;display:none;left:10px;margin:auto;position:absolute;width:85px;margin-left:-20px" onclick="left()">Left Panel</button>
			<button id="inside_m" class="button-5" style="font-size:0.8em;padding-left:5px;padding-right:5px;display:;left:0px;right:0px;margin:auto;position:absolute;width:70px" onclick="anim()">Next</button>
			<button id="cover_m" class="button-5" style="font-size:0.8em;padding-left:5px;padding-right:5px;display:none;left:0;right:0;margin:auto;position:absolute;width:70px" onclick="anim_r()">
			</button>
			<button id="right" class="button-5" style="font-size:0.8em;padding-left:5px;padding-right:5px;display:none;right:10px;margin:auto;position:absolute;width:85px;margin-right:-20px" onclick="right()">Right Panel</button>
		</div>
	</div>
	<div id="bc" style="display:none;position:fixed;left:0;right:0;top:0;bottom:0; width:100%; height:100%; text-align: center; background: #000; opacity:1; z-index:99999999999999"></div>
	<div id="bt" contentEditable style="display:none;position:fixed;left:0;right:0;top:0;bottom:0; width:300px; height:400px; margin: auto; text-align: center; background: #f0f0f0; padding:20px; opacity:1; z-index:99999999999999">
		<?=$msg;?>
	</div>
	<a href="javascript:save_msg_edit()"><button id="bb" class="button-5" style="display:none;font-size:12px!Important;position:fixed;left:0;right:0;bottom:20px; width:240px; height:40px; margin: auto; text-align: center; background: red; z-index:99999999999999">Save Changes and Return</button></a>

    <script src="./assets/js/plugins.js"></script>
    <script src="./assets/js/bootstrap.min.js"></script>
    <script src="./assets/js/jquery-ui.min.js"></script>
    <script src="js/location.js" type="text/javascript" charset="utf-8"></script>	
	<script src="js/utils.js"type="text/javascript"></script>
	<script src="dz/dropzone.js"type="text/javascript"></script>
	<script src="https://apis.google.com/js/client:platform.js?onload=start" async defer></script>	
	
	<script>
		function finalizeS() {
			
			var url='send_mail.php?card_id='+window.localStorage.getItem('card_id')+'&to_name='+window.localStorage.getItem('to_name')+'&to_email='+window.localStorage.getItem('recepient_email')+'&subject=New Greeting Card'
			console.log(url)
			$.ajax({
				url: url,
				success:function(data){
				alert('Card Sent!')
				}
			})
		}
		var video_played=false
		
		window.localStorage.setItem('card_id',qs('card_id'))
		window.localStorage.setItem('card_cover',qs('card_cover'))
		var card_id	= window.localStorage.getItem('card_id')
		var card_cover = window.localStorage.getItem('card_cover')
		
		function get_payment() {
			window.top.location.href='search.php?card_id='+card_id
			//window.top.location.href='stripe/?card_id='+card_id+'&<?=http_build_query($_GET);?>'
		}
		function updatecover() {
			window.localStorage.setItem('card_cover',window.localStorage.getItem('cover_id'))
			$$('card_cover').src='assets/cards/'+window.localStorage.getItem('cover_id')+'.png'
		}
		updatecover()
		function editcover() {
			window.parent.frames['ccv'].style.display='block'
		}
		
		function mobileCheck() {
			if ( navigator.userAgent.match(/Android/i)
				 || navigator.userAgent.match(/webOS/i)
				 || navigator.userAgent.match(/iPhone/i)
				 || navigator.userAgent.match(/iPad/i)
				 || navigator.userAgent.match(/iPod/i)
				 || navigator.userAgent.match(/BlackBerry/i)
				 || navigator.userAgent.match(/Windows Phone/i)) {
				return true 
			} else {
				return false
			}
		}
		
		function fem() {
			document.getElementById('bc').style.display=''
			document.getElementById('bt').style.display=''
			document.getElementById('bb').style.display=''
		}

		function em() {
			document.getElementById('top_level').style.position='absolute'
			document.getElementById('top_level').style.zIndex='-9'
			document.getElementById('c3').setAttribute('contentEditable',true)
			document.getElementById('c2').style.transform='translateX(0px)'
		}
		function playNow() {
			document.getElementById('vd').style.display='block'
			document.getElementById('vd').play()
			video_played=true
		}
		var pane='right'
		function anim() {
			if(mobileCheck()===true) {	
			//	tmr=setTimeout('_left()',5000)
			}
			document.getElementById('ff').className='dd flip-card flip-card-inner'
			if(mobileCheck()===true) {	
				document.getElementById('inside_m').style.display='none'
				document.getElementById('cover_m').style.display=''
				document.getElementById('inside').style.display='none'
				document.getElementById('cover').style.display='none'
			} else {
				document.getElementById('inside').style.display='none'
				document.getElementById('cover').style.display='block'
			}
			document.getElementById('left').style.display='block'
			document.getElementById('right').style.display='block'		
			document.getElementById('barDesktop').className='dd left2'		
			document.getElementById('vd').style.display='block'		
			document.getElementById('vd').play()
			document.getElementById('top_level').style.transform='translateX(150px)'
			document.getElementById('c3').style.transform='translateX(150px)'
			document.getElementById('c4').style.transform='translateX(150px)'
			document.getElementById('finalize').style.display=''
		}

		function anim_r() {
			document.getElementById('vd').style.display='none'		
			document.getElementById('ff').className='dd flip-card-return flip-card-inner'
			if(mobileCheck()===true) {	
				document.getElementById('inside_m').style.display=''
				document.getElementById('cover_m').style.display='none'
				document.getElementById('inside').style.display='none'
				document.getElementById('cover').style.display='none'
				document.getElementById('finalize').style.display=''
			} else {
				document.getElementById('inside').style.display='block'
				document.getElementById('cover').style.display='none'
				document.getElementById('finalize').style.display='none'
			}
			document.getElementById('left').style.display='none'
			document.getElementById('right').style.display='none'
			document.getElementById('barDesktop').className='dd right2'				
		}
		function right() {
			pane='right'
			document.getElementById('inn').className='dd right flip-card-inner'
			if(mobileCheck()===true) {	
				document.getElementById('inside_m').style.display='none'
				document.getElementById('cover_m').style.display=''
				document.getElementById('inside').style.display='none'
				document.getElementById('cover').style.display='none'
			} else {
				document.getElementById('cover').style.display='block'
				document.getElementById('cover_m').style.display='none'
				document.getElementById('inside_m').style.display='none'
				document.getElementById('inside').style.display='none'
			}
		}
		function _left() {
		//	clearTimeout(tmr)
		//	tmr=setTimeout('left()',5000)
		}
		function left() {
			document.getElementById('inn').className='dd left flip-card-inner'
			document.getElementById('inside_m').style.display='none'
			document.getElementById('inside').style.display='none'
			document.getElementById('cover_m').style.display='none'
			document.getElementById('cover').style.display='none'
			$('#inside').hide()
			$('#cover').hide()
			$('#inside_m').hide()
			$('#cover_m').hide()
			pane='left'
		}
		if(mobileCheck()===true) {	
			document.getElementById('barDesktop').style.display='none'
			document.getElementById('barMobile').style.display=''
		} else {
			document.getElementById('barDesktop').style.display=''
			document.getElementById('barMobile').style.display='none'
		}
		var msg
		function setCard(g) {
			$('#cm').hide()
			if (g=='birthday') {
				msg='Happy Birthday'
			} else if (g=='new_year') {
				msg='Happy New Year'
			} else if (g=='xmas') {
				msg='Happy Christmas'
			} else if (g=='anniversary') {
				msg='Happy Anniversary'
			} else if (g=='halloween') {
				msg='Happy Halloween'
			} else if (g=='easter') {
				msg='Happy Easter'
			} else if (g=='custom') {
				$('#cm').show()
			}
		}
		// $$('msg').onkeypress=function(e){
			// console.log(e)
			// if ($$('msg').textContent.length>420) e.preventDefault()
			// e.target.style.fontFamily='Gloria Hallelujah'
		// }
		
		function qs1() {
			var str1=[]
			var url=''
			var pre
		    str1=location.search.split('?')[1]
			for (var i=0; i<str1.split('&').length; i++) {
				if (i>0) pre='&'
					else pre='?'
				url+=pre+str1.split('&')[i].split('=')[0]+'="+'+str1.split('&')[i].split('=')[0]+'\+\"'
			}
			console.log(url)
		}
		Dropzone.autoDiscover=false
		var sites
		var imagesUploaded
		var r,k,c,t2,t3,t4,intr=50,currImg
		var rat=1
		var rx=0, t, ext
		var set=false
		var usr_img=[]
		var ext
		var collection_set=0
		var channel_set=0
		var cnn=0
		
		function getCookie(cname)	{
			var name = cname + "=";
			var ca = document.cookie.split(';');
			for(var i=0; i<ca.length; i++) {
			  var c = ca[i].trim();
			  if (c.indexOf(name)==0) return c.substring(name.length,c.length);
			}
			return "";
		}
	var myDropzone, tfiles=0
	var gui
	var src_element

	function add_assets(asset,asset_type,asset_media_type,card_id) {
		u="x_update_greeting.php?field=asset&value="+asset+"&key_field=card_id&key_value=" + card_id
		$.ajax({url:u,success:function(data){}})
		u="x_update_greeting.php?field=asset_media_type&value="+asset_media_type+"&key_field=card_id&key_value=" + card_id
		$.ajax({url:u,success:function(data){}})
		u="x_update_greeting.php?field=asset_type&value="+asset_type+"&key_field=card_id&key_value=" + card_id
		$.ajax({url:u,success:function(data){$.alert('Saved!')}})
	}
	
	function activate_upload(x) {
		var gauge=[ ];
		var target=[ ];
		var url="php/x_greeting_uploads.php"
		if (!myDropzone) {
			myDropzone = new Dropzone("form#member"+x, { 
				url: url,
				timeout:0,
				maxFilesize: 1000000000,
				dictDefaultMessage: "<br><br><div id='xe"+x+"' style='background:;color:#fff;font-family:Open sans;color:#fff;font-weight:300;top:0;position:absolute;width:100%;left:0;margin-top:20px;top:8%;right:0;margin-left:auto;margin-right:auto;height:75%;z-index:99999999999999999;color:black;margin-top:-15px'>DRAG DROP FILES<br><b>OR</b><br>CLICK HERE<br><img style='width:70px' src='https://lushmatch.com/sdfm/images/cap04.png'></div>",
				createImageThumbnails:true,
				init: function() {
					this.on("sending", function(file, xhr, formData){
						gui=guid()
						var in_co, in_ch
						setCookie('attach_id',gui)
						ext=file.name.split('.')[1]
						console.log('x_update_upload.php?asset=' + gui + '.' + ext + '&card_id=' + card_id)
						$.ajax({
							url: 'x_update_upload.php?asset=' + gui + '.' + ext + '&card_id=' + card_id,
							success:function(){
								
							}
						})
						try {
							if (ext=='mp4' || ext=='wmv' || ext=='qt' || ext=='mov' || ext=='avi' || ext=='mkv' || ext=='mpg') {
								//	$$('p'+x).innerHTML='<video src="greetings/'+gui+'.'+ext+'" style="width:100%;margin:10px"></video>'
								asset_media_type='video'
							} else if (ext=='mp3' || ext=='wma' || ext=='qt' || ext=='au' || ext=='wav') {
								//	$$('p'+x).innerHTML='<audio src="greetings/'+gui+'.'+ext+'" style="width:100%;margin:10px"></audio>'
								asset_media_type='audio'
							} else if (ext=='png' || ext=='jpg' || ext=='gif' || ext=='bmp'){
								asset_media_type='image'
							}
							if (x==10) {
								src_element='msg_image'
							} else if (x==12) {
								src_element='user_photo'
							} else if (x==11) {
								src_element='mood_music'
							} else if (x==9) {
								src_element='voice_over'
							} else if (x==8) {
								src_element='user_video'
							}
							formData.append("attach_id", gui);
							formData.append("user_type", getCookie('user_type'));
							formData.append("fan_mid", qs('fan_mid'));
							formData.append("provider_mid", getCookie('mid'));
							formData.append("card_id", card_id);
							formData.append("asset_media_type", asset_media_type);
							formData.append("asset_type", src_element);
							formData.append("asset", gui + '.' + ext);
						} catch (e) {}
					});
				}			
			});
		}
		myDropzone.on("addedfile", function(file) {
			//wait('Uploading File, Please Wait...')
		});
			
		myDropzone.on("complete", function(file) {
			if (x==8 || x==9 || x==11){
				if (x==8) {
					setTimeout(function(){
						$$('vd').src='greetings/'+getCookie('attach_id')+'.'+ext
						$('.dz-error-mark').hide()
						$('.dz-success-mark').hide()
						$('.dz-progress').hide()
						$('.dz-details').hide()
						$('.dz-image').hide()
					},3000)
					add_assets(getCookie('attach_id') +' .' + ext, src_element, 'video')
				} else {
					setTimeout(function(){
						//$$('member'+x).innerHTML='<audio src=\'greetings/'+getCookie(\'attach_id\') + '.' + ext+' style="max-height:120px!Important;opacity:1" autoplay muted></audio>'
					},3000)
				}
			}
		})
	//	hide()
	}
	function setCookie(cname,cvalue,exdays)	{
		var d = new Date();
		d.setTime(d.getTime()+(exdays*24*60*60*1000));
		var expires = "expires="+d.toGMTString();
		document.cookie = cname + "=" + cvalue + "; " + expires;
	 }	
	
	function print_blocks(ppx) {
		if (ppx=='undefined') return false
		ppx=ppx.toString()
		var f
		var px=''
		var x=''
		if (ppx*1<10) x='00'+ppx
		if ((ppx*1>9) && (ppx*1<100)) x='0'+ppx
		if (ppx*1==100) x=ppx
			
		if (!wd) var wd='20px'
		for (f=0;f<=x.length-1;f++) {
			var dg=x.substring(f,f+1)
			if (dg !== '.')
				px=px+'<span><img src="https://gaysugardaddyfinder.com/assets/img/' + dg + '.png" style="width:'+wd+'"></span>'
		}
		return '<span style="padding-top:5px">'+px+'</span>'
	}
	
	var done=false
		function win() {
			if (done===true) return false
			$.alert({
				title: 'Success',
				content: 'Image(s) uploaded, saved and cataloged. Whats next?',
				btnClose:true,
				buttons: {
					b1: {
						text: 'Home',
						btnClass: 'btn-white',
						action: function(){
							location.href='home.html'
						}
					},
					b2: {
						text: 'My Uploads',
						btnClass: 'btn-white',
						action: function(){
							location.href='show_uploads.html'
						}
					},
					b3: {
						text: 'My Account',
						btnClass: 'btn-red',
						action: function(){
							location.href='settings.html'
						}
					}
				}
			});
			done=true
		}
		function fail(error) {
			alert("An error has occurred: "+error)
		}	
	var currPage='upload_photo.html'
	function fs(el) {
		if (!el) el = document.documentElement
		, rfs = // for newer Webkit and Firefox
			   el.requestFullScreen
			|| el.webkitRequestFullScreen
			|| el.mozRequestFullScreen
			|| el.msRequestFullScreen
		;
		if(typeof rfs!="undefined" && rfs){
		  rfs.call(el);
		} else if(typeof window.ActiveXObject!="undefined"){
		  // for Internet Explorer
		  var wscript = new ActiveXObject("WScript.Shell");
		  if (wscript!=null) {
			 wscript.SendKeys("{F11}");
		  }
		}
		}
	//setTimeout('fs()',1000)
	function guid() {
	  function s4() {
		return Math.floor((1 + Math.random()) * 0x10000)
		  .toString(16)
		  .substring(1);
	  }
	  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
		s4() + '-' + s4() + s4() + s4();
	}
	var sa=[]
	var ra=[]
	function saveSite(s) {
		sites=$('.select-1').val().toString()
		console.log(sites)
	}
	
			function generateRandomString(j){
				if (!j) j=16
				var op=''
				var arr=new Array('a','b','c','d','e','f','i','j','k','m','n','p','q','r','s','t','u','v','w','x','y','z',0,1,2,3,4,5,6,7,8,9,0,0,1,2,3,4,5,6,7,8,9,0)
				for (k=0;k<j;k++) {
					var inx=randomIntFromInterval(0,arr.length-1)
					op += arr[inx]
				}
				return op;
			}
			function guid() {
			  function s4() {
				return Math.floor((1 + Math.random()) * 0x10000)
				  .toString(16)
				  .substring(1);
			  }
			  return s4() + '-' + s4();
			}

	function $$(obj) {
		return document.getElementById(obj)
	}		
	
	function delCookie(cname) {
		var d = new Date();
		d.setTime(d.getTime());
		var expires = "expires="+d.toGMTString();
		document.cookie = cname + "=" + "" + "; " + expires;
	 } 
	function setCookie(cname,cvalue,exdays)	{
		var d = new Date();
		d.setTime(d.getTime()+(exdays*24*60*60*1000));
		var expires = "expires="+d.toGMTString();
		document.cookie = cname + "=" + cvalue + "; " + expires;
	 }
	function c_us() {
		window.top.contactUs()
	}
	function $$(objID) {
		return document.getElementById(objID)
	}
	function getCookie(cname)	{ 
		if (window.location.href.indexOf('file://')>=0) {
			var cvalue = window.localStorage.getItem(cname);
			return cvalue
		} else {
			var name = cname + "="; 
			var ca = document.cookie.split(';'); 
			for(var i=0; i<ca.length; i++) { 
			  var c = ca[i].trim(); 
			  if (c.indexOf(name)==0) return c.substring(name.length,c.length); 
			} 
			return ""; 
		}
	} 
	
		function save_msg_edit() {
			$$('bc').style.display=''
			$$('card_cover').src='assets/cards/'+window.localStorage.getItem('card_cover')+'.png'
			$$('c4').innerHTML=$$('bt').innerHTML
			u="x_update_greeting.php?field=msg_personal&value="+$$('msg_personal').innerHTML+"&key_field=card_id&key_value=" + qs('card_id')
			console.log(u)
			$.ajax({url:u,
				success:function(data){
					u="x_update_greeting.php?field=msg_personal&value="+$$('msg_personal').innerHTML+"&key_field=card_id&key_value=" + qs('card_id')
					console.log(u)
					$.ajax({url:u,
						success:function(data){
							window.top.frames[0].hideEditArea()
						}
					})
				}
			})
		}
		var tr=10
		function inc_row() {
			if (row<tr-1) row++
			get_content()
		}
		function dec_row() {
			row--
			if (row<0) row=0
			get_content()
		}
		function hideEditArea() {
			$$('bc').style.display='none'		
			$$('bb').style.display='none'		
			$$('bt').style.display='none'		
		}
		var row=0
		var oid='<?=$_GET['occasion_id'];?>'
		window.localStorage.setItem('occasion_id',oid)
		function get_content() {
			if (oid!=='custom') {
				console.log('x_get_greeting_content.php?occasionID='+oid+'&record='+row)
				$.ajax({url:'x_get_greeting_content.php?occasionID='+oid+'&record='+row+'&card_id='+window.localStorage.getItem('card_id'),success:function(data){
					var ms=JSON.parse(data)
					tr=ms.split('|')[1]*1
					var pmsg = window.localStorage.getItem('msg')
					$$('c4').innerHTML='<div id="msg_canned" style="padding-top:40px;font-size:0.9em">'+ ms.split('|')[0] + '</div><div id="msg_personal" contentEditable class="" style="position:absolute;background:#fcfcfc;padding:10px;width:90%;height:175px;color:grey;font-size:14px;bottom:25px;font-family:Gloria Hallelujah">' + 'Dear ' + window.localStorage.getItem('to_name') + ',<br>' + '[Insert Optional Message Here]' + '<br>' +window.localStorage.getItem('from_name') +'</div>'
					$$('bt').innerHTML='<div id="msg_canned" style="padding-top:40px;font-size:0.9em">'+ ms.split('|')[0] + '</div><div contentEditable id="msg_personal" class="" style="position:absolute;background:#fcfcfc;padding:10px;width:90%;height:175px;color:grey;font-size:14px;bottom:25px;font-family:Gloria Hallelujah">' + 'Dear ' + window.localStorage.getItem('to_name') + ',<br>' + '[Insert Optional Message Here]' + '<br>' +window.localStorage.getItem('from_name') +'</div>'
				}})
			} else {
				$$('c4').innerHTML='<div style="padding-top:75px;font-size:0.9em">'+window.localStorage.getItem('msg')+'</div>'
				$$('bt').innerHTML='<div style="padding-top:75px;font-size:0.9em">'+window.localStorage.getItem('msg')+'</div>'
			}
		}
		setTimeout('get_content()',10)
		</script>		
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script> 		
	</body>
</html>
