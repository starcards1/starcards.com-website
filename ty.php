<?php
require_once 'vendor/autoload.php';
use Twilio\Rest\Client;
use Twilio\TwiML\VoiceResponse;
$msg1=urldecode($_GET['msg1']);
$msg2=urldecode($_GET['msg2']);
header('Content-Type: application/xml');
$response = new VoiceResponse();
$response->say($msg1);
$response->play("https://linqstar.com/assets/beep.mp3");
$response->say($msg2);
echo $response;