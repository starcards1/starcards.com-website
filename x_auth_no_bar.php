<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
header("Access-Control-Allow-Origin: https://linqstar.com");
include "../class/utils.class.php";
$c = new utils;
$c->connect("199.91.65.83","voxeo"); 
parse_str(http_build_query($_GET));
$ipx=$c->getIP();
$r=$c->redis;
if (!$hash) $hash=$_REQUEST['hash'];
$q=$r->hGetAll($hash);
$user["members"]=json_decode($q["members"]);
$user["profile"]=json_decode($q["profile"]);
$user["tags"]=explode(",",json_decode($q["tags"]));
$user["orders"]=json_decode($q["orders"]);
$user["hot_lists"]=json_decode($q["hot_lists"]);
$user["messages"]=json_decode($q["messages"]);
$mid=$user["members"]->mid;
$pid=$user["profile"]->pid;
$logged_in=in_array($mid,$r->sMembers("logged_in"));
$orders=$user["orders"];
$profile=$user["profile"];
$tags=$user["tags"];
$members=$user["members"];
$messages=$user["messages"];
$name=$members->name;
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"></meta>
	</head>
	<body>
		

<link rel="stylesheet" href="css/simple-line-icons.css">
<link rel="stylesheet" href="css/magnific-popup.css">
<link rel="stylesheet" href="css/style.css">
<script src="js/jquery-3.1.0.min.js"></script>
<script src="js/jquery.tweet.min.js"></script>
<script src="js/jquery.xmalert.min.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<script>
	function logout() {
			$.confirm({
				title:'Logout?',
				content:'This action will log you out. Confirm?',
				buttons: {
					Confirm: function(){
						clear_all()
						setTimeout(function(){
							location.href='index.php'
						},1)
					},
					Cancel: function(){
						
					}
				}
			});
	}

	function clear_all() {
		if (mob===true) {
			 window.localStorage.clear();
		} else {
			document.cookie.split(";").forEach(function(c) { document.cookie = c.replace(/^ +/, "").replace(/=.*/, "=;expires=" + new Date().toUTCString() + ";path=/"); });
		}
	}	
		
	function alert_user(type) {
        $('body').xmalert({ 
            x: 'right',
            y: 'bottom',
            xOffset: 30,
            yOffset: 30,
            alertSpacing: 20,
            lifetime: 6000,
            fadeDelay: 0.3,
            template: 'item',
            title: 'Backend Driven Custom Alert',
			paragraph: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.',
            timestamp: '2 hours ago',
            imgSrc: 'images/'+type+'.png',
            iconClass: 'icon-heart'
        });
    }

	function alert_admin() {
        $('body').xmalert({ 
            x: 'right',
            y: 'bottom',
            xOffset: 30,
            yOffset: 30,
            alertSpacing: 50,
            lifetime: 6000,
            fadeDelay: 0.3,
            template: 'survey',
            title: 'What would you improve?',
            paragraph: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.',
            timestamp: '2 hours ago',
            imgSrc: 'images/dashboard/alert-logo.png',
            buttonSrc: [ '#','#' ],
            buttonText: 'Take the <span class="primary">Survey!</span>',
        });
    }

	function alert_tip() {
        $('body').xmalert({ 
            x: 'right',
            y: 'bottom',
            xOffset: 30,
            yOffset: 30,
            alertSpacing: 20,
            lifetime: 6000,
            fadeDelay: 0.3,
            template: 'review',
            title: 'New Fast Checkout',
            paragraph: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.',
            timestamp: '14 hours ago',
            buttonSrc: [ '#','#' ],
        });
    }
	
	function alert_info() {
        $('body').xmalert({ 
            x: 'right',
            y: 'top',
            xOffset: 30,
            yOffset: 30,
            alertSpacing: 40,
            lifetime: 6000,
            fadeDelay: 0.3,
            template: 'messageInfo',
            title: 'Info Alert Box',
            paragraph: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.',
        });
    }

	function alert_success() {
	if (!msg) msg='Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.'
        $('body').xmalert({ 
            x: 'right',
            y: 'top',
            xOffset: 30,
            yOffset: 30,
            alertSpacing: 40,
            lifetime: 6000,
            fadeDelay: 0.3,
            template: 'messageSuccess',
            title: 'Success Alert Box',
            paragraph: msg
        });
    }

	function alert_error(msg) {
	if (!msg) msg='Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.'
        $('body').xmalert({ 
            x: 'right',
            y: 'top',
            xOffset: 30,
            yOffset: 30,
            alertSpacing: 40,
            lifetime: 6000,
            fadeDelay: 0.3,
            template: 'messageError',
            title: 'Error Alert Box',
            paragraph: msg,
        });
	}
    /*------------------
		PROMO POPUP
	------------------*/
	function alert_promo(trigger_element) {
		$('#'+trigger_element.id).magnificPopup({
			type: 'inline',
			removalDelay: 300,
			mainClass: 'mfp-fade',
			closeMarkup: '<div class="close-btn mfp-close"><svg class="svg-plus"><use xlink:href="#svg-plus"></use></svg></div>'
		});
	}
</script>
</body>
</html>