<form action="<?php echo get_stylesheet_directory_uri(); ?>/charge.php" method="POST" id="payment-form">
		
	<!-- //Header -->
	<!--<div class="form-header"><p>Powered by Stripe</p></div>-->
	<span  id="card-errors" class="payment-errors" style="color: red; font-size: 22px; "></span>

	<!-- // First and Last Name -->
	<div class="form-row left"><label for="firstname" class="next-line">First Name</label><input type="text" name="fname" value="<?php ( isset( $_POST['fname'] ) ? $first_name : null ) ?>"></div>
	<div class="form-row right"><label for="lastname" class="next-line">Last Name</label><input type="text" name="lname" value="<?php ( isset( $_POST['lname'] ) ? $last_name : null ) ?>"></div>

	<!-- //Email -->
	<div><label for="email">Email <strong>*</strong></label><input type="text" name="email" value="<?php ( isset( $_POST['email'] ) ? $email : null ) ?>"></div>
	<!-- //Phone-->
	<div><label for="phone">Phone</label><input type="tel" name="phone" value="<?php ( isset( $_POST['phone'] ) ? $phone : null ) ?>"></div>

	<div id="card-element">
	  <!-- a Stripe Element will be inserted here. -->
	</div>

	<!--Submit-->
	<button type="submit" id="payment-button">Register</button>


</form>
<script>
// Stripe API Key
var stripe = Stripe('pk_test_XXXXXXXXXXXXXXXXXXXXXXXX');
var elements = stripe.elements();
// Custom Styling
var style = {
    base: {
        color: '#32325d',
        lineHeight: '24px',
        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
        fontSmoothing: 'antialiased',
        fontSize: '16px',
        '::placeholder': {
            color: '#aab7c4'
        }
    },
    invalid: {
        color: '#fa755a',
        iconColor: '#fa755a'
    }
};

// Create an instance of the card Element
var card = elements.create('card', {style: style});

// Add an instance of the card Element into the `card-element` <div>
card.mount('#card-element');

// Handle real-time validation errors from the card Element.
card.addEventListener('change', function(event) {
    var displayError = document.getElementById('card-errors');

    if (event.error) {
        displayError.textContent = event.error.message;
    } else {
        displayError.textContent = '';
    }
});


// Handle form submission
var form = document.getElementById('payment-form');

form.addEventListener('submit', function(event) {
    event.preventDefault();

    stripe.createToken(card).then(function(result) {
        if (result.error) {
            // Inform the user if there was an error
            var errorElement = document.getElementById('card-errors');
            errorElement.textContent = result.error.message;
        } else {
            stripeTokenHandler(result.token);
        }
    });
});


// Send Stripe Token to Server
function stripeTokenHandler(token) {
    // Insert the token ID into the form so it gets submitted to the server
    var form = document.getElementById('payment-form');

    // Add Stripe Token to hidden input
    var hiddenInput = document.createElement('input');
    hiddenInput.setAttribute('type', 'hidden');
    hiddenInput.setAttribute('name', 'stripeToken');
    hiddenInput.setAttribute('value', token.id);
    form.appendChild(hiddenInput);

    // Submit form
    form.submit();
}
</script>