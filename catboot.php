<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>LinqStar Card Asset catalog</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">
		<link rel="stylesheet" href="css/filetree.css" type="text/css" >
		<style>
			.catalog {
				width:100%;
				height:100%;
				background: white
			}
		</style>
		<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
	
</head>

<body id="page-top">
    <div id="wrapper">
        <div id="content-wrapper" class="d-flex flex-column">
            <div id="content">
					<div class="card-body">
						<h4 class="small font-weight-bold">Card Creation: <span class="float-right"> 20% Complete!</span></h4>
						<div class="progress">
							<div class="progress-bar bg-success" role="progressbar" style="width: 20%"
								aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
						</div>
					</div>
                    <div class="row" style="display:none">
                        <div class="col-lg-6 mb-4">
                            <div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">Detailed Status</h6>
                                </div>
                                <div class="card-body">
                                    <h4 class="small font-weight-bold">Default Card<span
                                            class="float-right">100% Complete</span></h4>
                                    <div class="progress mb-4">
                                        <div class="progress-bar bg-info" role="progressbar" style="width: 100%"
                                            aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">Custom Card Status</h6>
                                </div>
                                <div class="card-body">
                                    <h4 class="small font-weight-bold">Uploaded Video<span
                                            class="float-right">100% Completed</span></h4>
                                    <div class="progress mb-4">
                                        <div class="progress-bar bg-warning" role="progressbar" style="width: 100%"
                                            aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                    <h4 class="small font-weight-bold">Ordered Celebrity Video<span
                                            class="float-right">Not Started</span></h4>
                                    <div class="progress mb-4">
                                        <div class="progress-bar bg-info" role="progressbar" style="width: 0%"
                                            aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                    <h4 class="small font-weight-bold">Customized Card Cover<span
                                            class="float-right">Not Started</span></h4>
                                    <div class="progress mb-4">
                                        <div class="progress-bar bg-info" role="progressbar" style="width: 0%"
                                            aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                    <h4 class="small font-weight-bold">Customized Card Inside<span
                                            class="float-right">Not Started</span></h4>
                                    <div class="progress mb-4">
                                        <div class="progress-bar bg-info" role="progressbar" style="width: 0%"
                                            aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                    <h4 class="small font-weight-bold">Added Custom Message<span
                                            class="float-right">Not Started</span></h4>
                                    <div class="progress mb-4">
                                        <div class="progress-bar bg-info" role="progressbar" style="width: 0%"
                                            aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
					</div>
                    <div class="row" style="display:none">
                        <div class="col-lg-6 mb-4">
                            <div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <a href="javascript:window.parent.close_catalog()"><h6 class="m-0 font-weight-bold text-primary">Close and Return to card</h6></a>
                                </div>
                            </div>
						</div>

<script type="text/javascript" >
$(document).ready( function() {

	$( '#container' ).html( '<ul class="filetree start"><li class="wait">' + 'Generating Tree...' + '<li></ul>' );
	
	getfilelist( $('#container') , 'VisualCardBuilder' );
	var data=[ ]
	function getfilelist( cont, root ) {
	
		$( cont ).addClass( 'wait' );
			
		$.post( 'foldertree.php', { dir: root }, function( data ) {
			console.log(data)
			if (data.indexOf('<div')>=0) {
				$('#container').hide()
				var dd=document.createElement('div')
				dd.innerHTML=data
				$('#cat').append(dd)
			} else {
				$( cont ).find( '.start' ).html( '' );
				$( cont ).removeClass( 'wait' ).append( data );
				if( 'Sample' == root ) 
					$( cont ).find('UL:hidden').show();
				else 
					$( cont ).find('UL:hidden').slideDown({ duration: 500, easing: null });
			}
			
		});
	}
	
	$( '#container' ).on('click', 'LI A', function(e) {
		var entry = $(this).parent();
		e.preventDefault();
		if( entry.hasClass('folder') ) {
			if( entry.hasClass('collapsed') ) {
				entry.find('UL').remove();
				getfilelist( entry, escape( $(this).attr('rel') ));
				entry.removeClass('collapsed').addClass('expanded');
			}
			else {
				
				entry.find('UL').slideUp({ duration: 500, easing: null });
				entry.removeClass('expanded').addClass('collapsed');
			}
		} else {
			fs(e.target)
		}
	return false;
	});
	
});
function saveAsset(io) {
	window.localStorage.setItem('selected_asset',io)
}

function fs(obj) {
	console.log(obj.rel)
	$.alert({
		title:'Preview Image',
		content: 'url:iView.php?image='+obj.rel,
		useBootstrap: false,
		columnClass: '320px',
	})

}


</script>
    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>
<div id="container"> </div>
<div id="selected_file"></div>
<div class="container">
	<div id="cat" class="row"></div>
</div>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
</body>
</html>