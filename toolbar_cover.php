<style>
		.bt {
			color:white;
			background: #00d9a3;
			font-size: 12px;
			padding:5px 10px;
			margin-top: -7px;
			border-radius:4px;
			font-weight:bold;
		}
		.pc {
			color:#333;
			background:  #f0f0f0;
			font-size: 12px;
			padding:4px 8px;
			margin-top: -5px;
			border-radius:4px;
			
		}
		.pc2 {
			color:white;
			background:  #00d9a3;
			font-size: 12px;
			padding:7px 10px;
			margin-top: -5px;
			border-radius:4px;
			font-weight:bold;
		}
		.bg-success2 {
			background: #00d9a3
		}
		.bg2 {
			background: #00d9a3!Important
		}
		.card-body{
			padding:10px!Important;
			box-sizing:border-box!Important;
			
		}
		.disabled {
			color:white;
			background:  #f0f0f0;
			font-size: 12px;
			padding:7px 10px;
			margin-top: -5px;
			border-radius:4px;
			font-weight:bold;
		}
</style>
<link href="css/sb-admin-2.min.css" rel="stylesheet">
<div id="wrapper" style="position:fixed!Important;top:0;left:0;width:100%;height:50px;background:white;z-index:99999999999999999999999999999;min-height:70px!Important">
	<div id="content-wrapper" class="d-flex flex-column">
		<div id="content" style="max-height:70px">
			<nav id="basic_toolbar" onclick="toggle_nav_basic()" class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow text-left">
				<div class="card-body">
					<h4 class="small font-weight-bold" style="text-decoration:none!Important;margin-top:-10px">BASIC CARD<span id="h4_card_creation" class="float-right">50% Complete</span></h4>
					<div style="display:" class="progress" style="margin-bottom:-10px">
						<div id="card_creation" class="progress-bar bg-success" role="progressbar" style="width: 50%"
							aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">
						</div>
					</div>
				</div>
			</nav>
			<div id="nav_detail_basic" class="row" style="display:none;text-align:left">
				<div class="col-lg-6 mb-4">
					<div class="card shadow mb-4">
						<div class="card-body">
							<h4 class="small font-weight-bold">Card Data Entry <span
								 id="h4_card_data_e_basic"	class="pc"> 100% </span><span
									class="float-right pc2">Edit</span></h4>
							<div class="progress mb-4" style="margin-top:10px">
								<div id="card_data_e_basic" class="progress-bar bg2" role="progressbar" style="width: 100%"
									aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
							</div>
							<h4  class="small font-weight-bold">Select Card Cover <span
								id="h4_cover_c_basic" 	class="pc"> 100% </span><span
									class="float-right pc2">Edit</span></h4>
							<div class="progress mb-4" style="margin-top:10px">
								<div id="cover_c_basic" class="progress-bar bg2" role="progressbar" style="width: 100%"
									aria-valuenow="1" aria-valuemin="0" aria-valuemax="100"></div>
							</div>
							<h4 class="small font-weight-bold"> Notify Recipient Via
							<div style="margin-top:10px">
								<table style="width:100%" class="font-weight-normal">
									<tr>
										<td style="width:33%; text-align:center">
											<img src="assets/chk_on.png" style="width:25px"> Email
										</td>
										<td style="width:33%; text-align:center">
											<img src="assets/chk_on.png" style="width:25px"> Text
										</td>
										<td style="width:33%; text-align:center">
											<img src="assets/chk_on.png" style="width:25px"> Voice
										</td>
									</tr>
								</table>
							</div>
							<h4 class="small font-weight-bold"> End of Greeting Video call
							<div style="margin-top:10px">
								<table style="width:100%" class="font-weight-normal">
									<tr>
										<td style="width:33%; text-align:center">
											<img src="assets/chk_on.png" style="width:25px"> <span id="vcc">Enabled</span>
										</td>
										<td style="width:33%; text-align:center">
										</td>
										<td style="width:33%; text-align:center">
										</td>
									</tr>
								</table>
							</div>		
						</div>
					</div>
				</div>
			</div>
			<nav id="custom_toolbar" onclick="toggle_nav()" class="max-height:70px;navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow text-left;" style="display:none">
				<div class="card-body">
					<h4 class="small font-weight-bold" style="text-decoration:none!Important;text-align:left">Custom Card Creation: <span id="h4_card_creation" class="float-right"> 20% Complete!</span></h4>
					<div class="progress">
						<div id="card_creation" class="progress-bar bg-success" role="progressbar" style="width: 20%"
							aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
						</div>
					</div>
				</div>
			</nav>
			<div id="nav_detail_custom" class="row" style="display:none;text-align:left;max-height:70px;margin:0">
				<div class="col-lg-6 mb-4">
					<div class="card shadow mb-4">
						<div class="card-header py-3">
							<h6 class="m-0 font-weight-bold text-primary">Customized Card with Video Call<span class="float-right"><button class="btn btn-sm btn-default" disabled>Send</button></span></h6>
						</div>									
						<div class="card-body" style="padding:0">
							<h4 class="small font-weight-bold">Card Data Entry <span
								 id="h4_card_data_e"	class="pc"> 100% </span><span
									class="float-right pc2">Edit</span></h4>
							<div class="progress mb-4" style="margin-top:10px">
								<div id="card_data_e" class="progress-bar bg-success2" role="progressbar" style="width: 100%"
									aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
							</div>
							<h4 class="small font-weight-bold">Upload Video <span
								 id="h4_video_u"	class="pc"> 0% </span><span
									class="float-right pc2">Upload Now</span></h4>
							<div class="progress mb-4" style="margin-top:10px">
								<div id="video_u" class="progress-bar bg-success2" role="progressbar" style="width: 0%"
									aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
							</div>
							<h4  class="small font-weight-bold">Order Celebrity Video <span
								 id="h4_celeb_video_o"	class="pc"> 0% </span><span
									class="float-right pc2">Order Now</span></h4>
							<div class="progress mb-4" style="margin-top:10px">
								<div id="celeb_video_o" class="progress-bar bg-success2" role="progressbar" style="width: 0%"
									aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
							</div>
							<h4  class="small font-weight-bold">Change Cover <span
								id="h4_cover_c" class="pc"> 0% </span><span
									class="float-right pc2">Change Now</span></h4>
							<div class="progress mb-4" style="margin-top:10px">
								<div id="cover_c" class="progress-bar bg-success2" role="progressbar" style="width: 1%"
									aria-valuenow="1" aria-valuemin="0" aria-valuemax="100"></div>
							</div>
							<h4 class="small font-weight-bold">Change Canned Greeting <span
								 id="h4_canned_gr"	class="pc"> 0% </span><span
									class="float-right pc2">Change Now</span></h4>
							<div class="progress mb-4" style="margin-top:10px">
								<div id="canned_gr" class="progress-bar bg-success2" role="progressbar" style="width: 0%"
									aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
							</div>
							<h4 class="small font-weight-bold">Add Personal Message <span
								 id="h4_personal_m"	class="pc"> 0% </span><span
									class="float-right pc2">Add Now</span></h4>
							<div class="progress mb-4" style="margin-top:10px">
								<div id="personal_m" class="progress-bar bg-success2" role="progressbar" style="width: 0%"
									aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
							</div>
							<h4 class="small font-weight-bold"> Change Fonts & Colors<span
								id="h4_fonts_c"	class="pc"> 0% </span><span
									class="float-right pc2">Change Now</span></h4>
							<div class="progress mb-4" style="margin-top:10px">
								<div id="fonts_c" class="progress-bar bg-success2" role="progressbar" style="width: 0%"
									aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
							</div>
							<h4 class="small font-weight-bold"> Notify Recipient Via
							<div style="margin-top:10px">
								<table style="width:100%" class="font-weight-normal">
									<tr>
										<td style="width:33%; text-align:center">
											<img src="assets/chk_on.png" style="width:25px"> Email
										</td>
										<td style="width:33%; text-align:center">
											<img src="assets/chk_on.png" style="width:25px"> Text
										</td>
										<td style="width:33%; text-align:center">
											<img src="assets/chk_on.png" style="width:25px"> Voice
										</td>
									</tr>
								</table>
							</div>
							<h4 class="small font-weight-bold"> End of Greeting Video call
							<div style="margin-top:10px">
								<table style="width:100%" class="font-weight-normal">
									<tr>
										<td style="width:33%; text-align:center">
											<img src="assets/chk_on.png" style="width:25px"> <span id="vcc">Enabled</span>
										</td>
										<td style="width:33%; text-align:center">
										</td>
										<td style="width:33%; text-align:center">
										</td>
									</tr>
								</table>
							</div>		
						</div>
					 </div>
				</div>
			</div>
	
		</div>
	</div>
</div>
<script>
	var ov=0
	var bar_cats=[ ]
	bar_cats=['card_data_e','video_u','celeb_video_o','cover_c','canned_gr','personal_m','fonts_c']
	function update_progress_bars_custom(bar_vals) {
		var pro=window.localStorage.getItem('progress_bar')
		var br=[ ]
		br=bar_vals.split(',')
		ov=0
		for (var i=0;i<=6; i++) {
			set_bar(bar_cats[i], br[i])
			ov=ov+br[i]*1
		}
		ov=Math.floor(ov/7)
		set_bar('card_creation',ov)
	}
	
	function update_progress_bars_basic(bar_vals) {
		var pro=window.localStorage.getItem('progress_bar')
		var br=[ ]
		br=bar_vals.split(',')
		ov=0
		for (var i=0;i<=6; i++) {
			set_bar(bar_cats[i], br[i])
			ov=ov+br[i]*1
		}
		ov=Math.floor(ov/7)
		set_bar('card_creation',ov)
	}
	
	$$('wrapper').style.maxWidth=screen_dimensions()[0]+'px'
	function set_bar(id,amt) {
		$$(id).style.width=amt+'%'
		$$(id).setAttribute('aria-valuenow',amt)
		if ($$('h4_'+id)) {
			$$('h4_'+id).innerHTML=amt + '%'
			if (id=='card_creation') $$('h4_'+id).innerHTML=amt + '% Complete'
			$$('h4_'+id).style.background='#000'
			$$('h4_'+id).style.borderRadius='4px'
			$$('h4_'+id).style.padding='4px'
			$$('h4_'+id).style.fontSize='12px'
			$$('h4_'+id).style.color='white'
			if (amt*1<100) {
				$$(id).className='progress-bar bg-warning'
			} else {
				$$(id).className='progress-bar bg-info'
			}
		}
	}
	var nav_detail_visible=false
	function toggle_nav_basic() {
		if (nav_detail_visible===false) {
			$$('nav_detail_basic').style.display='block'
			nav_detail_visible=true
			$$('wrapper').style.height='100%'
		} else {
			$$('nav_detail_basic').style.display='none'
			nav_detail_visible=false
			$$('wrapper').style.height='7px'
		}
	}
</script>