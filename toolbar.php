<style>
		.bt {
			color:white;
			background: #00d9a3;
			font-size: 12px;
			padding:5px 10px;
			margin-top: -7px;
			border-radius:4px;
			font-weight:bold;
		}
		.pc {
			color:#333;
			background:  #f0f0f0;
			font-size: 12px;
			padding:4px 8px;
			margin-top: -5px;
			border-radius:4px;
			
		}
		.pc2 {
			color:white;
			background:  #00d9a3;
			font-size: 12px;
			padding:7px 10px;
			margin-top: -5px;
			border-radius:4px;
			font-weight:bold;
		}
		.bg-success2 {
			background: #00d9a3
		}
		.bg2 {
			background: #00d9a3!Important
		}
		.cardbody{
			padding:10px!Important;
			margin:0;
			
		}
		#i_p{text-align:left!Important}
</style>
<link href="css/sb-admin-2.min.css" rel="stylesheet">
<div id="wrapper" onclick="toggle()" style="position:fixed!Important;top:0px;left:0;width:100%;background:white;z-index:99999999999999999999999999999;max-height:90px!Important;margin:0;overflow:hidden">
	<div id="content-wrapper" class="d-flex flex-column" style="max-height:90px;margin:0;position:fixed;background:white">
		<div id="content" style="max-height:90px;margin:0;background:white">
			<div id="basic_toolbar" class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow text-left">
				<div class="card-body">
					<h4 class="small font-weight-bold" style="text-decoration:none!Important;margin-bottom:-10px">BASIC CARD: 100% Complete <span id="h4_card_creation" class="float-right pc2">Send Now</span></h4>
					<div style="display:none" class="progress">
						<div id="card_creation" class="progress-bar bg-success" role="progressbar" style="width: 100%"
							aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">
						</div>
					</div>
				</div>
			</div>
			<div id="nav_detail_basic" class="row" style="display:none;text-align:left;margin:0">
				<div class="col-lg-6 mb-4">
					<div class="card shadow mb-4">
						<div class="cardbody">
							<h4 class="small font-weight-bold">Card Data Entry <span
								 id="h4_card_data_e_basic"	class="pc"> 100% </span><span
									class="float-right pc2">Edit</span></h4>
							<div class="progress mb-4" style="margin-top:10px">
								<div id="card_data_e_basic" class="progress-bar bg2" role="progressbar" style="width: 100%"
									aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
							</div>
							<h4  class="small font-weight-bold">Select Card Cover <span
								id="h4_cover_c_basic" 	class="pc"> 100% </span><span
									class="float-right pc2">Edit</span></h4>
							<div class="progress mb-4" style="margin-top:10px">
								<div id="cover_c_basic" class="progress-bar bg2" role="progressbar" style="width: 100%"
									aria-valuenow="1" aria-valuemin="0" aria-valuemax="100"></div>
							</div>
							<h4 class="small font-weight-bold"> Notify Recipient Via
							<div style="margin-top:10px">
								<table style="width:100%" class="font-weight-normal">
									<tr>
										<td style="width:33%; text-align:center">
											<img src="assets/chk_on.png" style="width:25px"> Email
										</td>
										<td style="width:33%; text-align:center">
											<img src="assets/chk_on.png" style="width:25px"> Text
										</td>
										<td style="width:33%; text-align:center">
											<img src="assets/chk_on.png" style="width:25px"> Voice
										</td>
									</tr>
								</table>
							</div>
							<h4 class="small font-weight-bold"> End of Greeting Video call
							<div style="margin-top:10px">
								<table style="width:100%" class="font-weight-normal">
									<tr>
										<td style="width:33%; text-align:center">
											<img src="assets/chk_on.png" style="width:25px"> <span id="vcc">Enabled</span>
										</td>
										<td style="width:33%; text-align:center">
										</td>
										<td style="width:33%; text-align:center">
										</td>
									</tr>
								</table>
							</div>		
						</div>
					</div>
				</div>
			</div>				
			<div id="custom_toolbar">
				<div class="cardbody">
					<h4 class="small font-weight-bold" style="text-decoration:none!Important;text-align:left">Custom Card: <span id="h4_custom_card_creation" class="float-right"></span></h4>
					<div class="progress">
						<div id="custom_card_creation" class="progress-bar bg-success" role="progressbar" style="width: 20%"
							aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
						</div>
					</div>
				</div>
				<div id="t_tabs1" class="www_box2" style="font-size:11px;border-bottom:20px solid #fff;width:100%;background:white;height:22px;text-align:center;padding-top:3px"><span style="margin-bottom:-2px;margin-top:2px;font-size:12px" id="show_hide1"><b>HIDE</b></span></div>
				<div id="t_tabs2" class="www_box2" style="display:none;font-size:11px;border-bottom:20px solid #fff;width:100%;background:white;height:22px;text-align:center;padding-top:3px"><span id="tab_format" style="padding-bottom:3px;padding-top:3px;padding-left:10px;padding-right:10px;margin:0px" class="float-left">FORMAT</span><span onclick="spawn_image()" id="spawn" style="padding-bottom:3px;padding-top:3px;padding-left:10px;padding-right:10px;margin:0px" class="float-left">ADD IMAGE</span><span onclick="toggle_move()" id="tab_move" style="padding-bottom:3px;padding-top:3px;padding-left:10px;padding-right:10px;margin:0px" class="float-left">EDITING IMAGES</span><span style="margin-bottom:-2px;margin-top:2px;font-size:12px" id="show_hide2"><b>HIDE</b></span><span id="tab_close" style="padding-bottom:3px;padding-top:3px;padding-left:10px;padding-right:10px;margin:0px;background:red;color:white" class="float-right">CLOSE PANE</span><span id="tab_options" style="padding-bottom:3px;padding-top:3px;padding-left:10px;padding-right:10px;margin:0px" class="float-right">OPTIONS</span></div>
				<div id="t_tabs3" style="width:100%;height:250px;display:none;background:#fff;top:80px;text-align:left;padding:25px;position:fixed">	
					<h4 class="small font-weight-bold" style="width:100%;text-align:left"> Notify Recipient Via
					<div style="margin-top:10px">
						<table style="width:100%" class="font-weight-normal">
							<tr>
								<td style="width:33%; text-align:center">
									<img src="assets/chk_on.png" style="width:25px"> Email
								</td>
								<td style="width:33%; text-align:center">
									<img src="assets/chk_on.png" style="width:25px"> Text
								</td>
								<td style="width:33%; text-align:center">
									<img src="assets/chk_on.png" style="width:25px"> Voice
								</td>
							</tr>
						</table>
					</div>
					<h4 class="small font-weight-bold"> End of Greeting Video call
					<div style="margin-top:10px">
						<table style="width:100%" class="font-weight-normal">
							<tr>
								<td style="width:33%; text-align:center">
									<img src="assets/chk_on.png" style="width:25px"> <span id="vcc">Enabled</span>
								</td>
								<td style="width:33%; text-align:center">
								</td>
								<td style="width:33%; text-align:center">
								</td>
							</tr>
						</table>
					</div>	
				</div>
			<div id="nav_detail_custom2" class="row" style="display:none;text-align:left;max-height:0px;margin:0">

			</div>
		</div>
	</div>
	
</div>

<script>
	var ov=0
	var bar_cats=[ ]
	bar_cats=['card_data_e','video_u','celeb_video_o','cover_c','canned_gr','personal_m','fonts_c']
	function update_progress_bars_custom(bar_vals) {
		var pro=window.localStorage.getItem('progress_bar')
		var br=[ ]
		br=bar_vals.split(',')
		ov=0
		for (var i=0;i<=6; i++) {
			set_bar(bar_cats[i], br[i])
			ov=ov+br[i]*1
		}
		ov=Math.floor(ov/7)
		set_bar('card_creation',ov)
	}
	
	function update_progress_bars_basic(bar_vals) {
		var pro=window.localStorage.getItem('progress_bar')
		var br=[ ]
		br=bar_vals.split(',')
		ov=0
		for (var i=0;i<=6; i++) {
			set_bar(bar_cats[i], br[i])
			ov=ov+br[i]*1
		}
		ov=Math.floor(ov/7)
		set_bar('card_creation',ov)
	}
	var x=0
	var tmb
	var tbrr='dn'
	
	function toggle() {
		if (tbrr=='dn') {
			x=0
			up()
			tbrr='up'
		} else {
			x=-60
			tbrr='dn'
			dn()
		}
	}
	
	function up() {
		if (x>-60) {
			x--
			console.log(x)
			$$('wrapper').style.marginTop=x+'px'
			tmb=setTimeout('up()',10)
		} else {
			clearTimeout(tmb)
			$$('show_hide1').innerHTML='<b>SHOW</b>'
			$$('show_hide2').innerHTML='<b>SHOW</b>'
		}
	}
	function dn() {
		if (x<0) {
			x++
			console.log(x)
			$$('wrapper').style.marginTop=x+'px'
			tmb=setTimeout('dn()',10)
		} else {
			clearTimeout(tmb)
			$$('show_hide1').innerHTML='HIDE'
			$$('show_hide2').innerHTML='HIDE'
		}
	}
	$$('wrapper').style.maxWidth=screen_dimensions()[0]+'px'
	function incBar(x) {
		var bar_total=window.localStorage.getItem('custom_bar_total')
		if (!bar_total) bar_total=0
		bar_total=bar_total*1
		bar_total+=x*1
		if (bar_total>100) bar_total=100
		window.localStorage.setItem('custom_bar_total',bar_total)
		setBarTotal(bar_total)
	}	

	function setBarTotal(bar_total) {
		set_bar('custom_card_creation',bar_total)
	}

	function show_bar()	{
		bar_total=window.localStorage.getItem('custom_bar_total')
		set_bar('custom_card_creation',bar_total)
	}

	function set_bar(id,amt) {
		$$(id).style.width=amt+'%'
		$$(id).setAttribute('aria-valuenow',amt)
		if ($$('h4_'+id)) {
			if (id=='custom_card_creation') {
				$$('h4_'+id).innerHTML=amt + '% Complete'
				$$('h4_'+id).style.borderRadius='4px'
				$$('h4_'+id).style.padding='4px'
				$$('h4_'+id).style.fontSize='12px'
				$$('h4_'+id).style.color='#333'
			} else {
				$$('h4_'+id).innerHTML=amt + '%'
				$$('h4_'+id).style.background='#000'
				$$('h4_'+id).style.borderRadius='4px'
				$$('h4_'+id).style.padding='4px'
				$$('h4_'+id).style.fontSize='12px'
				$$('h4_'+id).style.color='white'
			}
			if (amt*1<100) {
				$$(id).className='progress-bar bg-warning'
			} else {
				$$(id).className='progress-bar bg-info'
			}
		}
	}
	var nav_detail_visible=false
	function toggle_nav_basic() {

	}
</script>