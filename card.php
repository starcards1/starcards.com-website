<?php
	require_once "../class/utils.class.php";
	$c=new utils;
	$c->connect("199.91.65.83","voxeo");
	parse_str(http_build_query($_GET));
	$pro=$c->query("select * from celeb_profiles where mid=$provider_mid");
	foreach($pro[0] as $key => $value) {
		${$key}=$value;
	}
	$ta=$c->query("select * from celeb_tags where pid=$pid");
	$t=explode(",",$ta[0]['tags']);
	$r=$c->query("select * from greeting_cards where card_id=$card_id")[0];
	$cover_id=$r['cover_id'];
	$asset=$r['asset'];
	$msg=$r['msg'];
	
?>
<script src="assets/js/jquery.js"type="text/javascript"></script>
<script src="assets/js/jquery-ui.min.js" type="text/javascript" charset="utf-8"></script>	
<script src="js/tag-it.js" type="text/javascript" charset="utf-8"></script>	
<script src="assets/js/bootstrap.min.js"type="text/javascript"></script>
<script src="assets/js/utils.js"type="text/javascript"></script>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>LinqStar</title>
	<link href="doc/css/bootstrap.min.css" rel="stylesheet">
	<link href="doc/css/style.css" rel="stylesheet">
	<link href="doc/css/menu.css" rel="stylesheet">
	<link href="doc/css/vendors.css" rel="stylesheet">
	<link href="doc/css/icon_fonts/css/all_icons_min.css" rel="stylesheet">
    
	<!-- YOUR CUSTOM CSS -->
	<link href="doc/css/custom.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="assets/magic.css"/>
    <link rel="stylesheet" href="assets/css/jquery-ui.min.css"/>
    <link rel="stylesheet" href="assets/css/main.css"/>
    <link rel="stylesheet" href="css/shadows.css"/>
    <link rel="stylesheet" href="dz/dropzone.css"/>
	<link href="style.css" rel="stylesheet" type="text/css" />
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:300|Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
	<style>
		* {
		font-size:14px}
		div input span {font-size:14px;}
		.card_cover{
			background:aliceblue;
			border:0px solid white;
			height:175px;
			margin:10px;
			padding:0
		}
		img {
			draggable:true;
		}

.flip-card {
  background-color: transparent;
  width: 100%;
  max-width:400px;
  height: 100%;
  perspective: 1000px;
}

.flip-card-inner {
  position: relative;
  text-align: right;
  transition: transform 0.6s;
  transform-style: preserve-3d;
  box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
}
.flip-card:hover .flip-card-inner {
 // transform: rotateY(-180deg);
  //margin-right:400px
}
		
	</style>
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Allura&family=Gloria+Hallelujah&family=Homemade+Apple&display=swap" rel="stylesheet">	
    <link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon"/>
</head>
<body  style="background:url(assets/bg.png)no-repeat;background-size:cover">
	<div class="text-center" style="margin-top:0px">
		<div class="account-wrapper" style="margin:auto;padding:10px;max-width:1000px;background:none;width:100%;">
			<div id="apex" class="account-body">
				<div class="row">
				</div>
				<span id="part1">
				</span>	
				<br>				
				<span id="part2" style="display:none">
					<div class="row text-left">
						<div class="col-md-12 text-center">
						Please enter at least one
							<div class="col-md-6 text-left" style="margin:auto;border:10px solid white;border-radius:8px;padding:20px;background:aliceblue">
								<div>
									<label>Give your card a title</label>
									<input class="form-control" type="text" id="title" />
								</div>
								<div>
									<label>Recepient Name</label>
									<input class="form-control" type="text" id="recepient_name" />
								</div>
								<div>
									<label>Recepient Mobile</label>
									<input class="form-control" type="text" id="recepient_mobile" />
								</div>
								<div>
									<label>Recepient Email</label>
									<input class="form-control" type="text" id="recepient_email" />
								</div>
								<div>
									<label>From Display Name</label>
									<input class="form-control" type="text" id="sender_name" />
								</div>
							</div>
						</div>
						<div class="col-md-12 text-center">
							<div class="col-md-6 text-center" style="margin:auto">
								<button style="width:100%;max-width:400px" onclick="step1()" class="button-5">Go back</button>
								<button style="width:100%;max-width:400px" onclick="step3()" class="button-5">Next - Enter Message</button>
							</div>
						</div>
					</div>
				</span>					
				<span id="part3" style="display:none">
					<div class="row text-left">

							<div class="col-md-12 text-left" style="margin:auto;border:0px solid white;border-radius:8px;padding:20px;background:">
								<div class="col-md-6 text-left" style="margin:auto">
									<label style="color:white">Please enter a personalized message.<br>Limit: 420 Characters. Choose wisely.</label>
									<div style="min-width:100%;background:white;font-size:14px;height:425px;;line-height:1.3em;font-size:21px;font-family:Gloria Hallelujah!Important;padding:20px;color:blue" id="msg" onkeypress="setStyle()" contentEditable></div>
								</div>
								<br>
								<br>
							</div>
							
						<div class="col-md-12 text-center">
							<div class="col-md-6 text-center" style="margin:auto">
								<button style="width:100%;max-width:400px" onclick="step4()" class="button-5">Save and Preview</button>
								<button style="width:100%;max-width:400px" onclick="" class="button-5">Go back</button>
							</div>
						</div>
					</div>
				</span>			
				<span id="part4" style="display:none">
					<div class="row text-center">
						<div class="col-md-12" style="width:100%;height:600px; background:none; margin:auto;padding:0;max-width:400px">
							<div class="flip-card">
								<div class="flip-card-inner">
									<a href="javascript:step5()"><img id="card_cover" style="width:100%;height:100%;margin:0;padding:0;box-shadow:0 0 10px #000" /></a>
								</div>
							</div>
						</div>
						<button class="button-5" onclick="step5()" style="position:absolute;left:0;right:0;margin:auto;bottom:20px;z-index:99999999999999">Next</button>
					</div>
				</span>					
				<span id="part5" style="display:none">
					<div class="row text-left">
							<div class="col-md-12 www_box" style="width:100%;height:600px;max-width:400px;background:url(assets/bg.png); margin:0;padding:0;border-radius:5px 0 0 5px;">
								<video id="ass" src="greetings/<?=$asset;?>" style="object-fit:cover; margin:auto;margin-top:30%; background:url(assets/bg.png); drop-shadow:inset 0 0 15px #000; background-size:cover; width:300px;height:300px;border-radius:300px;border-radius:10px solid white"></video>"
							</div>
							<div class="col-md-12 www_box" style="width:100%;height:600px;max-width:400px; background:#f0f0f0; margin:0;padding:0;border-radius:0 5px 5px 0;text-align:center">
								<div id="pane2" style="margin:auto;width:70%;height:70%;margin-top:20%;margin-bottom:15%;background:aliceblue;color:blue;text-align:left;vertical-align:middle;line-height:1.3em;font-size:21px;font-family:Gloria Hallelujah!Important;padding:20px" class="www_box"></div>
								<button class="button-5" onclick="show_video()"  style="position:absolute;left:0;right:0;margin:auto;bottom:20px;z-index:99999999999999">Next</button>
							</div>
						</div>
						<div class="col-md-12 text-center">
						</div>						
					</div>
				</span>					
				<span id="part6" style="display:none">
					<div class="row text-center">
						<div class="col-md-12 text-center">
							<div class="col-md-8" style="margin:auto">
								<h4 style="color:white">Add Video Shout-out</h4>

								<!--Upload Sender Photos-->
								<div class="row" id="part12" style="display:none;margin-top:10px;margin-bottom:10px">
									<div id="p11"></div>
									<div class="col-md-12" class="www_box" style="padding:10px;width:400px;height:225px">
										<form action="php/x_greeting_uploads.php"  style="width:100%;border:none;background:none;margin-top:-40px;background:none" class="dropzone dz-preview dropzone-previews dz-clickable  dz-thunbnails js-dropzone dz-dropzone dz-dropzone-boxed gradient-overlay-half-primary-v4" id="member12"></form>
									</div>
								</div>									
								<button onclick="upload_my_photo()" style="background:#E28E6F;color:white;width:100%;max-width:400px" class="button-5">Upload Background Mood Music</button><br>

								<!--Upload Sender Video-->
								<div class="row" id="part8" style="display:none;margin-top:10px;margin-bottom:10px">
									<div id="p8"></div>
									<div class="col-md-12" class="www_box" style="padding:10px;width:400px;height:225px">
										<form action="php/x_greeting_uploads.php"  style="width:100%;border:none;background:none;margin-top:-40px;background:none" class="dropzone dz-preview dropzone-previews dz-clickable  dz-thunbnails js-dropzone dz-dropzone dz-dropzone-boxed gradient-overlay-half-primary-v4" id="member8"></form>
									</div>
								</div>									
								<button onclick="upload_my_video()" style="background:#E28E6F;color:white;width:100%;max-width:400px" class="button-5">Upload Recorded Video Message Now</button><br>

								<!--Upload Sender Voice Over-->
								<div class="row" id="part9" style="display:none;margin-top:10px;margin-bottom:10px">
									<div id="p9"></div>
									<div class="col-md-12" class="www_box" style="padding:10px;width:400px;height:225px">
										<form action="php/x_greeting_uploads.php"  style="width:100%;border:none;background:none;margin-top:-40px;background:none" class="dropzone dz-preview dropzone-previews dz-clickable  dz-thunbnails js-dropzone dz-dropzone dz-dropzone-boxed gradient-overlay-half-primary-v4" id="member9"></form>
									</div>
								</div>
								<button onclick="upload_voice_over()" style="background:#E28E6F;color:white;width:100%;max-width:400px" class="button-5">Upload Voice-over Now</button><br>

								<!--Upload Handwritten Note-->
								<div class="row" id="part10" style="display:none;margin-top:10px;margin-bottom:10px">
									<div id="p10"></div>
									<div class="col-md-12" class="www_box" style="padding:10px;width:400px;height:225px">
										<form action="php/x_greeting_uploads.php"  style="width:100%;border:none;background:none;margin-top:-40px;background:none" class="dropzone dz-preview dropzone-previews  dz-clickable  dz-thunbnails js-dropzone dz-dropzone dz-dropzone-boxed gradient-overlay-half-primary-v4" id="member10"></form>
									</div>
								</div>									
								<button onclick="upload_handwritten_card()" style="background:#E28E6F;color:white;width:100%;max-width:400px" class="button-5">Upload Message in your handwriting</button><br>

								<!--Upload Mood Music-->
								<div class="row" id="part11" style="display:none;margin-top:10px;margin-bottom:10px">
									<div id="p11"></div>
									<div class="col-md-12" class="www_box" style="padding:10px;width:400px;height:225px">
										<form action="php/x_greeting_uploads.php"  style="width:100%;border:none;background:none;margin-top:-40px;background:none" class="dropzone dz-preview dropzone-previews dz-clickable   dz-thunbnails dz-dropzone-boxed gradient-overlay-half-primary-v4" id="member11"></form>
									</div>
								</div>									
								<button onclick="upload_mood_music()" style="background:#E28E6F;color:white;width:100%;max-width:400px" class="button-5">Upload Background Mood Music</button><br>
								

								<button onclick="buy_celebrity_video()" style="background:#E13A98;color:white;width:100%;max-width:400px" class="button-5">Pay for Celebrity Video Message</button><br>
								<button onclick="buy_celebrity_live_call()" style="background:#E13A98;color:white;width:100%;max-width:400px" class="button-5">Pay for Celebrity Live Call</button><br>
								<button onclick="step7()" style="width:100%;max-width:400px;background:#8500B2;color:#fff" class="button-5">Configure Delivery and Send</button>
							</div>
						</div>
					</div>
				</span>	
				<div id="part7" style="display:none">
					<div class="row text-center">
						<div class="col-md-12 text-center">
							<div class="col-md-6" style="margin:auto">
								<h4 style="color:white">Configure Delivery</h4>
								<button onclick="step1()" style="width:100%;max-width:400px" class="button-5">Start Over</button><br>
								<button style="width:100%;max-width:400px;font-family:Open Sans!Important" class="button-5">Send it Now</button><br>
								<div id="scl8r1" class="row text-left" style="display:none">
									<div class="col-md-1">

									</div>
									<div class="col-md-5">
										<div style="color:white"><b>SEND DATE</b></div><input class="form-control td-input" type="text" id="booking_date" style="font-size:1em">
									</div>
									<div class="col-md-5">
										<div style="color:white"><b>SEND TIME</b></div><input class="form-control td-input" type="text" id="start_time" style="font-size:1em">
									</div>
									<div class="col-md-1">

									</div>
								</div>
								<div id="scl8r2" class="row text-left" style="display:none">
										<div class="col-md-1">

										</div>
										<div class="col-md-10">
											<button onclick="save_later()" style="width:100%; background:url(assets/bg.png);background-size:cover;color:white" class="button-5">Save and Schedule</button><br>
										</div>
										<div class="col-md-1">

										</div>
									</div>
								<button onclick="later()" style="width:100%;max-width:400px" class="button-5">Schedule for Later</button><br>
								<button style="width:100%;max-width:400px" class="button-5">Send On My Cue</button>
							</div>
						</div>
					</div>
				</div>	
			</div>
		</div>
	</div>
	<script src="assets/js/wait.js"></script>
	<script src="assets/js/common.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="./assets/js/modernizr-3.6.0.min.js"></script>
    <script src="./assets/js/plugins.js"></script>
    <script src="./assets/js/bootstrap.min.js"></script>
    <script src="./assets/js/jquery-ui.min.js"></script>
    <script src="js/location.js" type="text/javascript" charset="utf-8"></script>	
	<script src="js/utils.js"type="text/javascript"></script>
	<script src="dz/dropzone.js"type="text/javascript"></script>
	<script src="https://apis.google.com/js/client:platform.js?onload=start" async defer></script>	
	<script type="text/javascript">
		var strx=''
		var tmr
		function keyPressName(myEventKeyName){
			  var pressedKey;
			  if(window.event){
				 pressedKey = myEventKeyName.keyCode;
			  } else if(myEventKeyName.which)
			  {
				 pressedKey = myEventKeyName.which;
		   }
				console.log(pressedKey)
				if(pressedKey==13) {
					 strx+='<br><br>'
				} else {
					strx+=String.fromCharCode(pressedKey);
				}
		   $$('preview2').innerHTML=strx
		}
		function refs() {
			 $$('preview2').innerHTML=$$('msg').value
		}
		function step1(){
			$('#part2').hide()
			$('#part3').hide()
			$('#part4').hide()
			$('#part1').show()
			$('#part5').hide()
			$('#part6').hide()
			$('#part7').hide()
		}
		function step2(cover_id){
			$('#part1').hide()
			$('#part3').hide()
			$('#part4').hide()
			$('#part2').show()
			$('#part5').hide()
			$('#part6').hide()
			$('#part7').hide() 
			setCookie('cover_id',cover_id)
		}
		var u ='',card_id
		function step3(){
			$('#part1').hide()
			$('#part3').show()
			$('#part4').hide()
			$('#part2').hide()
			$('#part5').hide()
			$('#part6').hide()
			$('#part7').hide()
			u="x_create_greeting.php?title="+$$('title').value+"&sender_name="+$$('sender_name').value+"&sender_email="+getCookie('email')+"&sender_mobile="+getCookie('mobile')+"&recepient_name="+$$('recepient_name').value+"&recepient_email="+$$('recepient_email').value+"&recepient_mobile="+$$('recepient_mobile').value+"&cover_id="+getCookie('cover_id')
			console.log(u)
			$.ajax({url:u,success:function(data){setCookie('card_id',data); card_id=data; $.alert('Saved!')}})
		}
		function step4(){
			$('#part1').hide()
			$('#part3').hide()
			$('#part4').show()
			$('#card_cover').addClass('magictime twisterInUp');
			$$('part4').style.background='none'
			$('#part2').hide()
			$('#part5').hide()
			$('#part6').hide()
			$('#part7').hide()
			setCookie('msg',$$('msg').value)
			$$('card_cover').src='assets/cards/<?=$cover_id;?>.png'
			$$('pane2').innerHTML='<?=$msg;?>'
		}
	
		function step5(){
			$('#part1').hide()
			$('#part3').hide()
			$('#part5').show()
			$('#part2').hide()
			$('#part4').hide()
			$('#part6').hide()
			$('#part7').hide()
			$('#part4').addClass('magictime perspectiveLeft')
			window.scrollTo(0,1000)
		}
	

		
		function step6(){
			$('#part1').hide()
			$('#part3').hide()
			$('#part6').show()
			$('#part2').hide()
			$('#part4').hide()
			$('#part5').hide()
			$('#part7').hide()
		}
		function step7(){
			$('#part1').hide()
			$('#part3').hide()
			$('#part7').show()
			$('#part2').hide()
			$('#part4').hide()
			$('#part5').hide()
			$('#part6').hide()
		}

		function upload_my_photo(){
			$('#part8').hide()
			$('#part9').hide()
			$('#part10').hide()
			$('#part11').hide()
			$('#part12').show()
			activate_upload(12)
		}

		function upload_my_video(){
			$('#part8').show()
			$('#part9').hide()
			$('#part10').hide()
			$('#part11').hide()
			$('#part12').hide()
			activate_upload(8)
		}
		function upload_voice_over(){
			$('#part9').show()
			$('#part8').hide()
			$('#part10').hide()
			$('#part11').hide()
			$('#part12').hide()
			activate_upload(9)
		}
		function upload_handwritten_card(){
			$('#part10').show()
			$('#part9').hide()
			$('#part8').hide()
			$('#part11').hide()
			$('#part12').hide()
			activate_upload(10)
		}

		function upload_mood_music(){
			$('#part11').show()
			$('#part9').hide()
			$('#part10').hide()
			$('#part8').hide()
			$('#part12').hide()
			activate_upload(11)
		}

		function buy_celebrity_video(){
			$('#part9').hide()
			$('#part10').hide()
			$('#part11').hide()
			$('#part8').hide()
		}

		function buy_celebrity_live_call(){
			$('#part8').hide()
			$('#part9').hide()
			$('#part10').hide()
			$('#part11').hide()
		}

		function later() {
			if ($$('scl8r1').style.display=='none') {
				$$('scl8r1').style.display='' 
				$$('scl8r2').style.display='' 
			} else {
				$$('scl8r1').style.display='none' 
				$$('scl8r2').style.display='none' 
			}
		}
		
		function setStyle() {
			if ($$('msg').textContent.length>20) $$('msg').textContent=$$('msg').textContent.substr(0,$$('msg').textContent.length-1)
			var nodes = document.getElementById('msg').childNodes;
			for(var i=0; i<nodes.length; i++) {
				if (nodes[i].nodeName.toLowerCase() == 'div') {
					 nodes[i].style.fontFamily='Gloria Hallelujah'
					 nodes[i].style.fontSize='21px'
					 nodes[i].style.color='blue'
					 nodes[i].position='absolute'
					 nodes[i].draggable=true
				 }
			}
		}
		
		$$('msg').onkeypress=function(e){
			console.log(e)
			if ($$('msg').textContent.length>420) e.preventDefault()
			e.target.style.fontFamily='Gloria Hallelujah'
		}
		
		function qs1() {
			var str1=[]
			var url=''
			var pre
		    str1=location.search.split('?')[1]
			for (var i=0; i<str1.split('&').length; i++) {
				if (i>0) pre='&'
					else pre='?'
				url+=pre+str1.split('&')[i].split('=')[0]+'="+'+str1.split('&')[i].split('=')[0]+'\+\"'
			}
			console.log(url)
		}
		Dropzone.autoDiscover=false
		var sites
		var imagesUploaded
		var r,k,c,t2,t3,t4,intr=50,currImg
		var rat=1
		var rx=0, t, ext
		var set=false
		var usr_img=[]
		var ext
		var collection_set=0
		var channel_set=0
		var cnn=0
		
		function getCookie(cname)	{
			var name = cname + "=";
			var ca = document.cookie.split(';');
			for(var i=0; i<ca.length; i++) {
			  var c = ca[i].trim();
			  if (c.indexOf(name)==0) return c.substring(name.length,c.length);
			}
			return "";
		}
	var myDropzone, tfiles=0
	var gui
	var src_element

	function add_assets(asset,asset_type,asset_media_type,card_id) {
		u="x_update_greeting.php?field=asset&value="+asset+"&key_field=card_id&key_value=" + card_id
		$.ajax({url:u,success:function(data){}})
		u="x_update_greeting.php?field=asset_media_type&value="+asset_media_type+"&key_field=card_id&key_value=" + card_id
		$.ajax({url:u,success:function(data){}})
		u="x_update_greeting.php?field=asset_type&value="+asset_type+"&key_field=card_id&key_value=" + card_id
		$.ajax({url:u,success:function(data){$.alert('Saved!')}})
	}
	
	function activate_upload(x) {
		var gauge=[ ];
		var target=[ ];
		var url="https://linqstar.com/php/x_greeting_uploads.php"
		if (!myDropzone) {
			myDropzone = new Dropzone("form#member"+x, { 
				url: url,
				timeout:0,
				maxFilesize: 1000000000,
				dictDefaultMessage: "<br><br><div id='xe"+x+"' style='background:;color:#fff;font-family:Open sans;color:#fff;font-weight:300;top:0;position:absolute;width:100%;left:0;margin-top:20px;top:8%;right:0;margin-left:auto;margin-right:auto;height:75%;z-index:99999999999999999;color:black;margin-top:-15px'>DRAG DROP FILES<br><b>OR</b><br>CLICK HERE<br><img style='width:70px' src='https://lushmatch.com/sdfm/images/cap04.png'></div>",
				createImageThumbnails:true,
				init: function() {
					this.on("sending", function(file, xhr, formData){
						gui=guid()
						var in_co, in_ch
						setCookie('attach_id',gui)
						ext=file.name.split('.')[1]
						try {
						if (ext=='mp4' || ext=='wmv' || ext=='qt' || ext=='mov' || ext=='avi' || ext=='mkv' || ext=='mpg') {
							$$('p'+x).innerHTML='<video src="greetings/'+gui+'.'+ext+'" style="width:100%;margin:10px"></video>'
							asset_media_type='video'
						} else if (ext=='mp3' || ext=='wma' || ext=='qt' || ext=='au' || ext=='wav') {
							$$('p'+x).innerHTML='<audio src="greetings/'+gui+'.'+ext+'" style="width:100%;margin:10px"></audio>'
							asset_media_type='audio'
						} else if (ext=='png' || ext=='jpg' || ext=='gif' || ext=='bmp'){
							asset_media_type='image'
						}
						if (x==10) {
							src_element='msg_image'
						} else if (x==12) {
							src_element='user_photo'
						} else if (x==11) {
							src_element='mood_music'
						} else if (x==9) {
							src_element='voice_over'
						} else if (x==8) {
							src_element='user_video'
						}
							formData.append("attach_id", gui);
							formData.append("user_type", getCookie('user_type'));
							formData.append("fan_mid", qs('fan_mid'));
							formData.append("provider_mid", getCookie('mid'));
							formData.append("card_id", card_id);
							formData.append("asset_media_type", asset_media_type);
							formData.append("asset_type", src_element);
							formData.append("asset", gui + '.' + ext);
						} catch (e) {}
					});
				}			
			});
		}
		myDropzone.on("addedfile", function(file) {
			//wait('Uploading File, Please Wait...')
		});
			
		myDropzone.on("complete", function(file) {
			if (x==8 || x==9 || x==11){
				if (x==8) {
					setTimeout(function(){
						$$('member'+x).innerHTML='<video src="greetings/'+getCookie('attach_id')+'.'+ext+'" style="max-height:250px!Important;opacity:1/border:10px solid white;" autoplay muted></video>'
					},3000)
					add_asset(getCookie('attach_id') +' .' + ext,src_element,asset_media_type)
				} else {
					setTimeout(function(){
						$$('member'+x).innerHTML='<audio src="greetings/'+getCookie('attach_id')+'.'+ext+'" style="max-height:120px!Important;opacity:1" autoplay muted></audio>'
					},3000)
				}
			}
		})
	//	hide()
	}
	function setCookie(cname,cvalue,exdays)	{
		var d = new Date();
		d.setTime(d.getTime()+(exdays*24*60*60*1000));
		var expires = "expires="+d.toGMTString();
		document.cookie = cname + "=" + cvalue + "; " + expires;
	 }	
	
	function print_blocks(ppx) {
		if (ppx=='undefined') return false
		ppx=ppx.toString()
		var f
		var px=''
		var x=''
		if (ppx*1<10) x='00'+ppx
		if ((ppx*1>9) && (ppx*1<100)) x='0'+ppx
		if (ppx*1==100) x=ppx
			
		if (!wd) var wd='20px'
		for (f=0;f<=x.length-1;f++) {
			var dg=x.substring(f,f+1)
			if (dg !== '.')
				px=px+'<span><img src="https://gaysugardaddyfinder.com/assets/img/' + dg + '.png" style="width:'+wd+'"></span>'
		}
		return '<span style="padding-top:5px">'+px+'</span>'
	}
	
	var done=false
		function win() {
			if (done===true) return false
			$.alert({
				title: 'Success',
				content: 'Image(s) uploaded, saved and cataloged. Whats next?',
				btnClose:true,
				buttons: {
					b1: {
						text: 'Home',
						btnClass: 'btn-white',
						action: function(){
							location.href='home.html'
						}
					},
					b2: {
						text: 'My Uploads',
						btnClass: 'btn-white',
						action: function(){
							location.href='show_uploads.html'
						}
					},
					b3: {
						text: 'My Account',
						btnClass: 'btn-red',
						action: function(){
							location.href='settings.html'
						}
					}
				}
			});
			done=true
		}
		function fail(error) {
			alert("An error has occurred: "+error)
		}	
	var currPage='upload_photo.html'
	function fs(el) {
		if (!el) el = document.documentElement
		, rfs = // for newer Webkit and Firefox
			   el.requestFullScreen
			|| el.webkitRequestFullScreen
			|| el.mozRequestFullScreen
			|| el.msRequestFullScreen
		;
		if(typeof rfs!="undefined" && rfs){
		  rfs.call(el);
		} else if(typeof window.ActiveXObject!="undefined"){
		  // for Internet Explorer
		  var wscript = new ActiveXObject("WScript.Shell");
		  if (wscript!=null) {
			 wscript.SendKeys("{F11}");
		  }
		}
		}
	//setTimeout('fs()',1000)
	function guid() {
	  function s4() {
		return Math.floor((1 + Math.random()) * 0x10000)
		  .toString(16)
		  .substring(1);
	  }
	  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
		s4() + '-' + s4() + s4() + s4();
	}
	var sa=[]
	var ra=[]
	function saveSite(s) {
		sites=$('.select-1').val().toString()
		console.log(sites)
	}
	
			function generateRandomString(j){
				if (!j) j=16
				var op=''
				var arr=new Array('a','b','c','d','e','f','i','j','k','m','n','p','q','r','s','t','u','v','w','x','y','z',0,1,2,3,4,5,6,7,8,9,0,0,1,2,3,4,5,6,7,8,9,0)
				for (k=0;k<j;k++) {
					var inx=randomIntFromInterval(0,arr.length-1)
					op += arr[inx]
				}
				return op;
			}
			function guid() {
			  function s4() {
				return Math.floor((1 + Math.random()) * 0x10000)
				  .toString(16)
				  .substring(1);
			  }
			  return s4() + '-' + s4();
			}

	function $$(obj) {
		return document.getElementById(obj)
	}		
	
	function delCookie(cname) {
		var d = new Date();
		d.setTime(d.getTime());
		var expires = "expires="+d.toGMTString();
		document.cookie = cname + "=" + "" + "; " + expires;
	 } 
	function setCookie(cname,cvalue,exdays)	{
		var d = new Date();
		d.setTime(d.getTime()+(exdays*24*60*60*1000));
		var expires = "expires="+d.toGMTString();
		document.cookie = cname + "=" + cvalue + "; " + expires;
	 }
	function c_us() {
		window.top.contactUs()
	}
	function $$(objID) {
		return document.getElementById(objID)
	}
	function getCookie(cname)	{ 
		if (window.location.href.indexOf('file://')>=0) {
			var cvalue = window.localStorage.getItem(cname);
			return cvalue
		} else {
			var name = cname + "="; 
			var ca = document.cookie.split(';'); 
			for(var i=0; i<ca.length; i++) { 
			  var c = ca[i].trim(); 
			  if (c.indexOf(name)==0) return c.substring(name.length,c.length); 
			} 
			return ""; 
		}
	} 
	function show_video() {
		$$('ass').play()
		window.scrollTo(0,0)	
	}

	</script>
	<script src="doc/js/common_scripts.min.js"></script>
	<script src="doc/js/functions.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script> 
</body>
</html>

<?
	echo "<script>setTimeout('step4(\'$cover_id\')',10);</script>";
?>
<script>
setTimeout('step2("<?=$cover_id;?>")',1)
</script>