<? include "x_auth.php"; ?>
<?
	$orders=$c->query("select * from orders where fan_mid=" . $_COOKIE['mid']);
	for ($i=0; $i<count($orders); $i++) {
		$pms[]=$orders[$i]['provider_mid']; 
	}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Voxeo Login</title>
    <link rel="stylesheet" href="https://linqstar.com/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://linqstar.com/assets/css/all.min.css">
    <link rel="stylesheet" href="https://linqstar.com/assets/css/animate.css">
    <link rel="stylesheet" href="https://linqstar.com/assets/css/nice-select.css">
    <link rel="stylesheet" href="https://linqstar.com/assets/css/owl.min.css">
    <link rel="stylesheet" href="https://linqstar.com/assets/css/jquery-ui.min.css">
    <link rel="stylesheet" href="https://linqstar.com/assets/css/magnific-popup.css">
    <link rel="stylesheet" href="https://linqstar.com/assets/css/flaticon.css">
    <link rel="stylesheet" href="https://linqstar.com/assets/css/main.css">
    <link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon">
</head>
<body>
    <div class="preloader">
        <div class="preloader-inner">
            <div class="preloader-icon">
                <span></span>
                <span></span>
            </div>
        </div>
    </div>

		<!--============= Sign In Section Ends Here =============-->
		<section>
		    <div class="account-section bg_img" data-background="https://linqstar.com/assets/images/account-bg.jpg">

            <div class="container text-center">
            <img src="https://linqstar.com/assets/images/logo2.png" style="width:200px;margin:auto" alt="logo">
               <div class="row text-center">

				<div class="col-md-12" style="">
			<div class="account-wrapper" style="max-width:100%;padding:25px">
				<? if ($user['members']->type=='fan') { ?>
						<br><h4>MY ORDERS</h4>
							<div class="row mb-30 justify-content-center">					
							<?
								for ($i=0; $i<count($pms); $i++) {
									$celeb=$c->query("select * from celeb_profiles where  mid=" . $pms[$i]);
									$k=$i+1;
							?>
								<div class="col-lg-3 col-sm-6" style="padding:0;margin:30px">
									<div class="to-access-item">
										<div style="top:0;border-radius:20px 20px 0 0 ;text-align:left;position:absolute;height:60px;background:#a3d900;width:100%;left:0">
											<img src="ccc.png" style="height:30px;margin-top:15px;margin-left:20px">
										</div>
											<br>
											<div style="width:100%">
												<span class="title"><b><?=$orders[$i]->service_name;?></b></span>
												<br><span class="title"><?=$orders[$i]->sub_service_name;?></span>
											</div>										
											<div class="to-access-thumb" style="width:80px;height:80px">
											<span class="anime"></span>
											<div class="thumb">
												<img src="<?=$celeb[0]['photo'];?>" style="width:80px;height:80px;border-radius:160px">
											</div>
										</div>
										<h4 class="title">$<?=$orders[$i]->amount;?></h4>
										<div class="title"><b><?=$celeb[0]['name'];?></b></div>
										<div class="title"><?=$celeb[0]['celeb_category'];?></div>
										<div style="position:absolute;text-align:center;margin-top:10px">
											<a style="margin:20px;" href="javascript:view_details('<?=$orders[$i]->id;?>')"><div class="button-5" style="width:115px;margin:auto;left:0;right:0;padding:15px">Details</div></a>
											<a style="margin:20px;" href="javascript:view_receipt('<?=$orders[$i]->id;?>')"><div class="button-5" style="width:115px;margin:auto;left:0;right:0;padding:15px;margin-left:10px">Receipt</div></a>
										</div>
									<br></div>
								</div>
								<div class="modal fade" id="<?=$orders[$i]->id;?>" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
									<div class="modal-dialog modal-lg modal-dialog-scrollable">
										<div class="modal-content text-center">
											<div class="modal-body">
												<?=file_get_contents(json_decode($orders[$i]->payment_response)->receipt_url);?>
											</div>
												<button style="left:0;right:0;margin:auto;position:absolute;bottom:25px;width:140px;height:50px;" type="button" class="button-5" data-dismiss="modal" aria-label="Close">
												  <span aria-hidden="true">CLOSE</span>
												</button>
		
										</div>
									</div>
								</div>

							<?
								}
							?>
							</div>
						<br><br><hr><br><h4>MY CELEBRTIES</h4>
						<div id="my_celebrities" class="row justify-content-center mb-30-none">
							<?
								for ($i=0; $i<count($pms); $i++) {
									$celeb=$c->query("select * from celeb_profiles where  mid=" . $pms[$i]);
									foreach($celeb[0] as $key => $value) {
										${$key}=$value;
									}
							?>
								<div class="col-md-2 col-lg-2" style="min-width:190px">
									<div class="am-item" style="max-height:205px!Important;">
										<?=$star;?> 
										<img onclick="javascript:del(<?=$pid;?>)" src="<?=$photo;?>" style="border:10px solid white;top:25px;left:0;right:0;margin:auto;margin-top:-60px;width:125px!Important;height:135px!Important;max-height:125px!Important;border-radius:125px;vertical-align:top" alt="feature">
										<div class="am-content" style="margin-top:-15px;font-size:0.8em">
											<div style="font-weight:bold;color:#000"><?=$service_tag;?><?=substr(strtoupper(strtoupper($name)),0,16);?></div>
											<div style="font-size:0.8em;margin-top:-10px"><? echo $provider_type;?></div>
											<div style="font-size:0.7em;margin-top:-10px"><? echo substr(strtoupper($user_category),0,20);?></div>
										</div>
									</div>
								</div>
							<?
								}
							?>
						</div>
						<br><br><hr><br>
						<h4>MY CHANNELS</h4>
						<div id="fan_channels" class="row justify-content-center mb-30-none">
							<?
								for ($i=0; $i<count($pms); $i++) {
									$celeb=$c->query("select * from celeb_profiles where  mid=" . $pms[$i]);
									foreach($celeb[0] as $key => $value) {
										${$key}=$value;
									}
							?>
								<div class="col-md-2 col-lg-2" style="min-width:190px">
									<div class="am-item" style="max-height:205px!Important;">
										<?=$star;?> 
										<img onclick="javascript:del(<?=$pid;?>)" src="<?=$photo;?>" style="border:10px solid white;top:25px;left:0;right:0;margin:auto;margin-top:-60px;width:125px!Important;height:135px!Important;max-height:125px!Important;border-radius:125px;vertical-align:top" alt="feature">
										<div class="am-content" style="margin-top:-15px;font-size:0.8em">
											<div style="font-weight:bold;color:#000"><?=$service_tag;?><?=substr(strtoupper(strtoupper($name)),0,16);?></div>
											<div style="font-size:0.8em;margin-top:-10px"><? echo $provider_type;?></div>
											<div style="font-size:0.7em;margin-top:-10px"><? echo substr(strtoupper($user_category),0,20);?></div>
										</div>
									</div>
								</div>
							<?
								}
							?>
						</div>	
						<br><br><hr><br><h4>MY FAVORITES</h4>
						<div id="fan_favs" class="row justify-content-center mb-30-none">
							<?
								for ($i=0; $i<count($pms); $i++) {
									$celeb=$c->query("select * from celeb_profiles where  mid=" . $pms[$i]);
									foreach($celeb[0] as $key => $value) {
										${$key}=$value;
									}
							?>
								<div class="col-md-2 col-lg-2" style="min-width:190px">
									<div class="am-item" style="max-height:205px!Important;">
										<?=$star;?> 
										<img onclick="javascript:del(<?=$pid;?>)" src="<?=$photo;?>" style="border:10px solid white;top:25px;left:0;right:0;margin:auto;margin-top:-60px;width:125px!Important;height:135px!Important;max-height:125px!Important;border-radius:125px;vertical-align:top" alt="feature">
										<div class="am-content" style="margin-top:-15px;font-size:0.8em">
											<div style="font-weight:bold;color:#000"><?=$service_tag;?><?=substr(strtoupper(strtoupper($name)),0,16);?></div>
											<div style="font-size:0.8em;margin-top:-10px"><? echo $provider_type;?></div>
											<div style="font-size:0.7em;margin-top:-10px"><? echo substr(strtoupper($user_category),0,20);?></div>
										</div>
									</div>
								</div>
							<?
								}
							?>
						</div>						
						<br><br><hr><br><h4>MY INVITATIONS</h4>
						<div id="fan_invites" class="row justify-content-center mb-30-none">
						</div>						
						<br><br><br><br><h4>MY CALENDAR</h4>
						<div id="my_calendar" class="row justify-content-center mb-30-none">
							<iframe src="https://linqstar.com/cal/calendar.php?calendar_id=<?=$members->private_cal_id;?>" style="border:none;width:100%;height:600px"></iframe>
						</div>						
						<br><br><br><br>
					<? } else { ?>
						<br>
							<div class="account-wrapper" style="max-width:100%;padding:25px;padding-top:0">
							<br><h4  style="border-bottom:10px solid #e0e0e0;max-width:600px;margin:auto;width:80%;margin-top:-20px;"><img src="assets/images/1ch7.png" style="height:120px"><br>NOTIFICATIONS AND REQUIRED ACTION</h4>
								<div id="" class="container">
								<br><br><div id="" class="row">
									<div class="col-md-12" style="min-width:190px">
										<h5 style="color:silver">ACTION REQUIRED</h5>
									</div>
								</div>
								<div id="" class="row">
									<div class="col-md-6" style="min-width:190px;text-align:right">
										New Orders
									</div>
									<div class="col-md-6" style="min-width:190px;text-align:left">
											2
									</div>
								</div>
								<div id="" class="row">
									<div class="col-md-6" style="min-width:190px;text-align:right">
										New Subscription Approval Requests
									</div>
									<div class="col-md-6" style="min-width:190px;text-align:left">
										0
									</div>
								</div>
								<div id="" class="row">
									<div class="col-md-6" style="min-width:190px;text-align:right">
										Channel Access Requests
									</div>
									<div class="col-md-6" style="min-width:190px;text-align:left">
										0
									</div>
								</div>
								<div id="" class="row">
									<div class="col-md-12" style="min-width:190px">
										<br><br><h5 style="color:silver">OTHER ACTIVITY and DATA</h5>
									</div>
								</div>								
								<div id="" class="row">
									<div class="col-md-6" style="min-width:190px;text-align:right">
										New Messages
									</div>
									<div class="col-md-6" style="min-width:190px;text-align:left">
										0
									</div>
								</div>
								<div id="" class="row">
									<div class="col-md-6" style="min-width:190px;text-align:right">
										New Comments
									</div>
									<div class="col-md-6" style="min-width:190px;text-align:left">
										0
									</div>
								</div>
								<div id="" class="row">
									<div class="col-md-6" style="min-width:190px;text-align:right">
										New Replies
									</div>
									<div class="col-md-6" style="min-width:190px;text-align:left">
										0	
									</div>
								</div>
								<div id="" class="row">
									<div class="col-md-6" style="min-width:190px;text-align:right">
										New Likes
									</div>
									<div class="col-md-6" style="min-width:190px;text-align:left">
										0	
									</div>
								</div>
 								<div id="" class="row">
									<div class="col-md-12" style="min-width:190px">
										<br><br><h5 style="color:silver">EARNINGS</h5>
									</div>
								</div>								
								<div id="" class="row">
									<div class="col-md-6" style="min-width:190px;text-align:right">
										Revenue, Apr 1st to date
									</div>
									<div class="col-md-6" style="min-width:190px;text-align:left">
											0
									</div>
								</div>                               
								<div id="" class="row">
									<div class="col-md-6" style="min-width:190px;text-align:right">
										Avg Monthly Revenue
									</div>
									<div class="col-md-6" style="min-width:190px;text-align:left">
											0
									</div>
								</div>
								<div id="" class="row">
									<div class="col-md-6" style="min-width:190px;text-align:right">
										Trend
									</div>
									<div class="col-md-6" style="min-width:190px;text-align:left">
										No Data. Get off your ass and do some work.
									</div>
								</div>                               
							</div>
						</div>
						</div>
						<br>						
						<br>
						<div class="account-wrapper" style="max-width:100%;padding:25px">
						<br><h4  style="border-bottom:10px solid #e0e0e0;max-width:640px;margin:auto"><img src="assets/images/1orders_open.png" style="height:120px"><br>ACTIVE ORDERS</h4>
							<div id="my_orders"  class="row justify-content-center mb-30-none">
							<?
								$ord=$c->query("select * from orders where provider_mid=$mid and order_status='OPEN'");
								for ($i=0; $i<count($ord); $i++) {
									$order_id=$ord[$i]['id'];
									$in=$ord[$i]['service_id'];
									$service_name=strToUpper($ord[$i]['service_name']);
									$sub_service_name=strToUpper($ord[$i]['sub_service_name']);
									$amt=$ord[$i]['amount'];
									$txn=$ord[$i]['txn_id'];
									$fan_mid=$ord[$i]['fan_mid'];
									$overdue_days=time()-strtotime($ord[$i]['create_at']);
									$overdue_days=round($overdue_days/(3600*24))-1;
									if ($overdue_days>0) {
										$bg="background:red";
										$od="<b>$overdue_days DAYS OVERDUE</b>";
										$clr="red";
									} else {
										$bg="";
										$overdue_days=abs($overdue_days);
										if ($overdue_days*1==0) {
											$od="<b>DUE TODAY!</b>";
										} else {
											$od="<b>DUE IN $overdue_days DAY(S)</b>";
										}
										$clr="black";
									}
									$due=date("Y-m-d",strtotime($ord[$i]['create_at'])*1+3600*24);
									$service_id=$ord[$i]['service_id'];
									$channel_id=$c->query("select channel_id from celeb_channels where initiated_by='fan' and channel_service_id=$service_id and provider_mid=$mid limit 1")[0]['channel_id'];
								//	$c->show("select channel_id from `voxeo`.`celeb_channels` where initiated_by='fan' and channel_service_id=$service_id and provider_mid=$mid limit 1")[0]['channel_id'];
								?>
								<div class="col-sm-4" style="padding:0;margin:5px;font-size:0.8em;max-width:200px">
									<div class="to-access-item" style="height:260px;margin-top:25px;">
										<div style="width:80%;text-align:center;margin-top:-60px;color:<?=$clr;?>;font-size:10px;position:absolute"><?=$od;?></div>											
										<div style="width:100%;margin-top:-20px">
											
											<input type="text" id="reschedule<?=$i;?>" placeholder="Set Delivery Date"><br>
											<span class="title"><b>ORDER ID</b></span>
											<span class="title"><?=$order_id;?></span>
											<div style="margin-top:-10px;color:maroon" class="title"><b><?=$service_name;?></b></div>
											<div style="margin-top:-10px;color:skyblue" class="title"><b><?=$sub_service_name;?></b></div>
											<div style="margin-top:-10px;color:#000" ><b>PAID</b> <span class="title">$<?=$amt;?></div>
											<div style="margin-top:-10px;margin-bottom:10px;color:#000"><b>DUE</b> <span class="title"><?=$due;?></div>
											<a href="setChannel.php?order_id=<?=$order_id;?>&fan_mid=<?=$fan_mid;?>&channel_id=<?=$channel_id;?>&hash=<?=$hash;?>" style="<?=$bg;?>" class="button-2">FILL ORDER</a>
										</div>										
									</div>
								</div>
								<div class="modal fade" id="detail_<?=$orders[$i]->id;?>" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
									<div class="modal-dialog modal-lg modal-dialog-scrollable">
										<div class="modal-content text-center">
											<div class="modal-body">
												ORDER DETAILS<br><br><br><br><br><br><br><br><br>
											</div>
											<button style="left:0;right:0;margin:auto;position:absolute;bottom:25px;width:140px;height:50px;" type="button" class="button-5" data-dismiss="modal" aria-label="Close">
											  <span aria-hidden="true">CLOSE</span>
											</button>
										</div> 
									</div> 
								</div>
							<script>
							  $( function() {
								$( "#reschedule<?=$i;?>" ).datepicker();
							  } );
							</script>
  
							<?
								}
							?>

							</div>
							</div>
							<br>
							<div class="account-wrapper" style="max-width:100%;padding:25px;padding-top:10px">
							<br><h4  style="border-bottom:10px solid #e0e0e0;max-width:600px;margin:auto;width:80%"><img src="assets/images/1orders_closed.png" style="height:120px"><br>CLOSED ORDERS</h4>
							<div id="my_orders"  class="row justify-content-center mb-30-none">
							<?
								$ord=$c->query("select * from orders where provider_mid=$mid and order_status='CLOSED'");
								for ($i=0; $i<count($ord); $i++) {
									$order_id=$ord[$i]['id'];
									$in=$ord[$i]['service_id'];
									$service_name=strToUpper($ord[$i]['service_name']);
									$sub_service_name=strToUpper($ord[$i]['sub_service_name']);
									$amt=$ord[$i]['amount'];
									$txn=$ord[$i]['txn_id'];
									$fan_mid=$ord[$i]['fan_mid'];
									$overdue_days=time()-strtotime($ord[$i]['create_at']);
									$overdue_days=round($overdue_days/(3600*24))-1;
									if ($overdue_days>0) {
										$bg="background:red";
										$od="<b>$overdue_days DAYS OVERDUE</b>";
										$clr="red";
									} else {
										$bg="";
										$overdue_days=abs($overdue_days);
										if ($overdue_days*1==0) {
											$od="<b>DUE TODAY!</b>";
										} else {
											$od="<b>DUE IN $overdue_days DAY(S)</b>";
										}
										$clr="black";
									}
									$due=date("Y-m-d",strtotime($ord[$i]['create_at'])*1+3600*24);
									$service_id=$ord[$i]['service_id'];
									$channel_id=$c->query("select channel_id from celeb_channels where initiated_by='fan' and service_id=$service_id and provider_mid=$mid limit 1")[0]['channel_id'];
								?>
								<div class="col-sm-4" style="padding:0;margin:5px;font-size:0.8em;max-width:200px">
									<div class="to-access-item" style="height:260px;margin-top:25px;">
										<div style="width:100%;margin-top:-20px">
											<a href="javascript:view_details('<?=$orders[$i]->id;?>')" style="color:black;padding:5px;padding-left:10px;padding-right:10px;margin-bottom:10px" class="button-3"><b>DETAILS</b></a><br>
											<span class="title"><b>ORDER ID</b></span>
											<span class="title"><?=$order_id;?></span>
											<div style="margin-top:-10px;color:maroon" class="title"><b><?=$service_name;?></b></div>
											<div style="margin-top:-10px;color:skyblue" class="title"><b><?=$sub_service_name;?></b></div>
											<div style="margin-top:-10px;color:#000" ><b>PAID</b> <span class="title">$<?=$amt;?></div>
											<div style="margin-top:-10px;margin-bottom:10px;color:#000"><b>DUE</b> <span class="title"><?=$due;?></div>
												ORDER CLOSED
										</div>										
									</div>
								</div>
									<div class="modal fade" id="detail_<?=$orders[$i]->id;?>" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
										<div class="modal-dialog modal-lg modal-dialog-scrollable">
											<div class="modal-content text-center">
												<div class="modal-body">
													ORDER DETAILS<br><br><br><br><br><br><br><br><br>
												</div>
												<button style="left:0;right:0;margin:auto;position:absolute;bottom:25px;width:140px;height:50px;" type="button" class="button-5" data-dismiss="modal" aria-label="Close">
												  <span aria-hidden="true">CLOSE</span>
												</button>
											</div> 
										</div> 
									</div>
								<?
									}
								?>

							</div>
							</div>

							<br>
							<div class="account-wrapper text-center" style="max-width:100%;padding:25px;padding-top:10px">
							<br><h4  style="border-bottom:10px solid #e0e0e0;max-width:600px;margin:auto;width:100%"><img src="assets/images/1orders_closed.png" style="height:120px"><br>PAST INVITES SENT</h4>
								<br><br><br><B><div class="row" style="width:90%;padding:0;font-size:0.8em;border-bottom:5px solid skyblue;margin:auto">
									<div class="col-sm-1">
										ORDERID
									</div>
									<div class="col-sm-4">
										SERVICE
									</div>
									<div class="col-sm-1">
										AMT
									</div>
									<div class="col-sm-1">
										START
									</div>
									<div class="col-sm-1">
										END
									</div>
									<div class="col-sm-1">
										COUNT
									</div>
									<div class="col-sm-2">
										ACTION
									</div>
								</div></B>
							<?
								$ord=$c->query("select * from orders where provider_mid=$mid and order_status='CLOSED'");
								for ($i=0; $i<count($ord); $i++) {
									$order_id=$ord[$i]['id'];
									$in=$ord[$i]['service_id'];
									$service_name=strToUpper($ord[$i]['service_name']);
									$sub_service_name=strToUpper($ord[$i]['sub_service_name']);
									$amt=$ord[$i]['amount'];
									$txn=$ord[$i]['txn_id'];
									$fan_mid=$ord[$i]['fan_mid'];
									$due=date("Y-m-d",strtotime($ord[$i]['create_at'])*1+3600*24);
									$inv=$c->query("select * from channel_invites where order_id=" . $ord[$i]['id']);								
									$date_start=$inv[0]['start_date'];
									$date_end=$inv[0]['end_date'];
									$times_viewed=$inv[0]['views'];

								?>
								<div class="row" style="width:90%;padding:0;font-size:0.8em;border-bottom:1px solid #f0f0f0;margin:auto;padding:5px">
									<div class="col-sm-1" style="margin-top:10px">
										<a href="javascript:view_details('<?=$ord[$i]['id'];?>')" style="color:black" class=""><b><?=$ord[$i]['id'];?></b></a>
									</div>
									<div class="col-sm-4" style="margin-top:10px">
										<?=$service_name;?>
									</div>
									<div class="col-sm-1" style="margin-top:10px">
										$<?=$amt;?>
									</div>
									<div class="col-sm-1" style="margin-top:10px">
										<?=substr(explode(" ",$date_start)[0],5,strlen(explode(" ",$date_start)[0]));?>
									</div>
									<div class="col-sm-1" style="margin-top:10px">
										<?=substr(explode(" ",$date_end)[0],5,strlen(explode(" ",$date_end)[0]));?>
									</div>
									<div class="col-sm-1" style="margin-top:10px">
										<?=$times_viewed;?>
									</div>
									<div class="col-sm-2" style="margin-top:0px">
										<button class="button-4" style="padding-left:3px;padding-right:3px">EXTEND EXPIRY</button>
									</div>
								</div>

									<div class="modal fade" id="detail_<?=$orders[$i]->id;?>" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
										<div class="modal-dialog modal-lg modal-dialog-scrollable">
											<div class="modal-content text-center">
												<div class="modal-body">
													ORDER DETAILS<br><br><br><br><br><br><br><br><br>
												</div>
												<button style="left:0;right:0;margin:auto;position:absolute;bottom:25px;width:140px;height:50px;" type="button" class="button-5" data-dismiss="modal" aria-label="Close">
												  <span aria-hidden="true">CLOSE</span>
												</button>
											</div> 
										</div> 
									</div>
								<?
									}}
								?>
								</div>
							<br>

							<br><br>							
							<div class="account-wrapper" style="max-width:100%;padding:25px">
							<br><h4  style="border-bottom:10px solid #e0e0e0;max-width:600px;margin:auto;width:80%"><img src="assets/images/ch11.png" style="height:70px"><br>MY SUBSCRIPTION CHANNELS</h4>
								<div id="fan_channels" class="row justify-content-center mb-30-none" style="padding:25px;font-size:0.8em">
									<?
											$ch=$c->query("select * from celeb_channels where  provider_mid=" . $mid . " and initiated_by='celeb'");
												echo "<div class='col-md-2' style='margin-top:5px;text-align:left;color:black;padding:2px'><b>TITLE</b></div>";
												echo "<div class='col-md-2 text-left'><b>CHANNEL TYPE</b></div>";
												echo "<div class='col-md-1 text-left'><b>ASSETS</b></div>";
												echo "<div class='col-md-1 text-left'><b>$ SALES</b></div>";
												echo "<div class='col-md-1 text-left'><b>ALERTS</b></div>";
												echo "<div style='margin-top:5px;' class='col-md-5 text-left'><b>SELECT ACTION</b></div>";
												echo "<div class='col-md-12' style='border-bottom:5px solid #c0c0c0'></div>";
											for ($i=0; $i<count($ch); $i++) {
												echo "<div class='col-md-2' style='margin-top:5px;text-align:left;color:black;padding:2px'><b>" . ($i+1) . ". </b> " . $ch[$i]['title'] . "</div>";
												echo "<div class='col-md-2 text-left'> " . $ch[$i]['channel_type'] . "</div><div class='col-md-1 text-left'> " . $ch[$i]['asset_count'] . "</div><div class='col-md-1 text-left'> " . $ch[$i]['sales'] . "</div><div class='col-md-1 text-left'> " . $ch[$i]['alerts'] . "</div><div style='margin-top:5px;' class='col-md-5 text-left'> <a href='javascript:view_assets(" . $ch[$i]['channel_id'] . ")'><img src='assets/trash_pink.png' style='height:30px;margin:5px'></a> <a href='javascript:spawn_new(" . $ch[$i]['channel_id'] . ")' class='button-2' style='padding:5px;background:thistle;color:black;;padding-left:10px;padding-right:10px'>View Assets</a> <a href='javascript:spawn_new(\"" . $ch[$i]['channel_type'] . "\"," . $ch[$i]['channel_id'] . ")' class='button-2' style='padding:5px;padding-left:10px;padding-right:10px;background:skyblue;color:black'></b>Add New <b>" . $ch[$i]['channel_type'] . "</b></a></div>";
											}
												echo "<div class='col-md-12' style='margin-top:5px;text-align:center;color:black;padding:10px'><button class='button-5' onclick='location.href=\"addChannel.php?hash=$hash\"'>Add New Revenue Maker Channel</button></div>";
									?>

								</div>
							</div>
							<br><br>							
							<div class="account-wrapper" style="max-width:100%;padding:25px;height:1000px">
							<br><h4  style="border-bottom:10px solid #e0e0e0;max-width:600px;margin:auto;width:80%"><img src="assets/images/ch100.png" style="height:70px"><br>MY CALENDAR</h4>
								<div id="" class="row justify-content-center mb-30-none" style="background:white">
										<div class="col-md-10" style="min-width:190px;background:white">
												<iframe src="cal/calendar.php?calendar_id=<?=$members->public_cal_id;?>" style="border:none;width:100%;height:774px;overflow:hidden;background:white"></iframe>
										</div>
								</div>
								<div id="my_history" class="row justify-content-center mb-30-none"></div>
							</div>
							<br>
							<div class="account-wrapper" style="max-width:100%;padding:25px">
							<br><h4  style="border-bottom:10px solid #e0e0e0;max-width:600px;margin:auto;width:80%"><img src="assets/images/ch6.png" style="height:90px"><br>MY SUBSCRIBERS</h4>
								<div id="fan_channels" class="row justify-content-center mb-30-none">
									<?
											$sub=$c->query("select * from celeb_subscribers where  provider_mid=" . $mid);
											if (count($sub)==0) { 
												echo "<br>No Subscribers Yet!<br><br>";
											}
											for ($i=0; $i<count($sub); $i++) {
									?>
										<div class="col-md-2 col-lg-2" style="min-width:190px">
											<div class="am-item" style="max-height:205px!Important;">
												<?=$star;?> 
												<img onclick="javascript:del(<?=$fan_mid;?>)" src="<?=$filename_1;?>" style="border:10px solid white;top:25px;left:0;right:0;margin:auto;margin-top:-60px;width:125px!Important;height:135px!Important;max-height:125px!Important;border-radius:125px;vertical-align:top" alt="feature">
												<div class="am-content" style="margin-top:-15px;font-size:0.8em">
													<div style="font-weight:bold;color:#000"><?=$login;?><?=substr(strtoupper(strtoupper($name)),0,16);?></div>
													<div style="font-size:0.8em;margin-top:-10px"><? echo $location;?></div>
													<div style="font-size:0.8em;margin-top:-10px"><? echo $age;?> <? echo $gender;?></div>
												</div>
											</div>
										</div>
										
									<?
										/*
											1. NOTIFICATIONS
											2. STATS AND REPORTS
											3. HOTLISTS
											4. SETTINGS
										*/	
										}
									?>
								</div>
							</div>
						<br>						
					</div>
				</div>
			</div>
		  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#reschedule" ).datepicker();
  } );
  </script>	
		<script>
		function view_receipt(id) {
			console.log(id)
			$('#'+id).modal('show')
		}
		function view_details(id) {
$.confirm({
    title: 'Prompt!',
    content: '' +
    '<form action="" class="formName">' +
    '<div class="form-group">' +
    '<label>Enter New Time</label>' +
    '<input type="text" id="tm" placeholder="New Time" class="name form-control" required />' +
    '</div>' +
    '</form>',
    buttons: {
        formSubmit: {
            text: 'Submit',
            btnClass: 'btn-blue',
            action: function () {
                var name = this.$content.find('.name').val();
                if(!name){
                    $.alert('provide a valid name');
                    return false;
                }
                $.alert('Your name is ' + name);
            }
        },
        cancel: function () {
            //close
        },
    },
    onContentReady: function () {
        // bind to events
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
    }
});		}
		</script>
	</section>
	<script>

	var x=[ ]
	var private_invite=0
	function spawn_new(type,cid) {
		var url='x_get_channel_service.php?channel_id='+cid
		console.log(url)
		$.ajax({
			url:url,
			success:function(data){
				x=JSON.parse(data)
				if (x) {
					$('#channel_service_desc').html(x.channel_service_desc)
					setCookie('channel_service_id',x.channel_service_id)
					setCookie('channel_service_name',x.channel_service_name)
					setCookie('channel_service_design',x.designPage)
					setCookie('channel_service_view',x.viewPage)
					setCookie('channel_service_control',x.controlPage)
					location.href='https://linqstar.com/'+x.designPage+'?channel_id='+cid+'&hash=<?=$hash;?>'
				}
			}
		})
	}
	
	
	</script>
    <script src="https://linqstar.com/assets/js/jquery-3.3.1.min.js"></script>
    <script src="https://linqstar.com/assets/js/modernizr-3.6.0.min.js"></script>
    <script src="https://linqstar.com/assets/js/plugins.js"></script>
    <script src="https://linqstar.com/assets/js/bootstrap.min.js"></script>
    <script src="https://linqstar.com/assets/js/magnific-popup.min.js"></script>
    <script src="https://linqstar.com/assets/js/jquery-ui.min.js"></script>
    <script src="https://linqstar.com/assets/js/wow.min.js"></script>
    <script src="https://linqstar.com/assets/js/waypoints.js"></script>
    <script src="https://linqstar.com/assets/js/nice-select.js"></script>
    <script src="https://linqstar.com/assets/js/owl.min.js"></script>
    <script src="https://linqstar.com/assets/js/counterup.min.js"></script>
    <script src="https://linqstar.com/assets/js/paroller.js"></script>
    <script src="https://linqstar.com/assets/js/countdown.js"></script>
    <script src="https://linqstar.com/assets/js/main.js"></script>

<script src="https://linqstar.com/js/utils.js"></script>
<script src="https://linqstar.com/js/wait.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/4.0.1/socket.io.js"></script>
<script src="https://linqstar.com/js/jsSocketNode.js"></script>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#reschedule" ).datepicker();
  } );
  </script>
</body>
</html>