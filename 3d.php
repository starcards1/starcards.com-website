<?
include "../class/utils.class.php";
$c=new utils;
$c->connect("199.91.65.83","voxeo");
parse_str(http_build_query($_GET));
$q=$c->query("select channel_asset_ids from celeb_channels where channel_is_default='1' and provider_mid=$mid");
$assets=$q[0]['channel_asset_ids'];
$pics=$c->query("select * from celeb_media where media_id in ($assets)");
?>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="flip/deploy/css/flipbook.style.css">
<link rel="stylesheet" type="text/css" href="flip/deploy/css/font-awesome.css">
<script src="flip/deploy/js/flipbook.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#container").flipBook({
            pages:[
			<? for ($i=0; $i<count($pics); $i++) { ?>
                {src:"uploads/<?=$pics[$i]['media_filename'];?>", thumb:"thumbs/<?=$pics[$i]['media_filename'];?>", title:"<?=$pics[$i]['media_title'];?>",htmlContent:'<div> Sample HTML Content - description of the image </div>'},
			<? } ?>
            ],
			singlePageMode:false,
			backgroundColor:'#fff',
			layout:1,
			menu2Transparent:true,
			menu:false,
			icons:'material'
        });
    })
</script>
</head>
	<body>
		<div id="container" style="position:absolute;width:95%;height:600px"/>
	</body>
</html>
