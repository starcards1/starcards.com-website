<?php
	include "x_auth.php";
	require_once "../class/utils.class.php";
	$c=new utils;
	$c->connect("199.91.65.83","voxeo");
	parse_str(http_build_query($_GET));
	$pro=$c->query("select * from celeb_profiles where mid=$provider_mid");
	foreach($pro[0] as $key => $value) {
		${$key}=$value;
	}
	$ta=$c->query("select * from celeb_tags where pid=$pid");
	$t=explode(",",$ta[0]['tags']);
?>
<script src="assets/js/jquery.js"type="text/javascript"></script>
<script src="assets/js/jquery-ui.min.js" type="text/javascript" charset="utf-8"></script>	
<script src="js/tag-it.js" type="text/javascript" charset="utf-8"></script>	
<script src="assets/js/bootstrap.min.js"type="text/javascript"></script>
<script src="assets/js/utils.js"type="text/javascript"></script>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Voxeo</title>
	<link href="doc/css/bootstrap.min.css" rel="stylesheet">
	<link href="doc/css/style.css" rel="stylesheet">
	<link href="doc/css/menu.css" rel="stylesheet">
	<link href="doc/css/vendors.css" rel="stylesheet">
	<link href="doc/css/icon_fonts/css/all_icons_min.css" rel="stylesheet">
    
	<!-- YOUR CUSTOM CSS -->
	<link href="doc/css/custom.css" rel="stylesheet">
	
    <link rel="stylesheet" href="assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="assets/css/animate.css"/>
    <link rel="stylesheet" href="assets/css/jquery-ui.min.css"/>
    <link rel="stylesheet" href="assets/css/main.css"/>
	<link href="style.css" rel="stylesheet" type="text/css" />
    <link href="css/jquery.tagit.css" rel="stylesheet" type="text/css">
	<style>
		div {font-size:12px;}
	</style>
    <link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon"/>
</head>
<body  style="background:url(assets/images/account-bg.jpg)no-repeat;background-size:cover">
	    <img style="width:200px;margin:auto;left:0;right:0;position:absolute;top:10px" src="assets/images/logo2.png" alt="logo"/>
		<div class="text-center" style="margin-top:200px">
            <div id="cf" class="account-wrapper" style="margin:auto;padding:25px">
                <div class="account-body">
					<div class="row">
						<div class="col-md-12 text-center">
							<h3 style="color:maroon">NEW BROADCAST</h3>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 text-left">
							<div><b>BROADCAST NAME</b></div><input id="broadcast_name" type="text" style="background:none;height:40px;border-radius:6px;border:1px solid silver">
						</div>
						<div class="col-md-6 text-left">
							<div class="checkbox">
								<div><b>EVENT ACCESS</b></div><input type="checkbox" class="css-checkbox" id="visit1" name="visit1" onclick="swx()">
								<label for="visit1" class="css-label"><span id="l1">PRIVATE, INVITE ONLY</span></label>
							</div>							
						</div>
					</div>
					<br>
					<div class="row text-left">
						<div class="col-md-3">
							<div><b>COST</b></div><input class="form-control" type="text" id="bcost" style="font-size:0.8em">
						</div>
						<div class="col-md-3">
							<div><b>DATE</b></div><input class="form-control td-input" type="text" id="booking_date" style="font-size:0.8em">
						</div>
						<div class="col-md-3">
							<div><b>START TIME</b></div><input class="form-control td-input" type="text" id="start_time" style="font-size:0.8em">
						</div>
						<div class="col-md-3">
							<div><b>END TIME</b></div><input class="form-control td-input" type="text" id="end_time" style="font-size:0.8em">
						</div>
					</div>
					<br>
					<div class="row text-left">
						<div class="col-md-6">
							<div><b>RECURS ONCE PER</b></div>
							Month<!--<select id="period"><option value="Once">Single Event, No Repeat</option><option value="1">Daily</option><option value="7">Weekly</option><option value="31">Monthly</option><option value="365">Yearly</option></select>-->
						</div>
						<div class="col-md-3">
							<div><b>x REPEAT</b></div><input class="form-control" type="text" id="repeat" style="font-size:0.8em;width:75px">
						</div>
						<div class="col-md-3">
							<div class="checkbox">
								<div><b>MODEL</b></div><input type="checkbox" class="css-checkbox" id="ppv" name="ppv" onclick="pwx()">
								<label for="ppv" class="css-label"><span id="p1">PPV</span></label>
							</div>							
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-12 text-left">
							<div><b>BROADCAST DESCRIPTION</b></div><textarea id="desc" style="height:100px;border-radius:6px;border:1px solid silver"></textarea>
						</div>
					</div>
					<div style="margin-top:15px" class="row">
						<div class="col-md-12 text-left">
							<div><b>ADD BROADCAST TAGS</b></div><input id="channeltags" style="border-radius:4px;height:35px!Important;padding:0!Important;overflow:hidden!Important;margin-left:0"/>
						</div>
						<br>
						<div class="col-md-6 text-center">
							<a class="button-5" href="javascript:add_broadcast()"> GO LIVE NOW! </a>
						</div>
						<div class="col-md-6 text-center">
							<a class="button-5" href="javascript:schedule_broadcast()" style="background:skyblue;box-shadow:0 0 10px aliceblue;color:#000"> SCHEDULE IT </a>
						</div>
					</div>
				</div>
			</div>
			<br>
		</div>
		
	<script src="assets/js/wait.js"></script>
	<script src="assets/js/common.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="./assets/js/modernizr-3.6.0.min.js"></script>
    <script src="./assets/js/plugins.js"></script>
    <script src="./assets/js/bootstrap.min.js"></script>
    <script src="./assets/js/jquery-ui.min.js"></script>
    <script src="./assets/js/main.js"></script>
    <script src="assets/js/common.js"></script>
    <script src="js/aes.js"></script>
    <script src="js/tag-it.js" type="text/javascript" charset="utf-8"></script>	
    <script src="js/location.js" type="text/javascript" charset="utf-8"></script>	
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDAuvU_A1iMThoe1i6Joi26nSTDQkjKlDA&libraries=places"></script>

	<script type="text/javascript">
	var is_default=0
	function swx() {
		private_invite=(private_invite==0)?1:0
	}
	var is_ppv=0
	function pwx() {
		is_ppv=(is_ppv==0)?1:0
	}
	
	
	function toggle_default(img) {
		if (img.src.indexOf('chk_on')>=0) {
			img.src='chk_off.png'
			is_default=0
		} else {
			img.src='chk_on.png'
			is_default=1
		}
	}
	var x=[ ]
	var private_invite=0
	function chk_defs(){
		var url='x_get_channel_service.php?channel_id='+qs('channel_id')
		console.log(url)
		$.ajax({
			url:url,
			success:function(data){
				x=JSON.parse(data)
				if (x) {
					$('#channel_service_desc').html(x.channel_service_desc)
					setCookie('channel_service_id',x.channel_service_id)
					setCookie('channel_service_name',x.channel_service_name)
					setCookie('channel_service_design',x.designPage)
					setCookie('channel_service_view',x.viewPage)
					setCookie('channel_service_control',x.controlPage)
				}
			}
		})
	}
	
	setTimeout(function(){
		chk_defs()
	},10)
	
	var content,provider_mid
	
	function add_broadcast() {
		var channel_service_name=getCookie('channel_service_name')
		var bcost=$$('bcost').value
		var tg=''
		for (var i=0; i<tgs.length; i++) {
			tg+=tgs[i]+','
		}
		if (!provider_mid) provider_mid=getCookie('mid')
		tg=tg.substr(0,tg.length-1);
		var period=$$('period').value
		var qty_period=$$('qty_period').value
		
		var url='https://linqstar.com/x_add_broadcast.php?broadcast_name='+$$('broadcast_name').value+'&desc='+$$('desc').value+'&provider_mid='+getCookie('mid')+'&tags='+tg+'&channel_service_name='+channel_service_name+'&bcost='+bcost+'&designPage='+getCookie('channel_service_design')+'&viewPage='+getCookie('channel_service_view')+'&controlPage='+getCookie('channel_service_control')+'&initiated_by=celeb&channel_service_id='+getCookie('channel_service_id')+'&start_date='+$$('booking_date').value+'&start_time='+$$('start_time').value+'&end_time='+$$('end_time').value+'&channel_id='+qs('channel_id')+'&private_invite='+private_invite+'&period='+period+'&qty_period='+qty_period+'&ppv='+is_ppv
		console.log(url)
		$.ajax({
				url:url,
			success:
				function(msg){
					if (isNaN(msg)) {
							content='Are you ready to broadcast? You are all set! Click below to go to the broadcasting section'
							$.alert({
								title: 'Success',
								content: content,
								buttons: {
									b1: {
										text: 'Start My Broadcast!',
										btnClass: 'btn-white',
										action: function(){
											location.href=msg+'&hash=<?=$hash;?>'
										}
									},
									b2: {
										text: 'Home',
										btnClass: 'btn-white',
										action: function(){
											location.href='home.php?hash=<?=$hash;?>'
										}
									}
								}
							});
						}
					}
				})
			}
	var sites
	var imagesUploaded
	var r,k,c,t2,t3,t4,intr=50,currImg
	var rat=1
	var rx=0, t, ext
	var set=false
	var usr_img=[]
	var collection_set=0
	var channel_set=0
	var channel_id
	function getCookie(cname)	{
		var name = cname + "=";
		var ca = document.cookie.split(';');
		for(var i=0; i<ca.length; i++) {
		  var c = ca[i].trim();
		  if (c.indexOf(name)==0) return c.substring(name.length,c.length);
		}
		return "";
	}

	function setCookie(cname,cvalue,exdays)	{
		var d = new Date();
		d.setTime(d.getTime()+(exdays*24*60*60*1000));
		var expires = "expires="+d.toGMTString();
		document.cookie = cname + "=" + cvalue + "; " + expires;
	 }	
	
	function print_blocks(ppx) {
		if (ppx=='undefined') return false
		ppx=ppx.toString()
		var f
		var px=''
		var x=''
		if (ppx*1<10) x='00'+ppx
		if ((ppx*1>9) && (ppx*1<100)) x='0'+ppx
		if (ppx*1==100) x=ppx
			
		if (!wd) var wd='20px'
		for (f=0;f<=x.length-1;f++) {
			var dg=x.substring(f,f+1)
			if (dg !== '.')
				px=px+'<span><img src="https://gaysugardaddyfinder.com/assets/img/' + dg + '.png" style="width:'+wd+'"></span>'
		}
		return '<span style="padding-top:5px">'+px+'</span>'
	}
	
	var done=false
		function win() {
			if (done===true) return false
			$.alert({
				title: 'Success',
				content: 'Image(s) uploaded, saved and cataloged. Whats next?',
				btnClose:true,
				buttons: {
					b1: {
						text: 'Home',
						btnClass: 'btn-white',
						action: function(){
							location.href='home.html'
						}
					},
					b2: {
						text: 'My Uploads',
						btnClass: 'btn-white',
						action: function(){
							location.href='show_uploads.html'
						}
					},
					b3: {
						text: 'My Account',
						btnClass: 'btn-red',
						action: function(){
							location.href='settings.html'
						}
					}
				}
			});
			done=true
		}
		function fail(error) {
			alert("An error has occurred: "+error)
		}	
		var currPage='upload_photo.html'
		function fs(el) {
		if (!el) el = document.documentElement
		, rfs = // for newer Webkit and Firefox
			   el.requestFullScreen
			|| el.webkitRequestFullScreen
			|| el.mozRequestFullScreen
			|| el.msRequestFullScreen
		;
		if(typeof rfs!="undefined" && rfs){
		  rfs.call(el);
		} else if(typeof window.ActiveXObject!="undefined"){
		  // for Internet Explorer
		  var wscript = new ActiveXObject("WScript.Shell");
		  if (wscript!=null) {
			 wscript.SendKeys("{F11}");
		  }
		}
		}
	//setTimeout('fs()',1000)
	function guid() {
	  function s4() {
		return Math.floor((1 + Math.random()) * 0x10000)
		  .toString(16)
		  .substring(1);
	  }
	  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
		s4() + '-' + s4() + s4() + s4();
	}
	var sa=[]
	var ra=[]
	function saveSite(s) {
		sites=$('.select-1').val().toString()
		console.log(sites)
	}
	
			function generateRandomString(j){
				if (!j) j=16
				var op=''
				var arr=new Array('a','b','c','d','e','f','i','j','k','m','n','p','q','r','s','t','u','v','w','x','y','z',0,1,2,3,4,5,6,7,8,9,0,0,1,2,3,4,5,6,7,8,9,0)
				for (k=0;k<j;k++) {
					var inx=randomIntFromInterval(0,arr.length-1)
					op += arr[inx]
				}
				return op;
			}
			function guid() {
			  function s4() {
				return Math.floor((1 + Math.random()) * 0x10000)
				  .toString(16)
				  .substring(1);
			  }
			  return s4() + '-' + s4();
			}
			function wait(txt) {
				if (!txt) var txt='Please wait...'
				var tx=document.createElement('div')
				var md=document.createElement('div')
				var im=document.createElement("img")
				tx.id='tx'
				md.id='md'
				im.id='im'
				im.src="https://gaysugardaddyfinder.com/assets/images/load.gif"
				im.style.cssText='width:40px;position:fixed;left:0;right:0;top:0;bottom:0;margin:auto;z-index:999999999999999999999'
				md.style.cssText='width:100%;height:100%;position:fixed;left:0;right:0;top:0;bottom:0;margin:auto;z-index:999999999999999999999;background:#000;opacity:0.3'
				tx.style.cssText='color:#fff;width:300px;height:50px;text-align:center;position:fixed;left:0;right:0;top:0;bottom:0;margin:auto;z-index:99999999999999999999999;padding-top:100px'
				tx.textContent=txt
				document.documentElement.appendChild(md)
				document.documentElement.appendChild(im)
				document.documentElement.appendChild(tx)
			}
			function wait_alt(txt) {
				if (!txt) var txt='Please wait...'
				var tx=document.createElement('div')
				var md=document.createElement('div')
				var im=document.createElement("img")
				tx.id='tx'
				md.id='md'
				im.id='im'
				im.src="https://gaysugardaddyfinder.com/assets/images/load.gif"
				im.style.cssText='width:40px;position:fixed;left:0;right:0;top:0;bottom:0;margin:auto;z-index:99'
				md.style.cssText='width:100%;height:100%;position:fixed;left:0;right:0;top:0;bottom:0;margin:auto;z-index:99;background:#000;opacity:0.3'
				tx.style.cssText='color:#fff;width:300px;height:50px;text-align:center;position:fixed;left:0;right:0;top:0;bottom:0;margin:auto;z-index:99;padding-top:100px'
				tx.textContent=txt
				document.documentElement.appendChild(md)
				document.documentElement.appendChild(tx)
			}
	
			function hide() {
				$$('im').style.display='none'
				$$('tx').style.display='none'
				$$('md').style.display='none'
			}		
		function $$(obj) {
			return document.getElementById(obj)
		}		
	
	function delCookie(cname) {
		var d = new Date();
		d.setTime(d.getTime());
		var expires = "expires="+d.toGMTString();
		document.cookie = cname + "=" + "" + "; " + expires;
	 } 
	function setCookie(cname,cvalue,exdays)	{
		var d = new Date();
		d.setTime(d.getTime()+(exdays*24*60*60*1000));
		var expires = "expires="+d.toGMTString();
		document.cookie = cname + "=" + cvalue + "; " + expires;
	 }
	function c_us() {
		window.top.contactUs()
	}
	function $$(objID) {
		return document.getElementById(objID)
	}
	function getCookie(cname)	{ 
		if (window.location.href.indexOf('file://')>=0) {
			var cvalue = window.localStorage.getItem(cname);
			return cvalue
		} else {
			var name = cname + "="; 
			var ca = document.cookie.split(';'); 
			for(var i=0; i<ca.length; i++) { 
			  var c = ca[i].trim(); 
			  if (c.indexOf(name)==0) return c.substring(name.length,c.length); 
			} 
			return ""; 
		}
	} 
	var tgs=[ ]
	setTimeout(function(){
		$("#channeltags").tagit({
				singleField: true,
				allowSpaces: true,
				tagLimit:10,
			afterTagAdded: function(event, ui) {
				tgs.push(ui.tag[0].innerText)
			}
		})
	},1000)
</script>
<script src="doc/js/common_scripts.min.js"></script>
<script src="doc/js/functions.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script> 
</body>
</html>