<?php
	require_once "../class/utils.class.php";
	$c=new utils;
	$c->connect("199.91.65.83","voxeo");
	parse_str(http_build_query($_GET));
	$pro=$c->query("select * from celeb_profiles where mid=$provider_mid");
	foreach($pro[0] as $key => $value) {
		${$key}=$value;
	}
	$ta=$c->query("select * from celeb_tags where pid=$pid");
	$t=explode(",",$ta[0]['tags']);
?>

<!DOCTYPE html>
<!-- saved from url=(0041)file:///D:/00%20Free%20Videos/Anfra2.html -->
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>LinqStar Video Greeting</title>
		<link rel="stylesheet" href="greet_files/bootstrap.min.css">
		<link rel="stylesheet" href="greet_files/line-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="greet_files/main.css">
		<link rel="stylesheet" type="text/css" href="greet_files/covid.css">
		<link href="./greet_files/css2" rel="stylesheet">
	</head>
	<body>
		<div class="ugf-covid covid-bg">
		  <div class="container">
			<div class="row">
			  <div class="col-lg-6 offset-lg-3 col-md-8 offset-md-2">
				<div class="covid-wrap">
					<div style="display:block!Important;margin-top:-50px" class="covid-test-wrap test-step active">
					  <div class="test-progress">
						<a href="greet_start.php"><img src="assets/back1.png" style="width:40px;position:absolute;left:10px;margin-top:15px"></a>
						<div class="test-progress-step">
						  <span class="step-number">2/7</span>
						  <svg>
							<circle class="step-7" cx="30" cy="30" r="28" stroke-width="4" fill="none" role="slider" aria-orientation="vertical" aria-valuemin="0" aria-valuemax="100" aria-valuenow="50"></circle>
						  </svg>
						</div>
					  </div>
					  <h3>Recipient Info</h3>
					  <p>Who are we sending this card to?</p>
					  <div class="step-block">
						<div class="row">
						  <div class="col-sm-6">
							<div class="form-group">
							  <input id="recipient_name" type="text" class="form-control" placeholder="Recipient Name" required="">
							</div>
						  </div>
						  <div class="col-sm-6">
							<div class="form-group">
							  <input id="recipient_mobile" onblur="mobf()" type="text" class="form-control" placeholder="Recipient Phone Number" required=""> <!-- error class: has-error -->
							</div>
						  </div>
						  <div class="col-sm-6">
							<div class="form-group">
							  <input id="recipient_email" type="email" class="form-control" placeholder="Recipient Email" required="">
							</div>
						  </div>
						</div>
						<button onclick="nextTo()" class="button">Next</button>
					  </div>
					</div>
				</div>
			  </div>
			</div>
	  
		  </div>
		</div>

		<script src="jquery.js"></script>
		<script src="js/mask.js"></script>
		<script src="js/utils.js"></script>
		<script>
		
			var occasion, prev, card_type
			var go=true
			function nextTo() {
				if (!$$('recipient_name').value) {
					go=false
					$.alert({
						title: 'Error',
						content:'Recipients Name Cannot be blank',
						type: 'red',
						buttons:
						{
							ok: function(){
								go=true
							}
						}
					})
					return false
				}
				if (isValidMobile($$('recipient_mobile').value)===false) {
					go=false
					$.alert({
						title:'Error',
						type: 'red',
						content:'Recipients Mobile must be 10 digits, all numbers plus an optional [+CountryCode]',
						buttons:
						{
							ok: function(){
								go=true
							}
						}
					})
					return false
				}
				if (go===false) {
					return false
				} else {
					window.localStorage.setItem('recipient_name',$$('recipient_name').value)
					window.localStorage.setItem('recipient_mobile',isValidMobile($$('recipient_mobile').value))
					window.localStorage.setItem('recipient_email',$$('recipient_email').value)
					console.log(window.localStorage.getItem('recipient_mobile'))
					location.href='greet_from.php'
				}
				
			}
		
			setTimeout(function(){
				$$('recipient_mobile').value=''
				$$('recipient_name').value=''
				$$('recipient_email').value=''
			},500)

			function mobf() {
				$('#recipient_mobile').mask("+1 (999) 999-9999");
			}

		</script>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>  
		<script src="./greet_files/custom.js.download"></script>
	</body>
</html>