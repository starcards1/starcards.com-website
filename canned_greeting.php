<?php
	require_once "../class/utils.class.php";
	$c=new utils;
	$c->connect("199.91.65.83","voxeo");
	parse_str(http_build_query($_GET));
	$pro=$c->query("select * from celeb_profiles where mid=$provider_mid");
	foreach($pro[0] as $key => $value) {
		${$key}=$value;
	}
	$ta=$c->query("select * from celeb_tags where pid=$pid");
	$t=explode(",",$ta[0]['tags']);
?>
<script src="assets/js/jquery.js"type="text/javascript"></script>
<script src="assets/js/jquery-ui.min.js" type="text/javascript" charset="utf-8"></script>	
<script src="js/tag-it.js" type="text/javascript" charset="utf-8"></script>	
<script src="assets/js/bootstrap.min.js"type="text/javascript"></script>
<script src="assets/js/utils.js"type="text/javascript"></script>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>LinqStar</title>
	<link href="doc/css/bootstrap.min.css" rel="stylesheet">
	<link href="doc/css/style.css" rel="stylesheet">
	<link href="doc/css/menu.css" rel="stylesheet">
	<link href="doc/css/vendors.css" rel="stylesheet">
	<link href="doc/css/icon_fonts/css/all_icons_min.css" rel="stylesheet">
    
	<!-- YOUR CUSTOM CSS -->
	<link href="doc/css/custom.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="assets/css/animate.css"/>
    <link rel="stylesheet" href="assets/css/jquery-ui.min.css"/>
    <link rel="stylesheet" href="assets/css/main.css"/>
    <link rel="stylesheet" href="css/shadows.css"/>
    <link rel="stylesheet" href="dz/dropzone.css"/>
	<link href="style.css" rel="stylesheet" type="text/css" />
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

	<link href='https://fonts.googleapis.com/css?family=Open+Sans:300|Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>	<style>
		* {
		font-size:14px;
		border-radius:0;}
		div input span {font-size:14px;}
		
		.card_cover{
			background:aliceblue;
			border:0px solid white;
			height:175px;
			margin:10px;
			padding:0
		}
		img {
			draggable:true;
		}
		audio{
			display:none!Important;
			position:absolute
		}
		
	</style>
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Poiret+One&display=swap" rel="stylesheet">
<style>
	h1{
		font-family:Poiret One!Important;
		font-size:24px
	}
</style>
	
<script src="//cdn.ckeditor.com/4.10.1/basic/ckeditor.js"></script>
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Allura&family=Gloria+Hallelujah&family=Homemade+Apple&display=swap" rel="stylesheet">	
    <link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon"/>
</head>

<body  style="background:#ccc">
	<div class="text-center" style="padding-top:40px">
		<div class="account-wrapper" style="margin:auto;padding:0px;max-width:1000px;background:#f0f0f0;border-radius:0!Important">
			<div class="row">		
				<img src="alt/images/banner.jpg">
				<div style="width:100%" class="form-group">				
					<div class="col-md-12" style="text-align:center;font-weight:300;font-family:Open Sans">					
						<BR><BR>
						<h1>SELECT GREETING MESSAGE</h1>					

					</div>
				</div>
			</div>
			<img src="alt/bar.png" style="width:100%">			
			<br><br><div class="row">		
				<div style="width:90%;margin:auto" class="form-group">				
					<button class="col-md-12" onclick="preview()" style="display:none;disabled:true;height:30px;background:#fff;border:none;color:silver;height:40px;;margin-top:25px">PREVIEW CARD</button>								
					<button class="col-md-6" onclick="another()" style="height:30px;background:aliceblue;border:none">Change Default Message</button><button class="col-md-6" onclick="stylize()" style="color:white;height:30px;background:gainsboro;color:black;border:none">Customize Message Style</button>
					<div contentEditable id="apex" class="col-md-12" style="text-align:center;font-weight:300;font-family:Open Sans;background:white;padding:50px;left:0;right:0;margin:auto;width:100%">					
					
					</div>
					<button onclick="next()" class="col-md-12" style="height:50px;background:#a3d900">Save and Proceed</button><br><br>
				</div>
			</div>
			<img src="alt/bar.png" style="width:100%">			
		</div>
	</div>
	<script>
async function selectText(el){
	var sel, range;
	if (window.getSelection && document.createRange) { //Browser compatibility
	  sel = await window.getSelection();
	  if(sel.toString() == ''){ //no text selection
			range = document.createRange(); //range object
			range.selectNodeContents(el); //sets Range
			sel.removeAllRanges(); //remove all ranges from selection
			sel.addRange(range);//add Range to a Selection.
	  }
	}else if (document.selection) { //older ie
		sel = document.selection.createRange();
		if(sel.text == ''){ //no text selection
			range = document.body.createTextRange();//Creates TextRange object
			range.moveToElementText(el);//sets Range
			range.select(); //make selection.
		}
	}
}	
	var ck
        function stylize() {
			CKEDITOR.replace( 'apex' );
		}
		function cl() {
			window.parent.frames['ccv'].style.display='none'
		}
		var oid=window.localStorage.getItem('occasion_id')
		var row=0
		var msg
		var msg_count=0
		var ck
		function next() {
			if (CKEDITOR.instances['apex']) {
				ck=CKEDITOR.instances['apex'].getData();
			} else {
				ck=$$('apex').innerHTML
			}
			window.localStorage.setItem('canned',ck)
			window.localStorage.setItem('step2',true)
			location.href='custom_greeting.php'
		}
		var step1=window.localStorage.getItem('step1')
		var step2=window.localStorage.getItem('step2')
		var step3=window.localStorage.getItem('step3')
		var step4=window.localStorage.getItem('step4')
		var step5=window.localStorage.getItem('step5')
		
		function previewShow() {
			if (step1 && step2 && step3 && step4 && step5) {
				$$('previewBtn').style.display=''
			} else {
				$$('previewBtn').style.display='none'
			}
		}
		
		//previewShow()
		
		function another() {
			if (row<msg_count) {
				row++
			} else {
				row=0
			}
			get_content()
		}
		
		function get_content() {
			if (oid!=='custom') {
				$.ajax({url:'x_get_greeting_content.php?occasionID='+oid+'&record='+row,success:function(data){
					var ms=JSON.parse(data)
					msg=ms.split('|')[0]
					msg_count=ms.split('|')[1]*1
					$$('apex').innerHTML=msg
				}})
			}
		}		
		get_content()
		setTimeout(function(){
			selectText($$('apex'))
		},1000)
	</script>
</body>
</html>