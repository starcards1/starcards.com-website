<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Voxeo Login</title>

    <link rel="stylesheet" href="./assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="./assets/css/all.min.css">
    <link rel="stylesheet" href="./assets/css/animate.css">
    <link rel="stylesheet" href="./assets/css/nice-select.css">
    <link rel="stylesheet" href="./assets/css/owl.min.css">
    <link rel="stylesheet" href="./assets/css/jquery-ui.min.css">
    <link rel="stylesheet" href="./assets/css/magnific-popup.css">
    <link rel="stylesheet" href="./assets/css/flaticon.css">
    <link rel="stylesheet" href="./assets/css/main.css">
    <link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon">
</head>
<body>
    <div class="preloader">
        <div class="preloader-inner">
            <div class="preloader-icon">
                <span></span>
                <span></span>
            </div>
        </div>
    </div>
    <div class="account-section bg_img" data-background="./assets/images/account-bg.jpg">
        <div class="container">
            <div class="account-title text-center">
                <a href="#0" class="logo">
                    <img src="./assets/images/logo2.png" style="width:200px" alt="logo">
                </a>
            </div>
            <div class="account-wrapper">
                <div class="account-body">
                    <form class="account-form">
                        <div class="form-group">
                            <label for="sign-up">Your Email </label>
                            <input type="text" placeholder="Enter Your Email " id="email">
                        </div>
                        <div class="form-group">
                            <label for="pass">Password</label>
                            <input type="text" placeholder="Enter Your Password" id="pswd">
                            <span class="sign-in-recovery">Forgot your password? <a href="reset-password.html">recover password</a></span>
                        </div>
                        <div class="form-group text-center">
                           <a class="button-5" href="javascript:login1()">Login</a>
                        </div>
                    </form>
                </div>
                <div class="account-header pb-0">
                    <span class="d-block mt-15">Don't have an account? <a href="join.php">Sign Up Here</a></span>
                </div>
            </div>
        </div>
    </div>
    <!--============= Sign In Section Ends Here =============-->

    <script src="./assets/js/jquery-3.3.1.min.js"></script>
    <script src="./assets/js/modernizr-3.6.0.min.js"></script>
    <script src="./assets/js/plugins.js"></script>
    <script src="./assets/js/bootstrap.min.js"></script>
    <script src="./assets/js/magnific-popup.min.js"></script>
    <script src="./assets/js/jquery-ui.min.js"></script>
    <script src="./assets/js/wow.min.js"></script>
    <script src="./assets/js/waypoints.js"></script>
    <script src="./assets/js/nice-select.js"></script>
    <script src="./assets/js/owl.min.js"></script>
    <script src="./assets/js/counterup.min.js"></script>
    <script src="./assets/js/paroller.js"></script>
    <script src="./assets/js/countdown.js"></script>
    <script src="./assets/js/common.js"></script>
    <script src="./assets/js/main.js"></script>
    <script src="./assets/js/wait.js"></script>
    <script src="js/utils.js"></script>
    <script src="js/aes.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
</body>
<script>
			var o,enc,name,ms,fn
			function login1() {
				var m = $("#email").val(),
				p = $("#pswd").val();
				if (!m || !p) {
					jconfirm({
						title: 'Error!',
						type: 'red',
						theme: 'modern',
						content: 'Invalid Username/Password',
						buttons: {
							Retry: function () {
								loginForm2()
							}
						}
					});
				} else {
					wait('Authenticating, please wait...')
					$.ajax({
						url: 'x_login.php?user_input=' + m + '&pswd=' + p,
						cache: !1, 
						success: function(em) {
							console.log(em)
							ms = JSON.parse(em);
							setCookie("session", encrypt(em,ms.hash))
							fn=ms.filename_1.split('/')[ms.filename_1.split('/').length-1]
							if (ms=='error') {
								hide()
								$('.wait_cnt').hide()
								//$$('modalLogin').style.display='none'
								//$('.user_login').hide()
								
								jconfirm({
									title: 'Error!',
									type: 'red',
									theme: 'modern',
									content: 'Invalid Username/Password',
									buttons: {
										Retry: function () {
											//loginForm()
										}
									}
								});
							} else {
								$('.wait_cnt').hide()
								setCookie("lat", ms.lat)
								setCookie("lng", ms.lng)
								setCookie("location", ms.city+' '+ms.state+' '+ms.zip)
								setCookie('name',ms.name)
								setCookie('mid',ms.mid)
								setCookie('mobile',ms.mobile)
								setCookie('fos',ms.os)
								setCookie('saveUser',$$('email').value)
								setCookie('savePass',$$('pswd').value)
								setCookie('hash',ms.hash)
								location.href=qs('referer')+'?hash='+ms.hash
							}
						}
					})
				}
			}
</script>
</html>