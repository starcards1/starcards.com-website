<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta name="format-detection" content="telephone=no">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width">


    <title></title>

    <meta name="description" content="One-to-One Video Chat using RTCMultiConnection. Only one user can join a room." />
    <meta name="keywords" content="WebRTC,RTCMultiConnection,Demos,Experiments,Samples,Examples" />
	<link href="https://sugardaddylocal.com/assets/css/stream.css" rel="stylesheet" type="text/css" />
	<link href="css/stream.css" rel="stylesheet" type="text/css" />
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:300' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:300' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.2.0/jquery-confirm.min.css">
	<script src="js/jquery.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.2.0/jquery-confirm.min.js"></script>
			<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
			<link rel="stylesheet" href="https://rawgit.com/enyo/dropzone/master/dist/dropzone.css">
			<link rel="stylesheet" href="https://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
			<link rel="stylesheet" href="css/magic.css">
			<script src="js/utils.js"></script>
			<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
			<script src="js/jquery.js"></script>
			<script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>		

			<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>

    <style>
        * {
            word-wrap:break-word;
			font-family: Open Sans ;
			font-size:15px;
			border:none!Important;
        }
        video {
            object-fit: fill;
            width: 30%;
        }
        button,
        input,
        select {
            font-weight: normal;
            padding: 2px 4px;
            text-decoration: none;
            display: inline-block;
            text-shadow: none;
            font-size: 16px;
            outline: none;
        }

        .make-center {
            text-align: center;
            padding: 5px 10px;
        }

        img, input, textarea {
          max-width: 100%
        }

        @media all and (max-width: 500px) {
            .fork-left, .fork-right, .github-stargazers {
                display: none;
            }
        }
		video {
		top: 0px; left: 0px; position: absolute !important; width: 100% !important; height: 100% !important; max-height: 100% !important; object-fit:fill;
		}

    </style>
</head>

<script src="https://apis.google.com/js/client.js"></script>
<body>
<div class="g-signin2" class="smallh" data-onsuccess="onSignIn" data-theme="light" style="position:absolute;display:"></div>

  <meta name="google-signin-scope" content="profile email">
    <meta name="google-signin-client_id" content="1043239518917-u67s0l0s9gsd2shj3gjp5evsonlh8917.apps.googleusercontent.com">
<div id="info"></div>
	</body>
	<script src="js/wait.js" async defer></script>  
	<script src="https://apis.google.com/js/platform.js" async defer></script>  
  <script>

		
		var wl = window.location.href;
		var mob = (window.location.href.indexOf('file://')>=0);

		function qs(name, url) {
			if (!url) {
			  url = window.location.href;
			}
			name = name.replace(/[\[\]]/g, "\\$&");
			var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
				results = regex.exec(url);
			if (!results) return null;
			if (!results[2]) return '';
			return decodeURIComponent(results[2].replace(/\+/g, " "));
		}

		function setCookie(cname,cvalue)	{
			if (mob===true) {
				window.localStorage.setItem(cname, cvalue);
			} else {
				var d = new Date(); 
				d.setTime(d.getTime()+(1*24*60*60*1000)); 
				var expires = "expires="+d.toGMTString(); 
				document.cookie = cname + "=" + cvalue + "; " + expires; 
			}
		} 
 		function getCookie(cname)	{ 
			if (mob===true) {
				var cvalue = window.localStorage.getItem(cname);
				return cvalue
			} else {
				var name = cname + "="; 
				var ca = document.cookie.split(';'); 
				for(var i=0; i<ca.length; i++) { 
				  var c = ca[i].trim(); 
				  if (c.indexOf(name)==0) return c.substring(name.length,c.length); 
				} 
				return ""; 
			}
		} 

		function delCookie(cname) {
			if (mob===true) {
				window.localStorage.removeItem(cname);
			} else {
				var d = new Date();
				d.setTime(d.getTime());
				var expires = "expires="+d.toGMTString();
				document.cookie = cname + "=" + "" + "; " + expires;
			}
		}
		
		function clear_all() {
			if (mob===true) {
				 window.localStorage.clear();
			} else {
				document.cookie.split(";").forEach(function(c) { document.cookie = c.replace(/^ +/, "").replace(/=.*/, "=;expires=" + new Date().toUTCString() + ";path=/"); });
			}	
		}

		function isNumber(n) {
		  return !isNaN(parseFloat(n)) && isFinite(n);
		}

	
    function renderButton() {
      gapi.signin2.render('my-signin2', {
        'client_id':  '1043239518917-u67s0l0s9gsd2shj3gjp5evsonlh8917.apps.googleusercontent.com',
        'scope': 'profile email',
        'width': 50,
        'height': 50,
        'longtitle': true,
        'theme': 'dark',
        'onsuccess': onSuccess,
        'onfailure': onFailure
      });
    }
      function onSignIn(googleUser) {
        // Useful data for your client-side scripts:
        var profile = googleUser.getBasicProfile();
        console.log("ID: " + profile.getId()); // Don't send this directly to your server!
        console.log('Full Name: ' + profile.getName());
        console.log('Given Name: ' + profile.getGivenName());
        console.log('Family Name: ' + profile.getFamilyName());
        console.log("Image URL: " + profile.getImageUrl());
        console.log("Email: " + profile.getEmail());

        // The ID token you need to pass to your backend:
        var id_token = googleUser.getAuthResponse().id_token;
        console.log("ID Token: " + id_token);
		//createGUser(profile.getName(), profile.getImageUrl(), profile.getEmail())
		setTimeout(function(){
			googleAuth()
		},3000)
      };

	function googleAuth() {
		var config = {
			'client_id': '1043239518917-u67s0l0s9gsd2shj3gjp5evsonlh8917.apps.googleusercontent.com', 
			'scope': 'https://www.google.com/m8/feeds'
		};
		gapi.auth.authorize(config, function() {
			console.log(gapi.auth.getToken());  
			fetch(gapi.auth.getToken());  
		});		
	}


	var arr=[ ]
	var mid=window.localStorage.getItem('mid')
	var ctr=1

	function fetch(token) {
	  var info=document.getElementById('info')
	    var url="x_fetch_contacts.php?access_token=" + token.access_token + "&alt=json&mid="+mid+'&owner_mobile='+window.localStorage.getItem('sender_mobile')
		console.log(url)
		wait()
		$.ajax({
		    url: url,
		    success:function(dat) {
			//	console.log(dat)
				var data=JSON.parse(dat)
				console.log(data)
				//return
				var ctr=0
				var rw=0
				str = '<table border=0 cellspacing=0  cellpadding=0><tr>'
				for (i=0;i<(data.length);i++) {

					var name=data[i].name
					if (data[i].mobile) var phone=data[i].mobile
					if (data[i].email) var email=data[i].email
					str += '<div style="font-size:15px;background:#fff;width:100%"><div class="www_box2" style="padding:6px;border-top:2px solid black; background: whitesmoke"><b style="font-size:15px">' + ctr + '. ' + name +'</b></div><div style="background:#fff;opacity:1;padding:4px;border-top:1px solid silver"><img src="assets/mobile_icon.png" style="width:15px;padding:1px;margin-right:15px;margin-left:10px;">'+ phone  + '</div><div style="background:#fff;opacity:1;padding:4px;border-bottom:1px solid black;border-top:1px solid black;font-size:15px"><img src="assets/email_icon.png" style="width:20px;padding:0px;margin-right:15px;margin-left:10px;">'+ email  + '</div></div>'
					ctr++
				}
				str += '</tr></table>'
				info.innerHTML = str
				hide()
		    }
		});
	}	
	
	function isValidMobile(f) {
		var number = f.value
		var m = /^((\+)?[1-9]{1,2})?([-\s\.])?((\(\d{1,4}\))|\d{1,4})(([-\s\.])?[0-9]{1,12}){1,2}$/;
		if(!m.test(number)) {
				jmsg('Invalid Mobile! Format:  1(123)456-7890 or 123-456-7890 or 123.456.7890. Just about any format, as long as the number is valid!','err')
				return false
		} else {
			return true
		}
	}	
	function saveMobile(){
		if (isValidMobile(document.getElementById('mob'))) {
			setCookie('mobile',document.getElementById('mob').value,365)
			mobile=getCookie('mobile')
			$('#modalJMsg').modal('hide')
			document.getElementById('msgBody').style.maxHeight='600px'
		} else {
			document.getElementById('mobRequiredMsg').style.color='red'
			document.getElementById('mobRequiredMsg').textContent='Invalid Mobile Number. Please re-enter.'
		}
	}

	function populate(m) {
		$('#cell').val(m)
		$('#modalJMsg').modal('hide')
	}
	
	function inviteUser(m) {
		$('#to_cell').val(m)
		$('#modalJMsg').modal('hide')
	}

	function video(m) {
		location.href='v.php?login='+getCookie('mobile')+'&to='+m
		$('#cell').val(m)
		$('#modalJMsg').modal('hide')
	}

	var ig

	WebFontConfig = {
	google: { families: [ 'Poiret+One::latin', 'Open+Sans+:300' ] }};
	(function() {
		var wf = document.createElement('script');
		wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
		'://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
		wf.type = 'text/javascript';
		wf.async = 'true';
		var s = document.getElementsByTagName('script')[0];
		s.parentNode.insertBefore(wf, s);
	})();


	
</script>
		
</body>

</html>
