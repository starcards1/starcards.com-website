<?php
	require_once "../class/utils.class.php";
	$c=new utils;
	$c->connect("199.91.65.83","voxeo");
	parse_str(http_build_query($_GET));
	$pro=$c->query("select * from celeb_profiles where mid=$provider_mid");
	foreach($pro[0] as $key => $value) {
		${$key}=$value;
	}
	$ta=$c->query("select * from celeb_tags where pid=$pid");
	$t=explode(",",$ta[0]['tags']);
?>
<script src="assets/js/jquery.js"type="text/javascript"></script>
<script src="assets/js/jquery-ui.min.js" type="text/javascript" charset="utf-8"></script>	
<script src="js/tag-it.js" type="text/javascript" charset="utf-8"></script>	
<script src="assets/js/bootstrap.min.js"type="text/javascript"></script>
<script src="assets/js/utils.js"type="text/javascript"></script>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>LinqStar</title>
	<link href="doc/css/bootstrap.min.css" rel="stylesheet">
	<link href="doc/css/style.css" rel="stylesheet">
	<link href="doc/css/menu.css" rel="stylesheet">
	<link href="doc/css/vendors.css" rel="stylesheet">
	<link href="doc/css/icon_fonts/css/all_icons_min.css" rel="stylesheet">
    
	<!-- YOUR CUSTOM CSS -->
	<link href="doc/css/custom.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="assets/css/animate.css"/>
    <link rel="stylesheet" href="assets/css/jquery-ui.min.css"/>
    <link rel="stylesheet" href="assets/css/main.css"/>
    <link rel="stylesheet" href="css/shadows.css"/>
    <link rel="stylesheet" href="dz/dropzone.css"/>
	<link href="style.css" rel="stylesheet" type="text/css" />
	<link href="alt/style.css" rel="stylesheet" type="text/css" />
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:300|Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
	<style>
		* {
		font-size:14px;font-family:Leaner-Thin}
		div input span {font-size:14px;}
		.card_cover{
			background:aliceblue;
			border:0px solid white;
			height:175px;
			margin:10px;
			padding:0
		}
		img {
			draggable:true;
		}
    @font-face {
		font-family: 'Leaner Thin';
		font-style: normal;
		font-weight: normal;
		src: local('Leaner Thin'), url('alt/Leaner-Thin.woff') format('woff');
    }		
	</style>

<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Allura&family=Gloria+Hallelujah&family=Homemade+Apple&display=swap" rel="stylesheet">	
    <link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon"/>
</head>
<body  style="background:url(assets/bg.png)no-repeat;background-size:cover">
	<img style="width:200px;margin:auto;left:0;right:0;position:absolute;top:10px" src="assets/images/logo2.png" alt="logo"/>
	<div class="text-center" style="margin-top:200px">
		<div class="account-wrapper" style="margin:auto;padding:10px;max-width:1000px;background:none">
			<div id="apex" class="account-body">
				<div class="row">
					<div class="col-md-12 text-center">
						<h2 style="color:maroon">CREATE NEW CARD</h2>
					</div>
				</div>
				<span id="part1">
					<div class="row text-center" style="background:;width:100%;margin:0;padding:0">
						<div class="col-md-12 text-center"><br>
						<h4 style="color:white">Select Card Cover</h3>
							<div class="row text-center">
								<?	for ($i=1; $i<=20; $i++) { ?>
									<div onclick="step2('c<?=$i;?>')" class="col-md-2 card_cover www_box">
										<img src="assets/cards/c<?=$i;?>.png" style="width:100%;height:100%">
									</div>
								<? } ?>	
							</div>
						</div>
					</div>
				</span>	
				<br>				
				<span id="part2" style="display:none">
					<div class="row text-left">
						<div class="col-md-12 text-center">
						Please enter at least one
							<div class="col-md-6 text-left" style="margin:auto;border:10px solid white;border-radius:8px;padding:20px;background:aliceblue">
								<div>
									<label>Give your card a title</label>
									<input class="form-control" type="text" id="title" />
								</div>
								<div>
									<label>Recepient Name</label>
									<input class="form-control" type="text" id="recepient_name" />
								</div>
								<div>
									<label>Recepient Mobile</label>
									<input class="form-control" type="text" id="recepient_mobile" />
								</div>
								<div>
									<label>Recepient Email</label>
									<input class="form-control" type="text" id="recepient_email" />
								</div>
								<div>
									<label>From Display Name</label>
									<input class="form-control" type="text" id="sender_name" />
								</div>
							</div>
						</div>
						<div class="col-md-12 text-center">
							<div class="col-md-6 text-center" style="margin:auto">
								<button style="width:100%;max-width:400px" onclick="step1()" class="button-5">Go back</button>
								<button style="width:100%;max-width:400px" onclick="step3()" class="button-5">Next - Enter Message</button>
							</div>
						</div>
					</div>
				</span>					
				<span id="part3" style="display:none">
					<div class="row text-left">

							<div class="col-md-12 text-left" style="margin:auto;border:0px solid white;border-radius:8px;padding:20px;background:">
								<div class="col-md-6 text-left" style="margin:auto">
									<label style="color:white">Please enter a personalized message.<br>Limit: 420 Characters. Choose wisely.</label>
									<div style="min-width:100%;background:white;font-size:14px;height:425px;;line-height:1.3em;font-size:21px;font-family:Gloria Hallelujah!Important;padding:20px;color:blue" id="msg" onkeypress="setStyle()" contentEditable></div>
								</div>
								<br>
								<br>
							</div>
							
						<div class="col-md-12 text-center">
							<div class="col-md-6 text-center" style="margin:auto">
								<button style="width:100%;max-width:400px" onclick="step4()" class="button-5">Save and Preview</button>
								<button style="width:100%;max-width:400px" onclick="step2()" class="button-5">Go back</button>
							</div>
						</div>
					</div>
				</span>					
				<span id="part4" style="display:none">
					<h4 style="color:white">Preview Card Cover</h4>
					<div class="row text-center">
						<div class="col-md-6 www_box" style="width:45%;height:600px; background:#f0f0f0; margin:auto;padding:0">
							<img id="card_cover" style="width:100%;height:100%;margin:0;padding:0" />
						</div>
					</div>
					<div class="col-md-12 text-center">
						<div class="col-md-6 text-center" style="margin:auto">
							<button style="width:100%;max-width:400px" onclick="step3()" class="button-5">Go back</button>
							<button style="width:100%;max-width:400px" onclick="step5()" class="button-5">Preview Inside</button><br>
							<button style="width:100%;max-width:400px" onclick="step6()" class="button-5">Add Video Shout-out</button><br>
							<button style="width:100%;max-width:400px" onclick="step7()" class="button-5">Configure Delivery and Send</button>
						</div>
					</div>						
				</span>					
				<span id="part5" style="display:none">
					<h4 style="color:white">Preview Card Inside</h4>
					<div class="row text-left">
							<div class="col-md-6 www_box" style="width:45%;height:600px; background:silver; margin:0;padding:0">
								<div id="pane1" style="display:none;margin:auto;width:70%;height:70%;margin-top:20%;margin-bottom:15%;background:url(assets/bg.png);color:white;text-align:center;vertical-align:middle;line-height:1.4em;" class="www_box"><div style="display:none;position:absolute;left:0;right:0;top:0;bottom:0;border-radius:300px;background:#660066;width:225px;height:225px;margin:auto;font-family:Gloria Hallelujah;font-size:24px;box-shadow:0 0 15px #000;opacity:0.5"><br><br>Thanks<br>for using<br><span style="line-height:1.4em;font-size:36px;font-family:Gloria Hallelujah!Important">LinqStar!</span></div></div>
							</div>
							<div class="col-md-6 www_box" style="width:45%;height:600px; background:#f0f0f0; margin:0;padding:0">
								<div id="pane2" style="margin:auto;width:70%;height:70%;margin-top:20%;margin-bottom:15%;background:aliceblue;color:blue;text-align:left;vertical-align:middle;line-height:1.3em;font-size:21px;font-family:Gloria Hallelujah!Important;padding:20px" class="www_box"></div>
							</div>
						</div>
						<div class="col-md-12 text-center">
							<div class="col-md-6 text-center" style="margin:auto">
								<button style="width:100%;max-width:400px" onclick="step6()" class="button-5">Add Video Shout-out</button><br>
								<button style="width:100%;max-width:400px" onclick="step7()" class="button-5">Configure Delivery and Send</button>
								<button style="width:100%;max-width:400px" onclick="step4()" class="button-5">Go back</button>
							</div>
						</div>						
					</div>
				</span>		
				<h4 style="color:white">Add Video Shout-out</h4>
		
				<div class="table table-striped" class="files" id="previews">
  <div style="font-size:24px;font-weight:300;color:white"><br>NOW USING GALLERY</div><form action="php/x_gallery_uploads"  style="width:100%;border:none;background:none" class="dropzone dz-preview dz-thumbnail" id="member"></form>
  <div id="template" class="file-row">
    <!-- This is used as the file preview template -->
    <div>
        <span class="preview"><img data-dz-thumbnail /></span>
    </div>
    <div>
        <p class="name" data-dz-name></p>
        <strong class="error text-danger" data-dz-errormessage></strong>
    </div>
    <div>
        <p class="size" data-dz-size></p>
        <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
          <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
        </div>
    </div>
    <div>
      <button class="btn btn-primary start">
          <i class="glyphicon glyphicon-upload"></i>
          <span>Start</span>
      </button>
      <button data-dz-remove class="btn btn-warning cancel">
          <i class="glyphicon glyphicon-ban-circle"></i>
          <span>Cancel</span>
      </button>
      <button data-dz-remove class="btn btn-danger delete">
        <i class="glyphicon glyphicon-trash"></i>
        <span>Delete</span>
      </button>
    </div>
  </div>

				<span id="part6" style="display:none">
					<div class="row text-center">
						<div class="col-md-12 text-center">
							<div class="col-md-6" style="margin:auto">
						
						<!--
							8. Upload Videos
							9  Voice-over
							10. Handwritten
							11. Mood Music
							12. Upload Photos
						-->
						

								<button onclick="upload_voice_over()" style="background:#E28E6F;color:white;width:100%;max-width:400px" class="button-5">Upload Voice-over Now</button><br>
								<button onclick="upload_my_photo()" style="background:#E28E6F;color:white;width:100%;max-width:400px" class="button-5">Upload Background Mood Music</button><br>
								<button onclick="upload_my_video()" style="background:#E28E6F;color:white;width:100%;max-width:400px" class="button-5">Upload Recorded Video Message Now</button><br>
								<button onclick="upload_handwritten_card()" style="background:#E28E6F;color:white;width:100%;max-width:400px" class="button-5">Upload Message in your handwriting</button><br>
								<button onclick="upload_mood_music()" style="background:#E28E6F;color:white;width:100%;max-width:400px" class="button-5">Upload Background Mood Music</button><br>
								<button onclick="buy_celebrity_video()" style="background:#E13A98;color:white;width:100%;max-width:400px" class="button-5">Pay for Celebrity Video Message</button><br>
								<button onclick="buy_celebrity_live_call()" style="background:#E13A98;color:white;width:100%;max-width:400px" class="button-5">Pay for Celebrity Live Call</button><br>
								<button onclick="step7()" style="width:100%;max-width:400px;background:#8500B2;color:#fff" class="button-5">Configure Delivery and Send</button>
							</div>
						</div>
					</div>
				</span>	
				<span id="part7" style="display:none">
					<div class="row text-center">
						<div class="col-md-12 text-center">
							<div class="col-md-6" style="margin:auto">
								<h4 style="color:white">Configure Delivery</h4>
								<button onclick="step1()" style="width:100%;max-width:400px" class="button-5">Start Over</button><br>
								<button style="width:100%;max-width:400px;font-family:Open Sans!Important" class="button-5">Send it Now</button><br>
								<div id="scl8r1" class="row text-left" style="display:none">
									<div class="col-md-1">

									</div>
									<div class="col-md-5">
										<div style="color:white"><b>SEND DATE</b></div><input class="form-control td-input" type="text" id="booking_date" style="font-size:1em">
									</div>
									<div class="col-md-5">
										<div style="color:white"><b>SEND TIME</b></div><input class="form-control td-input" type="text" id="start_time" style="font-size:1em">
									</div>
									<div class="col-md-1">

									</div>
								</div>
								<div id="scl8r2" class="row text-left" style="display:none">
										<div class="col-md-1">

										</div>
										<div class="col-md-10">
											<button onclick="save_later()" style="width:100%; background:url(assets/bg.png);background-size:cover;color:white" class="button-5">Save and Schedule</button><br>
										</div>
										<div class="col-md-1">

										</div>
									</div>
								<button onclick="later()" style="width:100%;max-width:400px" class="button-5">Schedule for Later</button><br>
								<button style="width:100%;max-width:400px" class="button-5">Send On My Cue</button>
							</div>
						</div>
					</div>
				</span>	
			</div>
		</div>
	</div>
	<script src="assets/js/wait.js"></script>
	<script src="assets/js/common.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="./assets/js/modernizr-3.6.0.min.js"></script>
    <script src="./assets/js/plugins.js"></script>
    <script src="./assets/js/bootstrap.min.js"></script>
    <script src="./assets/js/jquery-ui.min.js"></script>
    <script src="js/location.js" type="text/javascript" charset="utf-8"></script>	
	<script src="js/utils.js"type="text/javascript"></script>
	<script src="dz/dropzone.js"type="text/javascript"></script>
	<script src="https://apis.google.com/js/client:platform.js?onload=start" async defer></script>	
	<script type="text/javascript">
		var strx=''
		var tmr
		function keyPressName(myEventKeyName){
			  var pressedKey;
			  if(window.event){
				 pressedKey = myEventKeyName.keyCode;
			  } else if(myEventKeyName.which)
			  {
				 pressedKey = myEventKeyName.which;
		   }
				console.log(pressedKey)
				if(pressedKey==13) {
					 strx+='<br><br>'
				} else {
					strx+=String.fromCharCode(pressedKey);
				}
		   $$('preview2').innerHTML=strx
		}
		function refs() {
			 $$('preview2').innerHTML=$$('msg').value
		}
		function step1(){
			$('#part2').hide()
			$('#part3').hide()
			$('#part4').hide()
			$('#part1').show()
			$('#part5').hide()
			$('#part6').hide()
			$('#part7').hide()
		}
		function step2(id){
			$('#part1').hide()
			$('#part3').hide()
			$('#part4').hide()
			$('#part2').show()
			$('#part5').hide()
			$('#part6').hide()
			$('#part7').hide() 
			setCookie('card_cover',id)
		}
		var u ='',card_id
		function step3(){
			$('#part1').hide()
			$('#part3').show()
			$('#part4').hide()
			$('#part2').hide()
			$('#part5').hide()
			$('#part6').hide()
			$('#part7').hide()
			u="x_create_greeting.php?title="+$$('title').value+"&sender_name="+$$('sender_name').value+"&sender_email="+getCookie('email')+"&sender_mobile="+getCookie('mobile')+"&recepient_name="+$$('recepient_name').value+"&recepient_email="+$$('recepient_email').value+"&recepient_mobile="+$$('recepient_mobile').value+"&card_cover="+getCookie('card_cover')
			$.ajax({url:u,success:function(data){setCookie('card_id',data); card_id=data; $.alert('Saved!')}})
		}
		function step4(){
			$('#part1').hide()
			$('#part3').hide()
			$('#part4').show()
			$('#part2').hide()
			$('#part5').hide()
			$('#part6').hide()
			$('#part7').hide()
			setCookie('msg',$$('msg').value)
			$$('card_cover').src='assets/cards/'+getCookie('card_cover')+'.png'
			$$('pane2').innerHTML=$$('msg').innerHTML
			u="x_update_greeting.php?field=msg&value="+$$('msg').innerHTML+"&key_field=card_id&key_value=" + card_id
			console.log(u)
			$.ajax({url:u,success:function(data){$.alert('Saved!')}})
		}
	
		function step5(){
			$('#part1').hide()
			$('#part3').hide()
			$('#part5').show()
			$('#part2').hide()
			$('#part4').hide()
			$('#part6').hide()
			$('#part7').hide()
		}
	

		
		function step6(){
			$('#part1').hide()
			$('#part3').hide()
			$('#part6').show()
			$('#part2').hide()
			$('#part4').hide()
			$('#part5').hide()
			$('#part7').hide()
		}
		function step7(){
			$('#part1').hide()
			$('#part3').hide()
			$('#part7').show()
			$('#part2').hide()
			$('#part4').hide()
			$('#part5').hide()
			$('#part6').hide()
		}

		function upload_my_photo(){
			$('#part8').hide()
			$('#part9').hide()
			$('#part10').hide()
			$('#part11').hide()
			$('#part12').show()
			activate_upload(12)
		}

		function upload_my_video(){
			$('#part8').show()
			$('#part9').hide()
			$('#part10').hide()
			$('#part11').hide()
			$('#part12').hide()
			activate_upload(8)
		}
		function upload_voice_over(){
			$('#part9').show()
			$('#part8').hide()
			$('#part10').hide()
			$('#part11').hide()
			$('#part12').hide()
			activate_upload(9)
		}
		function upload_handwritten_card(){
			$('#part10').show()
			$('#part9').hide()
			$('#part8').hide()
			$('#part11').hide()
			$('#part12').hide()
			activate_upload(10)
		}

		function upload_mood_music(){
			$('#part11').show()
			$('#part9').hide()
			$('#part10').hide()
			$('#part8').hide()
			$('#part12').hide()
			activate_upload(11)
		}

		function buy_celebrity_video(){
			$('#part9').hide()
			$('#part10').hide()
			$('#part11').hide()
			$('#part8').hide()
		}

		function buy_celebrity_live_call(){
			$('#part8').hide()
			$('#part9').hide()
			$('#part10').hide()
			$('#part11').hide()
		}

		function later() {
			if ($$('scl8r1').style.display=='none') {
				$$('scl8r1').style.display='' 
				$$('scl8r2').style.display='' 
			} else {
				$$('scl8r1').style.display='none' 
				$$('scl8r2').style.display='none' 
			}
		}
		
		function setStyle() {
			if ($$('msg').textContent.length>20) $$('msg').textContent=$$('msg').textContent.substr(0,$$('msg').textContent.length-1)
			var nodes = document.getElementById('msg').childNodes;
			for(var i=0; i<nodes.length; i++) {
				if (nodes[i].nodeName.toLowerCase() == 'div') {
					 nodes[i].style.fontFamily='Gloria Hallelujah'
					 nodes[i].style.fontSize='21px'
					 nodes[i].style.color='blue'
					 nodes[i].position='absolute'
					 nodes[i].draggable=true
				 }
			}
		}
		
		$$('msg').onkeypress=function(e){
			console.log(e)
			if ($$('msg').textContent.length>420) e.preventDefault()
			e.target.style.fontFamily='Gloria Hallelujah'
		}
		
		function qs1() {
			var str1=[]
			var url=''
			var pre
		    str1=location.search.split('?')[1]
			for (var i=0; i<str1.split('&').length; i++) {
				if (i>0) pre='&'
					else pre='?'
				url+=pre+str1.split('&')[i].split('=')[0]+'="+'+str1.split('&')[i].split('=')[0]+'\+\"'
			}
			console.log(url)
		}
		Dropzone.autoDiscover=false
		var sites
		var imagesUploaded
		var r,k,c,t2,t3,t4,intr=50,currImg
		var rat=1
		var rx=0, t, ext
		var set=false
		var usr_img=[]
		var ext
		var collection_set=0
		var channel_set=0
		var cnn=0
		
		function getCookie(cname)	{
			var name = cname + "=";
			var ca = document.cookie.split(';');
			for(var i=0; i<ca.length; i++) {
			  var c = ca[i].trim();
			  if (c.indexOf(name)==0) return c.substring(name.length,c.length);
			}
			return "";
		}
	var gui
	var src_element
	</script>	<!-- HTML heavily inspired by http://blueimp.github.io/jQuery-File-Upload/ -->
<div class="table table-striped" class="files" id="previews">

  <div id="template" class="file-row">
    <!-- This is used as the file preview template -->
    <div>
        <span class="preview"><img data-dz-thumbnail /></span>
    </div>
    <div>
        <p class="name" data-dz-name></p>
        <strong class="error text-danger" data-dz-errormessage></strong>
    </div>
    <div>
        <p class="size" data-dz-size></p>
        <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
          <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
        </div>
    </div>
    <div>
      <button class="btn btn-primary start">
          <i class="glyphicon glyphicon-upload"></i>
          <span>Start</span>
      </button>
      <button data-dz-remove class="btn btn-warning cancel">
          <i class="glyphicon glyphicon-ban-circle"></i>
          <span>Cancel</span>
      </button>
      <button data-dz-remove class="btn btn-danger delete">
        <i class="glyphicon glyphicon-trash"></i>
        <span>Delete</span>
      </button>
    </div>
  </div>

</div>
<script>
// Get the template HTML and remove it from the doumenthe template HTML and remove it from the doument
var previewNode = document.querySelector("#template");
previewNode.id = "";
var previewTemplate = previewNode.parentNode.innerHTML;
previewNode.parentNode.removeChild(previewNode);

var myDropzone = new Dropzone(document.body, { // Make the whole body a dropzone
  url: "/target-url", // Set the url
  thumbnailWidth: 80,
  thumbnailHeight: 80,
  parallelUploads: 20,
  previewTemplate: previewTemplate,
  autoQueue: false, // Make sure the files aren't queued until manually added
  previewsContainer: "#previews", // Define the container to display the previews
  clickable: ".fileinput-button" // Define the element that should be used as click trigger to select files.
});

myDropzone.on("addedfile", function(file) {
  // Hookup the start button
  file.previewElement.querySelector(".start").onclick = function() { myDropzone.enqueueFile(file); };
});

// Update the total progress bar
myDropzone.on("totaluploadprogress", function(progress) {
  document.querySelector("#total-progress .progress-bar").style.width = progress + "%";
});

myDropzone.on("sending", function(file) {
  // Show the total progress bar when upload starts
  document.querySelector("#total-progress").style.opacity = "1";
  // And disable the start button
  file.previewElement.querySelector(".start").setAttribute("disabled", "disabled");
});

// Hide the total progress bar when nothing's uploading anymore
myDropzone.on("queuecomplete", function(progress) {
  document.querySelector("#total-progress").style.opacity = "0";
});

// Setup the buttons for all transfers
// The "add files" button doesn't need to be setup because the config
// `clickable` has already been specified.
document.querySelector("#actions .start").onclick = function() {
  myDropzone.enqueueFiles(myDropzone.getFilesWithStatus(Dropzone.ADDED));
};
document.querySelector("#actions .cancel").onclick = function() {
  myDropzone.removeAllFiles(true);
};


</script>