<script>
	function add_stream() {
		var url="https://linqstar.com/x_add_stream.php?stream=<?=$stream;?>&type=Conference&room=<?=$room;?>&mid="+getCookie('mid')
		$.ajax({url:url,success:function(data){alert(data)}})
	}
	document.addEventListener('visibilitychange', function() {
		if (document.visibilityState == 'hidden') { 
			navigator.sendBeacon("https://linqstar.com/x_remove_stream.php?stream=<?=$stream;?>", []);
		}
	});

	function _exit(e){
		e.preventDefault();
		alert('This action will disconnect you from the conference.')
		navigator.sendBeacon("https://linqstar.com/x_remove_stream.php?stream=<?=$stream;?>", []);
		e.returnValue = '';
	}
	setTimeout(function(){
		add_stream()			
	},1)
	window.addEventListener('beforeunload', _exit);
</script>