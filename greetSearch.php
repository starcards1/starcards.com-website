<?
	require_once "../class/utils.class.php";
	$c=new utils;
	$c->connect("199.91.65.83","voxeo");
	parse_str(http_build_query($_GET));
?>
<!DOCTYPE html>
<html lang="en">
<head>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TLSJ9XC');</script>
<!-- End Google Tag Manager -->

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>LinQStar.com - Celebrities at your  fingertips </title>
	<link rel="stylesheet" href="css/style.css">    <!-- If you want the jQuery UI "flick" theme, you can use this instead, but it's not scoped to just Tag-it like tagit.ui-zendesk is: -->
    <link rel="stylesheet" href="./assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="./assets/css/animate.css">
    <link rel="stylesheet" href="./assets/css/owl.min.css">
    <link rel="stylesheet" href="./assets/css/jquery-ui.min.css">
    <link rel="stylesheet" href="./assets/css/main.css">
    <link href="css/jquery.tagit.css" rel="stylesheet" type="text/css">
    <link href="css/magic.css" rel="stylesheet" type="text/css">
    <link href="css/gloss.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="css/simple-line-icons.css">
	<link rel="stylesheet" href="css/magnific-popup.css">

    <!-- jQuery and jQuery UI are required dependencies. -->
    <!-- Although we use jQuery 1.4 here, it's tested with the latest too (1.8.3 as of writing this.) -->

    <link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon">
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:300|Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
	<style>
	body {
		font-family: 'Source Sans Pro', sans-serif;
		font-size: 14px;
		color: #666;
}

h1 {
		text-align: center;
		margin-bottom: 0;
		margin-top: 15px;
}

#lean_overlay {
		position: fixed;
		z-index: 100;
		top: 0px;
		left: 0px;
		height: 100%;
		width: 100%;
		background: #000;
		display: none;
}

.popupContainer {
		position: absolute;
		width: 330px;
		height: auto;
		left: 45%;
		top: 60px;
		background: #FFF;
}

#modal_trigger {
		margin: 40px auto;
		width: 200px;
		display: block;
		border-radius: 4px;
}

.btn {
		padding: 10px 20px;
		background: #F4F4F2;
}

.btn_red {
		background: #ED6347;
		color: #FFF;
}

.btn:hover {
		background: #E4E4E2;
}

.btn_red:hover {
		background: #C12B05;
}

a.btn {
		color: #666;
		text-align: center;
		text-decoration: none;
}

a.btn_red {
		color: #FFF;
}

.one_half {
		width: 50%;
		display: block;
		float: left;
}

.one_half.last {
		width: 45%;
		margin-left: 5%;
}
/* Popup Styles*/

.popupHeader {
		font-size: 16px;
		text-transform: uppercase;
}

.popupHeader {
		background: #F4F4F2;
		position: relative;
		padding: 10px 20px;
		font-weight: bold;
}

.popupHeader .modal_close {
		position: absolute;
		right: 0;
		top: 0;
		padding: 10px 15px;
		background: #E4E4E2;
		cursor: pointer;
		color: #aaa;
		font-size: 16px;
}

.popupBody {
		padding: 20px;
}
/* Social Login Form */

.social_login {}

.social_login .social_box {
		display: block;
		clear: both;
		padding: 10px;
		margin-bottom: 10px;
		background: #F4F4F2;
		overflow: hidden;
}

.social_login .icon {
		display: block;
		width: 10px;
		padding: 5px 10px;
		margin-right: 10px;
		float: left;
		color: #FFF;
		font-size: 16px;
		text-align: center;
}

.social_login .fb .icon {
		background: #3B5998;
}

.social_login .google .icon {
		background: #DD4B39;
}

.social_login .icon_title {
		display: block;
		padding: 5px 0;
		float: left;
		font-weight: bold;
		font-size: 16px;
		color: #777;
}

.social_login .social_box:hover {
		background: #E4E4E2;
}

.centeredText {
		text-align: center;
		margin: 20px 0;
		clear: both;
		overflow: hidden;
		text-transform: uppercase;
}

.action_btns {
		clear: both;
		overflow: hidden;
}

.action_btns a {
		display: block;
}
/* User Login Form */

.user_login {
		display: none;
}

.user_login label {
		display: block;
		margin-bottom: 5px;
}

.user_login input[type="text"],
.user_login input[type="email"],
.user_login input[type="password"] {
		display: block;
		width: 90%;
		padding: 10px;
		color: #666;
}

.user_login input[type="checkbox"] {
		float: left;
		margin-right: 5px;
}

.user_login input[type="checkbox"]+label {
		float: left;
}

.user_login .checkbox {
		margin-bottom: 10px;
		clear: both;
		overflow: hidden;
}

.forgot_password {
		display: block;
		margin: 20px 0 10px;
		clear: both;
		overflow: hidden;
		text-decoration: none;
		color: #ED6347;
}
/* User Register Form */

.user_register {
		display: none;
}

.user_register label {
		display: block;
		margin-bottom: 5px;
}

.user_register input[type="text"],
.user_register input[type="email"],
.user_register input[type="password"] {
		display: block;
		width: 90%;
		padding: 10px;
		color: #666;
}

.user_register input[type="checkbox"] {
		float: left;
		margin-right: 5px;
}

.user_register input[type="checkbox"]+label {
		float: left;
}

.user_register .checkbox {
		margin-bottom: 10px;
		clear: both;
		overflow: hidden;
}
		* {
			font-family:Roboto!Important;
		}
		input li ul {height:35px}
		h5 {
			font-family:Roboto!Important;
			text-transform:upperCase;
			color:#000!Important
			
		}
		h1 {
			font-family:Roboto!Important;
			text-transform:upperCase;
			font-size:21px;

			
		}
		h2 {
			font-family:Roboto!Important;
			text-transform:upperCase;
			font-size:18px;
		}
		h3 {
			font-family:Roboto!Important;
			text-transform:upperCase
		}
		h4 {
			font-family:Roboto!Important;
			text-transform:upperCase
		}
		h6 {
			font-family:Roboto!Important;
			text-transform:upperCase
		}
		p {
			font-family:Roboto!Important;
			text-transform:upperCase
		}
		.bgx {
			background: -webkit-linear-gradient(0deg, #e1358f 0%, #7e6ce7 100%);
			box-shadow: 4.232px 12.292px 10.56px 0.44px rgb(121 107 232 / 50%);
			padding:10px!Important;
			color:white!Important;
			border-radius:50px!Important;
			font-size:20px!Important;
			text-align:center!Important;
		}
		.bgx1 {
			background: -webkit-linear-gradient(0deg, #e1358f 0%, #7e6ce7 100%);
			box-shadow: 4.232px 12.292px 10.56px 0.44px rgb(121 107 232 / 50%);
			padding:6px!Important;
			color:white!Important;
			border-radius:5px!Important;
			font-size:20px!Important;
			text-align:center!Important;
		}
		.gc {
			padding:5px;border-radius:6px;background:gold;font-size:16px;font-family:Roboto;
		}
		.gcs {
			padding:5px;border-radius:6px;background:lightcyan;font-size:16px;font-family:Roboto;color:black;font-weight:bold
		}
		.gcr {
			padding:5px;border-radius:6px;background:red;font-size:16px;font-family:Roboto;
		}
		.hide {
			display:none
		}

.bubbledLeft,.bubbledRight {
    margin-top: 25px;
    padding: 10px;
    max-width: 70%;
    clear: both;
    position: relative;
}

.bubbledLeft{
    float: left;
    margin-right: auto;
    margin-left: 15px;
    -webkit-border-radius:0px 8px 8px 8px;
    -moz-border-radius: 0px 8px 8px 8px;
    -o-border-radius: 0px 8px 8px 8px;
    -ms-border-radius: 0px 8px 8px 8px;
    border-radius: 0px 8px 8px 8px;
    color: 000;
	font-family:Roboto;
	font-size:12px!Important;
	padding-top:20px
}


.bubbledRight{
    float: right;
    margin-left: auto;
    margin-right: 15px;
    text-align: right;
    -webkit-border-radius: 8px 0px 8px 8px;
    -moz-border-radius: 8px 0px 8px 8px;
    -o-border-radius: 8px 0px 8px 8px;
    -ms-border-radius: 8px 0px 8px 8px;
    border-radius:8px 0px 8px 8px;
    color: 000;
	font-family:Roboto;
	font-size:12px!Important;
}

input,li,ul {max-height:45px}
input[type="text"], input[type="email"], input[type="password"], input[type="number"] {
	height:30px!Important
}
.row {
	margin:auto!Important;
}
* {
  font-family: "Roboto", sans-serif;
  }
.button {
  font-size: 1.0rem;
  font-weight: 700;
  color: #ffffff;
  background: #1e85ff;
  width: 100%;
  font-family: "Roboto", sans-serif;
  border-radius: 30px;
  height: 40px;
  border: 0;
  text-align: center;
  margin-top: auto;
  outline: none;
  -webkit-box-shadow: none;
  box-shadow: none;

}
.pricing-range-bottom {
	padding-left:0;
	padding-right:0;
	background:white;
}
.container{
background:white;
}
	</style>
	    <link href="./greet_files/css2" rel="stylesheet">

</head>

<body style="overflow-x:none!Important;background:#f0f0f0">
<!-- Google Tag Manager (noscript) -->
    <!--============= ScrollToTop Section Starts Here =============-->
    <div class="preloader">
        <div class="preloader-inner">
            <div class="preloader-icon">
                <span></span>
                <span></span>
            </div>
        </div>
    </div>
    <a href="#0" class="scrollToTop">^</i></a>
    <div class="overlay"></div>
    <!--============= ScrollToTop Section Ends Here =============-->

    <!--============= Header Section Ends Here =============-->


    <!--============= Banner Section Starts Here =============-->

	
    <!--============= Pricing Section Starts Here =============-->
 <h1>ORDER CELEBRITY VIDEO</h1>   
  <section class="pricing-section padding-top oh padding-bottom pb-md-0 bg_img pos-rel" id="pricing" STYLE="padding-top:40px">
        <div class="container">

            <div class="range-wrapper-2">
                <div class="pricing-range" style="border-radius: 0 0 8px 8px">
                    <div style="display:none" class="pricing-range-top pt-0">
                        <div class="tags-area">
                            <h3 class="tags">Search</h3>
                        </div>
                        <div class="amount-area">
                            <div class="item">
                                <h2 class="title"><sup>$</sup>50.00</h2>
                            </div>
                           <div class="item">
                                <h2 class="title"><sup>$</sup><span id="amount"></span></h2>
                            </div>
                        </div>
                        <div class="invest-range-area">
                            <div class="invest-amount" data-min="1.00 USD" data-max="1000 USD">
                                <div id="usd" class="invest-range-slider"></div>
                            </div>
                        </div>
                    </div>
					<h2 class="title button-4" style="text-align:left;color:white;border-radius: 4px 4px 0 0">CELEBRITY SEARCH</h2>
							<div id="search_header" style="display:none;position:absolute;width:100%;text-align:left;margin-top:0px;margin-left:10px">
								<span style="position:absolute;margin-top:10px;margin-left:10px">
									<span style="color:maroon;font-size:18px">FOUND:</span>
									<span style="background:white;border-radius:6px;padding:5px;font-weight:bolder!Important;color:#000!Important;font-family:Roboto!Important;font-size:18px">
										<b><span id="srt">0</span></b>
									</span>
								</span>
								<span style="position:absolute;right:200px;display:none">
									<a href="javascript:search_prev()" class="button-2">PREV</a> 
									<a href="javascript:search_next()" class="button-2">NEXT</a>
								</span>
							</div>
						<div style="color:white;position:absolute;text-align:center;margin-left:10px">
						</div>
	                    <div class="pricing-range-bottom" style="border-radius:4px">
					
						<div class="pricing-range" style="width:100%;margin-bottom:5px;border-radius:0 0 8px 8px">
							<input id="search" value="" style="border-radius:4px;width:100%;height:35px!Important;padding:0!Important;overflow:hidden!Important"/> <button id="bssx" class="button" style="width:100%;background:#1e85ff;color:white;border-radius:4px;margin-top:5px">Search</button>
                       </div>
					<div class="row" style="width:100%;line-height:14px;text-align:left;color:grey;background:lightyellow;border-radius:8px;border:1px solid silver;padding:10px" id="trx">
						Apply multiple filters sequentially to keep narrowing search results. Example:<br><br><b>STEP 1</b> First, search by "movie star". To search for movie stars in Los Angeles only, you would:
						<br><br><b>STEP 2</b> In the search box, next to "movie star", type "Los Angeles" and press enter.<br><br>The search results now consist of movie stars in Los Angeles who are willing to record custom greeting for your card, for a fee.
					</div> 
					<div class="row" style="width:100%;" id="srx">
					</div> 
           
					</div>
                </div>
            </div>
        </div>
    </section>
  
	


    <!--============= Footer Section Ends Here =============-->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="./assets/js/modernizr-3.6.0.min.js"></script>
    <script src="./assets/js/plugins.js"></script>
    <script src="./assets/js/bootstrap.min.js"></script>
    <script src="./assets/js/jquery-ui.min.js"></script>
    <script src="./assets/js/wow.min.js"></script>
    <script src="./assets/js/owl.min.js"></script>
    <script src="./assets/js/paroller.js"></script>
    <script src="./assets/js/main.js"></script>
    <script src="assets/js/common.js"></script>
    <script src="js/aes.js"></script>
    <script src="js/tag-it.js" type="text/javascript" charset="utf-8"></script>	
    <script src="js/location.js" type="text/javascript" charset="utf-8"></script>	
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDAuvU_A1iMThoe1i6Joi26nSTDQkjKlDA&libraries=places"></script>
	<script>
		function close_log() {
			$('#modalLogin').hide()
			$('#cover').hide()
			$('#popupHeader').hide()
		}

	</script>
	<section id="modalLogin" class="www_box2 popupContainer text-center" style="border-radius:10px;position:;display:none;z-index:999999999999999999999; margin:auto;left:0;right:0;margin-top:20%;opacity:1;max-width:240px">
		<div id="cover" style="display:;position:fixed;width:100%;height:100%;background:#000;opacity:0.8;z-index:;top:0;left:0;bottom:0;right:0;background:#000!Important; background-size:cover;"></div>
		<div class="popupHeader"  style="background:url(assets/rbg<?=rand(1,18);?>.png) no-repeat; border-radius:10px; background-size:cover;color:white;text-align:center;width:240px;padding:30px;padding-right:10px;box-shadow:0 0 70px 10px rgba(0,0,0,0.1)">
			<!-- Username & Password Login form -->
			<a href="javascript:close_log()"><img src="assets/close.png" style="width:15px;position:absolute;right:10px;top:5px"></a>
			<div class="" style="text-align:left;max-width:250px;margin:0">
				<form>
					<label style="font-size:12px; font-weight:100;color:white;position:absolute;margin-top:3px">Email / Username</label>
					<br/>
					<input style="width:90%" id="email" type="text" />
					<br />
					<br />
					<label style="font-size:12px; font-weight:100;color:white;position:absolute;margin-top:-22px">Password</label>
					<input style="width:90%" id="pswd" type="password" />
					<br />
					<div id="inmsg" class="checkbox">

					</div>
					<br/>
					<div class="">
						<div class="one_half" style="padding:0px;margin:0"><a id="back_btn" style="font-size:12px;;color:#ffff!Important;box-shadow:0 0 10px rgba(0,0,0,0.1);padding-left:8px;padding-right:8px;color:white" href="register.html" class="button-5">Join Us Now</a></div>
						<div class="one_half" style="padding:0px;margin:0"><a onclick="login1()" style="font-size:12px;color:#fff!Important;box-shadow:0 0 10px rgba(0,0,0,0.1);padding-left:8px;padding-right:8px;color:white" href="#" class="button-5">Login Here</a></div>
					</div>
					<div style="display:none" class="">
						<div class="full" style="padding:3px;margin:0">
							<a href="javascript:FBLogin()"><img src="https://sugarmatch.com/img/fb.png" style="width:100%"></a>
						</div>
					</div>
					<br/>
				</form>
				<br />
				<a href="javascript:showForgot()" class="forgot" style="font-weight:100;color:white;font-size:12px">Forgot password?</a>
			</div>

			<!-- Username & Password Login form -->
			<div class="forgot_password" style="display:none;color:#333">
				<form>
					Enter your email <span style="color:red">OR</span> mobile number. Your password will be sent to your mobile phone via sms
					<br /><br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input id="email" type="text" placeholder="Enter your Email Address" style="height:50px!Important;border-radius:7px!Important;border:1px solid aliceblue!Important;padding:4px;width:90%"/>
					<br /><br />OR
					<input id="mobileF" type="text" placeholder="Enter your Mobile Number" style="height:50px!Important;border-radius:7px!Important;border:1px solid aliceblue!Important;padding:4px;width:90%"/>

					<div class="action_btns" style="margin-top:35px;">
						<div class="one_half" style="padding:3px;margin:0"><a id="back_btn_forgot" style="background:white!Important;color:#333!Important;box-shadow:0 0 10px rgba(0,0,0,0.1)" href="javascript:backForgot()" class="btn btn_back"><i class="fa fa-angle-double-left"></i> Back</a></div>
						<div class="one_half last" style="padding:3px;margin:0"><a style="background:white!Important;color:#333!Important;box-shadow:0 0 10px rgba(255,0,0,0.1)" href="javascript:sendForgot()" class="btn">Send</a></div>
					</div>
				</form>
			</div>
		</div>
	</section>
	<div id="auth_bar" style="display:none;width:100%;position:fixed;height:50px;background:#000;color:#fff;top:0;left:0;z-index:99999999999999;opacity:0.7">
	</div>

<div class="modal fade" id="m2" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered">
    <div class="modal-content" STYLE="color:#000">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">SELECT SUB SERVICE FOR LIVE CALL</h5>
         <span aria-hidden="true" data-dismiss="modal"> <b>EXIT</b> </span>
	  </div>
      <div id="ssr" class="modal-body">
	    <div class="container-fluid">
			<div class="row">
		<?	$q=$c->query("select * from subservices where service_type='Live Call'");
			for ($i=0; $i<count($q); $i++) {
				foreach($q[$i] as $key => $value) {
					${$key}=$value;
				}
			?>
				<div class="col-md-6">
					<a href="javascript:var sub_name='<?=$service_name;?>',id='<?=$q[$i]["id"];?>';$('#myModal').modal('hide')"><img src="b03.png" style="width:15px"> <?=$service_name;?></a>
				</div>
			<?
			}
		?>	
		  </div>
		</div>	  
	  </div>
</div>
</div>
</div>
	
<div class="modal fade" id="m3" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered">
    <div class="modal-content" STYLE="color:#000">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">SELECT SUB SERVICE FOR RECORDED MESSAGE</h5>
         <span aria-hidden="true" data-dismiss="modal"> <b>EXIT</b> </span>
	  </div>
      <div id="ssr" class="modal-body">
	    <div class="container-fluid">
			<div class="row">
		<?	$q=$c->query("select * from subservices where service_type='Recorded Message'");
			for ($i=0; $i<count($q); $i++) {
				foreach($q[$i] as $key => $value) {
					${$key}=$value;
				}
			?>
				<div class="col-md-6">
					<a href="javascript:var sub_name='<?=$service_name;?>',id='<?=$q[$i]["id"];?>';$('#myModal').modal('hide')"><img src="b03.png" style="width:15px"> <?=$service_name;?></a>
				</div>
			<?
			}
		?>	
		  </div>
		</div>	  
	  </div>
</div>
</div>
</div>
		
<div class="modal fade" id="myModal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered">
    <div class="modal-content" STYLE="color:#000">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">SELECT SUB FOR SOCIAL FUNCTIONS</h5>
         <span aria-hidden="true" data-dismiss="modal"> <b>EXIT</b> </span>
		</div>
      <div id="ssr" class="modal-body">
	    <div class="container-fluid">
			<div class="row">
		<?	$q=$c->query("select * from subservices where service_type='Social Functions'");
			for ($i=0; $i<count($q); $i++) {
				foreach($q[$i] as $key => $value) {
					${$key}=$value;
				}
			?>
				<div class="col-md-6">
					<a href="javascript:var sub_name='<?=$service_name;?>',id='<?=$q[$i]["id"];?>';$('#myModal').modal('hide')"><img src="b03.png" style="width:15px"> <?=$service_name;?></a>
				</div>
			<?
			}
		?>	
		  </div>
		</div>
      </div>
    </div>
  </div>
</div>	
<script async src="https://www.googletagmanager.com/gtag/js?id=G-T1BEF6BKFY"></script>
<script>
	var session=[ ]
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-T1BEF6BKFY');
	
	function display_order_card(objID) {
		if (getCookie('current_card')) $$(getCookie('current_card')).style.display='none'
		$$(objID).style.display='block'
		$$('scover').style.display='block'
		setCookie('current_card',objID)
	}
	
	function hide_order_card(objID) {
		if (getCookie('current_card')) $$(getCookie('current_card')).style.display='none'
		$$(objID).style.display='none'
		$$('scover').style.display='none'
	}
	
	function display_order_card1(objID) {
		if (getCookie('current_card')) $$(getCookie('current_card')).style.display='none'
		$$(objID).style.display='block'
		$$('scover').style.display='none'
		setCookie('current_card',objID)
	}
	
	function hide_order_card1(objID) {
		if (getCookie('current_card')) $$(getCookie('current_card')).style.display='none'
		$$(objID).style.display='none'
		$$('scover').style.display='none'
	}
	
	//href='stripe/?item_name=$item_name&amt=$item_amt&email=$email&item_id=$item_id'
	function payment(name,provider_type,provider_mid,service_name,service_id,item_amt,blk,p_mobile,p_os) {
		location.href='https://linqstar.com/greetPay.php?item_name='+service_name+'&amt='+item_amt+'&email='+window.localStorage.getItem('sender_email')+'&item_id='+service_id+'&name='+name+'&provider_type='+provider_type+'&provider_mid='+provider_mid+'&p_mobile='+p_mobile+'&p_os='+p_os+'&f_os='+getCookie('fos')+'&f_mobile='+window.localStorage.getItem('sender_mobile')+'&card_id='+qs('card_id')
	}
	function build_card(name,provider_type,provider_mid,service_name,service_id,item_amt,blk,p_mobile,p_os) {
		if (!getCookie('hash')) {
			loginForm2()
		} else {
			location.href='m.php?item_name='+service_name+'&amt='+item_amt+'&email='+session.email+'&item_id='+service_id+'&name='+name+'&provider_type='+provider_type+'&provider_mid='+provider_mid+'&p_mobile='+p_mobile+'&p_os='+p_os+'&f_os='+getCookie('fos')+'&f_mobile='+getCookie('mobile')+'&hash='+getCookie('hash')
		}
	}
	var name,provider_type,provider_mid,service_name,service_id,item_amt,blk,p_mobile,p_os
	function show_sub_services(name,provider_type,provider_mid,service_name,service_id,item_amt,blk,p_mobile,p_os) {
		$('#myModal').modal()
		$('#myModal').on('hidden.bs.modal', function (e) {
	  		location.href='stripe/?item_name='+service_name+'_'+sub_name+'&amt='+item_amt+'&email='+session.email+'&item_id='+service_id+'|'+id+'&name='+name+'&provider_type='+provider_type+'&provider_mid='+provider_mid+'&p_mobile='+p_mobile+'&p_os='+p_os+'&f_os='+getCookie('fos')+'&f_mobile='+getCookie('mobile')+'&hash='+getCookie('hash')
		})		
	}
	
	function sel_sub_service(id,sub_name) {
		location.href='stripe/?item_name='+service_name+'_'+sub_name+'&amt='+item_amt+'&email='+session.email+'&item_id='+service_id+'|'+id+'&name='+name+'&provider_type='+provider_type+'&provider_mid='+provider_mid+'&p_mobile='+p_mobile+'&p_os='+p_os+'&f_os='+getCookie('fos')+'&f_mobile='+getCookie('mobile')+'&hash='+getCookie('hash')
	}
	
	function view_profile(provider_mid) {
		location.href='https://linqstar.com/profile.php?provider_mid='+provider_mid
	}
	


	function wait(txt) {
		wait3(txt)
	}
	function wait3(txt,op) {
		$('.wait_cnt').remove()
		if (!op) op=1
		if (!txt) txt='Loading...'
		wait_cnt=document.createElement('div')
		wait_ldr=document.createElement('div')
		wait_txt=document.createElement('div')
		wait_ldr.id='wait_ldr'
		wait_txt.id='wait_txt'
		wait_cnt.className='wait_cnt'
		wait_cnt.style.cssText='width:300px!Important;max-width:70%; margin:auto; z-index:99999999999999999;position:fixed;top:0;left:0;right:0;bottom:0;'
		wait_ldr.style.cssText='width:300px!Important;max-width:90%;background:#fff;display:none;z-index:99999999999999999;position:fixed;top:0;left:0;right:0;bottom:0;height:65px;opacity:1;margin:auto;background:#fff;border-radius:12px;opacity:1'
		wait_txt.style.cssText='width:300px!Important;max-width:90%;display:none;opacity:1;left:75px;opacity:1;margin:auto;height:50px;text-align:left;margin-top:5px'
		wait_txt.innerHTML='<div style="font-size:1" id="loader_txt"><img style="width:50px;margin-top:15px;position:absolute" src="assets/loading.gif"><span style="position:absolute;left:60px;top:25px;font-family:Roboto;font-size:15px"><b>'+txt+'</b></span></div>'
		wait_cnt.appendChild(wait_ldr)	
		wait_ldr.appendChild(wait_txt)	
		document.documentElement.appendChild(wait_cnt)	
		wait_ldr.style.display=''
		wait_txt.style.display=''
		setTimeout(function(){
			$('.wait_cnt').remove()
		},60000)
	}
		var o,enc,name,ms,fn
		
		function login1() {
			$$('modalLogin').style.display='none'
			$('.user_login').hide()
			var m = $("#email").val(),
			p = $("#pswd").val();
			if (!m || !p) {
				jconfirm({
					title: 'Error!',
					type: 'red',
					theme: 'modern',
					content: 'Invalid Username/Password',
					buttons: {
						Retry: function () {
							loginForm2()
						}
					}
				});
			} else {
				console.log('https://linqstar.com/x_login.php?user_input=' + m + '&pswd=' + p)
				wait('Authenticating, please wait...')
				$.ajax({
					url: 'https://linqstar.com/x_login.php?user_input=' + m + '&pswd=' + p,
					cache: !1, 
					success: function(em) {
						console.log(em)
						ms = JSON.parse(em);
						if (ms.error) {
							$('.jc').hide()
							$('.wait_cnt').hide()
							//$$('modalLogin').style.display='none'
							//$('.user_login').hide()
							
							jconfirm({
								title: 'Error!',
								type: 'red',
								theme: 'modern',
								content: 'Invalid Username/Password',
								buttons: {
									Retry: function () {
										//loginForm()
									}
								}
							});								
						} else {
							setCookie("session", encrypt(em,ms.hash))
							fn=ms.filename_1.split('/')[ms.filename_1.split('/').length-1]
							$('.wait_cnt').hide()
							setCookie("lat", ms.lat)
							setCookie("lng", ms.lng)
							setCookie("location", ms.city+' '+ms.state+' '+ms.zip)
							setCookie('name',ms.name)
							setCookie('mid',ms.mid)
							setCookie('user_type',ms.type)
							setCookie('mobile',ms.mobile)
							setCookie('fos',ms.os)
							setCookie('saveUser',$$('email').value)
							setCookie('savePass',$$('pswd').value)
							setCookie('hash',ms.hash)
							window.localStorage.setItem('login',ms.login);
							window.localStorage.setItem('mid',ms.mid);
							window.localStorage.setItem('user_type',ms.type);
							window.localStorage.setItem('hash',ms.hash);
							setCookie('login',ms.login);
							$('#auth_bar').show()
							$('#auth_bar').html("<button onclick='logout1()' style='border-radius:0;background:red;color:white;width:125px' class='btn'>Logout!</button><button style='border-radius:0;background:black;color:white;width:200px' class='btn'>Welcome, " + ms.name + "</a><a href='home.php?hash=" + ms.hash + "' style='position:absolute;right:0'><img src='assets/tab_members_home.png' style='height:50px'></a>")
							$$('auth_bar').style.width=window_width()+'px'
							alert_user('logged_in')
							setTimeout(function(){
								alert_admin()
							},5000)
						}
					}
				})
			}
		}
	
		function logout1() {
			$.confirm({
				title:'Logout?',
				content:'This action will log you out. Confirm?',
				buttons: {
					Confirm: function(){
						clear_all()
						setTimeout(function(){
							location.href='index.php'
						},1)
					},
					Cancel: function(){
						
					}
				}
			});
		}

		function clear_all() {
			if (mob===true) {
				 window.localStorage.clear();
			} else {
				document.cookie.split(";").forEach(function(c) { document.cookie = c.replace(/^ +/, "").replace(/=.*/, "=;expires=" + new Date().toUTCString() + ";path=/"); });
			}
			
		}	
	function loginForm2() {
		$$('modalLogin').style.display=''
		$('.social_login').hide()
		$('.user_login').show()
	}
	
	function backLogin() {
		$$('modalLogin').style.display='none'
		$('.user_login').hide()
	}
	function backForgot() {
		$('.forgot_password').hide()
		$('.user_login').show()
	}
	function backRegister() {
		$('.social_login').show()
		$('.user_register').hide()
	}
	function regForm() {
		location.href='register.html'
		$('.social_login').hide()
		$('.user_register').show()
	}
	function showForgot() {
		$('.user_login').hide()
		$('.forgot_password').show()
	}
	var wl  = window.location.href;
	var mob = (window.location.href.indexOf('file://')>=0);

	function setCookie(cname,cvalue)	{
		window.localStorage.setItem(cname, cvalue);
		if (mob===true) {
			
		} else {
			var d = new Date(); 
			d.setTime(d.getTime()+(1*24*60*60*1000)); 
			var expires = "expires="+d.toGMTString(); 
			document.cookie = cname + "=" + cvalue + "; " + expires; 
		}
	} 

	function getCookie(cname)	{ 
		if (mob===true) {
			var cvalue = window.localStorage.getItem(cname);
			return cvalue
		} else {
			var name = cname + "="; 
			var ca = document.cookie.split(';'); 
			for(var i=0; i<ca.length; i++) { 
			  var c = ca[i].trim(); 
			  if (c.indexOf(name)==0) return c.substring(name.length,c.length); 
			} 
			return ""; 
		}
	} 


	function handleOpenURL(url) {
		setCookie('url',url)
		if (url.split('/')[0]=='user') {
			var st='user'
			var q=url.split('/')[1]
			location.href='search.html?q='+q
			setCookie('x_source',q)
		} else {
			var st='location'
			var q=url.split('/')[1]
			location.href='search.html?type=search_location&q='+q
			setCookie('x_source',q)
		}
	}

	function qs(name, url) {
		if (!url) {
		  url = window.location.href;
		}
		name = name.replace(/[\[\]]/g, "\\$&");
		var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
			results = regex.exec(url);
		if (!results) return null;
		if (!results[2]) return '';
		return decodeURIComponent(results[2].replace(/\+/g, " "));
	}

	function setVersion() {
		setCookie('version',version)
	}
	
	function $$(obj) {
		return document.getElementById(obj)
	}
	function go_members_home() {
		window.top.location.href='home.html'
	}
		
	function joinFree() {
		window.location.href='register.html'	
	}

	function login() {
		$$('modalLogin').style.display=''
	}
	var id
	function getPosition(position) {
		if (getCookie('accurate') && getCookie('zip')) return false
		var geocoder = new google.maps.Geocoder();
		var latlng = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
		geocoder.geocode({'latLng': latlng}, function(results, status) {
		if (status == google.maps.GeocoderStatus.OK) {
			if (results[0]) {
				formatted_address = results[0].formatted_address
				var address = "", city = "", state = "", zip = "", country = "";
				for (var i = 0; i < results[0].address_components.length; i++) {
					var addr = results[0].address_components[i];
					if (addr.types[0] == 'country') 
						country = addr.long_name;
					else if (addr.types[0] == 'street_address')
						address = address + addr.long_name;
					else if (addr.types[0] == 'establishment')
						address = address + addr.long_name;
					else if (addr.types[0] == 'route')
						address = address + addr.long_name;
					else if (addr.types[0] == 'postal_code')
						zip = addr.short_name;
					else if (addr.types[0] == ['administrative_area_level_1'])
						state = addr.short_name;
					else if (addr.types[0] == ['locality'])
						city = addr.long_name;
					}
					street_no=results[0].address_components[0].short_name
					street=results[0].address_components[1].short_name
					full_address = formatted_address
					full_address=street_no + ', ' + street + ' ' + city + ' ' + state + ' ' + zip + ' '  + country
					setCookie('location', street_no + ' ' + street + ' ' + city + ', ' + state + ' ' + zip)
					setCookie('street_no',street_no)
					setCookie('street',street)
					setCookie('city',city)
					setCookie('state',state)
					setCookie('zip',zip)
					setCookie('lat',position.coords.latitude)
					setCookie('lng',position.coords.longitude)
					setCookie('accurate','1')
				}
			}
		});
	}

	function sendForgot() {
		var url='https://lushmatch.com/sdfm/api/x_send_password.php?login='+$$('loginF').value+'&mobile='+$$('mobileF').value
		//console.log(url)
		$.ajax({url:url,success:function(data){
			$.alert({title:'',content:data})
		}})
	}

		function alt_location(err) {
			//console.log (err.message)
			$.ajax({
				url: "https://lushmatch.com/sdfm/api/x_location.php",
				type: "GET",
				dataType: "html",
				success: function(msg){
					if (!!msg) {
						var data=JSON.parse(msg)[0]
						setCookie('lat', data.lat)
						setCookie('lng', data.lng)
						setCookie('city', data.city)
						setCookie('state', data.state)
						setCookie('zip', data.zip)
						setCookie('location', data.city + ' ' + data.state + ' ' + data.zip)
					}
				}
			})
		}


version=2050

var gen
	function FBLogin(){
		facebookConnectPlugin.login(["public_profile","email","user_photos","user_gender"],function(result){
		facebookConnectPlugin.getAccessToken(function(token) {
			tk=token
			setCookie("token", token);
		});
		facebookConnectPlugin.api("/me?fields=email,name,picture,birthday,gender,albums.limit(50){name,count,cover_photo{picture}}",
			["public_profile","email","user_photos","user_gender"]
				,function(ud){
					var gender=ud.gender
					var uname=ud.name
					var email=ud.email
					var login=email.split('@')[0]
					var birthday=ud.birthday
					var yr=birthday.split('/')[birthday.split('/').length-1]
					var age
					if (gender=='male') gen='SugarDaddy'
						else gen='SugarBaby'
					if (yr*1<2000) {
						age=(2000 - yr*1) + (2020-2000);
					} else {
						age=2020-yr*1;
					}					
					var filename_1=ud.picture.data.url
					var id=ud.id
					$.ajax({url:'https://lushmatch.com/sdfm/api/x_validate_fb_email.php?email='+email,success:function(data){
						if (!isNaN(data)) {
							location.href='home.html?email='+email+'&gender='+gen+'&login='+login+'&mid='+data
						} else {
							var url='registerFacebook.html?name='+uname+'&email='+email+'&fbid='+id+'&gender='+gen+'&login='+login+'&age='+age+'&filename_1='+filename_1;
						}
					}})
					location.href=url
		},function(error){
			alert(JSON.stringify(error)); 
		  });
	   },function(error){
			alert(JSON.stringify(error));
		 });
	}
	   
</script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/4.0.1/socket.io.js"></script>

</body>

<script type="text/javascript">	
	var arrF=[ ]
	function $$(id) {
		return document.getElementById(id)
	}
	function qs(name, url) {
		if (!url) {
		  url = window.location.href;
		}
		name = name.replace(/[\[\]]/g, "\\$&");
		var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
			results = regex.exec(url);
		if (!results) return null;
		if (!results[2]) return '';
		return decodeURIComponent(results[2].replace(/\+/g, " "));
	}

	var wl  = window.location.href;
	var mob = (window.location.href.indexOf('file://')>=0);

	function setCookie(cname,cvalue)	{
		if (mob===true) {
			window.localStorage.setItem(cname, cvalue);
		} else {
			var d = new Date(); 
			d.setTime(d.getTime()+(1*24*60*60*1000)); 
			var expires = "expires="+d.toGMTString(); 
			document.cookie = cname + "=" + cvalue + "; " + expires; 
		}
	} 

	function getCookie(cname)	{ 
		if (mob===true) {
			var cvalue = window.localStorage.getItem(cname);
			return cvalue
		} else {
			var name = cname + "="; 
			var ca = document.cookie.split(';'); 
			for(var i=0; i<ca.length; i++) { 
			  var c = ca[i].trim(); 
			  if (c.indexOf(name)==0) return c.substring(name.length,c.length); 
			} 
			return ""; 
		}
	} 
	function clear_all() {
		if (mob===true) {
			 window.localStorage.clear();
		} else {
			document.cookie.split(";").forEach(function(c) { document.cookie = c.replace(/^ +/, "").replace(/=.*/, "=;expires=" + new Date().toUTCString() + ";path=/"); });
		}	
	}		
		
		var var_who,var_service,var_occupation,var_cname
		var filters=['who','service','occupation','cname','location','cost'];
		var currFilter=0
		var filter, oldFilter
		var xt=[ ]
		var nx=false
		filter=filters[0]
		function nextFilter() {
			oldFilter=filters[currFilter]
			currFilter++
			if (currFilter>5) currFilter=0
			filter=filters[currFilter]
			if(filter=='who') {
				$$('filter-who').className='col-sm-4 show'
				$$('filter-service').className='col-sm-4 hide'
				$$('filter-occupation').className='col-sm-4 hide'
				$$('filter-cname').className='col-sm-4 hide'
				$$('filter-location').className='col-sm-4 hide'
				$$('filter-cost').className='col-sm-4 hide' 
			}
//			$$('filter-'+oldFilter).hide()
//			$$('filter-'+filter).show()
			setTimeout(function(){
				$('#filter-'+oldFilter).show()
				$('#filter-'+oldFilter).addClass('magictime tinRightOut');
					setTimeout(function(){
						try {
							$('#filter-'+oldFilter).hide()
							$('#filter-'+filter).show()
							$('#filter-'+filter).addClass('magictime tinLeftIn');
						} catch(e){}
					}, 200);			
				}, 100);			
		try {
			$$('f-' + oldFilter).hide()
			$$('f-' + filter).show()
			} catch(e) {
				console.log(e)
			}
		}		
		function prevFilter() {
			try {
				oldFilter=filters[currFilter]
				currFilter--
				if (currFilter<0) currFilter=5
				filter=filters[currFilter]
				$('#filter-'+oldFilter).hide()
				$('#filter-'+filter).show()
				$('#f-' + oldFilter).hide()
				$('#f-' + filter).show()
			} catch (e) {}
		}		
			var vid1
			var vid2
			var vid3
			var vid4
			var vid=$$('vid1')
			var p
			var str
			var gtags=[ ]
			str=''
			var gts=[ ]
			
		function updateFilter(st,searchIndex) {
			if (st===true) {
				if (xt.indexOf(getCookie(searchIndex))>=0) {
					return
				} else {
					xt.push(getCookie(searchIndex))
				}
			} else {
				xt.splice(xt.indexOf(getCookie(searchIndex)),1)
			}
		}
		var start=0
		setCookie('start',start)
		var total
		var lastPage
		
		function next() {
			start=getCookie('start')
			if (!start) start=0
			start=start*1+20
			lastPage=Math.floor(getCookie('total')/20)+1
			if (start>=getCookie('total')*1) start=(lastPage-1)*20
			setCookie('start',start)
			doSearchFilter(q)
		}

		function prev() {
			start=getCookie('start')
			if (!start) start=0
			start=start*1-20
			if (start<=0) start=0
			setCookie('start',start)
			doSearchFilter(q)
		}
		var search_count=10;
		
		function search_next() {
			search_start=getCookie('search_start')
			if (!search_start) search_start=0
			search_start=search_start*1+search_count
			search_lastPage=Math.floor(getCookie('search_total')/20)+1
			if (search_start>=getCookie('search_total')*1) search_start=(search_lastPage-1)*20
			setCookie('search_start',search_start)
			setTimeout(function(){
				getSearchTags()
			},1)
		}

		function search_prev() {
			search_start=getCookie('search_start')
			if (!search_start) search_start=0
			search_start=search_start*1-search_count
			if (search_start<=0) search_start=0
			setCookie('search_start',search_start)
			setTimeout(function(){
				getSearchTags()
			},1)
		}
		
		var xt_save=[ ]
		function removeTag(tag) {
			//console.log(xt)
			xt.splice(xt.indexOf(tag),1);
			xt_save=xt
			//console.log(xt)
			var fil=arrF[tag]
			$('#'+fil).tagit("removeAll")
			q=xt.join('|')
			q=replaceAll(q+'',',','|')
			setCookie('q',q)
			$$('globalTags').innerHTML=q
			doSearchFilter(q)
		}
		
		function updateGlobalTags() {
			str=''
			for (p=0;p<xt.length; p++) {
				var gtid=xt[p].replace(/ /g,'_').toUpperCase()
				str+='<a href="javascript:removeTag(\''+xt[p]+'\')"><span class="button-5" style="padding:8px;border-radius:4px;font-size:0.8em;font-family:Roboto!Important;font-weight:normal">'+xt[p].toUpperCase()+'<img src="cross.png" style="width:30px;padding-bottom:5px"></span></a> ' 
			}	
			$$('globalTags').innerHTML=str
			str=''
		}
		var visits=0
		function doSearchFilter(q) {
			gtags=getCookie('q').split('|')
			for (p=0;p<xt.length; p++) {
				var gtid=xt[p].replace(/ /g,'_').toUpperCase()
				str+='<a href="javascript:removeTag(\''+xt[p]+'\')"><span class="button-5" style="padding:8px;border-radius:4px;font-size:0.8em;font-family:Roboto!Important;font-weight:normal">'+xt[p].toUpperCase()+'<img src="cross.png" style="width:30px;padding-bottom:5px"></span></a> ' 
			}
			if (str) $$('globalTags').innerHTML=str
			str=''
			//console.log("x_get_popular.php?term="+q)
			start=getCookie('start')
			if (!start) start=0
			$.ajax({
				url: "x_get_popular.php?term="+q+'&start='+start, 
				success: function( msg ) {
					//console.log(msg)
					var cnt=msg.split('|*|')[1]
					var result=msg.split('|*|')[0]
					//console.log(cnt)
					//console.log(result)
					$('#sr').html('<div style="position:absolute;width:150px;text-align:center"><span style="background:white;border-radius:6px;padding:5px;font-weight:bolder!Important;color:#000!Important;font-family:Roboto!Important;font-size:24px"><b>' + cnt + '</b></span></div></div>')
					$('#result2').html(result)
					setCookie('total',cnt)
					setCookie('total',cnt)
					if (visits>0) nextFilter()
					visits++ 
				}
			});
		}
		
		

		var search_start=0;
		
		function replaceAll(str, find, replace) {
		  return str.replace(new RegExp(find, 'g'), replace);
		}
		var term=[ ]
		var q=''
		jQuery(document).ready(function() {

		$( "#usd" ).slider({
		  range: "min",
		  value: 200,
		  min: 50,
		  max: 5000,
		  step: 50,
		  slide: function( event, ui ) {
			$( "#amount" ).html(ui.value + ".00");
		  }
		});
		$( "#amount" ).html($( "#usd" ).slider( "value" )  + ".00");
	  
		jQuery("#search").tagit({
				singleField: true,
				singleFieldNode: $('#txtSearch'),
				allowSpaces: true,
				minLength: 0,
				placeholderText:'Enter Any Keyword',
				removeConfirmation: true,
				afterTagAdded: function(event, ui) {
					if (q.length>0) q+=(ui.tag[0].innerText)+'|'
						else q=(ui.tag[0].innerText)
					q=$("#search").tagit("assignedTags")
					q=replaceAll(q+'',',','_')
					$.ajax({
						url: "x_search.php?start="+search_start, 
						data: { term:q },
						success: function( msg ) {
							$('#trx').hide()
							var data=msg.split('|')
							$('#srx').html(data[1])
							$('#srt').html(data[0])
							setCookie('search_total',data[0])
							$('#search_header').show()
						}
					});
				},
			tagSource: function( request, response ) {
				$.ajax({
					url: "x_search.php", 
					data: { term:request.term },
					dataType: "json",
					success: function( data ) {
						response( $.map( data, function( item ) {
							return {
								label: item.label,
								value: item.value
							}
						}));
					}
				});
			}
				
			});
	
		function getSearchTags() {
			$.ajax({
				url: "x_search.php?start="+search_start, 
				data: { term:q },
				success: function( msg ) {
					var data=msg.split('|')
					$('#srx').html(data[1])
					$('#srt').html(data[0])
				}
			});
		}
			
		var arr=[ ]
		var arr2=[ ]
		
		jQuery("#who").tagit({
			singleField: true,
			singleFieldNode: $('#txtWho'),
			allowSpaces: true,
			placeholderText:'Filter Provider Type',
			minLength: 0,
			removeConfirmation: true,
			tagLimit:1,
			afterTagAdded: function(event, ui) {
				//console.log(ui.tag[0].innerText)
				if (ui.tag[0].innerText) {
					arrF[ui.tag[0].innerText]=filter
					xt.push(ui.tag[0].innerText)
					q=xt.join('|')
					setCookie('q',q)
					setCookie('q1',ui.tag[0].innerText)
					doSearchFilter(q)
				}
			},
			afterTagRemoved: function(event, ui) {
				// do something special
				//xt.splice(xt.indexOf(ui.tag[0].innerText),1);
				updateGlobalTags()
				arrF[filter]=''
				q=xt.join('|')
				q=replaceAll(q+'',',','|')
				setCookie('q',q)
				doSearchFilter(q)
			},
			tagSource: function( request, response ) {
				$.ajax({
					url: "x_tag_ac_provider_type.php", 
					data: { term:request.term },
					dataType: "json",
					success: function( data ) {
						response( $.map( data, function( item ) {
							return {
								label: item.label,
								value: item.value
							}
						}));
					}
				});
			}
			});
			
		jQuery("#service").tagit({
			singleField: true,
			singleFieldNode: $('#txtCelebService'),
			allowSpaces: true,
			placeholderText:'Filter Celeb Services',
			minLength: 0,
			removeConfirmation: true,
			tagLimit:1,
			afterTagAdded: function(event, ui) {
				//console.log(ui.tag[0].innerText)
				if (ui.tag[0].innerText) {
					xt.push(ui.tag[0].innerText)
					arrF[ui.tag[0].innerText]=filter
					q=xt.join('|')
					setCookie('q',q)
					setCookie('q2',ui.tag[0].innerText)
					doSearchFilter(q)
				}
			},
			afterTagRemoved: function(event, ui) {
				// do something special
				//xt.splice(xt.indexOf(ui.tag[0].innerText.replace('�','')),1);
				updateGlobalTags()
				arrF[filter]=''
				q=xt.join('|')
				q=replaceAll(q+'',',','|')
				setCookie('q',q)
				doSearchFilter(q)
			},
			tagSource: function( request, response ) {
				$.ajax({
					url: "x_tag_ac_celeb_service_type.php", 
					data: { term:request.term },
					dataType: "json",
					success: function( data ) {
					}
				});
			}});

		jQuery("#pro_service").tagit({
			singleField: true,
			singleFieldNode: $('#txtProService'),
			allowSpaces: true,
			placeholderText:'Filter Professional Services',
			minLength: 0,
			removeConfirmation: true,
			tagLimit:1,
			afterTagAdded: function(event, ui) {
				//console.log(ui.tag[0].innerText)
				if (ui.tag[0].innerText) {
					xt.push(ui.tag[0].innerText)
					arrF[ui.tag[0].innerText]=filter
					q=xt.join('|')
					setCookie('q',q)
					setCookie('q2',ui.tag[0].innerText)
					doSearchFilter(q)
				}
			},
			afterTagRemoved: function(event, ui) {
				// do something special
				//xt.splice(xt.indexOf(ui.tag[0].innerText.replace('�','')),1);
				updateGlobalTags()
				arrF[filter]=''
				q=xt.join('|')
				q=replaceAll(q+'',',','|')
				setCookie('q',q)
				doSearchFilter(q)
			},
			tagSource: function( request, response ) {
				$.ajax({
					url: "x_tag_ac_pro_service_type.php", 
					data: { term:request.term },
					dataType: "json",
					success: function( data ) {
						response( $.map( data, function( item ) {
							return {
								label: item.label,
								value: item.value
							}
						}));
					}
				});
			}});

		jQuery("#occupation").tagit({
			singleField: true,
			singleFieldNode: $('#txtOccupation'),
			allowSpaces: true,
			placeholderText:'Filter Provider Details',
			minLength: 0,
			removeConfirmation: true,
			tagLimit:1,
			afterTagAdded: function(event, ui) {
				//console.log(ui.tag[0].innerText)
				if (ui.tag[0].innerText) {
					xt.push(ui.tag[0].innerText)
					arrF[ui.tag[0].innerText]=filter
					q=xt.join('|')
					setCookie('q',q)
					setCookie('q3',ui.tag[0].innerText)
					doSearchFilter(q)
				}
			},
			afterTagRemoved: function(event, ui) {
				// do something special
				//xt.splice(xt.indexOf(ui.tag[0].innerText.replace('�','')),1);
				updateGlobalTags()
				q=xt.join('|')
				q=replaceAll(q+'',',','|')
				arrF[filter]=''
				setCookie('q',q)
				doSearchFilter(q)
			},
			tagSource: function( request, response ) {
				$.ajax({
					url: "x_tag_ac_provider_type_detail.php", 
					data: { term:request.term },
					dataType: "json",
					success: function( data ) {
						response( $.map( data, function( item ) {
							return {
								label: item.label,
								value: item.value
							}
						}));
					}
				});
			}});

		jQuery("#cname").tagit({
			singleField: true,
			singleFieldNode: $('#txtName'),
			allowSpaces: true,
			placeholderText:'Filter Provider Name',
			minLength: 0,
			removeConfirmation: true,
			tagLimit:1,
			afterTagAdded: function(event, ui) {
				//console.log(ui.tag[0].innerText)
				if (ui.tag[0].innerText) {
					xt.push(ui.tag[0].innerText)
					arrF[ui.tag[0].innerText]=filter
					q=xt.join('|')
					setCookie('q',q)
					setCookie('q4',ui.tag[0].innerText)
					doSearchFilter(q)
				}
			},
			afterTagRemoved: function(event, ui) {
				// do something special
				//xt.splice(xt.indexOf(ui.tag[0].innerText.replace('�','')),1);
				updateGlobalTags()
				q=xt.join('|')
				q=replaceAll(q+'',',','|')
				arrF[filter]=''
				setCookie('q',q)
				doSearchFilter(q)
			},
			tagSource: function( request, response ) {
				$.ajax({
					url: "x_tag_ac_provider_names.php", 
					data: { term:request.term },
					dataType: "json",
					success: function( data ) {
						response( $.map( data, function( item ) {
							return {
								label: item.label,
								value: item.value
							}
						}));
					}
				});
			}});

		jQuery("#location").tagit({
			singleField: true,
			singleFieldNode: $('#txtLocation'),
			allowSpaces: true,
			placeholderText:'Filter Location',
			minLength: 0,
			removeConfirmation: true,
			tagLimit:3,
			afterTagAdded: function(event, ui) {
				//console.log(ui.tag[0].innerText)
				if (ui.tag[0].innerText) {
					xt.push(ui.tag[0].innerText)
					arrF[ui.tag[0].innerText]=filter
					q=xt.join('|')
					setCookie('q',q)
					setCookie('q5',ui.tag[0].innerText)
					doSearchFilter(q)
				}
			},
			afterTagRemoved: function(event, ui) {
				// do something special
				//xt.splice(xt.indexOf(ui.tag[0].innerText.replace('�','')),1);
				updateGlobalTags()
				arrF[filter]=''
				q=xt.join('|')
				q=replaceAll(q+'',',','|')
				setCookie('q',q)
				doSearchFilter(q)
			},
			tagSource: function( request, response ) {
				$.ajax({
					url: "x_tag_ac_location.php", 
					data: { term:request.term },
					dataType: "json",
					success: function( data ) {
						response( $.map( data, function( item ) {
							return {
								label: item.label,
								value: item.value
							}
						}));
					}
				});
			}});

		jQuery("#cost").tagit({
			singleField: true,
			singleFieldNode: $('#txtCost'),
			allowSpaces: true,
			placeholderText:'Filter Max Cost',
			minLength: 0,
			removeConfirmation: true,
			tagLimit:1,
			afterTagAdded: function(event, ui) {
				//console.log(ui.tag[0].innerText)
				if (ui.tag[0].innerText) {
					xt.push(ui.tag[0].innerText)
					arrF[ui.tag[0].innerText]=filter
					q=xt.join('|')
					setCookie('q',q)
					setCookie('q6',ui.tag[0].innerText)
					doSearchFilter(q)
				}
			},
			afterTagRemoved: function(event, ui) {
				// do something special
				//xt.splice(xt.indexOf(ui.tag[0].innerText.replace('�','')),1);
				updateGlobalTags()
				arrF[filter]=''
				q=xt.join('|')
				q=replaceAll(q+'',',','|')
				setCookie('q',q)
				doSearchFilter(q)
			},
		})});
 	function send_sms(to,message,from) {
		if (!from) from='12138553255'
		const accountSid = 'AC6a25f052059cd1b78a9295e4b5a1b608';
		const authToken = '91e0b23689566b46d3f0f9e92e92e466';
		const client = require('twilio')(accountSid, authToken);
		client.messages
			.create({
				body: message,
				from: from,
				to: to
		   })
		.then(message => {
			utils.getConnection(function(err, connection) {
				var sql="INSERT INTO `sms`.`sms_logs`(`to_mobile`, `from_mobile`, `to_long_code`, `from_long_code`, `message`, `timestamp`, `source`) VALUES ('"+to_mobile.replace('+','')+"', '"+from_mobile+"', '9999999999', '"+from+"', '"+message+"', '"+Date.now()+"', 'VIDEO_CALL_NOTIFICATION')"
				connection.query(sql, function (error, results, fields) {
					connection.release();
				});
			});		
		});
	}
	 
	function isValidMobile(mob) {
		if (!mob) return false
		var num = mob.trim();
		num = num.replace(/ /g,'');
		num = num.replace(/\./g,'');
		num = num.replace(/-/g,'');
		num = num.replace(/\(/g,'');
		num = num.replace(/\)/g,'');
		num = num.replace(/\[/g,'');
		num = num.replace(/\]/g,'');
		num = num.replace(/\~/g,'');
		num = num.replace(/\*/g,'');
		num = num.replace(/\{/g,'');
		num = num.replace(/\}/g,'');
		num = num.replace(/\+/g,'');
		if ((num+'').length<10) return false
		if (isNaN(num)) return false
		code='1'
		if ((num+'').length==10) {
			return (''+code+''+num+'')
		} else if ((num+'').length==11) {
			if (num.substr(0,1)==code) {
				return num
			} else {
				return false
			}
		} else if ((num+'').length>11) {
			return num
		}
	}	
	setCookie('current_card','')
	delCookie('current_card')
	
	function alert_user(type) {
        $('body').xmalert({ 
            x: 'right',
            y: 'bottom',
            xOffset: 30,
            yOffset: 30,
            alertSpacing: 20,
            lifetime: 6000,
            fadeDelay: 0.3,
            template: 'item',
            title: 'Backend Driven Custom Alert',
			paragraph: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.',
            timestamp: '2 hours ago',
            imgSrc: 'images/alerts/'+type+'.png',
            iconClass: 'icon-heart'
        });
    }
	function alert_admin() {
        $('body').xmalert({ 
            x: 'right',
            y: 'bottom',
            xOffset: 30,
            yOffset: 30,
            alertSpacing: 50,
            lifetime: 6000,
            fadeDelay: 0.3,
            template: 'survey',
            title: 'Welcome to LinQStar',
            paragraph: 'Make sure that:<br><br>1. Visit Hlp & FAQQS and review self help how to videos available at linqstar.com/help<br>2. For more great features visit your account page by clicking the home icon on the black toolbar above.',
            timestamp: '2 hours ago',
            imgSrc: 'images/alerts/help.png',
            buttonSrc: [ '#','#' ],
            buttonText: 'Take the <span class="primary">Survey!</span>',
        });
    }
	
	</script>
<script src="js/jquery.xmalert.min.js"></script>
<!-- Magnific Popup -->
<script src="js/jquery.magnific-popup.min.js"></script>	
	<script src="js/jsSocketNode.js"></script>
	<div style="position:fixed;left:0;top:0;margin:auto;width:100%;height:100%;display:none;z-index:999999;background:#000;opacity:0.9" id="scover"></div>
	</html>
<? $c->close(); ?>
