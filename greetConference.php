<?php
	require_once "../class/utils.class.php";
	$c=new utils;
	$c->connect("199.91.65.83","voxeo");
	parse_str(http_build_query($_GET));
	$arr1=array("/","+","%");
	$arr2=array("1001","2002","3003");
	$conference_id=str_replace($arr2,$arr1,($conference_id));	
	$conference_id=$c->decrypt($conference_id,"");
	$room=$conference_id;
	$stream='stream' . rand(11111111,99999999);
	$join=date("Y-m-d h:i:s");
	$status="joined";
	$c->insert("UPDATE `voxeo`.`greeting_conference_participants` SET `stream_name` = '$stream', `join_date_time` = '$join', `status`='$status' WHERE `id` = $participant_id")
?>
<!doctype html> <!-- Copyright © 2015 Infrared5, Inc. All rights reserved. The accompanying code comprising examples for use solely in conjunction with Red5 Pro (the "Example Code") is licensed to you by Infrared5 Inc.  in 
consideration of your agreement to the following license terms and conditions.  Access, use, modification, or redistribution of the accompanying code constitutes your acceptance of the following license terms and conditions. 
Permission is hereby granted, free of charge, to you to use the Example Code and associated documentation files (collectively, the "Software") without restriction, including without limitation the rights to use, copy, modify, 
merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions: The Software shall be used solely in conjunction 
with Red5 Pro. Red5 Pro is licensed under a separate end user license agreement (the "EULA"), which must be executed with Infrared5, Inc. An example of the EULA can be found on our website at: 
https://account.red5pro.com/assets/LICENSE.txt. The above copyright notice and this license shall be included in all copies or portions of the Software. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL INFRARED5, INC. BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. --> <html lang="eng">
  <head>
    <title>Conference</title>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta content="width=device-width, initial-scale=1, user-scalable=no" name="viewport">
    <script src="//webrtchacks.github.io/adapter/adapter-latest.js"></script>
    <script src="https://mediaserver.linqstar.com/webrtcexamples/lib/screenfull/screenfull.min.js"></script>
    <script src="https://mediaserver.linqstar.com/webrtcexamples/script/testbed-config.js"></script>
    <script src="https://mediaserver.linqstar.com/webrtcexamples/script/red5pro-utils.js"></script>
    <script src="https://mediaserver.linqstar.com/webrtcexamples/script/reachability.js"></script>
    <link rel="stylesheet" href="https://mediaserver.linqstar.com/webrtcexamples/css/reset.css">
    <link rel="stylesheet" href="https://mediaserver.linqstar.com/webrtcexamples/css/testbed.css">
    <link rel="stylesheet" href="https://mediaserver.linqstar.com/webrtcexamples/lib/red5pro/red5pro-media.css">
    <link rel='stylesheet' href='//fonts.googleapis.com/css?family=Lato%3A100%2C200%2C300%2C400%2C500%2C600%2C700%2C800%2C900%2C100i%2C200i%2C300i%2C400i%2C500i%2C600i%2C700i%2C800i%2C900i&#038;subset=latin&#038;ver=5.2.3' 
type='text/css' media='all' />
    
    <link rel="stylesheet" href="https://mediaserver.linqstar.com/webrtcexamples/css/conference.css">
    <script src="https://mediaserver.linqstar.com/webrtcexamples/script/subscription-status.js"></script>
  </head>
  <body>
    <style type="text/css">
    /* Top Bar CSS */
    #top-bar {
        padding: 16px 0 16px 0;
    }
    #top-bar-wrap {
        background-color: #3b3b3b;
    }
    #top-bar-wrap {
        border-color: #999999;
    }
    #top-bar-wrap, #top-bar-content strong {
        color: #dbdbdb;
    }
    #top-bar-content a, #top-bar-social-alt a {
        color: #dbdbdb;
    }
    #top-bar-content a:hover, #top-bar-social-alt a:hover {
        color: #db1f26;
    }
    .container {
        width: 1200px;
        max-width: 90%;
        margin: 0 auto;
    }
    @media only screen and (min-width: 1024px) {
      #top-bar-content {
        float: none;
        text-align: right;
      }
    }
    @media only screen and (max-width: 1023px) {
      #top-bar-content {
        float: none;
        text-align: center;
      }
    }
    #menu-top-menu {
      letter-spacing: .6px;
      box-sizing: border-box;
      border: 0;
      outline: 0;
      vertical-align: baseline;
      font-size: 100%;
      margin: 0;
      padding: 0;
      list-style: none;
      touch-action: pan-y;
    }
    
    .menu-item {
      color: #dbdbdb;
      letter-spacing: .6px;
      box-sizing: border-box;
      border: 0;
      outline: 0;
      vertical-align: baseline;
      font-size: 100%;
      margin: 0;
      padding: 0;
      list-style: none;
      position: relative;
      white-space: normal;
      display: inline-block;
      float: none;
      margin-right: 15px;
    }
    
    /* Typography CSS */
    #top-bar-content, #top-bar-social-alt {
        font-size: 13px;
        letter-spacing: .6px;
    }
    </style>
    <script src="https://mediaserver.linqstar.com/webrtcexamples/lib/smartmenus-1.1.0/libs/jquery/jquery.js"></script>
    <div id="app">
      <div id="back-link-container" style="text-align: left; display:none">
        <p class="back-link"><a href="https://mediaserver.linqstar.com/webrtcexamples/index.html">&lt; Settings</a></p>
        <p class="version-field">Testbed Version: 8.0.0</p>
      </div>
      <h1 class="centered test-title red-text">Conference</h1>
      <div class="conference-section">
        <div id="publisher-container" class="publisher-section publisher-container">
          <div class="settings-area" style="display:none">
            <p>
              <label for="room-field" class="settings-label">Room:</label>
              <input id="room-field" name="room-field" class="settings-input">
            </p>
          </div>
          <div id="publisher-session" class="publisher-session hidden">
            <p id="status-field" class="centered status-field">On hold.</p>
            <script src="https://mediaserver.linqstar.com/webrtcexamples/script/publisher-status.js"></script>
          <p id="statistics-field" class="centered status-field statistics-field hidden">
            <!-- only updated from WebRTC clients. -->
            Bitrate: <span id="bitrate-field" class="bitrate-field">N/A</span>.&nbsp;
            Packets Sent: <span id="packets-field" class="packets-field">N/A</span>.
            <br/>
            <span>Resolution: <span id="resolution-field" class="resolution-field">0x0</span>.
          </p>
          </div>
          <div class="centered video-holder">
          <video id="red5pro-publisher" class="red5pro-publisher"
                 controls autoplay playsinline muted
                 width="320" height="240">
          </video>
          </div>
          <div id="publisher-settings" class="publisher-settings hidden">
            <p class="remove-on-broadcast">
              <label class="device-label" for="streamname-field">Stream Name:</label>
              <input id="streamname-field" name="streamname-field" class="settings-input">
            </p>
            <hr class="paddedHR" />
            <p>
              <label class="device-label" for="camera-select">Camera:</label>
              <select class="control device-control settings-input" name="camera-select" id="camera-select"></select>
            </p>
            <p>
              <label class="device-label" for="microphone-select">Microphone:</label>
              <select class="control device-control settings-input" name="microphone-select" id="microphone-select"></select>
            </p>
            <p id="publisher-mute-notice" class="publisher-mute-notice remove-on-broadcast">
              <span class="device-info">Video and Audio can be turned off after publish has started.</span>
            </p>
            <p id="publisher-mute-controls" class="publisher-mute-controls hidden">
              <label for="video-check">Video: </label>
              <input type="checkbox" id="video-check" name="video-check" checked>
              <label for="audio-check">Audio: </label>
              <input type="checkbox" id="audio-check" name="audio-check" checked>
            </p>
            <p id="publisher-name-field" class="publisher-name-field hidden"></p>
          </div>
        </div>
        <div id="subscribers" class="subscribers"></div>
      </div>
      <div class="centered status-field hidden">
        <p>
          <input type="text" id="input-field" />
          <button id="send-button">send</button>
        </p>
        <hr>
        <p>
          <textarea id="so-field" disabled style="width: 50%; min-height: 240px; min-width: 320px"></textarea>
        </p>
      </div>
    </div>
	<div class="remove-on-broadcast">
	  <button id="join-button" class="ui-button">Join!</button>
	</div>
    <script src="https://mediaserver.linqstar.com/webrtcexamples/lib/es6/es6-promise.min.js"></script>
    <script src="https://mediaserver.linqstar.com/webrtcexamples/lib/es6/es6-bind.js"></script>
    <script src="https://mediaserver.linqstar.com/webrtcexamples/lib/es6/es6-array.js"></script>
    <script src="https://mediaserver.linqstar.com/webrtcexamples/lib/es6/es6-object-assign.js"></script>
    <script src="https://mediaserver.linqstar.com/webrtcexamples/lib/es6/es6-fetch.js"></script>
    <script src="https://mediaserver.linqstar.com/webrtcexamples/lib/red5pro/red5pro-sdk.min.js"></script>
    <script>
      (function(window) {
        var configuration = (function () {
          var conf = sessionStorage.getItem('r5proTestBed');
          try {
            return JSON.parse(conf);
          }
          catch (e) {
            console.error('Could not read testbed configuration from sessionstorage: ' + e.message);
          }
          return {}
        })();
    
        if (configuration.verboseLogging) {
          window.publisherLog = function (message) {
            console.log('[Red5ProRTMPPublisher:SWF] - ' + message);
          };
          window.subscriberLog = function (message) {
            console.log('[Red5ProRTMPSubscriber:SWF] - ' + message);
          };
        }
    
        if (configuration.authentication.enabled) {
          var node = document.createElement('div');
          node.classList.add('hint-block', 'auth-alert');
          var note = document.createElement('span');
          note.classList.add('strong');
          note.innerHTML = '*Authentication is Enabled*';
          var link = document.createElement('a');
          link.innerText = 'Click here to disable.';
          link.href= 'https://mediaserver.linqstar.com/webrtcexamples/index.html';
          link.classList.add('auth-link');
          node.appendChild(note);
          node.appendChild(link);
          var testBody = document.getElementById('back-link-container').nextElementSibling;
          testBody.parentNode.insertBefore(node, testBody);
        }
    
      })(this);
    </script>
    <script src="https://mediaserver.linqstar.com/webrtcexamples/test/conference/conference-subscriber.js"></script>
    <script src="https://mediaserver.linqstar.com/webrtcexamples/test/conference/device-selector-util.js"></script>
    <script src="https://mediaserver.linqstar.com/webrtcexamples/test/conference/index.js"></script>
  </body> </html>
