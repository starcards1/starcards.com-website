<?php
	require_once "../class/utils.class.php";
	$c=new utils;
	$c->connect("199.91.65.83","voxeo");
	parse_str(http_build_query($_GET));
	$pro=$c->query("select * from celeb_profiles where mid=$provider_mid");
	foreach($pro[0] as $key => $value) {
		${$key}=$value;
	}
	$ta=$c->query("select * from celeb_tags where pid=$pid");
	$t=explode(",",$ta[0]['tags']);
?>
<script src="assets/js/jquery.js"type="text/javascript"></script>
<script src="assets/js/jquery-ui.min.js" type="text/javascript" charset="utf-8"></script>	
<script src="js/tag-it.js" type="text/javascript" charset="utf-8"></script>	
<script src="assets/js/bootstrap.min.js"type="text/javascript"></script>
<script src="assets/js/utils.js"type="text/javascript"></script>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>LinqStar</title>
	<!-- YOUR CUSTOM CSS -->
	<link href="doc/css/custom.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="dz/dropzone.css"/>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:300|Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>	<style>
		* {
		font-size:14px;
		border-radius:0;}
		div input span {font-size:14px;}
		
		.card_cover{
			background:aliceblue;
			border:0px solid white;
			height:175px;
			margin:10px;
			padding:0
		}
		img {
			draggable:true;
		}
		audio{
			display:none!Important;
			position:absolute
		}
		
	</style>
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Allura&family=Gloria+Hallelujah&family=Homemade+Apple&display=swap" rel="stylesheet">	
    <link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon"/>
</head>

<body  style="background:#ccc">
	<div class="text-center" style="padding-top:40px">
		<div class="account-wrapper" style="margin:auto;padding:0px;max-width:1000px;background:#f0f0f0;border-radius:0!Important">
			<div class="row">		
				<img src="alt/images/banner.jpg">
				<div style="width:100%" class="form-group">				
					<div class="col-md-12" style="text-align:center;font-weight:300;font-family:Open Sans">					
						<BR><BR>
						<h1>CREATE AND SEND CUSTOM GREETINGS</h1>					

					</div>
				</div>
			</div>
			<img src="alt/bar.png" style="width:100%">			
			<div class="row">		
				<div style="width:100%;margin:auto" class="form-group">				
					<button id="previewBtn" class="col-md-12" onclick="preview()" style="display:none;disabled:true;height:30px;background:#fff;border:none;color:silver;height:40px;;margin-top:25px">PREVIEW CARD</button>								
					<button class="col-md-6" onclick="another()" style="height:30px;background:whitesmoke;border:none"></button><button class="col-md-6" onclick="stylize()" style="color:white;height:30px;background:gainsboro;color:black;border:none">Customize Message Style</button>
					<div contentEditable id="custom1" class="col-md-12" style="text-align:center;font-weight:300;font-family:Open Sans;background:white;padding:50px;left:0;right:0;margin:auto;width:90%">					
					
					</div>
					<button onclick="next()" class="col-md-12" style="height:50px;background:#a3d900">Save Message and Proceed</button><br><br>
				</div>
			</div>
			<img src="alt/bar.png" style="width:100%">			
		</div>
	</div>	
	<script src="//cdn.ckeditor.com/4.10.1/basic/ckeditor.js"></script>

	<script>
	var cu
        function stylize() {
			CKEDITOR.replace('custom1')
		}
	
		function cl() {
			window.parent.frames['ccv'].style.display='none'
		}
		var oid=window.localStorage.getItem('occasion_id')
		var row=0
		var msg
		var msg_count=0
		var sender_name=window.localStorage.getItem('sender_name')
		var sender_email=window.localStorage.getItem('sender_email')
		var sender_mobile=window.localStorage.getItem('sender_mobile')
		var recipient_name=window.localStorage.getItem('recipient_name')
		var recipient_email=window.localStorage.getItem('recipient_email')
		var recipient_mobile=window.localStorage.getItem('recipient_mobile')
		var card_cover=window.localStorage.getItem('card_cover')
		var occasion_id=window.localStorage.getItem('occasion_id')
		var asset=window.localStorage.getItem('asset')
		var canned=window.localStorage.getItem('canned')
		var custom=window.localStorage.getItem('custom1')
		var sender_mid, asset_type, asset_media_type

		function previewShow() {
			$$('previewBtn').style.display=''
		}

		var step1=window.localStorage.getItem('step1')
		var step2=window.localStorage.getItem('step2')
		var step3
		
						
		function next() {
			if (CKEDITOR.instances['custom1']) {
				cu=CKEDITOR.instances['custom1'].getData();
			} else {
				cu=$$('custom1').innerHTML
			}
			window.localStorage.setItem('custom',cu)
			url='x_create_greeting.php?sender_mid='+sender_mid+'&sender_name='+sender_name+'&sender_email='+sender_email+'&sender_mobile='+sender_mobile+'&recipient_name='+recipient_name+'&recipient_email='+recipient_email+'&recipient_mobile='+recipient_mobile+'&card_cover='+card_cover+'&occasion_id='+occasion_id+'&asset='+asset+'&canned='+canned+'&custom='+encodeURIComponent(window.localStorage.getItem('custom'))
			console.log(url)
			//alert(url)
			$.ajax({
				url: url,
				success: function(data){
					if (!isNaN(data)) {
						window.localStorage.setItem('card_id',data)
						window.localStorage.setItem('step3',true)
						step3=window.localStorage.getItem('step3')
						previewShow()
						location.href='card_uploads.php'
						//alert(data)
					} else {
						alert('Failed')
					}
				}
			})
		}
	stylize()
	</script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>	
</body>
</html>