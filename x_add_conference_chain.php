<?php

/*
	Creating a conference event will write to the following tables:
	
	1.	event_events
		Records the conference event for updating and displaying event in a calendar
	2.	celeb_channel_content
		Contains summary info about channel content and content type. Content is a collection of various types of assets.
	3.	celeb_conferences
		Conference is a type of asset, and hence the details for that channel that has conferences is stored here
	3.	celeb_conference_participants
		Conference is a type of asset, and hence the details for that channel that has conferences is stored here
	4.	celeb_live_events
		The act of distributing content is simply the process of releasing or publishing its individual assets,, and this, in its entirety, constitutes an event
	5.	celeb_live_event_members
		Recorded actions and  time-lines of all the members that participated in the event
	6.	celeb_sms_logs
		Records the details and purpose of any sms that is sent
	7.	celeb_action_logs
		Record of all Business activity level actions that are executed by any member
	8.	celeb_activity_tracker
		Click level detail logs of member actions.
		
	Before this:
	
	1.	Views and edits an existing channel to add this event to. Table: celeb_channels
	2.	Creates a new channel to store the event. Table: celeb_channels
	
	TO DO: 
	1.	security
	2.	payments and subscriptions
	
*/
include "../class/utils.class.php";
$c=new utils;
$c->connect("199.91.65.83","voxeo");
parse_str(http_build_query($_GET));
$start_time=date("h:i a",strtotime($start_time));
$end_time=date_format(date_add(date_create($start_time), new DateInterval('PT1H')), "h:i a");
$date_from=date("d-m-Y h:i a");
$date_to=date_format(date_add(date_create($date_from), new DateInterval('PT1H')), "h:i a");
$start_date=date("Y-m-d");
$c->insert("delete from `voxeo`.`greeting_conferences` where card_id=$card_id");
$sql="
	INSERT INTO `voxeo`.`greeting_conferences` (
		`title`,
		`start_date`,
		`start_time`,
		`end_time`,
		`status`,
		`owner_name`,
		`owner_mobile`, 
		`recipient_name`,
		`recipient_mobile`, 
		`card_id`
	)
	VALUES
	(
		'$title',
		'$start_date',
		'$start_time',
		'$end_time',
		'scheduled',
		'$owner_name',
		'$owner_mobile',
		'$recipient_name',
		'$recipient_mobile',
		'$card_id')";
	
	$conference_id=$c->insert($sql);
	$stream_name=$c->encrypt($conference_id,"");
	$arr1=array("/","+","%","=");
	$arr2=array("1001","2002","3003","");
	$room=str_replace($arr1,$arr2,($stream_name));
	$c->insert("update `voxeo`.`greeting_conferences` set `room`='$room' where `conference_id`=$conference_id");
	$status=1;

	$mobs=explode("|",$mobiles);
	$nams=explode("|",$names);
	$ucat=explode("|",$types);

	for ($i=0; $i<count($mobs); $i++) {
		$sql="INSERT INTO `voxeo`.`greeting_conference_participants` (
				`conference_id`,
				`card_id`,
				`participant_mobile`,
				`participant_name`,
				`participant_type`,
				`app`,
				`room`,
				`status`		
			)
				VALUES
			(
				$conference_id,
				$card_id,
				'". $mobs[$i] ."',
				'". $nams[$i] ."',
				'". $ucat[$i] ."',
				'$app',
				'$room',
				'pending'
			)";
		$participant_id=$c->insert($sql);
		$link="https://linqstar.com/gconf/" . $room . "/" . $participant_id;
		$c->insert("update `voxeo`.`greeting_conference_participants` set `link`='$link' where `id`=$participant_id");
		if ($ucat[$i]=='sender') echo $link;
	}
	$c->close();